<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);
(new Dotenv\Dotenv(__DIR__ . '/../'))->load();
return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'controllerMap' => [
        'fixture' => [
            'class' => 'yii\console\controllers\FixtureController',
            'namespace' => 'common\fixtures',
          ],
        'payment_recurrency' => [
            'class' => 'console\controllers\Payment_recurrencyController',
          ],
        'migrate-common-db' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationNamespaces' => ['common\migrations\db'],
            'migrationTable' => 'migrations',
            'migrationPath' => null,
        ],
    ],
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => getenv('email_host'),  // ej. smtp.mandrillapp.com o smtp.gmail.com
                'username' => getenv('email_username'),
                'password' => getenv('email_password'),
                'port' => getenv('email_port'),
                'encryption'=>getenv('email_encryption')// Seguridad
            ],
        ],
    ],
    'params' => $params,
];

