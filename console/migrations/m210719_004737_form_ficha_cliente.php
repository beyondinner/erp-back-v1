<?php

use yii\db\Migration;

/**
 * Class m210719_004737_form_ficha_cliente
 */
class m210719_004737_form_ficha_cliente extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        //TODO: Borrar tablas si existen para evitar problemas a la hora de emigrar
        /**
         * Tablas: form_ficha_name, form_ficha_label, form_ficha_label_value
         *
         *
         * */

        //TODO: Borramos los foreign keys si existen

        $table_name = $this->db->tablePrefix . 'form_ficha_label';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        if (array_key_exists('form_ficha_label_fk1',$exist_table->foreignKeys)){
            $this->execute("ALTER TABLE form_ficha_label DROP FOREIGN KEY form_ficha_label_fk1;");
        }
        $table_name = $this->db->tablePrefix . 'form_ficha_label_val';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        if (array_key_exists('form_ficha_label_val_fk1',$exist_table->foreignKeys)){
            $this->execute("ALTER TABLE form_ficha_label_val DROP FOREIGN KEY form_ficha_label_val_fk1;");
        }
        if (array_key_exists('form_ficha_label_val_fk2',$exist_table->foreignKeys)){
            $this->execute("ALTER TABLE form_ficha_label_val DROP FOREIGN KEY form_ficha_label_val_fk2;");
        }
        if (array_key_exists('form_ficha_label_val_fk3',$exist_table->foreignKeys)){
            $this->execute("ALTER TABLE form_ficha_label_val DROP FOREIGN KEY form_ficha_label_val_fk3;");
        }



        $this->execute("DROP TABLE IF EXISTS `form_ficha_name`");
        $this->execute("DROP TABLE IF EXISTS `form_ficha_label`");
        $this->execute("DROP TABLE IF EXISTS `form_ficha_label_val`");

        //TODO: Crear tabla para el nombre de la lista
        $this->execute("CREATE TABLE IF NOT EXISTS `form_ficha_name` ( `form_id` INT NOT NULL AUTO_INCREMENT , `form_name` VARCHAR(200) NOT NULL , PRIMARY KEY (`form_id`)) ENGINE = InnoDB; ");

        //TODO: Crear  tabla para el nombre de los labels de cada id de la lista
        $this->execute("CREATE TABLE `form_ficha_label` ( `label_id` INT NOT NULL AUTO_INCREMENT , `label_form_id` INT NOT NULL , `label_name` VARCHAR(200) NOT NULL , PRIMARY KEY (`label_id`)) ENGINE = InnoDB; ");

        //TODO: Crear  tabla con los valores para cada id
        $this->execute("CREATE TABLE `form_ficha_label_val` ( `val_id` INT NOT NULL AUTO_INCREMENT , `val_form_ficha_label_id` INT NOT NULL , `val_label_form_id` INT NOT NULL , `val_text` VARCHAR(200) NOT NULL, `val_id_user` INT NOT NULL , `val_created` datetime NOT NULL , PRIMARY KEY (`val_id`)) ENGINE = InnoDB; ");

        //TODO: AGREGAR FOREKEIGN KEYS
        $this->execute("ALTER TABLE `form_ficha_label` ADD CONSTRAINT `form_ficha_label_fk1` FOREIGN KEY (`label_form_id`) REFERENCES `form_ficha_name`(`form_id`) ON DELETE CASCADE ON UPDATE CASCADE; ");
        $this->execute("ALTER TABLE `form_ficha_label_val` ADD CONSTRAINT `form_ficha_label_val_fk1` FOREIGN KEY (`val_label_form_id`) REFERENCES `form_ficha_name`(`form_id`) ON DELETE CASCADE ON UPDATE CASCADE; ");
        $this->execute("ALTER TABLE `form_ficha_label_val` ADD CONSTRAINT `form_ficha_label_val_fk2` FOREIGN KEY (`val_form_ficha_label_id`) REFERENCES `form_ficha_label`(`label_id`) ON DELETE CASCADE ON UPDATE CASCADE; ");
        $this->execute("ALTER TABLE `form_ficha_label_val` ADD CONSTRAINT `form_ficha_label_val_fk3` FOREIGN KEY (`val_id_user`) REFERENCES `users`(`id_user`) ON DELETE CASCADE ON UPDATE CASCADE; ");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210719_004737_form_ficha_cliente cannot be reverted.\n";

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210719_004737_form_ficha_cliente cannot be reverted.\n";

        return false;
    }
    */
}
