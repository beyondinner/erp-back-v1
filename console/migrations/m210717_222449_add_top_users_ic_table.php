<?php

use yii\db\Migration;

/**
 * Class m210717_222449_add_top_users_ic_table
 */
class m210717_222449_add_top_users_ic_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        //$this->execute("DROP VIEW IF EXISTS `102017816_erp_inner`.`top_users_ic` CASCADE;");

        //$this->execute("CREATE OR REPLACE VIEW `102017816_erp_inner`.`top_users_ic` AS select `erp`.`club_members`.`club_members_name` AS `club_members_name`, `erp`.`club_members`.`club_members_email` AS `club_members_email`, `erp`.`club_members`.`clum_members_currency` AS `clum_members_currency` from `erp`.`club_members` order by `erp`.`club_members`.`clum_members_currency` desc limit 20;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210717_222449_add_top_users_ic_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210717_222449_add_top_users_ic_table cannot be reverted.\n";

        return false;
    }
    */
}
