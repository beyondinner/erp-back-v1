<?php

use yii\db\Migration;

/**
 * Class m210702_025552_update_payrolls_table
 */
class m210702_025552_update_payrolls_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        //$this->addColumn('payrolls','id_user', $this->integer(10)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210702_025552_update_payrolls_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210702_025552_update_payrolls_table cannot be reverted.\n";

        return false;
    }
    */
}
