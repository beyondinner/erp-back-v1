<?php


namespace console\controllers;


use erp\modules\managment\models\Club_members;
use erp\modules\managment\models\Contacts;
use erp\modules\nomenclatures\models\Adviser;
use erp\modules\nomenclatures\models\Contact_types;
use yii\console\Controller;

class UpdateAdviserController extends Controller
{
    public function actionIndex()
    {
        $this->update();
    }

    public function actionContacts()
    {
        $this->update_contacts();
    }

    public function actionPutMissingContacts()
    {
        $this->put_missing_contacts();
    }

    public function update()
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $result = [];
        try {
            $club_member_list = Club_members::find()->all();
            $count_alberto = 0;
            $count_amalia = 0;
            $count_ana = 0;
            $count_andrea_v = 0;
            $count_andy_b = 0;
            $count_anthony = 0;
            $count_bruno = 0;
            $count_carmen = 0;
            $count_clara = 0;
            $count_corrinne = 0;
            $count_debora = 0;
            $count_elena = 0;
            $count_elke = 0;
            $count_fabian = 0;
            $count_fara = 0;
            $count_filomena = 0;
            $count_francesca = 0;
            $count_gustavo = 0;
            $count_hanna = 0;
            $count_ilenia = 0;
            $count_iris = 0;
            $count_janicke = 0;
            $count_joakin = 0;
            $count_juha = 0;
            $count_kate = 0;
            $count_katarina = 0;
            $count_laura = 0;
            $count_leila = 0;
            $count_lorenzo = 0;
            $count_mariana = 0;
            $count_mariza = 0;
            $count_martine = 0;
            $count_melinda = 0;
            $count_michela = 0;
            $count_orlando = 0;
            $count_paola = 0;
            $count_paula = 0;
            $count_rebecca = 0;
            $count_remy = 0;
            $count_salla = 0;
            $count_sergio = 0;
            $count_vanesa = 0;
            $count_zarick = 0;

            foreach ($club_member_list as $index => $item) {
                if (array_search($item->id_adviser, array('--', 37, 39, 6, 140, 38))) {
                    $item->id_adviser = 178;
                    $count_alberto++;
                }
                if (array_search($item->id_adviser, array('--', 84, 17))) {
                    $item->id_adviser = 40;
                    $count_amalia++;
                }
                if (array_search($item->id_adviser, array('--', 101, 129))) {
                    $item->id_adviser = 80;
                    $count_ana++;
                }
                if (array_search($item->id_adviser, array('--', 74, 1))) {
                    $item->id_adviser = 45;
                    $count_andrea_v++;
                }
                if (array_search($item->id_adviser, array('--', 202))) {
                    $item->id_adviser = 122;
                    $count_andy_b++;
                }
                if (array_search($item->id_adviser, array('--', 216, 214))) {
                    $item->id_adviser = 191;
                    $count_anthony++;
                }
                if (array_search($item->id_adviser, array('--', 145, 99, 51, 73))) {
                    $item->id_adviser = 70;
                    $count_bruno++;
                }
                if (array_search($item->id_adviser, array('--', 16, 97, 22, 24))) {
                    $item->id_adviser = 23;
                    $count_carmen++;
                }
                if (array_search($item->id_adviser, array('--', 182, 153))) {
                    $item->id_adviser = 169;
                    $count_clara++;
                }
                if (array_search($item->id_adviser, array('--', 36, 83, 204))) {
                    $item->id_adviser = 104;
                    $count_corrinne++;
                }
                if (array_search($item->id_adviser, array('--', 56, 126, 102, 61, 85))) {
                    $item->id_adviser = 79;
                    $count_debora++;
                }
                if (array_search($item->id_adviser, array('--', 52, 52))) {
                    $item->id_adviser = 46;
                    $count_elena++;
                }
                if (array_search($item->id_adviser, array('--', 26, 103))) {
                    $item->id_adviser = 215;
                    $count_elke++;
                }

                if (array_search($item->id_adviser, array('--', 155, 94, 42))) {
                    $item->id_adviser = 206;
                    $count_fabian++;
                }

                if (array_search($item->id_adviser, array('--', 203))) {
                    $item->id_adviser = 184;
                    $count_fara++;
                }
                if (array_search($item->id_adviser, array('--', 50, 93, 58))) {
                    $item->id_adviser = 185;
                    $count_filomena++;
                }
                if (array_search($item->id_adviser, array('--', 150, 98))) {
                    $item->id_adviser = 114;
                    $count_francesca++;
                }
                if (array_search($item->id_adviser, array('--', 82, 81))) {
                    $item->id_adviser = 75;
                    $count_gustavo++;
                }
                if (array_search($item->id_adviser, array('--', 3, 59))) {
                    $item->id_adviser = 109;
                    $count_hanna++;
                }
                if (array_search($item->id_adviser, array('--', 146))) {
                    $item->id_adviser = 63;
                    $count_ilenia++;
                }
                if (array_search($item->id_adviser, array('--', 137, 87))) {
                    $item->id_adviser = 48;
                    $count_iris++;
                }
                if (array_search($item->id_adviser, array('--', 34))) {
                    $item->id_adviser = 112;
                    $count_janicke++;
                }
                if (array_search($item->id_adviser, array('--', 136, 33, 163))) {
                    $item->id_adviser = 124;
                    $count_joakin++;
                }
                if (array_search($item->id_adviser, array('--', 142, 21, 28, 29))) {
                    $item->id_adviser = 106;
                    $count_juha++;
                }
                if (array_search($item->id_adviser, array('--', 201, 134, 192))) {
                    $item->id_adviser = 213;
                    $count_kate++;
                }
                if (array_search($item->id_adviser, array('--', 78))) {
                    $item->id_adviser = 30;
                    $count_katarina++;
                }
                if (array_search($item->id_adviser, array('--', 54, 92, 210))) {
                    $item->id_adviser = 130;
                    $count_laura++;
                }
                if (array_search($item->id_adviser, array('--', 8, 31))) {
                    $item->id_adviser = 110;
                    $count_leila++;
                }
                if (array_search($item->id_adviser, array('--', 69))) {
                    $item->id_adviser = 68;
                    $count_lorenzo++;
                }
                if (array_search($item->id_adviser, array('--', 53))) {
                    $item->id_adviser = 120;
                    $count_mariana++;
                }
                if (array_search($item->id_adviser, array('--', 170, 152))) {
                    $item->id_adviser = 171;
                    $count_mariza++;
                }
                if (array_search($item->id_adviser, array('--', 211))) {
                    $item->id_adviser = 160;
                    $count_martine++;
                }
                if (array_search($item->id_adviser, array('--', 125))) {
                    $item->id_adviser = 116;
                    $count_melinda++;
                }
                if (array_search($item->id_adviser, array('--', 167, 19))) {
                    $item->id_adviser = 64;
                    $count_michela++;
                }
                if (array_search($item->id_adviser, array('--', 186))) {
                    $item->id_adviser = 190;
                    $count_orlando++;
                }
                if (array_search($item->id_adviser, array('--', 158))) {
                    $item->id_adviser = 113;
                    $count_paola++;
                }
                if (array_search($item->id_adviser, array('--', 43, 91))) {
                    $item->id_adviser = 18;
                    $count_paula++;
                }
                if (array_search($item->id_adviser, array('--', 196, 77, 14, 108, 76))) {
                    $item->id_adviser = 27;
                    $count_rebecca++;
                }
                if (array_search($item->id_adviser, array('--', 212))) {
                    $item->id_adviser = 194;
                    $count_remy++;
                }
                if (array_search($item->id_adviser, array('--', 168))) {
                    $item->id_adviser = 198;
                    $count_salla++;
                }
                if (array_search($item->id_adviser, array('--', 151, 12, 10, 25))) {
                    $item->id_adviser = 11;
                    $count_sergio++;
                }
                if (array_search($item->id_adviser, array('--', 13, 9, 90, 44))) {
                    $item->id_adviser = 62;
                    $count_vanesa++;
                }
                if (array_search($item->id_adviser, array('--', 131))) {
                    $item->id_adviser = 88;
                    $count_zarick++;
                }
                $item->save();
            }
            printf("Actualizados Alberto " . $count_alberto . "\n");
            printf("Actualizados Amalia " . $count_amalia . "\n");
            printf("Actualizados Ana " . $count_ana . "\n");
            printf("Actualizados Andrea_v " . $count_andrea_v . "\n");
            printf("Actualizados Andy_b " . $count_andy_b . "\n");
            printf("Actualizados Anthony " . $count_anthony . "\n");
            printf("Actualizados Bruno " . $count_bruno . "\n");
            printf("Actualizados Carmen " . $count_carmen . "\n");
            printf("Actualizados Clara " . $count_clara . "\n");
            printf("Actualizados Corrinne " . $count_corrinne . "\n");
            printf("Actualizados Debora " . $count_debora . "\n");
            printf("Actualizados Elena " . $count_elena . "\n");
            printf("Actualizados Elke " . $count_elke . "\n");
            printf("Actualizados Fabian " . $count_fabian . "\n");
            printf("Actualizados Fara " . $count_fara . "\n");
            printf("Actualizados Filomena " . $count_filomena . "\n");
            printf("Actualizados Francesca " . $count_francesca . "\n");
            printf("Actualizados Gustavo " . $count_gustavo . "\n");
            printf("Actualizados Hanna " . $count_hanna . "\n");
            printf("Actualizados Ilenia " . $count_ilenia . "\n");
            printf("Actualizados Iris " . $count_iris . "\n");
            printf("Actualizados Janicke " . $count_janicke . "\n");
            printf("Actualizados Joakin " . $count_joakin . "\n");
            printf("Actualizados Juha " . $count_juha . "\n");
            printf("Actualizados Kate " . $count_kate . "\n");
            printf("Actualizados Katarina " . $count_katarina . "\n");
            printf("Actualizados Laura " . $count_laura . "\n");
            printf("Actualizados Leila " . $count_leila . "\n");
            printf("Actualizados Lorenzo " . $count_lorenzo . "\n");
            printf("Actualizados Mariana " . $count_mariana . "\n");
            printf("Actualizados Mariza " . $count_mariza . "\n");
            printf("Actualizados Martine " . $count_martine . "\n");
            printf("Actualizados Melinda " . $count_melinda . "\n");
            printf("Actualizados Michela " . $count_michela . "\n");
            printf("Actualizados Orlando " . $count_orlando . "\n");
            printf("Actualizados Paola " . $count_paola . "\n");
            printf("Actualizados Paula " . $count_paula . "\n");
            printf("Actualizados Rebecca " . $count_rebecca . "\n");
            printf("Actualizados Remy " . $count_remy . "\n");
            printf("Actualizados Salla " . $count_salla . "\n");
            printf("Actualizados Sergio " . $count_sergio . "\n");
            printf("Actualizados Vanesa " . $count_vanesa . "\n");
            printf("Actualizados Zarick " . $count_zarick . "\n");
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public function update_contacts()
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $result = [];
        $count = 0;
        try {
            $advise_type=Contact_types::findOne(["contact_type_siglas" => "Asesor"]);
            if (!$advise_type) {
                $contact_type = new Contact_types();
                $contact_type->contact_type_siglas = "Asesor";
                $contact_type->contact_type_desc = "Asesor";
                $contact_type->created_at=date('Y-m-h');
                $contact_type->updated_at=date('Y-m-h');
                $contact_type->save();
            }
            $contacts = Contacts::find()->all();
            foreach ($contacts as $index => $contact) {
                if ($contact->fiscal_name == "SPBEIN89") {
                    $contact->adviser_id = 132;
                    printf("Actualizo a Erik\n");
                    $count++;
                }
                if ($contact->fiscal_name == "SPBEIN97") {
                    $contact->adviser_id = 75;
                    printf("Actualizo a Gustavo\n");
                    $count++;
                }
                if ($contact->fiscal_name == "SPBEIN102") {
                    $contact->adviser_id = 68;
                    printf("Actualizo a Lorenzo\n");
                    $count++;
                }
                if ($contact->fiscal_name == "SPBEIN108") {
                    $contact->adviser_id = 113;
                    printf("Actualizo a Paola\n");
                    $count++;
                }
                if ($contact->fiscal_name == "SPBEIN385") {
                    $contact->adviser_id = 213;
                    printf("Actualizo a Kate\n");
                    $count++;
                }
                if ($contact->fiscal_name == "ENBEIN594") {
                    $contact->adviser_id = 169;
                    printf("Actualizo a Clara\n");
                    $count++;
                }
                if ($contact->fiscal_name == "SPBEIN599") {
                    $contact->adviser_id = 62;
                    printf("Actualizo a Vanessa\n");
                    $count++;
                }
                if ($contact->fiscal_name == "SPBEIN601") {
                    $contact->adviser_id = 80;
                    printf("Actualizo a Andrea M\n");
                    $count++;
                }
                if ($contact->fiscal_name == "SPBEIN21") {
                    $contact->adviser_id = 45;
                    printf("Actualizo a Andrea V\n");
                    $count++;
                }
                if ($contact->fiscal_name == "SPBEIN77") {
                    $contact->adviser_id = 40;
                    printf("Actualizo a Amalia \n");
                    $count++;
                }
                if ($contact->fiscal_name == "DEBEIN87") {
                    $contact->adviser_id = 215;
                    printf("Actualizo a Elke\n");
                    $count++;
                }
                if ($contact->fiscal_name == "SPBEIN95") {
                    $contact->adviser_id = 185;
                    printf("Actualizo a Filomena\n");
                    $count++;
                }
                if ($contact->fiscal_name == "ITBEIN96") {
                    $contact->adviser_id = 114;
                    printf("Actualizo a Francesca\n");
                    $count++;
                }
                if ($contact->fiscal_name == "SPBEIN100") {
                    $contact->adviser_id = 130;
                    printf("Actualizo a Laura\n");
                    $count++;
                }
                if ($contact->fiscal_name == "SPBEIN648") {
                    $adviser = Adviser::findOne(["adviser" => "Francisco Vidal Parra"]);
                    if (!$adviser)
                        $adviser = new Adviser();
                    $adviser->adviser = "Francisco Vidal Parra";
                    $adviser->save();
                    $contact->adviser_id = $adviser->id_adviser;
                    printf("Actualizo a Laura\n");
                    $count++;
                }
                if ($contact->fiscal_name == "SPBEIN105") {
                    $contact->adviser_id = 64;
                    printf("Actualizo a Michela\n");
                    $count++;
                }
                if ($contact->fiscal_name == "SPBEIN108") {
                    $contact->adviser_id = 113;
                    printf("Actualizo a Paola Gadea\n");
                    $count++;
                }
                if ($contact->fiscal_name == "SPBEIN461") {
                    $contact->adviser_id = 124;
                    printf("Actualizo a Joakin\n");
                    $count++;
                }
                if ($contact->fiscal_name == "ENBEIN594") {
                    $contact->adviser_id = 1;
                    printf("Actualizo a Clara Rheman\n");
                    $count++;
                }
                if ($contact->fiscal_name == "SPBEIN596") {
                    $contact->adviser_id = 106;
                    printf("Actualizo a Juha\n");
                    $count++;
                }

                if ($contact->fiscal_name == "SPBEIN620") {
                    $contact->adviser_id = 63;
                    printf("Actualizo a Ilenia \n");
                    $count++;
                }

                if ($contact->fiscal_name == "SPBEIN766") {
                    $contact->adviser_id = 177;
                    printf("Actualizo a Elisa \n");
                    $count++;
                }
                if ($contact->fiscal_name == "ENBEIN775") {
                    $contact->adviser_id = 104;
                    printf("Actualizo a Corine\n");
                    $count++;
                }
                if ($contact->fiscal_name == "ITBEIN85") {
                    $contact->adviser_id = 4;
                    printf("Actualizo a Empresa\n");
                    $count++;
                }
                if ($contact->fiscal_name == "FRBEIN51") {
                    $contact->adviser_id = 191;
                    printf("Actualizo a Anthony\n");
                    $count++;
                }
                if ($contact->fiscal_name == "SPBEIN330") {
                    $contact->adviser_id = 122;
                    printf("Actualizo a Andy\n");
                    $count++;
                }
                if ($contact->fiscal_name == "SPBEIN79") {
                    $contact->adviser_id = 73;
                    printf("Actualizo a Bruno\n");
                    $count++;
                }
                if ($contact->fiscal_name == "ENBEIN91") {
                    $contact->adviser_id = 206;
                    printf("Actualizo a Fabian\n");
                    $count++;
                }
                if ($contact->fiscal_name == "SPBEIN101") {
                    $contact->adviser_id = 110;
                    printf("Actualizo a Leila\n");
                    $count++;
                }
                if ($contact->fiscal_name == "SPBEIN86") {
                    $contact->adviser_id = 46;
                    printf("Actualizo a Elena\n");
                    $count++;
                }
                $contact->updated_at = date('Y-m-d h:i:s');
                $contact->contact_type_id=6;
                $contact->save();
            }

            printf("Actualizados " . $count . "\n");

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    public function put_missing_contacts()
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $result = [];
        $count = 0;
        $not_found_club_member = 0;
        $found_club_member = 0;
        $insert_contact = 0;
        $not_insert_contact = 0;
        $codes = file_get_contents(__DIR__ . '/codes_contact/codes_contact.json');
        $codes = json_decode($codes);
        try {
            foreach ($codes as $index => $code) {
                $contact = Contacts::find()->where(['fiscal_name' => $code->code])->one();
                if (!$contact) {
                    printf("No encontrado " . $code->code . "\n");
                    $count++;
                    $clubmember = Club_members::findOne(["club_member_code" => $code->code]);
                    if ($clubmember) {
                        printf("Encontrado en Club " . $code->code . "\n");
                        $found_club_member++;
                        $newContact = new Contacts();
                        $newContact->contact_legal_type_id = 3;
                        $newContact->contact_full_name = $clubmember->club_members_name . ' ' . $clubmember->club_member_last_name;
                        $newContact->contact_commercial_name = $clubmember->adviser->adviser;
                        $newContact->contact_type_id = 3;
                        $email_dup = Contacts::findOne(['contact_email' => $clubmember->club_members_email]);
                        $email = $clubmember->club_members_email;
                        if ($email_dup)
                            $email = $clubmember->club_members_email . '<' . $code->code . '>';
                        $newContact->contact_email = $email;
                        $newContact->contact_phone = $clubmember->club_members_phone;
                        $newContact->contact_cellphone = $clubmember->club_members_phone;
                        $newContact->contact_website = null;
                        $newContact->contact_address = $clubmember->club_members_address;
                        $newContact->contact_postal_code = "--";
                        $newContact->country_id = $clubmember->id_country;
                        $newContact->state_id = $clubmember->id_state;
                        $newContact->contact_internal_reference = "--";
                        $newContact->language_id = $clubmember->id_languages;
                        $newContact->currency_id = 2;
                        $newContact->payment_type_id = 3;
                        $newContact->contact_status_id = 1;
                        $newContact->contact_picture = $clubmember->club_members_picture;
                        $newContact->created_at = date('Y-m-d h:i:s');
                        $newContact->updated_at = date('Y-m-d h:i:s');
                        $newContact->fiscal_name = $code->code;
                        $newContact->wallet = $clubmember->clum_members_currency;
                        $newContact->club_member_id = $clubmember->id_club_members;
                        $newContact->source = "ERP";
                        $result = $newContact->save();

                        if ($result) {
                            printf("Insertado en contact " . $code->code . "\n");
                            $insert_contact++;
                        } else {
                            printf("No insertado en contact " . $code->code . "\n");
                            $not_insert_contact++;
                        }

                    } else {
                        printf("No encontrado en Club " . $code->code . "\n");
                        $not_found_club_member++;
                    }
                }

            }
            printf("No encontrados en contact " . $count . "\n");
            printf("No encontrados en Club " . $not_found_club_member . "\n");
            printf("Encontrados en club " . $found_club_member . "\n");
            printf("Insertados en contact " . $insert_contact . "\n");
            printf("No insertados en contact " . $not_insert_contact . "\n");

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}

