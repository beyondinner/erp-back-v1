<?php

namespace console\controllers;

use erp\modules\billings\services\Payment_recurrencyService;
use fedemotta\cronjob\models\CronJob;
use yii\console\Controller;
use yii\console\ExitCode;

class Payment_recurrencyController extends \yii\console\Controller
{


    /**
     * Run SomeModel::some_method for a period of time
     * @param string $from
     * @param string $to
     * @return int exit code
     */
    public function actionInit($from, $to)
    {
        $dates = CronJob::getDateRange($from, $to);
        $command = CronJob::run($this->id, $this->action->id, 0, CronJob::countDateRange($dates));
        if ($command === false) {
            return ExitCode::UNSPECIFIED_ERROR;;
        } else {
            $service_recurrency= new Payment_recurrencyService();
            $service_recurrency->recurrency("Carlos");
            $command->finish();
            return ExitCode::OK;
        }
    }

    /**
     * Run SomeModel::some_method for today only as the default action
     * @return int exit code
     */
    public function actionIndex()
    {
        return $this->actionInit(date("Y-m-d"), date("Y-m-d"));
    }

    /**
     * Run SomeModel::some_method for yesterday
     * @return int exit code
     */
    public function actionYesterday()
    {
        return $this->actionInit(date("Y-m-d", strtotime("-1 days")), date("Y-m-d", strtotime("-1 days")));
    }
}