<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style>
        .container {
            background-repeat: no-repeat;
            background-position: center center;
            background-size: contain;
            background-attachment: fixed;
            height: 30.7rem;
            background-image: url('http://erpinnermastery.thecloudgroup.local/erp/web/assets/tarjeta/tarjeta.png');
        }

        .member_name {
            /*margin-top: 6.3rem;*/
            margin-left: 32rem;
            font-size: 20px;
        }

        .member_number {
            /*margin-top: 50rem;*/
            margin-left: 32rem;
            font-size: 20px;
        }
        .c_nombre{
            width: 32%;
            text-align: left;
            float: right;
            color: white;

        }
        .c_num{
            width: 32%;
            text-align: left;
            float: right;
            margin-top: -3.5px;
            color: #006e6a;
        }
        footer {
            position: absolute;
            margin-top: 19rem;
            width: 100%;
            height: 2.5rem;
            color: #006e6a;
        }
        article {
            position: absolute;
            margin-top: 5rem;
        }
    </style>
</head>
<body>
    <?php $this->beginBody() ?>
    <?= $content ?>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

