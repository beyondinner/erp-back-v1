<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user \common\modules\security\models\Users */
$verifyLink = Yii::$app->params['url'].'/site/reset_pass/'. $user->auth_key;

?>
<div class="verify-email">
    <p>Hola <?= Html::encode($user->email) ?>,</p>

    <p>Siga el link de abajo para realizar la activación no text:</p>
    <p>Contraseña generada:<strong><?= $password ?></strong></p>

    <p><?= Html::a(Html::encode($verifyLink), $verifyLink) ?></p>
</div>

