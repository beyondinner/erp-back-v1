<?php

namespace common\models;

use common\modules\security\models\Users;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Users
{

    public function __construct($config = [])
    {
        $this->attributes = parent::attributes();
        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return parent::rules();
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
        $user = new Users();
        $user->load($this->dirtyAttributes, '');
        $user->setPassword($this->pass);
        $user->generateAuthKey();
        //$user->generateEmailVerificationToken();
        //$this->sendEmail($user);
        return $user->save() ? $user : array('errors' => $user->errors);
    }

    /**
     * Sends confirmation email to user
     * @param Users $user user model to with email should be send
     * @return bool whether the email was sent
     */
    public function sendEmail($user)
    {
        $mailer = Yii::$app->mailer;
        $pass = Yii::$app->security->generateRandomString(8);
        $user->pass = $pass;
        $user->generateAuthKey();
        if (!$user->image)
            $user->image = "user.jpg";
        $user->save();
        $mailer->htmlLayout = '@common/mail/layouts/html';
        $mailer->textLayout = '@common/mail/layouts/text';
        try {
            $result = $mailer
                ->compose(
                    ['html' => '@common/mail/emailVerify-html'],
                    ['user' => $user,'password' => $pass]
                )
                ->setFrom([Yii::$app->params['supportEmail'] => 'BeyondInner reiniciar contraseña'])
                ->setTo($user->email)
                ->setSubject('Reiniciar contraseña ' . 'BeyondInner')
                ->send();
        } catch (\Swift_TransportException $e) {
            return ['success'=>false,'message'=>$e->getMessage()];
        }
        $message=$result?"Correo enviado satisfactoriamente":"Ocurrio un error en el envio del correo";
        return  ['success'=>$result,'message'=>$message];;
    }
}

