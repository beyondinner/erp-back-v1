<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111852_Payrolls
 */
class M210417_111852_Payrolls extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'payrolls';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('payrolls',
            [
                'id_payroll' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'employee_id' =>$this->integer(10)->notNull(),
                'payroll_date' =>$this->date()->notNull(),
                'payroll_desc' =>$this->text(),
                'payroll_concept' =>$this->string(255),
                'payroll_account' =>$this->string(255),
                'payroll_tax' =>$this->double(22),
                'payroll_total' =>$this->integer(10),
                'payroll_tags' =>$this->string(255),
                'payroll_default_account' =>$this->string(255),
                'payroll_attachment' =>$this->string(255),
                'payroll_status_id' =>$this->integer(10)->notNull(),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                 'PRIMARY KEY (`id_payroll`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_payroll'))
                $this->addColumn('payrolls', 'id_payroll', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('employee_id'))
                $this->addColumn('payrolls', 'employee_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('payroll_date'))
                $this->addColumn('payrolls', 'payroll_date', $this->date()->notNull());
             else{
                $this->alterColumn('payrolls', 'payroll_date', $this->date()->notNull());
                }
            if (!$exist_table->getColumn('payroll_desc'))
                $this->addColumn('payrolls', 'payroll_desc', $this->text());
             else{
                $this->alterColumn('payrolls', 'payroll_desc', $this->text());
                }
            if (!$exist_table->getColumn('payroll_concept'))
                $this->addColumn('payrolls', 'payroll_concept', $this->string(255));
             else{

                $this->alterColumn('payrolls', 'payroll_concept', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('payroll_account'))
                $this->addColumn('payrolls', 'payroll_account', $this->string(255));
             else{

                $this->alterColumn('payrolls', 'payroll_account', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('payroll_tax'))
                $this->addColumn('payrolls', 'payroll_tax', $this->double(22));
             else{
                $this->alterColumn('payrolls', 'payroll_tax', $this->double(22));
                }
            if (!$exist_table->getColumn('payroll_total'))
                $this->addColumn('payrolls', 'payroll_total', $this->integer(10));
             else{
                $this->alterColumn('payrolls', 'payroll_total', $this->integer(10));
                }
            if (!$exist_table->getColumn('payroll_tags'))
                $this->addColumn('payrolls', 'payroll_tags', $this->string(255));
             else{

                $this->alterColumn('payrolls', 'payroll_tags', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('payroll_default_account'))
                $this->addColumn('payrolls', 'payroll_default_account', $this->string(255));
             else{

                $this->alterColumn('payrolls', 'payroll_default_account', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('payroll_attachment'))
                $this->addColumn('payrolls', 'payroll_attachment', $this->string(255));
             else{

                $this->alterColumn('payrolls', 'payroll_attachment', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('payroll_status_id'))
                $this->addColumn('payrolls', 'payroll_status_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('payrolls', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('payrolls', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('payrolls', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('payrolls', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            }
        /*Generating index*/

        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('payrolls_fk',$exist_table->foreignKeys) || !array_key_exists('payroll_status_id',$exist_table->foreignKeys['payrolls_fk'])) 
            $this->addForeignKey(
                'payrolls_fk',
                'payrolls',
                'payroll_status_id',
                'payroll_status',
                'id_payroll_status',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('payrolls_fk','payrolls' );
            $this->addForeignKey(
                'payrolls_fk',
                'payrolls',
                'payroll_status_id',
                'payroll_status',
                'id_payroll_status',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('payrolls_fk1',$exist_table->foreignKeys) || !array_key_exists('employee_id',$exist_table->foreignKeys['payrolls_fk1'])) 
            $this->addForeignKey(
                'payrolls_fk1',
                'payrolls',
                'employee_id',
                'employees',
                'id_employee',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('payrolls_fk1','payrolls' );
            $this->addForeignKey(
                'payrolls_fk1',
                'payrolls',
                'employee_id',
                'employees',
                'id_employee',
                'CASCADE',
                'CASCADE'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Payrolls cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Payrolls cannot be reverted.


        return false;
    }
    */
}
