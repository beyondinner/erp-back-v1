<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111839_Role_action_access
 */
class M210417_111839_Role_action_access extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'role_action_access';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('role_action_access',
            [
                'id_role_action_access' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'id_role' =>$this->integer(10)->notNull(),
                'id_actions' =>$this->integer(10)->notNull(),
                'range_role_action_access' =>$this->boolean()->notNull(),
                'enabled' =>$this->boolean()->notNull(),
                'start_range' =>$this->date(),
                'end_range' =>$this->date(),
                 'PRIMARY KEY (`id_role_action_access`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_role_action_access'))
                $this->addColumn('role_action_access', 'id_role_action_access', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('id_role'))
                $this->addColumn('role_action_access', 'id_role', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('id_actions'))
                $this->addColumn('role_action_access', 'id_actions', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('range_role_action_access'))
                $this->addColumn('role_action_access', 'range_role_action_access', $this->boolean()->notNull());
             else{
                $this->alterColumn('role_action_access', 'range_role_action_access', $this->boolean()->notNull());
                }
            if (!$exist_table->getColumn('enabled'))
                $this->addColumn('role_action_access', 'enabled', $this->boolean()->notNull());
             else{
                $this->alterColumn('role_action_access', 'enabled', $this->boolean()->notNull());
                }
            if (!$exist_table->getColumn('start_range'))
                $this->addColumn('role_action_access', 'start_range', $this->date());
             else{
                $this->alterColumn('role_action_access', 'start_range', $this->date());
                }
            if (!$exist_table->getColumn('end_range'))
                $this->addColumn('role_action_access', 'end_range', $this->date());
             else{
                $this->alterColumn('role_action_access', 'end_range', $this->date());
                }
            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('id_role', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'id_role',
                'role_action_access',
                ['id_role','id_actions'],
                true
            );
        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('Refactions17',$exist_table->foreignKeys) || !array_key_exists('id_actions',$exist_table->foreignKeys['Refactions17'])) 
            $this->addForeignKey(
                'Refactions17',
                'role_action_access',
                'id_actions',
                'actions',
                'id_actions',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('Refactions17','role_action_access' );
            $this->addForeignKey(
                'Refactions17',
                'role_action_access',
                'id_actions',
                'actions',
                'id_actions',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('Refrole21',$exist_table->foreignKeys) || !array_key_exists('id_role',$exist_table->foreignKeys['Refrole21'])) 
            $this->addForeignKey(
                'Refrole21',
                'role_action_access',
                'id_role',
                'role',
                'id_role',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('Refrole21','role_action_access' );
            $this->addForeignKey(
                'Refrole21',
                'role_action_access',
                'id_role',
                'role',
                'id_role',
                'CASCADE',
                'CASCADE'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Role_action_access cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Role_action_access cannot be reverted.


        return false;
    }
    */
}
