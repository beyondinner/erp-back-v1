<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111850_Participants
 */
class M210417_111850_Participants extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'participants';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('participants',
            [
                'id_participant' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'participant_name' =>$this->string(50),
                'apellidos' =>$this->string(100),
                'participant_email' =>$this->string(80),
                'participant_phone' =>$this->string(20),
                'id_payment_types' =>$this->integer(10),
                'id_contact' =>$this->integer(10),
                'id_club_member' =>$this->integer(10),
                 'PRIMARY KEY (`id_participant`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_participant'))
                $this->addColumn('participants', 'id_participant', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('participant_name'))
                $this->addColumn('participants', 'participant_name', $this->string(50));
             else{

                $this->alterColumn('participants', 'participant_name', 'VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('apellidos'))
                $this->addColumn('participants', 'apellidos', $this->string(100));
             else{

                $this->alterColumn('participants', 'apellidos', 'VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('participant_email'))
                $this->addColumn('participants', 'participant_email', $this->string(80));
             else{

                $this->alterColumn('participants', 'participant_email', 'VARCHAR(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('participant_phone'))
                $this->addColumn('participants', 'participant_phone', $this->string(20));
             else{

                $this->alterColumn('participants', 'participant_phone', 'VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('id_payment_types'))
                $this->addColumn('participants', 'id_payment_types', $this->integer(10));

            if (!$exist_table->getColumn('id_contact'))
                $this->addColumn('participants', 'id_contact', $this->integer(10));

            if (!$exist_table->getColumn('id_club_member'))
                $this->addColumn('participants', 'id_club_member', $this->integer(10));

            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('participant_email', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'participant_email',
                'participants',
                ['participant_email'],
                true
            );
        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('participants_fk',$exist_table->foreignKeys) || !array_key_exists('id_payment_types',$exist_table->foreignKeys['participants_fk'])) 
            $this->addForeignKey(
                'participants_fk',
                'participants',
                'id_payment_types',
                'payment_types',
                'id_payment_type',
                'SET NULL',
                'SET NULL'
            );
           else {
            $this->dropForeignKey('participants_fk','participants' );
            $this->addForeignKey(
                'participants_fk',
                'participants',
                'id_payment_types',
                'payment_types',
                'id_payment_type',
                'SET NULL',
                'SET NULL'
            );
           }
        if ($exist_table === null || !array_key_exists('participants_fk1',$exist_table->foreignKeys) || !array_key_exists('id_contact',$exist_table->foreignKeys['participants_fk1'])) 
            $this->addForeignKey(
                'participants_fk1',
                'participants',
                'id_contact',
                'contacts',
                'id_contact',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('participants_fk1','participants' );
            $this->addForeignKey(
                'participants_fk1',
                'participants',
                'id_contact',
                'contacts',
                'id_contact',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('participants_fk2',$exist_table->foreignKeys) || !array_key_exists('id_club_member',$exist_table->foreignKeys['participants_fk2'])) 
            $this->addForeignKey(
                'participants_fk2',
                'participants',
                'id_club_member',
                'club_members',
                'id_club_members',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('participants_fk2','participants' );
            $this->addForeignKey(
                'participants_fk2',
                'participants',
                'id_club_member',
                'club_members',
                'id_club_members',
                'CASCADE',
                'CASCADE'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Participants cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Participants cannot be reverted.


        return false;
    }
    */
}
