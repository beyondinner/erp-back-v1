<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111827_Migrations
 */
class M210417_111827_Migrations extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'migrations';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('migrations',
            [
                'version' =>$this->string(180)->notNull()->unique(),
                'apply_time' =>$this->integer(10),
                 'PRIMARY KEY (`version`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('version'))
                $this->addColumn('migrations', 'version', $this->string(180)->notNull()->unique());

            if (!$exist_table->getColumn('apply_time'))
                $this->addColumn('migrations', 'apply_time', $this->integer(10));
             else{
                $this->alterColumn('migrations', 'apply_time', $this->integer(10));
                }
            }
        /*Generating index*/

        /*Generating foreignkey*/


    }

   public function down()
    {
        echo 'M210417_111812_Migrations cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Migrations cannot be reverted.


        return false;
    }
    */
}
