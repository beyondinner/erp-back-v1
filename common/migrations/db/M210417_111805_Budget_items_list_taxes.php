<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111805_Budget_items_list_taxes
 */
class M210417_111805_Budget_items_list_taxes extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'budget_items_list_taxes';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('budget_items_list_taxes',
            [
                'id_budget_items_list_taxes' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'budget_items_list_taxes_tax' =>$this->integer(10)->notNull(),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'tax_name' =>$this->string(20),
                 'PRIMARY KEY (`id_budget_items_list_taxes`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_budget_items_list_taxes'))
                $this->addColumn('budget_items_list_taxes', 'id_budget_items_list_taxes', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('budget_items_list_taxes_tax'))
                $this->addColumn('budget_items_list_taxes', 'budget_items_list_taxes_tax', $this->integer(10)->notNull());
             else{
                $this->alterColumn('budget_items_list_taxes', 'budget_items_list_taxes_tax', $this->integer(10)->notNull());
                }
            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('budget_items_list_taxes', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('budget_items_list_taxes', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('budget_items_list_taxes', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('budget_items_list_taxes', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('tax_name'))
                $this->addColumn('budget_items_list_taxes', 'tax_name', $this->string(20));
             else{

                $this->alterColumn('budget_items_list_taxes', 'tax_name', 'VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('tax_name', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'tax_name',
                'budget_items_list_taxes',
                ['tax_name'],
                true
            );
        /*Generating foreignkey*/


    }

   public function down()
    {
        echo 'M210417_111812_Budget_items_list_taxes cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Budget_items_list_taxes cannot be reverted.


        return false;
    }
    */
}
