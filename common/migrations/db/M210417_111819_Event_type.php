<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111819_Event_type
 */
class M210417_111819_Event_type extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'event_type';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('event_type',
            [
                'id_event_type' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'event_type' =>$this->string(20),
                'event_type_description' =>$this->text(),
                 'PRIMARY KEY (`id_event_type`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_event_type'))
                $this->addColumn('event_type', 'id_event_type', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('event_type'))
                $this->addColumn('event_type', 'event_type', $this->string(20));
             else{

                $this->alterColumn('event_type', 'event_type', 'VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('event_type_description'))
                $this->addColumn('event_type', 'event_type_description', $this->text());
             else{
                $this->alterColumn('event_type', 'event_type_description', $this->text());
                }
            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('event_type', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'event_type',
                'event_type',
                ['event_type'],
                true
            );
        /*Generating foreignkey*/


    }

   public function down()
    {
        echo 'M210417_111812_Event_type cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Event_type cannot be reverted.


        return false;
    }
    */
}
