<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111838_Role
 */
class M210417_111838_Role extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'role';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('role',
            [
                'id_role' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'id_father_role' =>$this->integer(10),
                'role_name' =>$this->string(255),
                 'PRIMARY KEY (`id_role`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_role'))
                $this->addColumn('role', 'id_role', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('id_father_role'))
                $this->addColumn('role', 'id_father_role', $this->integer(10));

            if (!$exist_table->getColumn('role_name'))
                $this->addColumn('role', 'role_name', $this->string(255));
             else{

                $this->alterColumn('role', 'role_name', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            }
        /*Generating index*/

        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('Refrole20',$exist_table->foreignKeys) || !array_key_exists('id_father_role',$exist_table->foreignKeys['Refrole20'])) 
            $this->addForeignKey(
                'Refrole20',
                'role',
                'id_father_role',
                'role',
                'id_role',
                'NO ACTION',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('Refrole20','role' );
            $this->addForeignKey(
                'Refrole20',
                'role',
                'id_father_role',
                'role',
                'id_role',
                'NO ACTION',
                'CASCADE'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Role cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Role cannot be reverted.


        return false;
    }
    */
}
