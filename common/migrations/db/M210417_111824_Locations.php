<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111824_Locations
 */
class M210417_111824_Locations extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'locations';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('locations',
            [
                'id_location' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'location' =>$this->string(255),
                 'PRIMARY KEY (`id_location`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_location'))
                $this->addColumn('locations', 'id_location', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('location'))
                $this->addColumn('locations', 'location', $this->string(255));
             else{

                $this->alterColumn('locations', 'location', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('location', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'location',
                'locations',
                ['location'],
                true
            );
        /*Generating foreignkey*/


    }

   public function down()
    {
        echo 'M210417_111812_Locations cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Locations cannot be reverted.


        return false;
    }
    */
}
