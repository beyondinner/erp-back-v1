<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111812_Contract_payment_unities
 */
class M210417_111812_Contract_payment_unities extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'contract_payment_unities';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('contract_payment_unities',
            [
                'id_contract_payment_unity' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'contract_payment_unity_siglas' =>$this->string(255),
                'contract_payment_unity_desc' =>$this->string(255),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                 'PRIMARY KEY (`id_contract_payment_unity`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_contract_payment_unity'))
                $this->addColumn('contract_payment_unities', 'id_contract_payment_unity', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('contract_payment_unity_siglas'))
                $this->addColumn('contract_payment_unities', 'contract_payment_unity_siglas', $this->string(255));
             else{

                $this->alterColumn('contract_payment_unities', 'contract_payment_unity_siglas', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('contract_payment_unity_desc'))
                $this->addColumn('contract_payment_unities', 'contract_payment_unity_desc', $this->string(255));
             else{

                $this->alterColumn('contract_payment_unities', 'contract_payment_unity_desc', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('contract_payment_unities', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('contract_payment_unities', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('contract_payment_unities', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('contract_payment_unities', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('contract_payment_unity_desc', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'contract_payment_unity_desc',
                'contract_payment_unities',
                ['contract_payment_unity_desc'],
                true
            );
        if ($exist_table === null || !array_key_exists('contract_payment_unity_siglas', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'contract_payment_unity_siglas',
                'contract_payment_unities',
                ['contract_payment_unity_siglas'],
                true
            );
        /*Generating foreignkey*/


    }

   public function down()
    {
        echo 'M210417_111812_Contract_payment_unities cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Contract_payment_unities cannot be reverted.


        return false;
    }
    */
}
