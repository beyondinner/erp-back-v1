<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111811_Contact_types
 */
class M210417_111811_Contact_types extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'contact_types';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('contact_types',
            [
                'id_contact_type' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'contact_type_siglas' =>$this->string(255),
                'contact_type_desc' =>$this->string(255),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                 'PRIMARY KEY (`id_contact_type`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_contact_type'))
                $this->addColumn('contact_types', 'id_contact_type', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('contact_type_siglas'))
                $this->addColumn('contact_types', 'contact_type_siglas', $this->string(255));
             else{

                $this->alterColumn('contact_types', 'contact_type_siglas', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('contact_type_desc'))
                $this->addColumn('contact_types', 'contact_type_desc', $this->string(255));
             else{

                $this->alterColumn('contact_types', 'contact_type_desc', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('contact_types', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('contact_types', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('contact_types', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('contact_types', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('contact_type_desc', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'contact_type_desc',
                'contact_types',
                ['contact_type_desc'],
                true
            );
        if ($exist_table === null || !array_key_exists('contact_type_siglas', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'contact_type_siglas',
                'contact_types',
                ['contact_type_siglas'],
                true
            );
        /*Generating foreignkey*/


    }

   public function down()
    {
        echo 'M210417_111812_Contact_types cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Contact_types cannot be reverted.


        return false;
    }
    */
}
