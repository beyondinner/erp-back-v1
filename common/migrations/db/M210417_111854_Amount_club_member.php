<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111854_Amount_club_member
 */
class M210417_111854_Amount_club_member extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'amount_club_member';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('amount_club_member',
            [
                'id_amount_club_member' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'id_user' =>$this->integer(10)->notNull(),
                'id_club_member' =>$this->integer(10)->notNull(),
                'amount' =>$this->float(12)->notNull(),
                'date_at' =>$this->dateTime()->notNull()->append(' NULL'),
                'amiunt_description' =>$this->text(),
                'type' =>$this->string(20),
                'email_receptor' =>$this->string(200),
                 'PRIMARY KEY (`id_amount_club_member`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_amount_club_member'))
                $this->addColumn('amount_club_member', 'id_amount_club_member', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('id_user'))
                $this->addColumn('amount_club_member', 'id_user', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('id_club_member'))
                $this->addColumn('amount_club_member', 'id_club_member', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('amount'))
                $this->addColumn('amount_club_member', 'amount', $this->float(12)->notNull());
             else{
                $this->alterColumn('amount_club_member', 'amount', $this->float(12)->notNull());
                }
            if (!$exist_table->getColumn('date_at'))
                $this->addColumn('amount_club_member', 'date_at', $this->dateTime()->notNull()->append(' NULL'));
             else{
                $this->alterColumn('amount_club_member', 'date_at', $this->dateTime()->notNull()->append(' NULL'));
                }
            if (!$exist_table->getColumn('amiunt_description'))
                $this->addColumn('amount_club_member', 'amiunt_description', $this->text());
             else{
                $this->alterColumn('amount_club_member', 'amiunt_description', $this->text());
                }
            if (!$exist_table->getColumn('type'))
                $this->addColumn('amount_club_member', 'type', $this->string(20));
             else{

                $this->alterColumn('amount_club_member', 'type', 'VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('email_receptor'))
                $this->addColumn('amount_club_member', 'email_receptor', $this->string(200));
             else{

                $this->alterColumn('amount_club_member', 'email_receptor', 'VARCHAR(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            }
        /*Generating index*/

        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('amount_club_member_fk',$exist_table->foreignKeys) || !array_key_exists('id_user',$exist_table->foreignKeys['amount_club_member_fk'])) 
            $this->addForeignKey(
                'amount_club_member_fk',
                'amount_club_member',
                'id_user',
                'users',
                'id_user',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('amount_club_member_fk','amount_club_member' );
            $this->addForeignKey(
                'amount_club_member_fk',
                'amount_club_member',
                'id_user',
                'users',
                'id_user',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('amount_club_member_fk1',$exist_table->foreignKeys) || !array_key_exists('id_club_member',$exist_table->foreignKeys['amount_club_member_fk1'])) 
            $this->addForeignKey(
                'amount_club_member_fk1',
                'amount_club_member',
                'id_club_member',
                'club_members',
                'id_club_members',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('amount_club_member_fk1','amount_club_member' );
            $this->addForeignKey(
                'amount_club_member_fk1',
                'amount_club_member',
                'id_club_member',
                'club_members',
                'id_club_members',
                'CASCADE',
                'CASCADE'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Amount_club_member cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Amount_club_member cannot be reverted.


        return false;
    }
    */
}
