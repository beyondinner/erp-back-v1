<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111905_Count_all
 */
class M210417_111905_Count_all extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->execute("DROP VIEW IF EXISTS count_all CASCADE;");

        $this->execute("CREATE OR REPLACE VIEW count_all(count_all) as select count(`billings`.`id_billings`) AS `count_all` from `billings` union select count(`club_members`.`id_club_members`) AS `count_club_members` from `club_members` union select count(`contacts`.`id_contact`) AS `count_contact` from `contacts` union select count(`products`.`id_product`) AS `count_products` from `products` union select count(`employees`.`id_employee`) AS `count_employee` from `employees` union select count(`services`.`id_service`) AS `count_services` from `services` union select count(`users`.`id_user`) AS `count_user` from `users`");
    }

   public function down()
    {
        echo 'M210417_111812_Count_all cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Count_all cannot be reverted.


        return false;
    }
    */
}
