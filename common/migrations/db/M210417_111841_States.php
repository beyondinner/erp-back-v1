<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111841_States
 */
class M210417_111841_States extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'states';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('states',
            [
                'id_state' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'state_name' =>$this->string(255),
                'country_code' =>$this->string(255),
                'district' =>$this->string(100),
                'country_id' =>$this->integer(10),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'population' =>$this->integer(10),
                'state_siglas' =>$this->string(255),
                'state_desc' =>$this->string(255),
                 'PRIMARY KEY (`id_state`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_state'))
                $this->addColumn('states', 'id_state', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('state_name'))
                $this->addColumn('states', 'state_name', $this->string(255));
             else{

                $this->alterColumn('states', 'state_name', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('country_code'))
                $this->addColumn('states', 'country_code', $this->string(255));
             else{

                $this->alterColumn('states', 'country_code', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('district'))
                $this->addColumn('states', 'district', $this->string(100));
             else{

                $this->alterColumn('states', 'district', 'VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('country_id'))
                $this->addColumn('states', 'country_id', $this->integer(10));

            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('states', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('states', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('states', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('states', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('population'))
                $this->addColumn('states', 'population', $this->integer(10));
             else{
                $this->alterColumn('states', 'population', $this->integer(10));
                }
            if (!$exist_table->getColumn('state_siglas'))
                $this->addColumn('states', 'state_siglas', $this->string(255));
             else{

                $this->alterColumn('states', 'state_siglas', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('state_desc'))
                $this->addColumn('states', 'state_desc', $this->string(255));
             else{

                $this->alterColumn('states', 'state_desc', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            }
        /*Generating index*/

        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('states_fk',$exist_table->foreignKeys) || !array_key_exists('country_id',$exist_table->foreignKeys['states_fk'])) 
            $this->addForeignKey(
                'states_fk',
                'states',
                'country_id',
                'countries',
                'id_country',
                'SET NULL',
                'SET NULL'
            );
           else {
            $this->dropForeignKey('states_fk','states' );
            $this->addForeignKey(
                'states_fk',
                'states',
                'country_id',
                'countries',
                'id_country',
                'SET NULL',
                'SET NULL'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_States cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_States cannot be reverted.


        return false;
    }
    */
}
