<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111835_Servicio_productos
 */
class M210417_111835_Servicio_productos extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'servicio_productos';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('servicio_productos',
            [
                'id' =>$this->integer(20)->append('AUTO_INCREMENT')->notNull()->unique(),
                'name' =>$this->string(191),
                'description' =>$this->text(),
                'price' =>$this->double(22)->notNull(),
                'max_part' =>$this->integer(10)->notNull(),
                'state' =>$this->smallInteger(5)->notNull(),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                 'PRIMARY KEY (`id`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id'))
                $this->addColumn('servicio_productos', 'id', $this->integer(20)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('name'))
                $this->addColumn('servicio_productos', 'name', $this->string(191));
             else{

                $this->alterColumn('servicio_productos', 'name', 'VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('description'))
                $this->addColumn('servicio_productos', 'description', $this->text());
             else{
                $this->alterColumn('servicio_productos', 'description', $this->text());
                }
            if (!$exist_table->getColumn('price'))
                $this->addColumn('servicio_productos', 'price', $this->double(22)->notNull());
             else{
                $this->alterColumn('servicio_productos', 'price', $this->double(22)->notNull());
                }
            if (!$exist_table->getColumn('max_part'))
                $this->addColumn('servicio_productos', 'max_part', $this->integer(10)->notNull());
             else{
                $this->alterColumn('servicio_productos', 'max_part', $this->integer(10)->notNull());
                }
            if (!$exist_table->getColumn('state'))
                $this->addColumn('servicio_productos', 'state', $this->smallInteger(5)->notNull());
             else{
                $this->alterColumn('servicio_productos', 'state', $this->smallInteger(5)->notNull());
                }
            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('servicio_productos', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('servicio_productos', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('servicio_productos', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('servicio_productos', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            }
        /*Generating index*/

        /*Generating foreignkey*/


    }

   public function down()
    {
        echo 'M210417_111812_Servicio_productos cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Servicio_productos cannot be reverted.


        return false;
    }
    */
}
