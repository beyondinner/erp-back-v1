<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111849_Orders_product
 */
class M210417_111849_Orders_product extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'orders_product';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('orders_product',
            [
                'id_order_products' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'id_order' =>$this->integer(10)->notNull(),
                'id_product' =>$this->integer(10),
                'id_services' =>$this->integer(10),
                'amount' =>$this->integer(10),
                'tax_id' =>$this->integer(10),
                 'PRIMARY KEY (`id_order_products`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_order_products'))
                $this->addColumn('orders_product', 'id_order_products', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('id_order'))
                $this->addColumn('orders_product', 'id_order', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('id_product'))
                $this->addColumn('orders_product', 'id_product', $this->integer(10));

            if (!$exist_table->getColumn('id_services'))
                $this->addColumn('orders_product', 'id_services', $this->integer(10));

            if (!$exist_table->getColumn('amount'))
                $this->addColumn('orders_product', 'amount', $this->integer(10));
             else{
                $this->alterColumn('orders_product', 'amount', $this->integer(10));
                }
            if (!$exist_table->getColumn('tax_id'))
                $this->addColumn('orders_product', 'tax_id', $this->integer(10));
             else{
                $this->alterColumn('orders_product', 'tax_id', $this->integer(10));
                }
            }
        /*Generating index*/

        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('orders_product_fk',$exist_table->foreignKeys) || !array_key_exists('id_order',$exist_table->foreignKeys['orders_product_fk'])) 
            $this->addForeignKey(
                'orders_product_fk',
                'orders_product',
                'id_order',
                'orders',
                'id_orders',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('orders_product_fk','orders_product' );
            $this->addForeignKey(
                'orders_product_fk',
                'orders_product',
                'id_order',
                'orders',
                'id_orders',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('orders_product_fk1',$exist_table->foreignKeys) || !array_key_exists('id_product',$exist_table->foreignKeys['orders_product_fk1'])) 
            $this->addForeignKey(
                'orders_product_fk1',
                'orders_product',
                'id_product',
                'products',
                'id_product',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('orders_product_fk1','orders_product' );
            $this->addForeignKey(
                'orders_product_fk1',
                'orders_product',
                'id_product',
                'products',
                'id_product',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('orders_product_fk2',$exist_table->foreignKeys) || !array_key_exists('id_services',$exist_table->foreignKeys['orders_product_fk2'])) 
            $this->addForeignKey(
                'orders_product_fk2',
                'orders_product',
                'id_services',
                'services',
                'id_service',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('orders_product_fk2','orders_product' );
            $this->addForeignKey(
                'orders_product_fk2',
                'orders_product',
                'id_services',
                'services',
                'id_service',
                'CASCADE',
                'CASCADE'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Orders_product cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Orders_product cannot be reverted.


        return false;
    }
    */
}
