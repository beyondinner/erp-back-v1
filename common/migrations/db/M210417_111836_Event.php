<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111836_Event
 */
class M210417_111836_Event extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'event';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('event',
            [
                'id_event' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'event_name' =>$this->string(255),
                'event_price' =>$this->float(12)->notNull(),
                'is_all_day' =>$this->boolean()->notNull(),
                'event_duration' =>$this->time()->notNull()->append(' NULL'),
                'emails' =>$this->text(),
                'phones' =>$this->text(),
                'id_location' =>$this->integer(10)->notNull(),
                'state' =>$this->string(20),
                'created_at' =>$this->dateTime()->notNull()->append(' NULL'),
                'date_init' =>$this->dateTime()->notNull()->append(' NULL'),
                'date_end' =>$this->dateTime()->notNull()->append(' NULL'),
                'id_register_type' =>$this->integer(10)->notNull(),
                'id_event_type' =>$this->integer(10)->notNull(),
                'documents' =>$this->text(),
                'amount_guess' =>$this->integer(10)->notNull(),
                 'PRIMARY KEY (`id_event`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_event'))
                $this->addColumn('event', 'id_event', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('event_name'))
                $this->addColumn('event', 'event_name', $this->string(255));
             else{

                $this->alterColumn('event', 'event_name', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('event_price'))
                $this->addColumn('event', 'event_price', $this->float(12)->notNull());
             else{
                $this->alterColumn('event', 'event_price', $this->float(12)->notNull());
                }
            if (!$exist_table->getColumn('is_all_day'))
                $this->addColumn('event', 'is_all_day', $this->boolean()->notNull());
             else{
                $this->alterColumn('event', 'is_all_day', $this->boolean()->notNull());
                }
            if (!$exist_table->getColumn('event_duration'))
                $this->addColumn('event', 'event_duration', $this->time()->notNull()->append(' NULL'));
             else{
                $this->alterColumn('event', 'event_duration', $this->time()->notNull()->append(' NULL'));
                }
            if (!$exist_table->getColumn('emails'))
                $this->addColumn('event', 'emails', $this->text());
             else{
                $this->alterColumn('event', 'emails', $this->text());
                }
            if (!$exist_table->getColumn('phones'))
                $this->addColumn('event', 'phones', $this->text());
             else{
                $this->alterColumn('event', 'phones', $this->text());
                }
            if (!$exist_table->getColumn('id_location'))
                $this->addColumn('event', 'id_location', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('state'))
                $this->addColumn('event', 'state', $this->string(20));
             else{

                $this->alterColumn('event', 'state', 'VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('event', 'created_at', $this->dateTime()->notNull()->append(' NULL'));
             else{
                $this->alterColumn('event', 'created_at', $this->dateTime()->notNull()->append(' NULL'));
                }
            if (!$exist_table->getColumn('date_init'))
                $this->addColumn('event', 'date_init', $this->dateTime()->notNull()->append(' NULL'));
             else{
                $this->alterColumn('event', 'date_init', $this->dateTime()->notNull()->append(' NULL'));
                }
            if (!$exist_table->getColumn('date_end'))
                $this->addColumn('event', 'date_end', $this->dateTime()->notNull()->append(' NULL'));
             else{
                $this->alterColumn('event', 'date_end', $this->dateTime()->notNull()->append(' NULL'));
                }
            if (!$exist_table->getColumn('id_register_type'))
                $this->addColumn('event', 'id_register_type', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('id_event_type'))
                $this->addColumn('event', 'id_event_type', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('documents'))
                $this->addColumn('event', 'documents', $this->text());
             else{
                $this->alterColumn('event', 'documents', $this->text());
                }
            if (!$exist_table->getColumn('amount_guess'))
                $this->addColumn('event', 'amount_guess', $this->integer(10)->notNull());
             else{
                $this->alterColumn('event', 'amount_guess', $this->integer(10)->notNull());
                }
            }
        /*Generating index*/

        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('event_fk',$exist_table->foreignKeys) || !array_key_exists('id_register_type',$exist_table->foreignKeys['event_fk'])) 
            $this->addForeignKey(
                'event_fk',
                'event',
                'id_register_type',
                'register_type',
                'id_register_type',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('event_fk','event' );
            $this->addForeignKey(
                'event_fk',
                'event',
                'id_register_type',
                'register_type',
                'id_register_type',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('event_fk1',$exist_table->foreignKeys) || !array_key_exists('id_event_type',$exist_table->foreignKeys['event_fk1'])) 
            $this->addForeignKey(
                'event_fk1',
                'event',
                'id_event_type',
                'event_type',
                'id_event_type',
                'NO ACTION',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('event_fk1','event' );
            $this->addForeignKey(
                'event_fk1',
                'event',
                'id_event_type',
                'event_type',
                'id_event_type',
                'NO ACTION',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('event_fk2',$exist_table->foreignKeys) || !array_key_exists('id_location',$exist_table->foreignKeys['event_fk2'])) 
            $this->addForeignKey(
                'event_fk2',
                'event',
                'id_location',
                'locations',
                'id_location',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('event_fk2','event' );
            $this->addForeignKey(
                'event_fk2',
                'event',
                'id_location',
                'locations',
                'id_location',
                'CASCADE',
                'CASCADE'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Event cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Event cannot be reverted.


        return false;
    }
    */
}
