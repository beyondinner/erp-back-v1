<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111829_Payment_types
 */
class M210417_111829_Payment_types extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'payment_types';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('payment_types',
            [
                'id_payment_type' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'payment_type_siglas' =>$this->string(255),
                'payment_type_desc' =>$this->string(255),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                 'PRIMARY KEY (`id_payment_type`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_payment_type'))
                $this->addColumn('payment_types', 'id_payment_type', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('payment_type_siglas'))
                $this->addColumn('payment_types', 'payment_type_siglas', $this->string(255));
             else{

                $this->alterColumn('payment_types', 'payment_type_siglas', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('payment_type_desc'))
                $this->addColumn('payment_types', 'payment_type_desc', $this->string(255));
             else{

                $this->alterColumn('payment_types', 'payment_type_desc', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('payment_types', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('payment_types', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('payment_types', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('payment_types', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('payment_type_desc', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'payment_type_desc',
                'payment_types',
                ['payment_type_desc'],
                true
            );
        if ($exist_table === null || !array_key_exists('payment_type_siglas', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'payment_type_siglas',
                'payment_types',
                ['payment_type_siglas'],
                true
            );
        /*Generating foreignkey*/


    }

   public function down()
    {
        echo 'M210417_111812_Payment_types cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Payment_types cannot be reverted.


        return false;
    }
    */
}
