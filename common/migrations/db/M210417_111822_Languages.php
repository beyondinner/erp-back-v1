<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111822_Languages
 */
class M210417_111822_Languages extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'languages';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('languages',
            [
                'id_language' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'language_siglas' =>$this->string(255),
                'language_desc' =>$this->string(255),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                 'PRIMARY KEY (`id_language`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_language'))
                $this->addColumn('languages', 'id_language', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('language_siglas'))
                $this->addColumn('languages', 'language_siglas', $this->string(255));
             else{

                $this->alterColumn('languages', 'language_siglas', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('language_desc'))
                $this->addColumn('languages', 'language_desc', $this->string(255));
             else{

                $this->alterColumn('languages', 'language_desc', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('languages', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('languages', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('languages', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('languages', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('language_desc', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'language_desc',
                'languages',
                ['language_desc'],
                true
            );
        if ($exist_table === null || !array_key_exists('language_siglas', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'language_siglas',
                'languages',
                ['language_siglas'],
                true
            );
        /*Generating foreignkey*/


    }

   public function down()
    {
        echo 'M210417_111812_Languages cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Languages cannot be reverted.


        return false;
    }
    */
}
