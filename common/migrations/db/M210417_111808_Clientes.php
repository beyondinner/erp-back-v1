<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111808_Clientes
 */
class M210417_111808_Clientes extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'clientes';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('clientes',
            [
                'id' =>$this->integer(20)->append('AUTO_INCREMENT')->notNull()->unique(),
                'fiscal_name' =>$this->string(191),
                'comercial_name' =>$this->string(191),
                'name' =>$this->string(191),
                'last_name' =>$this->string(191),
                'email' =>$this->string(191),
                'phone' =>$this->string(191),
                'use_products' =>$this->smallInteger(5),
                'state' =>$this->smallInteger(5)->notNull(),
                'wallet' =>$this->integer(10)->notNull(),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                 'PRIMARY KEY (`id`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id'))
                $this->addColumn('clientes', 'id', $this->integer(20)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('fiscal_name'))
                $this->addColumn('clientes', 'fiscal_name', $this->string(191));
             else{

                $this->alterColumn('clientes', 'fiscal_name', 'VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('comercial_name'))
                $this->addColumn('clientes', 'comercial_name', $this->string(191));
             else{

                $this->alterColumn('clientes', 'comercial_name', 'VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('name'))
                $this->addColumn('clientes', 'name', $this->string(191));
             else{

                $this->alterColumn('clientes', 'name', 'VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('last_name'))
                $this->addColumn('clientes', 'last_name', $this->string(191));
             else{

                $this->alterColumn('clientes', 'last_name', 'VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('email'))
                $this->addColumn('clientes', 'email', $this->string(191));
             else{

                $this->alterColumn('clientes', 'email', 'VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('phone'))
                $this->addColumn('clientes', 'phone', $this->string(191));
             else{

                $this->alterColumn('clientes', 'phone', 'VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('use_products'))
                $this->addColumn('clientes', 'use_products', $this->smallInteger(5));
             else{
                $this->alterColumn('clientes', 'use_products', $this->smallInteger(5));
                }
            if (!$exist_table->getColumn('state'))
                $this->addColumn('clientes', 'state', $this->smallInteger(5)->notNull());
             else{
                $this->alterColumn('clientes', 'state', $this->smallInteger(5)->notNull());
                }
            if (!$exist_table->getColumn('wallet'))
                $this->addColumn('clientes', 'wallet', $this->integer(10)->notNull());
             else{
                $this->alterColumn('clientes', 'wallet', $this->integer(10)->notNull());
                }
            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('clientes', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('clientes', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('clientes', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('clientes', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            }
        /*Generating index*/

        /*Generating foreignkey*/


    }

   public function down()
    {
        echo 'M210417_111812_Clientes cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Clientes cannot be reverted.


        return false;
    }
    */
}
