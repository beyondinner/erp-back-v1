<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111802_Actions
 */
class M210417_111802_Actions extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'actions';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('actions',
            [
                'id_actions' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'app' =>$this->string(40),
                'module' =>$this->string(40),
                'controller' =>$this->string(40),
                'action' =>$this->string(40),
                'default_access' =>$this->string(20),
                'enabled' =>$this->boolean()->notNull(),
                'global' =>$this->boolean(),
                 'PRIMARY KEY (`id_actions`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_actions'))
                $this->addColumn('actions', 'id_actions', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('app'))
                $this->addColumn('actions', 'app', $this->string(40));
             else{

                $this->alterColumn('actions', 'app', 'VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('module'))
                $this->addColumn('actions', 'module', $this->string(40));
             else{

                $this->alterColumn('actions', 'module', 'VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('controller'))
                $this->addColumn('actions', 'controller', $this->string(40));
             else{

                $this->alterColumn('actions', 'controller', 'VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('action'))
                $this->addColumn('actions', 'action', $this->string(40));
             else{

                $this->alterColumn('actions', 'action', 'VARCHAR(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('default_access'))
                $this->addColumn('actions', 'default_access', $this->string(20));
             else{

                $this->alterColumn('actions', 'default_access', 'VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('enabled'))
                $this->addColumn('actions', 'enabled', $this->boolean()->notNull());
             else{
                $this->alterColumn('actions', 'enabled', $this->boolean()->notNull());
                }
            if (!$exist_table->getColumn('global'))
                $this->addColumn('actions', 'global', $this->boolean());
             else{
                $this->alterColumn('actions', 'global', $this->boolean());
                }
            }
        /*Generating index*/

        /*Generating foreignkey*/


    }

   public function down()
    {
        echo 'M210417_111812_Actions cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Actions cannot be reverted.


        return false;
    }
    */
}
