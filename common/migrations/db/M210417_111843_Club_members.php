<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111843_Club_members
 */
class M210417_111843_Club_members extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'club_members';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('club_members',
            [
                'id_club_members' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'club_members_name' =>$this->string(255),
                'club_member_last_name' =>$this->string(50),
                'club_members_dni' =>$this->string(255),
                'club_members_birthday' =>$this->date()->notNull(),
                'club_members_email' =>$this->string(255),
                'club_members_phone' =>$this->string(255),
                'club_members_address' =>$this->string(255),
                'club_members_picture' =>$this->string(255),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'club_member_code' =>$this->string(255),
                'clum_members_currency' =>$this->float(12)->notNull(),
                'id_user' =>$this->integer(10)->notNull(),
                'id_languages' =>$this->integer(10)->notNull(),
                'id_adviser' =>$this->integer(10),
                'id_state' =>$this->integer(10),
                'id_country' =>$this->integer(10),
                'postal_code' =>$this->string(20),
                 'PRIMARY KEY (`id_club_members`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_club_members'))
                $this->addColumn('club_members', 'id_club_members', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('club_members_name'))
                $this->addColumn('club_members', 'club_members_name', $this->string(255));
             else{

                $this->alterColumn('club_members', 'club_members_name', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('club_member_last_name'))
                $this->addColumn('club_members', 'club_member_last_name', $this->string(50));
             else{

                $this->alterColumn('club_members', 'club_member_last_name', 'VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('club_members_dni'))
                $this->addColumn('club_members', 'club_members_dni', $this->string(255));
             else{

                $this->alterColumn('club_members', 'club_members_dni', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('club_members_birthday'))
                $this->addColumn('club_members', 'club_members_birthday', $this->date()->notNull());
             else{
                $this->alterColumn('club_members', 'club_members_birthday', $this->date()->notNull());
                }
            if (!$exist_table->getColumn('club_members_email'))
                $this->addColumn('club_members', 'club_members_email', $this->string(255));
             else{

                $this->alterColumn('club_members', 'club_members_email', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('club_members_phone'))
                $this->addColumn('club_members', 'club_members_phone', $this->string(255));
             else{

                $this->alterColumn('club_members', 'club_members_phone', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('club_members_address'))
                $this->addColumn('club_members', 'club_members_address', $this->string(255));
             else{

                $this->alterColumn('club_members', 'club_members_address', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('club_members_picture'))
                $this->addColumn('club_members', 'club_members_picture', $this->string(255));
             else{

                $this->alterColumn('club_members', 'club_members_picture', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('club_members', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('club_members', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('club_members', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('club_members', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('club_member_code'))
                $this->addColumn('club_members', 'club_member_code', $this->string(255));
             else{

                $this->alterColumn('club_members', 'club_member_code', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('clum_members_currency'))
                $this->addColumn('club_members', 'clum_members_currency', $this->float(12)->notNull());
             else{
                $this->alterColumn('club_members', 'clum_members_currency', $this->float(12)->notNull());
                }
            if (!$exist_table->getColumn('id_user'))
                $this->addColumn('club_members', 'id_user', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('id_languages'))
                $this->addColumn('club_members', 'id_languages', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('id_adviser'))
                $this->addColumn('club_members', 'id_adviser', $this->integer(10));

            if (!$exist_table->getColumn('id_state'))
                $this->addColumn('club_members', 'id_state', $this->integer(10));

            if (!$exist_table->getColumn('id_country'))
                $this->addColumn('club_members', 'id_country', $this->integer(10));

            if (!$exist_table->getColumn('postal_code'))
                $this->addColumn('club_members', 'postal_code', $this->string(20));
             else{

                $this->alterColumn('club_members', 'postal_code', 'VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('club_members_email', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'club_members_email',
                'club_members',
                ['club_members_email'],
                true
            );
        if ($exist_table === null || !array_key_exists('club_members_dni', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'club_members_dni',
                'club_members',
                ['club_members_dni'],
                true
            );
        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('club_members_fk',$exist_table->foreignKeys) || !array_key_exists('id_user',$exist_table->foreignKeys['club_members_fk'])) 
            $this->addForeignKey(
                'club_members_fk',
                'club_members',
                'id_user',
                'users',
                'id_user',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('club_members_fk','club_members' );
            $this->addForeignKey(
                'club_members_fk',
                'club_members',
                'id_user',
                'users',
                'id_user',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('club_members_fk1',$exist_table->foreignKeys) || !array_key_exists('id_languages',$exist_table->foreignKeys['club_members_fk1'])) 
            $this->addForeignKey(
                'club_members_fk1',
                'club_members',
                'id_languages',
                'languages',
                'id_language',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('club_members_fk1','club_members' );
            $this->addForeignKey(
                'club_members_fk1',
                'club_members',
                'id_languages',
                'languages',
                'id_language',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('club_members_fk2',$exist_table->foreignKeys) || !array_key_exists('id_adviser',$exist_table->foreignKeys['club_members_fk2'])) 
            $this->addForeignKey(
                'club_members_fk2',
                'club_members',
                'id_adviser',
                'adviser',
                'id_adviser',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('club_members_fk2','club_members' );
            $this->addForeignKey(
                'club_members_fk2',
                'club_members',
                'id_adviser',
                'adviser',
                'id_adviser',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('club_members_fk3',$exist_table->foreignKeys) || !array_key_exists('id_state',$exist_table->foreignKeys['club_members_fk3'])) 
            $this->addForeignKey(
                'club_members_fk3',
                'club_members',
                'id_state',
                'states',
                'id_state',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('club_members_fk3','club_members' );
            $this->addForeignKey(
                'club_members_fk3',
                'club_members',
                'id_state',
                'states',
                'id_state',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('club_members_fk4',$exist_table->foreignKeys) || !array_key_exists('id_country',$exist_table->foreignKeys['club_members_fk4'])) 
            $this->addForeignKey(
                'club_members_fk4',
                'club_members',
                'id_country',
                'countries',
                'id_country',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('club_members_fk4','club_members' );
            $this->addForeignKey(
                'club_members_fk4',
                'club_members',
                'id_country',
                'countries',
                'id_country',
                'CASCADE',
                'CASCADE'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Club_members cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Club_members cannot be reverted.


        return false;
    }
    */
}
