<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111803_Adviser
 */
class M210417_111803_Adviser extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'adviser';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('adviser',
            [
                'id_adviser' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'adviser' =>$this->string(100)->notNull(),
                 'PRIMARY KEY (`id_adviser`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_adviser'))
                $this->addColumn('adviser', 'id_adviser', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('adviser'))
                $this->addColumn('adviser', 'adviser', $this->string(100)->notNull());
             else{

                $this->alterColumn('adviser', 'adviser', 'VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL ');

                }
            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('adviser', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'adviser',
                'adviser',
                ['adviser'],
                true
            );
        /*Generating foreignkey*/


    }

   public function down()
    {
        echo 'M210417_111812_Adviser cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Adviser cannot be reverted.


        return false;
    }
    */
}
