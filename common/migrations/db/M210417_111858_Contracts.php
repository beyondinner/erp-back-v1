<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111858_Contracts
 */
class M210417_111858_Contracts extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'contracts';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('contracts',
            [
                'id_contract' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'employee_id' =>$this->integer(10)->notNull(),
                'contract_start_date' =>$this->date()->notNull(),
                'contract_end_date' =>$this->date()->notNull(),
                'contract_title' =>$this->string(255),
                'contract_type_id' =>$this->integer(10)->notNull(),
                'payment_type_id' =>$this->integer(10)->notNull(),
                'contract_payment_unity_id' =>$this->integer(10)->notNull(),
                'contract_net_salary' =>$this->double(22)->notNull(),
                'contract_payment_quantity' =>$this->integer(10)->notNull(),
                'contract_work_hours' =>$this->integer(10)->notNull(),
                'contract_days_per_week' =>$this->integer(10)->notNull(),
                'contract_attachment' =>$this->string(255),
                'contract_status_id' =>$this->integer(10)->notNull(),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                 'PRIMARY KEY (`id_contract`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_contract'))
                $this->addColumn('contracts', 'id_contract', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('employee_id'))
                $this->addColumn('contracts', 'employee_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('contract_start_date'))
                $this->addColumn('contracts', 'contract_start_date', $this->date()->notNull());
             else{
                $this->alterColumn('contracts', 'contract_start_date', $this->date()->notNull());
                }
            if (!$exist_table->getColumn('contract_end_date'))
                $this->addColumn('contracts', 'contract_end_date', $this->date()->notNull());
             else{
                $this->alterColumn('contracts', 'contract_end_date', $this->date()->notNull());
                }
            if (!$exist_table->getColumn('contract_title'))
                $this->addColumn('contracts', 'contract_title', $this->string(255));
             else{

                $this->alterColumn('contracts', 'contract_title', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('contract_type_id'))
                $this->addColumn('contracts', 'contract_type_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('payment_type_id'))
                $this->addColumn('contracts', 'payment_type_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('contract_payment_unity_id'))
                $this->addColumn('contracts', 'contract_payment_unity_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('contract_net_salary'))
                $this->addColumn('contracts', 'contract_net_salary', $this->double(22)->notNull());
             else{
                $this->alterColumn('contracts', 'contract_net_salary', $this->double(22)->notNull());
                }
            if (!$exist_table->getColumn('contract_payment_quantity'))
                $this->addColumn('contracts', 'contract_payment_quantity', $this->integer(10)->notNull());
             else{
                $this->alterColumn('contracts', 'contract_payment_quantity', $this->integer(10)->notNull());
                }
            if (!$exist_table->getColumn('contract_work_hours'))
                $this->addColumn('contracts', 'contract_work_hours', $this->integer(10)->notNull());
             else{
                $this->alterColumn('contracts', 'contract_work_hours', $this->integer(10)->notNull());
                }
            if (!$exist_table->getColumn('contract_days_per_week'))
                $this->addColumn('contracts', 'contract_days_per_week', $this->integer(10)->notNull());
             else{
                $this->alterColumn('contracts', 'contract_days_per_week', $this->integer(10)->notNull());
                }
            if (!$exist_table->getColumn('contract_attachment'))
                $this->addColumn('contracts', 'contract_attachment', $this->string(255));
             else{

                $this->alterColumn('contracts', 'contract_attachment', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('contract_status_id'))
                $this->addColumn('contracts', 'contract_status_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('contracts', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('contracts', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('contracts', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('contracts', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            }
        /*Generating index*/

        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('contracts_fk',$exist_table->foreignKeys) || !array_key_exists('employee_id',$exist_table->foreignKeys['contracts_fk'])) 
            $this->addForeignKey(
                'contracts_fk',
                'contracts',
                'employee_id',
                'employees',
                'id_employee',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('contracts_fk','contracts' );
            $this->addForeignKey(
                'contracts_fk',
                'contracts',
                'employee_id',
                'employees',
                'id_employee',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('contracts_fk1',$exist_table->foreignKeys) || !array_key_exists('contract_type_id',$exist_table->foreignKeys['contracts_fk1'])) 
            $this->addForeignKey(
                'contracts_fk1',
                'contracts',
                'contract_type_id',
                'contract_types',
                'id_contract_type',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('contracts_fk1','contracts' );
            $this->addForeignKey(
                'contracts_fk1',
                'contracts',
                'contract_type_id',
                'contract_types',
                'id_contract_type',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('contracts_fk2',$exist_table->foreignKeys) || !array_key_exists('payment_type_id',$exist_table->foreignKeys['contracts_fk2'])) 
            $this->addForeignKey(
                'contracts_fk2',
                'contracts',
                'payment_type_id',
                'payment_types',
                'id_payment_type',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('contracts_fk2','contracts' );
            $this->addForeignKey(
                'contracts_fk2',
                'contracts',
                'payment_type_id',
                'payment_types',
                'id_payment_type',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('contracts_fk3',$exist_table->foreignKeys) || !array_key_exists('contract_payment_unity_id',$exist_table->foreignKeys['contracts_fk3'])) 
            $this->addForeignKey(
                'contracts_fk3',
                'contracts',
                'contract_payment_unity_id',
                'contract_payment_unities',
                'id_contract_payment_unity',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('contracts_fk3','contracts' );
            $this->addForeignKey(
                'contracts_fk3',
                'contracts',
                'contract_payment_unity_id',
                'contract_payment_unities',
                'id_contract_payment_unity',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('contracts_fk4',$exist_table->foreignKeys) || !array_key_exists('contract_status_id',$exist_table->foreignKeys['contracts_fk4'])) 
            $this->addForeignKey(
                'contracts_fk4',
                'contracts',
                'contract_status_id',
                'contract_status',
                'id_contract_status',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('contracts_fk4','contracts' );
            $this->addForeignKey(
                'contracts_fk4',
                'contracts',
                'contract_status_id',
                'contract_status',
                'id_contract_status',
                'CASCADE',
                'CASCADE'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Contracts cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Contracts cannot be reverted.


        return false;
    }
    */
}
