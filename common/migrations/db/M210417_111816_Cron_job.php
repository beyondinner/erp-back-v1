<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111816_Cron_job
 */
class M210417_111816_Cron_job extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'cron_job';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('cron_job',
            [
                'id_cron_job' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'controller' =>$this->string(255)->notNull(),
                'action' =>$this->string(255)->notNull(),
                'limit' =>$this->integer(10),
                'offset' =>$this->integer(10),
                'running' =>$this->smallInteger(5)->notNull(),
                'success' =>$this->smallInteger(5)->notNull(),
                'started_at' =>$this->integer(10),
                'ended_at' =>$this->integer(10),
                'last_execution_time' =>$this->float(12),
                 'PRIMARY KEY (`id_cron_job`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_cron_job'))
                $this->addColumn('cron_job', 'id_cron_job', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('controller'))
                $this->addColumn('cron_job', 'controller', $this->string(255)->notNull());
             else{

                $this->alterColumn('cron_job', 'controller', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL ');

                }
            if (!$exist_table->getColumn('action'))
                $this->addColumn('cron_job', 'action', $this->string(255)->notNull());
             else{

                $this->alterColumn('cron_job', 'action', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL ');

                }
            if (!$exist_table->getColumn('limit'))
                $this->addColumn('cron_job', 'limit', $this->integer(10));
             else{
                $this->alterColumn('cron_job', 'limit', $this->integer(10));
                }
            if (!$exist_table->getColumn('offset'))
                $this->addColumn('cron_job', 'offset', $this->integer(10));
             else{
                $this->alterColumn('cron_job', 'offset', $this->integer(10));
                }
            if (!$exist_table->getColumn('running'))
                $this->addColumn('cron_job', 'running', $this->smallInteger(5)->notNull());
             else{
                $this->alterColumn('cron_job', 'running', $this->smallInteger(5)->notNull());
                }
            if (!$exist_table->getColumn('success'))
                $this->addColumn('cron_job', 'success', $this->smallInteger(5)->notNull());
             else{
                $this->alterColumn('cron_job', 'success', $this->smallInteger(5)->notNull());
                }
            if (!$exist_table->getColumn('started_at'))
                $this->addColumn('cron_job', 'started_at', $this->integer(10));
             else{
                $this->alterColumn('cron_job', 'started_at', $this->integer(10));
                }
            if (!$exist_table->getColumn('ended_at'))
                $this->addColumn('cron_job', 'ended_at', $this->integer(10));
             else{
                $this->alterColumn('cron_job', 'ended_at', $this->integer(10));
                }
            if (!$exist_table->getColumn('last_execution_time'))
                $this->addColumn('cron_job', 'last_execution_time', $this->float(12));
             else{
                $this->alterColumn('cron_job', 'last_execution_time', $this->float(12));
                }
            }
        /*Generating index*/

        /*Generating foreignkey*/


    }

   public function down()
    {
        echo 'M210417_111812_Cron_job cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Cron_job cannot be reverted.


        return false;
    }
    */
}
