<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111825_Logs
 */
class M210417_111825_Logs extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'logs';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('logs',
            [
                'id_log' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'log_action' =>$this->string(255),
                'log_description' =>$this->string(255),
                'user_id' =>$this->integer(10)->notNull(),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'table' =>$this->string(100),
                'id_table' =>$this->integer(10),
                'name_user' =>$this->string(80),
                 'PRIMARY KEY (`id_log`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_log'))
                $this->addColumn('logs', 'id_log', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('log_action'))
                $this->addColumn('logs', 'log_action', $this->string(255));
             else{

                $this->alterColumn('logs', 'log_action', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('log_description'))
                $this->addColumn('logs', 'log_description', $this->string(255));
             else{

                $this->alterColumn('logs', 'log_description', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('user_id'))
                $this->addColumn('logs', 'user_id', $this->integer(10)->notNull());
             else{
                $this->alterColumn('logs', 'user_id', $this->integer(10)->notNull());
                }
            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('logs', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('logs', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('logs', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('logs', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('table'))
                $this->addColumn('logs', 'table', $this->string(100));
             else{

                $this->alterColumn('logs', 'table', 'VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('id_table'))
                $this->addColumn('logs', 'id_table', $this->integer(10));
             else{
                $this->alterColumn('logs', 'id_table', $this->integer(10));
                }
            if (!$exist_table->getColumn('name_user'))
                $this->addColumn('logs', 'name_user', $this->string(80));
             else{

                $this->alterColumn('logs', 'name_user', 'VARCHAR(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            }
        /*Generating index*/

        /*Generating foreignkey*/


    }

   public function down()
    {
        echo 'M210417_111812_Logs cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Logs cannot be reverted.


        return false;
    }
    */
}
