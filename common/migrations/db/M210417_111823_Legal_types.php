<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111823_Legal_types
 */
class M210417_111823_Legal_types extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'legal_types';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('legal_types',
            [
                'id_legal_type' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'legal_type_siglas' =>$this->string(255),
                'legal_type_desc' =>$this->string(255),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                 'PRIMARY KEY (`id_legal_type`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_legal_type'))
                $this->addColumn('legal_types', 'id_legal_type', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('legal_type_siglas'))
                $this->addColumn('legal_types', 'legal_type_siglas', $this->string(255));
             else{

                $this->alterColumn('legal_types', 'legal_type_siglas', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('legal_type_desc'))
                $this->addColumn('legal_types', 'legal_type_desc', $this->string(255));
             else{

                $this->alterColumn('legal_types', 'legal_type_desc', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('legal_types', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('legal_types', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('legal_types', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('legal_types', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('legal_type_desc', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'legal_type_desc',
                'legal_types',
                ['legal_type_desc'],
                true
            );
        if ($exist_table === null || !array_key_exists('legal_type_siglas', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'legal_type_siglas',
                'legal_types',
                ['legal_type_siglas'],
                true
            );
        /*Generating foreignkey*/


    }

   public function down()
    {
        echo 'M210417_111812_Legal_types cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Legal_types cannot be reverted.


        return false;
    }
    */
}
