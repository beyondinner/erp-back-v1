<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111904_Cant_transaction
 */
class M210417_111904_Cant_transaction extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->execute("DROP VIEW IF EXISTS cant_transaction CASCADE;");

        $this->execute("CREATE OR REPLACE VIEW cant_transaction(club_members_name,club_members_email,count_transaction,club_members_picture) as select `club_members`.`club_members_name` AS `club_members_name`,`club_members`.`club_members_email` AS `club_members_email`,count(`club_member_history`.`id_club_member_history`) AS `count_transaction`,`club_members`.`club_members_picture` AS `club_members_picture` from (`club_members` join `club_member_history` on((`club_members`.`id_club_members` = `club_member_history`.`club_member_id`))) group by `club_members`.`club_members_name`,`club_members`.`club_members_email`,`club_members`.`club_members_picture` order by count(`club_member_history`.`id_club_member_history`) desc");
    }

   public function down()
    {
        echo 'M210417_111812_Cant_transaction cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Cant_transaction cannot be reverted.


        return false;
    }
    */
}
