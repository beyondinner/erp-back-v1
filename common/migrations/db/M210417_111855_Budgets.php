<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111855_Budgets
 */
class M210417_111855_Budgets extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'budgets';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('budgets',
            [
                'id_budget' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'contact_id' =>$this->integer(10)->notNull(),
                'budget_doc_number' =>$this->string(255),
                'budget_start_date' =>$this->date()->notNull(),
                'budget_expiration_date' =>$this->date()->notNull(),
                'payment_type_id' =>$this->integer(10)->notNull(),
                'budget_internal_desc' =>$this->text(),
                'budget_status_id' =>$this->integer(10)->notNull(),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                 'PRIMARY KEY (`id_budget`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_budget'))
                $this->addColumn('budgets', 'id_budget', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('contact_id'))
                $this->addColumn('budgets', 'contact_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('budget_doc_number'))
                $this->addColumn('budgets', 'budget_doc_number', $this->string(255));
             else{

                $this->alterColumn('budgets', 'budget_doc_number', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('budget_start_date'))
                $this->addColumn('budgets', 'budget_start_date', $this->date()->notNull());
             else{
                $this->alterColumn('budgets', 'budget_start_date', $this->date()->notNull());
                }
            if (!$exist_table->getColumn('budget_expiration_date'))
                $this->addColumn('budgets', 'budget_expiration_date', $this->date()->notNull());
             else{
                $this->alterColumn('budgets', 'budget_expiration_date', $this->date()->notNull());
                }
            if (!$exist_table->getColumn('payment_type_id'))
                $this->addColumn('budgets', 'payment_type_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('budget_internal_desc'))
                $this->addColumn('budgets', 'budget_internal_desc', $this->text());
             else{
                $this->alterColumn('budgets', 'budget_internal_desc', $this->text());
                }
            if (!$exist_table->getColumn('budget_status_id'))
                $this->addColumn('budgets', 'budget_status_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('budgets', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('budgets', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('budgets', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('budgets', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            }
        /*Generating index*/

        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('budgets_fk',$exist_table->foreignKeys) || !array_key_exists('budget_status_id',$exist_table->foreignKeys['budgets_fk'])) 
            $this->addForeignKey(
                'budgets_fk',
                'budgets',
                'budget_status_id',
                'budget_status',
                'id_budget_status',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('budgets_fk','budgets' );
            $this->addForeignKey(
                'budgets_fk',
                'budgets',
                'budget_status_id',
                'budget_status',
                'id_budget_status',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('budgets_fk1',$exist_table->foreignKeys) || !array_key_exists('payment_type_id',$exist_table->foreignKeys['budgets_fk1'])) 
            $this->addForeignKey(
                'budgets_fk1',
                'budgets',
                'payment_type_id',
                'payment_types',
                'id_payment_type',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('budgets_fk1','budgets' );
            $this->addForeignKey(
                'budgets_fk1',
                'budgets',
                'payment_type_id',
                'payment_types',
                'id_payment_type',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('budgets_fk2',$exist_table->foreignKeys) || !array_key_exists('contact_id',$exist_table->foreignKeys['budgets_fk2'])) 
            $this->addForeignKey(
                'budgets_fk2',
                'budgets',
                'contact_id',
                'contacts',
                'id_contact',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('budgets_fk2','budgets' );
            $this->addForeignKey(
                'budgets_fk2',
                'budgets',
                'contact_id',
                'contacts',
                'id_contact',
                'CASCADE',
                'CASCADE'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Budgets cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Budgets cannot be reverted.


        return false;
    }
    */
}
