<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111853_User_action_access
 */
class M210417_111853_User_action_access extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'user_action_access';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('user_action_access',
            [
                'id_user_action_access' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'id_user' =>$this->integer(10)->notNull(),
                'id_actions' =>$this->integer(10)->notNull(),
                'range_user_action_access' =>$this->boolean()->notNull(),
                'enabled' =>$this->boolean()->notNull(),
                'start_range' =>$this->date()->notNull(),
                'end_range' =>$this->date()->notNull(),
                 'PRIMARY KEY (`id_user_action_access`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_user_action_access'))
                $this->addColumn('user_action_access', 'id_user_action_access', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('id_user'))
                $this->addColumn('user_action_access', 'id_user', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('id_actions'))
                $this->addColumn('user_action_access', 'id_actions', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('range_user_action_access'))
                $this->addColumn('user_action_access', 'range_user_action_access', $this->boolean()->notNull());
             else{
                $this->alterColumn('user_action_access', 'range_user_action_access', $this->boolean()->notNull());
                }
            if (!$exist_table->getColumn('enabled'))
                $this->addColumn('user_action_access', 'enabled', $this->boolean()->notNull());
             else{
                $this->alterColumn('user_action_access', 'enabled', $this->boolean()->notNull());
                }
            if (!$exist_table->getColumn('start_range'))
                $this->addColumn('user_action_access', 'start_range', $this->date()->notNull());
             else{
                $this->alterColumn('user_action_access', 'start_range', $this->date()->notNull());
                }
            if (!$exist_table->getColumn('end_range'))
                $this->addColumn('user_action_access', 'end_range', $this->date()->notNull());
             else{
                $this->alterColumn('user_action_access', 'end_range', $this->date()->notNull());
                }
            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('id_user', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'id_user',
                'user_action_access',
                ['id_user','id_actions'],
                true
            );
        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('Refactions16',$exist_table->foreignKeys) || !array_key_exists('id_actions',$exist_table->foreignKeys['Refactions16'])) 
            $this->addForeignKey(
                'Refactions16',
                'user_action_access',
                'id_actions',
                'actions',
                'id_actions',
                'NO ACTION',
                'NO ACTION'
            );
           else {
            $this->dropForeignKey('Refactions16','user_action_access' );
            $this->addForeignKey(
                'Refactions16',
                'user_action_access',
                'id_actions',
                'actions',
                'id_actions',
                'NO ACTION',
                'NO ACTION'
            );
           }
        if ($exist_table === null || !array_key_exists('Refusers15',$exist_table->foreignKeys) || !array_key_exists('id_user',$exist_table->foreignKeys['Refusers15'])) 
            $this->addForeignKey(
                'Refusers15',
                'user_action_access',
                'id_user',
                'users',
                'id_user',
                'NO ACTION',
                'NO ACTION'
            );
           else {
            $this->dropForeignKey('Refusers15','user_action_access' );
            $this->addForeignKey(
                'Refusers15',
                'user_action_access',
                'id_user',
                'users',
                'id_user',
                'NO ACTION',
                'NO ACTION'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_User_action_access cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_User_action_access cannot be reverted.


        return false;
    }
    */
}
