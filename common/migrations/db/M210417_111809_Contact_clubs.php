<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111809_Contact_clubs
 */
class M210417_111809_Contact_clubs extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'contact_clubs';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('contact_clubs',
            [
                'id' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'full_name' =>$this->string(191),
                'birthday' =>$this->string(191),
                'phone' =>$this->string(191),
                'email' =>$this->string(191),
                'pass' =>$this->string(191),
                'dni' =>$this->string(191),
                'address' =>$this->string(191),
                'state' =>$this->smallInteger(5)->notNull(),
                'coins' =>$this->integer(10)->notNull(),
                'code' =>$this->string(191),
                'asesor' =>$this->string(191),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'unique_identification' =>$this->integer(10)->notNull(),
                 'PRIMARY KEY (`id`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id'))
                $this->addColumn('contact_clubs', 'id', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('full_name'))
                $this->addColumn('contact_clubs', 'full_name', $this->string(191));
             else{

                $this->alterColumn('contact_clubs', 'full_name', 'VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('birthday'))
                $this->addColumn('contact_clubs', 'birthday', $this->string(191));
             else{

                $this->alterColumn('contact_clubs', 'birthday', 'VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('phone'))
                $this->addColumn('contact_clubs', 'phone', $this->string(191));
             else{

                $this->alterColumn('contact_clubs', 'phone', 'VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('email'))
                $this->addColumn('contact_clubs', 'email', $this->string(191));
             else{

                $this->alterColumn('contact_clubs', 'email', 'VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('pass'))
                $this->addColumn('contact_clubs', 'pass', $this->string(191));
             else{

                $this->alterColumn('contact_clubs', 'pass', 'VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('dni'))
                $this->addColumn('contact_clubs', 'dni', $this->string(191));
             else{

                $this->alterColumn('contact_clubs', 'dni', 'VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('address'))
                $this->addColumn('contact_clubs', 'address', $this->string(191));
             else{

                $this->alterColumn('contact_clubs', 'address', 'VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('state'))
                $this->addColumn('contact_clubs', 'state', $this->smallInteger(5)->notNull());
             else{
                $this->alterColumn('contact_clubs', 'state', $this->smallInteger(5)->notNull());
                }
            if (!$exist_table->getColumn('coins'))
                $this->addColumn('contact_clubs', 'coins', $this->integer(10)->notNull());
             else{
                $this->alterColumn('contact_clubs', 'coins', $this->integer(10)->notNull());
                }
            if (!$exist_table->getColumn('code'))
                $this->addColumn('contact_clubs', 'code', $this->string(191));
             else{

                $this->alterColumn('contact_clubs', 'code', 'VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('asesor'))
                $this->addColumn('contact_clubs', 'asesor', $this->string(191));
             else{

                $this->alterColumn('contact_clubs', 'asesor', 'VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('contact_clubs', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('contact_clubs', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('contact_clubs', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('contact_clubs', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('unique_identification'))
                $this->addColumn('contact_clubs', 'unique_identification', $this->integer(10)->notNull());
             else{
                $this->alterColumn('contact_clubs', 'unique_identification', $this->integer(10)->notNull());
                }
            }
        /*Generating index*/

        /*Generating foreignkey*/


    }

   public function down()
    {
        echo 'M210417_111812_Contact_clubs cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Contact_clubs cannot be reverted.


        return false;
    }
    */
}
