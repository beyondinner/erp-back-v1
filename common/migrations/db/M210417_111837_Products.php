<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111837_Products
 */
class M210417_111837_Products extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'products';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('products',
            [
                'id_product' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'product_name' =>$this->string(255),
                'product_desc' =>$this->text(),
                'contact_id' =>$this->integer(10),
                'product_tax_buy' =>$this->double(22)->notNull(),
                'product_cost' =>$this->double(22)->notNull(),
                'product_subtotal' =>$this->double(22)->notNull(),
                'product_tax_sale' =>$this->double(22)->notNull(),
                'product_total' =>$this->double(22)->notNull(),
                'product_stock' =>$this->double(22)->notNull(),
                'product_sku' =>$this->string(255),
                'product_bar_code' =>$this->string(255),
                'product_factory_code' =>$this->string(255),
                'product_weigth' =>$this->double(22)->notNull(),
                'product_status_id' =>$this->integer(10)->notNull(),
                'product_picture' =>$this->string(255),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                 'PRIMARY KEY (`id_product`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_product'))
                $this->addColumn('products', 'id_product', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('product_name'))
                $this->addColumn('products', 'product_name', $this->string(255));
             else{

                $this->alterColumn('products', 'product_name', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('product_desc'))
                $this->addColumn('products', 'product_desc', $this->text());
             else{
                $this->alterColumn('products', 'product_desc', $this->text());
                }
            if (!$exist_table->getColumn('contact_id'))
                $this->addColumn('products', 'contact_id', $this->integer(10));
             else{
                $this->alterColumn('products', 'contact_id', $this->integer(10));
                }
            if (!$exist_table->getColumn('product_tax_buy'))
                $this->addColumn('products', 'product_tax_buy', $this->double(22)->notNull());
             else{
                $this->alterColumn('products', 'product_tax_buy', $this->double(22)->notNull());
                }
            if (!$exist_table->getColumn('product_cost'))
                $this->addColumn('products', 'product_cost', $this->double(22)->notNull());
             else{
                $this->alterColumn('products', 'product_cost', $this->double(22)->notNull());
                }
            if (!$exist_table->getColumn('product_subtotal'))
                $this->addColumn('products', 'product_subtotal', $this->double(22)->notNull());
             else{
                $this->alterColumn('products', 'product_subtotal', $this->double(22)->notNull());
                }
            if (!$exist_table->getColumn('product_tax_sale'))
                $this->addColumn('products', 'product_tax_sale', $this->double(22)->notNull());
             else{
                $this->alterColumn('products', 'product_tax_sale', $this->double(22)->notNull());
                }
            if (!$exist_table->getColumn('product_total'))
                $this->addColumn('products', 'product_total', $this->double(22)->notNull());
             else{
                $this->alterColumn('products', 'product_total', $this->double(22)->notNull());
                }
            if (!$exist_table->getColumn('product_stock'))
                $this->addColumn('products', 'product_stock', $this->double(22)->notNull());
             else{
                $this->alterColumn('products', 'product_stock', $this->double(22)->notNull());
                }
            if (!$exist_table->getColumn('product_sku'))
                $this->addColumn('products', 'product_sku', $this->string(255));
             else{

                $this->alterColumn('products', 'product_sku', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('product_bar_code'))
                $this->addColumn('products', 'product_bar_code', $this->string(255));
             else{

                $this->alterColumn('products', 'product_bar_code', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('product_factory_code'))
                $this->addColumn('products', 'product_factory_code', $this->string(255));
             else{

                $this->alterColumn('products', 'product_factory_code', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('product_weigth'))
                $this->addColumn('products', 'product_weigth', $this->double(22)->notNull());
             else{
                $this->alterColumn('products', 'product_weigth', $this->double(22)->notNull());
                }
            if (!$exist_table->getColumn('product_status_id'))
                $this->addColumn('products', 'product_status_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('product_picture'))
                $this->addColumn('products', 'product_picture', $this->string(255));
             else{

                $this->alterColumn('products', 'product_picture', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('products', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('products', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('products', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('products', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('key', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'key',
                'products',
                ['product_name','contact_id'],
                true
            );
        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('products_fk1',$exist_table->foreignKeys) || !array_key_exists('product_status_id',$exist_table->foreignKeys['products_fk1'])) 
            $this->addForeignKey(
                'products_fk1',
                'products',
                'product_status_id',
                'product_status',
                'id_product_status',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('products_fk1','products' );
            $this->addForeignKey(
                'products_fk1',
                'products',
                'product_status_id',
                'product_status',
                'id_product_status',
                'CASCADE',
                'CASCADE'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Products cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Products cannot be reverted.


        return false;
    }
    */
}
