<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111901_Budget_items_list
 */
class M210417_111901_Budget_items_list extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'budget_items_list';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('budget_items_list',
            [
                'id_budget_items_list' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'budget_id' =>$this->integer(10)->notNull(),
                'budget_items_list_concept' =>$this->string(255),
                'budget_items_list_desc' =>$this->string(255),
                'budget_items_list_quantity' =>$this->integer(10)->notNull(),
                'tax_id' =>$this->integer(10)->notNull(),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'budget_items_list_price' =>$this->float(12)->notNull(),
                'id_product' =>$this->integer(10),
                'id_service' =>$this->integer(10),
                 'PRIMARY KEY (`id_budget_items_list`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_budget_items_list'))
                $this->addColumn('budget_items_list', 'id_budget_items_list', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('budget_id'))
                $this->addColumn('budget_items_list', 'budget_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('budget_items_list_concept'))
                $this->addColumn('budget_items_list', 'budget_items_list_concept', $this->string(255));
             else{

                $this->alterColumn('budget_items_list', 'budget_items_list_concept', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('budget_items_list_desc'))
                $this->addColumn('budget_items_list', 'budget_items_list_desc', $this->string(255));
             else{

                $this->alterColumn('budget_items_list', 'budget_items_list_desc', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('budget_items_list_quantity'))
                $this->addColumn('budget_items_list', 'budget_items_list_quantity', $this->integer(10)->notNull());
             else{
                $this->alterColumn('budget_items_list', 'budget_items_list_quantity', $this->integer(10)->notNull());
                }
            if (!$exist_table->getColumn('tax_id'))
                $this->addColumn('budget_items_list', 'tax_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('budget_items_list', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('budget_items_list', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('budget_items_list', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('budget_items_list', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('budget_items_list_price'))
                $this->addColumn('budget_items_list', 'budget_items_list_price', $this->float(12)->notNull());
             else{
                $this->alterColumn('budget_items_list', 'budget_items_list_price', $this->float(12)->notNull());
                }
            if (!$exist_table->getColumn('id_product'))
                $this->addColumn('budget_items_list', 'id_product', $this->integer(10));

            if (!$exist_table->getColumn('id_service'))
                $this->addColumn('budget_items_list', 'id_service', $this->integer(10));

            }
        /*Generating index*/

        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('budget_items_list_fk',$exist_table->foreignKeys) || !array_key_exists('tax_id',$exist_table->foreignKeys['budget_items_list_fk'])) 
            $this->addForeignKey(
                'budget_items_list_fk',
                'budget_items_list',
                'tax_id',
                'budget_items_list_taxes',
                'id_budget_items_list_taxes',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('budget_items_list_fk','budget_items_list' );
            $this->addForeignKey(
                'budget_items_list_fk',
                'budget_items_list',
                'tax_id',
                'budget_items_list_taxes',
                'id_budget_items_list_taxes',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('budget_items_list_fk1',$exist_table->foreignKeys) || !array_key_exists('budget_id',$exist_table->foreignKeys['budget_items_list_fk1'])) 
            $this->addForeignKey(
                'budget_items_list_fk1',
                'budget_items_list',
                'budget_id',
                'budgets',
                'id_budget',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('budget_items_list_fk1','budget_items_list' );
            $this->addForeignKey(
                'budget_items_list_fk1',
                'budget_items_list',
                'budget_id',
                'budgets',
                'id_budget',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('budget_items_list_fk2',$exist_table->foreignKeys) || !array_key_exists('id_product',$exist_table->foreignKeys['budget_items_list_fk2'])) 
            $this->addForeignKey(
                'budget_items_list_fk2',
                'budget_items_list',
                'id_product',
                'products',
                'id_product',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('budget_items_list_fk2','budget_items_list' );
            $this->addForeignKey(
                'budget_items_list_fk2',
                'budget_items_list',
                'id_product',
                'products',
                'id_product',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('budget_items_list_fk3',$exist_table->foreignKeys) || !array_key_exists('id_service',$exist_table->foreignKeys['budget_items_list_fk3'])) 
            $this->addForeignKey(
                'budget_items_list_fk3',
                'budget_items_list',
                'id_service',
                'services',
                'id_service',
                'NO ACTION',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('budget_items_list_fk3','budget_items_list' );
            $this->addForeignKey(
                'budget_items_list_fk3',
                'budget_items_list',
                'id_service',
                'services',
                'id_service',
                'NO ACTION',
                'CASCADE'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Budget_items_list cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Budget_items_list cannot be reverted.


        return false;
    }
    */
}
