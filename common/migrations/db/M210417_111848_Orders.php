<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111848_Orders
 */
class M210417_111848_Orders extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'orders';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('orders',
            [
                'id_orders' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'orders_code' =>$this->string(20)->notNull(),
                'date_at' =>$this->dateTime()->append(' NULL'),
                'id_contact' =>$this->integer(10),
                'id_user' =>$this->integer(10),
                'id_order_status' =>$this->integer(10)->notNull(),
                'created_at' =>$this->dateTime()->append(' NULL'),
                'updated_at' =>$this->dateTime()->append(' NULL'),
                'order_description' =>$this->text(),
                 'PRIMARY KEY (`id_orders`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_orders'))
                $this->addColumn('orders', 'id_orders', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('orders_code'))
                $this->addColumn('orders', 'orders_code', $this->string(20)->notNull());
             else{

                $this->alterColumn('orders', 'orders_code', 'VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL ');

                }
            if (!$exist_table->getColumn('date_at'))
                $this->addColumn('orders', 'date_at', $this->dateTime()->append(' NULL'));
             else{
                $this->alterColumn('orders', 'date_at', $this->dateTime()->append(' NULL'));
                }
            if (!$exist_table->getColumn('id_contact'))
                $this->addColumn('orders', 'id_contact', $this->integer(10));

            if (!$exist_table->getColumn('id_user'))
                $this->addColumn('orders', 'id_user', $this->integer(10));

            if (!$exist_table->getColumn('id_order_status'))
                $this->addColumn('orders', 'id_order_status', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('orders', 'created_at', $this->dateTime()->append(' NULL'));
             else{
                $this->alterColumn('orders', 'created_at', $this->dateTime()->append(' NULL'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('orders', 'updated_at', $this->dateTime()->append(' NULL'));
             else{
                $this->alterColumn('orders', 'updated_at', $this->dateTime()->append(' NULL'));
                }
            if (!$exist_table->getColumn('order_description'))
                $this->addColumn('orders', 'order_description', $this->text());
             else{
                $this->alterColumn('orders', 'order_description', $this->text());
                }
            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('orders_code', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'orders_code',
                'orders',
                ['orders_code'],
                true
            );
        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('orders_fk',$exist_table->foreignKeys) || !array_key_exists('id_contact',$exist_table->foreignKeys['orders_fk'])) 
            $this->addForeignKey(
                'orders_fk',
                'orders',
                'id_contact',
                'contacts',
                'id_contact',
                'NO ACTION',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('orders_fk','orders' );
            $this->addForeignKey(
                'orders_fk',
                'orders',
                'id_contact',
                'contacts',
                'id_contact',
                'NO ACTION',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('orders_fk1',$exist_table->foreignKeys) || !array_key_exists('id_user',$exist_table->foreignKeys['orders_fk1'])) 
            $this->addForeignKey(
                'orders_fk1',
                'orders',
                'id_user',
                'users',
                'id_user',
                'NO ACTION',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('orders_fk1','orders' );
            $this->addForeignKey(
                'orders_fk1',
                'orders',
                'id_user',
                'users',
                'id_user',
                'NO ACTION',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('orders_fk2',$exist_table->foreignKeys) || !array_key_exists('id_order_status',$exist_table->foreignKeys['orders_fk2'])) 
            $this->addForeignKey(
                'orders_fk2',
                'orders',
                'id_order_status',
                'order_status',
                'id_order_status',
                'NO ACTION',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('orders_fk2','orders' );
            $this->addForeignKey(
                'orders_fk2',
                'orders',
                'id_order_status',
                'order_status',
                'id_order_status',
                'NO ACTION',
                'CASCADE'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Orders cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Orders cannot be reverted.


        return false;
    }
    */
}
