<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111900_Billings
 */
class M210417_111900_Billings extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'billings';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('billings',
            [
                'id_billings' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'billings_doc_number' =>$this->string(255),
                'budget_id' =>$this->integer(10),
                'billings_status_id' =>$this->integer(10)->notNull(),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'id_contact' =>$this->integer(10),
                'date_at' =>$this->dateTime()->notNull()->append(' NULL'),
                'expiration_date' =>$this->dateTime()->notNull()->append(' NULL'),
                'id_payment_type' =>$this->integer(10),
                'billing_description' =>$this->text(),
                'billing_types' =>$this->char(20),
                 'PRIMARY KEY (`id_billings`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_billings'))
                $this->addColumn('billings', 'id_billings', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('billings_doc_number'))
                $this->addColumn('billings', 'billings_doc_number', $this->string(255));
             else{

                $this->alterColumn('billings', 'billings_doc_number', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('budget_id'))
                $this->addColumn('billings', 'budget_id', $this->integer(10));

            if (!$exist_table->getColumn('billings_status_id'))
                $this->addColumn('billings', 'billings_status_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('billings', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('billings', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('billings', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('billings', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('id_contact'))
                $this->addColumn('billings', 'id_contact', $this->integer(10));

            if (!$exist_table->getColumn('date_at'))
                $this->addColumn('billings', 'date_at', $this->dateTime()->notNull()->append(' NULL'));
             else{
                $this->alterColumn('billings', 'date_at', $this->dateTime()->notNull()->append(' NULL'));
                }
            if (!$exist_table->getColumn('expiration_date'))
                $this->addColumn('billings', 'expiration_date', $this->dateTime()->notNull()->append(' NULL'));
             else{
                $this->alterColumn('billings', 'expiration_date', $this->dateTime()->notNull()->append(' NULL'));
                }
            if (!$exist_table->getColumn('id_payment_type'))
                $this->addColumn('billings', 'id_payment_type', $this->integer(10));

            if (!$exist_table->getColumn('billing_description'))
                $this->addColumn('billings', 'billing_description', $this->text());
             else{
                $this->alterColumn('billings', 'billing_description', $this->text());
                }
            if (!$exist_table->getColumn('billing_types'))
                $this->addColumn('billings', 'billing_types', $this->char(20));
             else{

                $this->alterColumn('billings', 'billing_types', 'CHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('billings_doc_number', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'billings_doc_number',
                'billings',
                ['billings_doc_number'],
                true
            );
        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('billings_fk',$exist_table->foreignKeys) || !array_key_exists('budget_id',$exist_table->foreignKeys['billings_fk'])) 
            $this->addForeignKey(
                'billings_fk',
                'billings',
                'budget_id',
                'budgets',
                'id_budget',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('billings_fk','billings' );
            $this->addForeignKey(
                'billings_fk',
                'billings',
                'budget_id',
                'budgets',
                'id_budget',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('billings_fk1',$exist_table->foreignKeys) || !array_key_exists('billings_status_id',$exist_table->foreignKeys['billings_fk1'])) 
            $this->addForeignKey(
                'billings_fk1',
                'billings',
                'billings_status_id',
                'billings_status',
                'id_billings_status',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('billings_fk1','billings' );
            $this->addForeignKey(
                'billings_fk1',
                'billings',
                'billings_status_id',
                'billings_status',
                'id_billings_status',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('billings_fk2',$exist_table->foreignKeys) || !array_key_exists('id_contact',$exist_table->foreignKeys['billings_fk2'])) 
            $this->addForeignKey(
                'billings_fk2',
                'billings',
                'id_contact',
                'contacts',
                'id_contact',
                'NO ACTION',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('billings_fk2','billings' );
            $this->addForeignKey(
                'billings_fk2',
                'billings',
                'id_contact',
                'contacts',
                'id_contact',
                'NO ACTION',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('billings_fk3',$exist_table->foreignKeys) || !array_key_exists('id_payment_type',$exist_table->foreignKeys['billings_fk3'])) 
            $this->addForeignKey(
                'billings_fk3',
                'billings',
                'id_payment_type',
                'payment_types',
                'id_payment_type',
                'NO ACTION',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('billings_fk3','billings' );
            $this->addForeignKey(
                'billings_fk3',
                'billings',
                'id_payment_type',
                'payment_types',
                'id_payment_type',
                'NO ACTION',
                'CASCADE'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Billings cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Billings cannot be reverted.


        return false;
    }
    */
}
