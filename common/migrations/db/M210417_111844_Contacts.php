<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111844_Contacts
 */
class M210417_111844_Contacts extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'contacts';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('contacts',
            [
                'id_contact' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'contact_legal_type_id' =>$this->integer(10)->notNull(),
                'contact_full_name' =>$this->string(255),
                'contact_commercial_name' =>$this->string(255),
                'contact_type_id' =>$this->integer(10)->notNull(),
                'contact_email' =>$this->string(255),
                'contact_phone' =>$this->string(255),
                'contact_cellphone' =>$this->string(255),
                'contact_website' =>$this->string(255),
                'contact_address' =>$this->string(255),
                'contact_postal_code' =>$this->string(255),
                'country_id' =>$this->integer(10),
                'state_id' =>$this->integer(10),
                'contact_internal_reference' =>$this->text(),
                'language_id' =>$this->integer(10)->notNull(),
                'currency_id' =>$this->integer(10)->notNull(),
                'payment_type_id' =>$this->integer(10)->notNull(),
                'contact_status_id' =>$this->integer(10)->notNull(),
                'contact_picture' =>$this->string(255),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'fiscal_name' =>$this->string(20),
                'wallet' =>$this->integer(10),
                'club_member_id' =>$this->integer(10),
                'source' =>$this->string(20),
                 'PRIMARY KEY (`id_contact`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_contact'))
                $this->addColumn('contacts', 'id_contact', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('contact_legal_type_id'))
                $this->addColumn('contacts', 'contact_legal_type_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('contact_full_name'))
                $this->addColumn('contacts', 'contact_full_name', $this->string(255));
             else{

                $this->alterColumn('contacts', 'contact_full_name', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('contact_commercial_name'))
                $this->addColumn('contacts', 'contact_commercial_name', $this->string(255));
             else{

                $this->alterColumn('contacts', 'contact_commercial_name', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('contact_type_id'))
                $this->addColumn('contacts', 'contact_type_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('contact_email'))
                $this->addColumn('contacts', 'contact_email', $this->string(255));
             else{

                $this->alterColumn('contacts', 'contact_email', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('contact_phone'))
                $this->addColumn('contacts', 'contact_phone', $this->string(255));
             else{

                $this->alterColumn('contacts', 'contact_phone', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('contact_cellphone'))
                $this->addColumn('contacts', 'contact_cellphone', $this->string(255));
             else{

                $this->alterColumn('contacts', 'contact_cellphone', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('contact_website'))
                $this->addColumn('contacts', 'contact_website', $this->string(255));
             else{

                $this->alterColumn('contacts', 'contact_website', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('contact_address'))
                $this->addColumn('contacts', 'contact_address', $this->string(255));
             else{

                $this->alterColumn('contacts', 'contact_address', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('contact_postal_code'))
                $this->addColumn('contacts', 'contact_postal_code', $this->string(255));
             else{

                $this->alterColumn('contacts', 'contact_postal_code', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('country_id'))
                $this->addColumn('contacts', 'country_id', $this->integer(10));

            if (!$exist_table->getColumn('state_id'))
                $this->addColumn('contacts', 'state_id', $this->integer(10));

            if (!$exist_table->getColumn('contact_internal_reference'))
                $this->addColumn('contacts', 'contact_internal_reference', $this->text());
             else{
                $this->alterColumn('contacts', 'contact_internal_reference', $this->text());
                }
            if (!$exist_table->getColumn('language_id'))
                $this->addColumn('contacts', 'language_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('currency_id'))
                $this->addColumn('contacts', 'currency_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('payment_type_id'))
                $this->addColumn('contacts', 'payment_type_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('contact_status_id'))
                $this->addColumn('contacts', 'contact_status_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('contact_picture'))
                $this->addColumn('contacts', 'contact_picture', $this->string(255));
             else{

                $this->alterColumn('contacts', 'contact_picture', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('contacts', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('contacts', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('contacts', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('contacts', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('fiscal_name'))
                $this->addColumn('contacts', 'fiscal_name', $this->string(20));
             else{

                $this->alterColumn('contacts', 'fiscal_name', 'VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('wallet'))
                $this->addColumn('contacts', 'wallet', $this->integer(10));
             else{
                $this->alterColumn('contacts', 'wallet', $this->integer(10));
                }
            if (!$exist_table->getColumn('club_member_id'))
                $this->addColumn('contacts', 'club_member_id', $this->integer(10));

            if (!$exist_table->getColumn('source'))
                $this->addColumn('contacts', 'source', $this->string(20));
             else{

                $this->alterColumn('contacts', 'source', 'VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('contact_email', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'contact_email',
                'contacts',
                ['contact_email'],
                true
            );
        if ($exist_table === null || !array_key_exists('contact_website', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'contact_website',
                'contacts',
                ['contact_website'],
                true
            );
        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('contacts_fk',$exist_table->foreignKeys) || !array_key_exists('contact_type_id',$exist_table->foreignKeys['contacts_fk'])) 
            $this->addForeignKey(
                'contacts_fk',
                'contacts',
                'contact_type_id',
                'contact_types',
                'id_contact_type',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('contacts_fk','contacts' );
            $this->addForeignKey(
                'contacts_fk',
                'contacts',
                'contact_type_id',
                'contact_types',
                'id_contact_type',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('contacts_fk1',$exist_table->foreignKeys) || !array_key_exists('contact_legal_type_id',$exist_table->foreignKeys['contacts_fk1'])) 
            $this->addForeignKey(
                'contacts_fk1',
                'contacts',
                'contact_legal_type_id',
                'legal_types',
                'id_legal_type',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('contacts_fk1','contacts' );
            $this->addForeignKey(
                'contacts_fk1',
                'contacts',
                'contact_legal_type_id',
                'legal_types',
                'id_legal_type',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('contacts_fk2',$exist_table->foreignKeys) || !array_key_exists('country_id',$exist_table->foreignKeys['contacts_fk2'])) 
            $this->addForeignKey(
                'contacts_fk2',
                'contacts',
                'country_id',
                'countries',
                'id_country',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('contacts_fk2','contacts' );
            $this->addForeignKey(
                'contacts_fk2',
                'contacts',
                'country_id',
                'countries',
                'id_country',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('contacts_fk3',$exist_table->foreignKeys) || !array_key_exists('state_id',$exist_table->foreignKeys['contacts_fk3'])) 
            $this->addForeignKey(
                'contacts_fk3',
                'contacts',
                'state_id',
                'states',
                'id_state',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('contacts_fk3','contacts' );
            $this->addForeignKey(
                'contacts_fk3',
                'contacts',
                'state_id',
                'states',
                'id_state',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('contacts_fk4',$exist_table->foreignKeys) || !array_key_exists('language_id',$exist_table->foreignKeys['contacts_fk4'])) 
            $this->addForeignKey(
                'contacts_fk4',
                'contacts',
                'language_id',
                'languages',
                'id_language',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('contacts_fk4','contacts' );
            $this->addForeignKey(
                'contacts_fk4',
                'contacts',
                'language_id',
                'languages',
                'id_language',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('contacts_fk5',$exist_table->foreignKeys) || !array_key_exists('currency_id',$exist_table->foreignKeys['contacts_fk5'])) 
            $this->addForeignKey(
                'contacts_fk5',
                'contacts',
                'currency_id',
                'currencies',
                'id_currency',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('contacts_fk5','contacts' );
            $this->addForeignKey(
                'contacts_fk5',
                'contacts',
                'currency_id',
                'currencies',
                'id_currency',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('contacts_fk6',$exist_table->foreignKeys) || !array_key_exists('payment_type_id',$exist_table->foreignKeys['contacts_fk6'])) 
            $this->addForeignKey(
                'contacts_fk6',
                'contacts',
                'payment_type_id',
                'payment_types',
                'id_payment_type',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('contacts_fk6','contacts' );
            $this->addForeignKey(
                'contacts_fk6',
                'contacts',
                'payment_type_id',
                'payment_types',
                'id_payment_type',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('contacts_fk7',$exist_table->foreignKeys) || !array_key_exists('contact_status_id',$exist_table->foreignKeys['contacts_fk7'])) 
            $this->addForeignKey(
                'contacts_fk7',
                'contacts',
                'contact_status_id',
                'contact_status',
                'id_contact_status',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('contacts_fk7','contacts' );
            $this->addForeignKey(
                'contacts_fk7',
                'contacts',
                'contact_status_id',
                'contact_status',
                'id_contact_status',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('contacts_fk8',$exist_table->foreignKeys) || !array_key_exists('club_member_id',$exist_table->foreignKeys['contacts_fk8'])) 
            $this->addForeignKey(
                'contacts_fk8',
                'contacts',
                'club_member_id',
                'club_members',
                'id_club_members',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('contacts_fk8','contacts' );
            $this->addForeignKey(
                'contacts_fk8',
                'contacts',
                'club_member_id',
                'club_members',
                'id_club_members',
                'CASCADE',
                'CASCADE'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Contacts cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Contacts cannot be reverted.


        return false;
    }
    */
}
