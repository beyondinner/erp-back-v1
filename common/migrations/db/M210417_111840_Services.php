<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111840_Services
 */
class M210417_111840_Services extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'services';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('services',
            [
                'id_service' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'service_name' =>$this->string(255),
                'service_code' =>$this->string(255),
                'service_desc' =>$this->text(),
                'service_cost' =>$this->double(22)->notNull(),
                'service_subtotal' =>$this->double(22)->notNull(),
                'service_tax_sale' =>$this->double(22)->notNull(),
                'service_total' =>$this->double(22)->notNull(),
                'service_picture' =>$this->string(255),
                'service_status_id' =>$this->integer(10)->notNull(),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                 'PRIMARY KEY (`id_service`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_service'))
                $this->addColumn('services', 'id_service', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('service_name'))
                $this->addColumn('services', 'service_name', $this->string(255));
             else{

                $this->alterColumn('services', 'service_name', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('service_code'))
                $this->addColumn('services', 'service_code', $this->string(255));
             else{

                $this->alterColumn('services', 'service_code', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('service_desc'))
                $this->addColumn('services', 'service_desc', $this->text());
             else{
                $this->alterColumn('services', 'service_desc', $this->text());
                }
            if (!$exist_table->getColumn('service_cost'))
                $this->addColumn('services', 'service_cost', $this->double(22)->notNull());
             else{
                $this->alterColumn('services', 'service_cost', $this->double(22)->notNull());
                }
            if (!$exist_table->getColumn('service_subtotal'))
                $this->addColumn('services', 'service_subtotal', $this->double(22)->notNull());
             else{
                $this->alterColumn('services', 'service_subtotal', $this->double(22)->notNull());
                }
            if (!$exist_table->getColumn('service_tax_sale'))
                $this->addColumn('services', 'service_tax_sale', $this->double(22)->notNull());
             else{
                $this->alterColumn('services', 'service_tax_sale', $this->double(22)->notNull());
                }
            if (!$exist_table->getColumn('service_total'))
                $this->addColumn('services', 'service_total', $this->double(22)->notNull());
             else{
                $this->alterColumn('services', 'service_total', $this->double(22)->notNull());
                }
            if (!$exist_table->getColumn('service_picture'))
                $this->addColumn('services', 'service_picture', $this->string(255));
             else{

                $this->alterColumn('services', 'service_picture', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('service_status_id'))
                $this->addColumn('services', 'service_status_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('services', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('services', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('services', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('services', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('service_name', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'service_name',
                'services',
                ['service_name'],
                true
            );
        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('services_fk',$exist_table->foreignKeys) || !array_key_exists('service_status_id',$exist_table->foreignKeys['services_fk'])) 
            $this->addForeignKey(
                'services_fk',
                'services',
                'service_status_id',
                'service_status',
                'id_service_status',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('services_fk','services' );
            $this->addForeignKey(
                'services_fk',
                'services',
                'service_status_id',
                'service_status',
                'id_service_status',
                'CASCADE',
                'CASCADE'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Services cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Services cannot be reverted.


        return false;
    }
    */
}
