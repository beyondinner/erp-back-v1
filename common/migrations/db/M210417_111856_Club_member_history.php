<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111856_Club_member_history
 */
class M210417_111856_Club_member_history extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'club_member_history';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('club_member_history',
            [
                'id_club_member_history' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'club_member_id' =>$this->integer(10)->notNull(),
                'product_id' =>$this->integer(10),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'ammount' =>$this->float(12)->notNull(),
                'services_id' =>$this->integer(10),
                'history_description' =>$this->text(),
                'id_amount_club_member' =>$this->integer(10),
                'cant_element' =>$this->integer(10)->notNull(),
                 'PRIMARY KEY (`id_club_member_history`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_club_member_history'))
                $this->addColumn('club_member_history', 'id_club_member_history', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('club_member_id'))
                $this->addColumn('club_member_history', 'club_member_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('product_id'))
                $this->addColumn('club_member_history', 'product_id', $this->integer(10));

            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('club_member_history', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('club_member_history', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('club_member_history', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('club_member_history', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('ammount'))
                $this->addColumn('club_member_history', 'ammount', $this->float(12)->notNull());
             else{
                $this->alterColumn('club_member_history', 'ammount', $this->float(12)->notNull());
                }
            if (!$exist_table->getColumn('services_id'))
                $this->addColumn('club_member_history', 'services_id', $this->integer(10));

            if (!$exist_table->getColumn('history_description'))
                $this->addColumn('club_member_history', 'history_description', $this->text());
             else{
                $this->alterColumn('club_member_history', 'history_description', $this->text());
                }
            if (!$exist_table->getColumn('id_amount_club_member'))
                $this->addColumn('club_member_history', 'id_amount_club_member', $this->integer(10));

            if (!$exist_table->getColumn('cant_element'))
                $this->addColumn('club_member_history', 'cant_element', $this->integer(10)->notNull());
             else{
                $this->alterColumn('club_member_history', 'cant_element', $this->integer(10)->notNull());
                }
            }
        /*Generating index*/

        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('club_member_history_fk',$exist_table->foreignKeys) || !array_key_exists('club_member_id',$exist_table->foreignKeys['club_member_history_fk'])) 
            $this->addForeignKey(
                'club_member_history_fk',
                'club_member_history',
                'club_member_id',
                'club_members',
                'id_club_members',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('club_member_history_fk','club_member_history' );
            $this->addForeignKey(
                'club_member_history_fk',
                'club_member_history',
                'club_member_id',
                'club_members',
                'id_club_members',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('club_member_history_fk1',$exist_table->foreignKeys) || !array_key_exists('product_id',$exist_table->foreignKeys['club_member_history_fk1'])) 
            $this->addForeignKey(
                'club_member_history_fk1',
                'club_member_history',
                'product_id',
                'products',
                'id_product',
                'NO ACTION',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('club_member_history_fk1','club_member_history' );
            $this->addForeignKey(
                'club_member_history_fk1',
                'club_member_history',
                'product_id',
                'products',
                'id_product',
                'NO ACTION',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('club_member_history_fk2',$exist_table->foreignKeys) || !array_key_exists('services_id',$exist_table->foreignKeys['club_member_history_fk2'])) 
            $this->addForeignKey(
                'club_member_history_fk2',
                'club_member_history',
                'services_id',
                'services',
                'id_service',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('club_member_history_fk2','club_member_history' );
            $this->addForeignKey(
                'club_member_history_fk2',
                'club_member_history',
                'services_id',
                'services',
                'id_service',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('club_member_history_fk3',$exist_table->foreignKeys) || !array_key_exists('id_amount_club_member',$exist_table->foreignKeys['club_member_history_fk3'])) 
            $this->addForeignKey(
                'club_member_history_fk3',
                'club_member_history',
                'id_amount_club_member',
                'amount_club_member',
                'id_amount_club_member',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('club_member_history_fk3','club_member_history' );
            $this->addForeignKey(
                'club_member_history_fk3',
                'club_member_history',
                'id_amount_club_member',
                'amount_club_member',
                'id_amount_club_member',
                'CASCADE',
                'CASCADE'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Club_member_history cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Club_member_history cannot be reverted.


        return false;
    }
    */
}
