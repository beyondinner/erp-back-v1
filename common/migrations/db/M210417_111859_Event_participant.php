<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111859_Event_participant
 */
class M210417_111859_Event_participant extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'event_participant';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('event_participant',
            [
                'id_event_participant' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'id_event' =>$this->integer(10)->notNull(),
                'id_participant' =>$this->integer(10)->notNull(),
                'days' =>$this->integer(10)->notNull(),
                'consume_products' =>$this->boolean()->notNull(),
                'id_payment_types' =>$this->integer(10)->notNull(),
                 'PRIMARY KEY (`id_event_participant`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_event_participant'))
                $this->addColumn('event_participant', 'id_event_participant', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('id_event'))
                $this->addColumn('event_participant', 'id_event', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('id_participant'))
                $this->addColumn('event_participant', 'id_participant', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('days'))
                $this->addColumn('event_participant', 'days', $this->integer(10)->notNull());
             else{
                $this->alterColumn('event_participant', 'days', $this->integer(10)->notNull());
                }
            if (!$exist_table->getColumn('consume_products'))
                $this->addColumn('event_participant', 'consume_products', $this->boolean()->notNull());
             else{
                $this->alterColumn('event_participant', 'consume_products', $this->boolean()->notNull());
                }
            if (!$exist_table->getColumn('id_payment_types'))
                $this->addColumn('event_participant', 'id_payment_types', $this->integer(10)->notNull());

            }
        /*Generating index*/

        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('event_participant_fk',$exist_table->foreignKeys) || !array_key_exists('id_event',$exist_table->foreignKeys['event_participant_fk'])) 
            $this->addForeignKey(
                'event_participant_fk',
                'event_participant',
                'id_event',
                'event',
                'id_event',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('event_participant_fk','event_participant' );
            $this->addForeignKey(
                'event_participant_fk',
                'event_participant',
                'id_event',
                'event',
                'id_event',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('event_participant_fk1',$exist_table->foreignKeys) || !array_key_exists('id_participant',$exist_table->foreignKeys['event_participant_fk1'])) 
            $this->addForeignKey(
                'event_participant_fk1',
                'event_participant',
                'id_participant',
                'participants',
                'id_participant',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('event_participant_fk1','event_participant' );
            $this->addForeignKey(
                'event_participant_fk1',
                'event_participant',
                'id_participant',
                'participants',
                'id_participant',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('event_participant_fk2',$exist_table->foreignKeys) || !array_key_exists('id_payment_types',$exist_table->foreignKeys['event_participant_fk2'])) 
            $this->addForeignKey(
                'event_participant_fk2',
                'event_participant',
                'id_payment_types',
                'payment_types',
                'id_payment_type',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('event_participant_fk2','event_participant' );
            $this->addForeignKey(
                'event_participant_fk2',
                'event_participant',
                'id_payment_types',
                'payment_types',
                'id_payment_type',
                'CASCADE',
                'CASCADE'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Event_participant cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Event_participant cannot be reverted.


        return false;
    }
    */
}
