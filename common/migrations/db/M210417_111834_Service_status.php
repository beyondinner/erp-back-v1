<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111834_Service_status
 */
class M210417_111834_Service_status extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'service_status';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('service_status',
            [
                'id_service_status' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'service_status_siglas' =>$this->string(255),
                'service_status_desc' =>$this->string(255),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                 'PRIMARY KEY (`id_service_status`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_service_status'))
                $this->addColumn('service_status', 'id_service_status', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('service_status_siglas'))
                $this->addColumn('service_status', 'service_status_siglas', $this->string(255));
             else{

                $this->alterColumn('service_status', 'service_status_siglas', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('service_status_desc'))
                $this->addColumn('service_status', 'service_status_desc', $this->string(255));
             else{

                $this->alterColumn('service_status', 'service_status_desc', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('service_status', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('service_status', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('service_status', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('service_status', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('updated_at', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'updated_at',
                'service_status',
                ['updated_at'],
                true
            );
        if ($exist_table === null || !array_key_exists('service_status_desc', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'service_status_desc',
                'service_status',
                ['service_status_desc'],
                true
            );
        if ($exist_table === null || !array_key_exists('service_status_siglas', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'service_status_siglas',
                'service_status',
                ['service_status_siglas'],
                true
            );
        /*Generating foreignkey*/


    }

   public function down()
    {
        echo 'M210417_111812_Service_status cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Service_status cannot be reverted.


        return false;
    }
    */
}
