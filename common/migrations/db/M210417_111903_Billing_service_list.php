<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111903_Billing_service_list
 */
class M210417_111903_Billing_service_list extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'billing_service_list';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('billing_service_list',
            [
                'id_billing_service_list' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'billing_service_list_price' =>$this->float(12)->notNull(),
                'billing_service_list_concept' =>$this->string(255),
                'billing_service_list_desc' =>$this->string(255),
                'billing_service_list_quantity' =>$this->integer(10)->notNull(),
                'tax_id' =>$this->integer(10)->notNull(),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'id_service' =>$this->integer(10),
                'id_billing' =>$this->integer(10)->notNull(),
                 'PRIMARY KEY (`id_billing_service_list`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_billing_service_list'))
                $this->addColumn('billing_service_list', 'id_billing_service_list', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('billing_service_list_price'))
                $this->addColumn('billing_service_list', 'billing_service_list_price', $this->float(12)->notNull());
             else{
                $this->alterColumn('billing_service_list', 'billing_service_list_price', $this->float(12)->notNull());
                }
            if (!$exist_table->getColumn('billing_service_list_concept'))
                $this->addColumn('billing_service_list', 'billing_service_list_concept', $this->string(255));
             else{

                $this->alterColumn('billing_service_list', 'billing_service_list_concept', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('billing_service_list_desc'))
                $this->addColumn('billing_service_list', 'billing_service_list_desc', $this->string(255));
             else{

                $this->alterColumn('billing_service_list', 'billing_service_list_desc', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('billing_service_list_quantity'))
                $this->addColumn('billing_service_list', 'billing_service_list_quantity', $this->integer(10)->notNull());
             else{
                $this->alterColumn('billing_service_list', 'billing_service_list_quantity', $this->integer(10)->notNull());
                }
            if (!$exist_table->getColumn('tax_id'))
                $this->addColumn('billing_service_list', 'tax_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('billing_service_list', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('billing_service_list', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('billing_service_list', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('billing_service_list', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('id_service'))
                $this->addColumn('billing_service_list', 'id_service', $this->integer(10));

            if (!$exist_table->getColumn('id_billing'))
                $this->addColumn('billing_service_list', 'id_billing', $this->integer(10)->notNull());

            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('billing_service_list_price', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'billing_service_list_price',
                'billing_service_list',
                ['billing_service_list_price'],
                true
            );
        if ($exist_table === null || !array_key_exists('billing_service_list_quantity', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'billing_service_list_quantity',
                'billing_service_list',
                ['billing_service_list_quantity'],
                true
            );
        if ($exist_table === null || !array_key_exists('tax_id_2', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'tax_id_2',
                'billing_service_list',
                ['tax_id'],
                true
            );
        if ($exist_table === null || !array_key_exists('created_at', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'created_at',
                'billing_service_list',
                ['created_at'],
                true
            );
        if ($exist_table === null || !array_key_exists('updated_at', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'updated_at',
                'billing_service_list',
                ['updated_at'],
                true
            );
        if ($exist_table === null || !array_key_exists('id_billing_2', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'id_billing_2',
                'billing_service_list',
                ['id_billing'],
                true
            );
        if ($exist_table === null || !array_key_exists('billing_service_list_desc', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'billing_service_list_desc',
                'billing_service_list',
                ['billing_service_list_desc'],
                true
            );
        if ($exist_table === null || !array_key_exists('billing_service_list_concept', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'billing_service_list_concept',
                'billing_service_list',
                ['billing_service_list_concept'],
                true
            );
        if ($exist_table === null || !array_key_exists('id_service_2', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'id_service_2',
                'billing_service_list',
                ['id_service'],
                true
            );
        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('billing_service_list_fk',$exist_table->foreignKeys) || !array_key_exists('id_billing',$exist_table->foreignKeys['billing_service_list_fk'])) 
            $this->addForeignKey(
                'billing_service_list_fk',
                'billing_service_list',
                'id_billing',
                'billings',
                'id_billings',
                'NO ACTION',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('billing_service_list_fk','billing_service_list' );
            $this->addForeignKey(
                'billing_service_list_fk',
                'billing_service_list',
                'id_billing',
                'billings',
                'id_billings',
                'NO ACTION',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('billing_service_list_fk1',$exist_table->foreignKeys) || !array_key_exists('id_service',$exist_table->foreignKeys['billing_service_list_fk1'])) 
            $this->addForeignKey(
                'billing_service_list_fk1',
                'billing_service_list',
                'id_service',
                'services',
                'id_service',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('billing_service_list_fk1','billing_service_list' );
            $this->addForeignKey(
                'billing_service_list_fk1',
                'billing_service_list',
                'id_service',
                'services',
                'id_service',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('billing_service_list_fk2',$exist_table->foreignKeys) || !array_key_exists('tax_id',$exist_table->foreignKeys['billing_service_list_fk2'])) 
            $this->addForeignKey(
                'billing_service_list_fk2',
                'billing_service_list',
                'tax_id',
                'budget_items_list_taxes',
                'id_budget_items_list_taxes',
                'NO ACTION',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('billing_service_list_fk2','billing_service_list' );
            $this->addForeignKey(
                'billing_service_list_fk2',
                'billing_service_list',
                'tax_id',
                'budget_items_list_taxes',
                'id_budget_items_list_taxes',
                'NO ACTION',
                'CASCADE'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Billing_service_list cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Billing_service_list cannot be reverted.


        return false;
    }
    */
}
