<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111831_Permissions
 */
class M210417_111831_Permissions extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'permissions';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('permissions',
            [
                'id_permission' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'permission_name' =>$this->string(255),
                'peromission_desc' =>$this->string(255),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                 'PRIMARY KEY (`id_permission`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_permission'))
                $this->addColumn('permissions', 'id_permission', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('permission_name'))
                $this->addColumn('permissions', 'permission_name', $this->string(255));
             else{

                $this->alterColumn('permissions', 'permission_name', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('peromission_desc'))
                $this->addColumn('permissions', 'peromission_desc', $this->string(255));
             else{

                $this->alterColumn('permissions', 'peromission_desc', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('permissions', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('permissions', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('permissions', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('permissions', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            }
        /*Generating index*/

        /*Generating foreignkey*/


    }

   public function down()
    {
        echo 'M210417_111812_Permissions cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Permissions cannot be reverted.


        return false;
    }
    */
}
