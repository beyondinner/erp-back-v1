<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111842_Users
 */
class M210417_111842_Users extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'users';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('users',
            [
                'id_user' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'nombre_usuario' =>$this->string(45),
                'apellido_usuario' =>$this->string(45),
                'username' =>$this->string(45),
                'pass' =>$this->string(255),
                'email' =>$this->string(45),
                'auth_key' =>$this->string(255),
                'created_at' =>$this->date(),
                'updated_at' =>$this->date(),
                'active' =>$this->boolean()->notNull(),
                'image' =>$this->string(20),
                'id_role' =>$this->integer(10)->notNull(),
                'status' =>$this->string(80),
                 'PRIMARY KEY (`id_user`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_user'))
                $this->addColumn('users', 'id_user', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('nombre_usuario'))
                $this->addColumn('users', 'nombre_usuario', $this->string(45));
             else{

                $this->alterColumn('users', 'nombre_usuario', 'VARCHAR(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('apellido_usuario'))
                $this->addColumn('users', 'apellido_usuario', $this->string(45));
             else{

                $this->alterColumn('users', 'apellido_usuario', 'VARCHAR(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('username'))
                $this->addColumn('users', 'username', $this->string(45));
             else{

                $this->alterColumn('users', 'username', 'VARCHAR(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('pass'))
                $this->addColumn('users', 'pass', $this->string(255));
             else{

                $this->alterColumn('users', 'pass', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('email'))
                $this->addColumn('users', 'email', $this->string(45));
             else{

                $this->alterColumn('users', 'email', 'VARCHAR(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('auth_key'))
                $this->addColumn('users', 'auth_key', $this->string(255));
             else{

                $this->alterColumn('users', 'auth_key', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('users', 'created_at', $this->date());
             else{
                $this->alterColumn('users', 'created_at', $this->date());
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('users', 'updated_at', $this->date());
             else{
                $this->alterColumn('users', 'updated_at', $this->date());
                }
            if (!$exist_table->getColumn('active'))
                $this->addColumn('users', 'active', $this->boolean()->notNull());
             else{
                $this->alterColumn('users', 'active', $this->boolean()->notNull());
                }
            if (!$exist_table->getColumn('image'))
                $this->addColumn('users', 'image', $this->string(20));
             else{

                $this->alterColumn('users', 'image', 'VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('id_role'))
                $this->addColumn('users', 'id_role', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('status'))
                $this->addColumn('users', 'status', $this->string(80));
             else{

                $this->alterColumn('users', 'status', 'VARCHAR(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('email', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'email',
                'users',
                ['email'],
                true
            );
        if ($exist_table === null || !array_key_exists('username', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'username',
                'users',
                ['username'],
                true
            );
        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('users_fk',$exist_table->foreignKeys) || !array_key_exists('id_role',$exist_table->foreignKeys['users_fk'])) 
            $this->addForeignKey(
                'users_fk',
                'users',
                'id_role',
                'role',
                'id_role',
                'NO ACTION',
                'NO ACTION'
            );
           else {
            $this->dropForeignKey('users_fk','users' );
            $this->addForeignKey(
                'users_fk',
                'users',
                'id_role',
                'role',
                'id_role',
                'NO ACTION',
                'NO ACTION'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Users cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Users cannot be reverted.


        return false;
    }
    */
}
