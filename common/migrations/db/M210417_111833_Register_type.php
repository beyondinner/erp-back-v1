<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111833_Register_type
 */
class M210417_111833_Register_type extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'register_type';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('register_type',
            [
                'id_register_type' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'resgister_type' =>$this->string(20),
                'register_type_description' =>$this->text(),
                 'PRIMARY KEY (`id_register_type`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_register_type'))
                $this->addColumn('register_type', 'id_register_type', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('resgister_type'))
                $this->addColumn('register_type', 'resgister_type', $this->string(20));
             else{

                $this->alterColumn('register_type', 'resgister_type', 'VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('register_type_description'))
                $this->addColumn('register_type', 'register_type_description', $this->text());
             else{
                $this->alterColumn('register_type', 'register_type_description', $this->text());
                }
            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('resgister_type', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'resgister_type',
                'register_type',
                ['resgister_type'],
                true
            );
        /*Generating foreignkey*/


    }

   public function down()
    {
        echo 'M210417_111812_Register_type cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Register_type cannot be reverted.


        return false;
    }
    */
}
