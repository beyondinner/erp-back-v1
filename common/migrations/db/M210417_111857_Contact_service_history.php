<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111857_Contact_service_history
 */
class M210417_111857_Contact_service_history extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'contact_service_history';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('contact_service_history',
            [
                'id_contact_service_history' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'id_contact' =>$this->integer(10)->notNull(),
                'id_services' =>$this->integer(10),
                'contact_service_history_desc' =>$this->text(),
                'type' =>$this->integer(10),
                'created_at' =>$this->dateTime()->append(' NULL'),
                'updated_at' =>$this->dateTime()->append(' NULL'),
                'amount' =>$this->float(12),
                 'PRIMARY KEY (`id_contact_service_history`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_contact_service_history'))
                $this->addColumn('contact_service_history', 'id_contact_service_history', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('id_contact'))
                $this->addColumn('contact_service_history', 'id_contact', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('id_services'))
                $this->addColumn('contact_service_history', 'id_services', $this->integer(10));

            if (!$exist_table->getColumn('contact_service_history_desc'))
                $this->addColumn('contact_service_history', 'contact_service_history_desc', $this->text());
             else{
                $this->alterColumn('contact_service_history', 'contact_service_history_desc', $this->text());
                }
            if (!$exist_table->getColumn('type'))
                $this->addColumn('contact_service_history', 'type', $this->integer(10));
             else{
                $this->alterColumn('contact_service_history', 'type', $this->integer(10));
                }
            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('contact_service_history', 'created_at', $this->dateTime()->append(' NULL'));
             else{
                $this->alterColumn('contact_service_history', 'created_at', $this->dateTime()->append(' NULL'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('contact_service_history', 'updated_at', $this->dateTime()->append(' NULL'));
             else{
                $this->alterColumn('contact_service_history', 'updated_at', $this->dateTime()->append(' NULL'));
                }
            if (!$exist_table->getColumn('amount'))
                $this->addColumn('contact_service_history', 'amount', $this->float(12));
             else{
                $this->alterColumn('contact_service_history', 'amount', $this->float(12));
                }
            }
        /*Generating index*/

        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('contact_service_history_fk',$exist_table->foreignKeys) || !array_key_exists('id_contact',$exist_table->foreignKeys['contact_service_history_fk'])) 
            $this->addForeignKey(
                'contact_service_history_fk',
                'contact_service_history',
                'id_contact',
                'contacts',
                'id_contact',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('contact_service_history_fk','contact_service_history' );
            $this->addForeignKey(
                'contact_service_history_fk',
                'contact_service_history',
                'id_contact',
                'contacts',
                'id_contact',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('contact_service_history_fk1',$exist_table->foreignKeys) || !array_key_exists('id_services',$exist_table->foreignKeys['contact_service_history_fk1'])) 
            $this->addForeignKey(
                'contact_service_history_fk1',
                'contact_service_history',
                'id_services',
                'services',
                'id_service',
                'SET NULL',
                'SET NULL'
            );
           else {
            $this->dropForeignKey('contact_service_history_fk1','contact_service_history' );
            $this->addForeignKey(
                'contact_service_history_fk1',
                'contact_service_history',
                'id_services',
                'services',
                'id_service',
                'SET NULL',
                'SET NULL'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Contact_service_history cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Contact_service_history cannot be reverted.


        return false;
    }
    */
}
