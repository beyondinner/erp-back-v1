<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111847_Event_product
 */
class M210417_111847_Event_product extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'event_product';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('event_product',
            [
                'id_event_product' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'created_at' =>$this->date(),
                'id_event' =>$this->integer(10)->notNull(),
                'id_product' =>$this->integer(10)->notNull(),
                'amount' =>$this->integer(10)->notNull(),
                 'PRIMARY KEY (`id_event_product`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_event_product'))
                $this->addColumn('event_product', 'id_event_product', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('event_product', 'created_at', $this->date());
             else{
                $this->alterColumn('event_product', 'created_at', $this->date());
                }
            if (!$exist_table->getColumn('id_event'))
                $this->addColumn('event_product', 'id_event', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('id_product'))
                $this->addColumn('event_product', 'id_product', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('amount'))
                $this->addColumn('event_product', 'amount', $this->integer(10)->notNull());
             else{
                $this->alterColumn('event_product', 'amount', $this->integer(10)->notNull());
                }
            }
        /*Generating index*/

        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('event_product_fk',$exist_table->foreignKeys) || !array_key_exists('id_event',$exist_table->foreignKeys['event_product_fk'])) 
            $this->addForeignKey(
                'event_product_fk',
                'event_product',
                'id_event',
                'event',
                'id_event',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('event_product_fk','event_product' );
            $this->addForeignKey(
                'event_product_fk',
                'event_product',
                'id_event',
                'event',
                'id_event',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('event_product_fk1',$exist_table->foreignKeys) || !array_key_exists('id_product',$exist_table->foreignKeys['event_product_fk1'])) 
            $this->addForeignKey(
                'event_product_fk1',
                'event_product',
                'id_product',
                'products',
                'id_product',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('event_product_fk1','event_product' );
            $this->addForeignKey(
                'event_product_fk1',
                'event_product',
                'id_product',
                'products',
                'id_product',
                'CASCADE',
                'CASCADE'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Event_product cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Event_product cannot be reverted.


        return false;
    }
    */
}
