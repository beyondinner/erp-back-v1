<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111902_Billing_product_list
 */
class M210417_111902_Billing_product_list extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'billing_product_list';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('billing_product_list',
            [
                'id_billing_product_list' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'billing_product_list_price' =>$this->float(12)->notNull(),
                'billing_product_list_concept' =>$this->string(255),
                'billing_product_list_desc' =>$this->string(255),
                'billing_product_list_quantity' =>$this->integer(10)->notNull(),
                'tax_id' =>$this->integer(10)->notNull(),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'id_product' =>$this->integer(10),
                'id_billing' =>$this->integer(10)->notNull(),
                 'PRIMARY KEY (`id_billing_product_list`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_billing_product_list'))
                $this->addColumn('billing_product_list', 'id_billing_product_list', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('billing_product_list_price'))
                $this->addColumn('billing_product_list', 'billing_product_list_price', $this->float(12)->notNull());
             else{
                $this->alterColumn('billing_product_list', 'billing_product_list_price', $this->float(12)->notNull());
                }
            if (!$exist_table->getColumn('billing_product_list_concept'))
                $this->addColumn('billing_product_list', 'billing_product_list_concept', $this->string(255));
             else{

                $this->alterColumn('billing_product_list', 'billing_product_list_concept', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('billing_product_list_desc'))
                $this->addColumn('billing_product_list', 'billing_product_list_desc', $this->string(255));
             else{

                $this->alterColumn('billing_product_list', 'billing_product_list_desc', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('billing_product_list_quantity'))
                $this->addColumn('billing_product_list', 'billing_product_list_quantity', $this->integer(10)->notNull());
             else{
                $this->alterColumn('billing_product_list', 'billing_product_list_quantity', $this->integer(10)->notNull());
                }
            if (!$exist_table->getColumn('tax_id'))
                $this->addColumn('billing_product_list', 'tax_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('billing_product_list', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('billing_product_list', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('billing_product_list', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('billing_product_list', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('id_product'))
                $this->addColumn('billing_product_list', 'id_product', $this->integer(10));

            if (!$exist_table->getColumn('id_billing'))
                $this->addColumn('billing_product_list', 'id_billing', $this->integer(10)->notNull());

            }
        /*Generating index*/

        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('billing_product_list_fk',$exist_table->foreignKeys) || !array_key_exists('id_product',$exist_table->foreignKeys['billing_product_list_fk'])) 
            $this->addForeignKey(
                'billing_product_list_fk',
                'billing_product_list',
                'id_product',
                'products',
                'id_product',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('billing_product_list_fk','billing_product_list' );
            $this->addForeignKey(
                'billing_product_list_fk',
                'billing_product_list',
                'id_product',
                'products',
                'id_product',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('billing_product_list_fk1',$exist_table->foreignKeys) || !array_key_exists('tax_id',$exist_table->foreignKeys['billing_product_list_fk1'])) 
            $this->addForeignKey(
                'billing_product_list_fk1',
                'billing_product_list',
                'tax_id',
                'budget_items_list_taxes',
                'id_budget_items_list_taxes',
                'NO ACTION',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('billing_product_list_fk1','billing_product_list' );
            $this->addForeignKey(
                'billing_product_list_fk1',
                'billing_product_list',
                'tax_id',
                'budget_items_list_taxes',
                'id_budget_items_list_taxes',
                'NO ACTION',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('billing_product_list_fk2',$exist_table->foreignKeys) || !array_key_exists('id_billing',$exist_table->foreignKeys['billing_product_list_fk2'])) 
            $this->addForeignKey(
                'billing_product_list_fk2',
                'billing_product_list',
                'id_billing',
                'billings',
                'id_billings',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('billing_product_list_fk2','billing_product_list' );
            $this->addForeignKey(
                'billing_product_list_fk2',
                'billing_product_list',
                'id_billing',
                'billings',
                'id_billings',
                'CASCADE',
                'CASCADE'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Billing_product_list cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Billing_product_list cannot be reverted.


        return false;
    }
    */
}
