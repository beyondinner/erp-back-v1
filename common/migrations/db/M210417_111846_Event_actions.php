<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111846_Event_actions
 */
class M210417_111846_Event_actions extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'event_actions';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('event_actions',
            [
                'id_event_actions' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'date_at' =>$this->dateTime()->notNull()->append(' NULL'),
                'id_user' =>$this->integer(10)->notNull(),
                'action' =>$this->string(20),
                'id_event' =>$this->integer(10)->notNull(),
                'description' =>$this->text(),
                 'PRIMARY KEY (`id_event_actions`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_event_actions'))
                $this->addColumn('event_actions', 'id_event_actions', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('date_at'))
                $this->addColumn('event_actions', 'date_at', $this->dateTime()->notNull()->append(' NULL'));
             else{
                $this->alterColumn('event_actions', 'date_at', $this->dateTime()->notNull()->append(' NULL'));
                }
            if (!$exist_table->getColumn('id_user'))
                $this->addColumn('event_actions', 'id_user', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('action'))
                $this->addColumn('event_actions', 'action', $this->string(20));
             else{

                $this->alterColumn('event_actions', 'action', 'VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('id_event'))
                $this->addColumn('event_actions', 'id_event', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('description'))
                $this->addColumn('event_actions', 'description', $this->text());
             else{
                $this->alterColumn('event_actions', 'description', $this->text());
                }
            }
        /*Generating index*/

        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('event_actions_fk',$exist_table->foreignKeys) || !array_key_exists('id_user',$exist_table->foreignKeys['event_actions_fk'])) 
            $this->addForeignKey(
                'event_actions_fk',
                'event_actions',
                'id_user',
                'users',
                'id_user',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('event_actions_fk','event_actions' );
            $this->addForeignKey(
                'event_actions_fk',
                'event_actions',
                'id_user',
                'users',
                'id_user',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('event_actions_fk1',$exist_table->foreignKeys) || !array_key_exists('id_event',$exist_table->foreignKeys['event_actions_fk1'])) 
            $this->addForeignKey(
                'event_actions_fk1',
                'event_actions',
                'id_event',
                'event',
                'id_event',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('event_actions_fk1','event_actions' );
            $this->addForeignKey(
                'event_actions_fk1',
                'event_actions',
                'id_event',
                'event',
                'id_event',
                'CASCADE',
                'CASCADE'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Event_actions cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Event_actions cannot be reverted.


        return false;
    }
    */
}
