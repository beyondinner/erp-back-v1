<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111815_Countries
 */
class M210417_111815_Countries extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'countries';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('countries',
            [
                'id_country' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'country_siglas' =>$this->string(255),
                'country_name' =>$this->string(255),
                'country_code' =>$this->integer(10),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'iso2' =>$this->string(20),
                 'PRIMARY KEY (`id_country`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_country'))
                $this->addColumn('countries', 'id_country', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('country_siglas'))
                $this->addColumn('countries', 'country_siglas', $this->string(255));
             else{

                $this->alterColumn('countries', 'country_siglas', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('country_name'))
                $this->addColumn('countries', 'country_name', $this->string(255));
             else{

                $this->alterColumn('countries', 'country_name', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('country_code'))
                $this->addColumn('countries', 'country_code', $this->integer(10));
             else{
                $this->alterColumn('countries', 'country_code', $this->integer(10));
                }
            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('countries', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('countries', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('countries', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('countries', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('iso2'))
                $this->addColumn('countries', 'iso2', $this->string(20));
             else{

                $this->alterColumn('countries', 'iso2', 'VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('country_name', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'country_name',
                'countries',
                ['country_name'],
                true
            );
        if ($exist_table === null || !array_key_exists('country_siglas', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'country_siglas',
                'countries',
                ['country_siglas'],
                true
            );
        /*Generating foreignkey*/


    }

   public function down()
    {
        echo 'M210417_111812_Countries cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Countries cannot be reverted.


        return false;
    }
    */
}
