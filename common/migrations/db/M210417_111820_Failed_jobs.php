<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111820_Failed_jobs
 */
class M210417_111820_Failed_jobs extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'failed_jobs';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('failed_jobs',
            [
                'id' =>$this->integer(20)->append('AUTO_INCREMENT')->notNull()->unique(),
                'connection' =>$this->text()->notNull(),
                'queue' =>$this->text()->notNull(),
                'payload' =>$this->text()->notNull(),
                'exception' =>$this->text()->notNull(),
                'failed_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                 'PRIMARY KEY (`id`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id'))
                $this->addColumn('failed_jobs', 'id', $this->integer(20)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('connection'))
                $this->addColumn('failed_jobs', 'connection', $this->text()->notNull());
             else{
                $this->alterColumn('failed_jobs', 'connection', $this->text()->notNull());
                }
            if (!$exist_table->getColumn('queue'))
                $this->addColumn('failed_jobs', 'queue', $this->text()->notNull());
             else{
                $this->alterColumn('failed_jobs', 'queue', $this->text()->notNull());
                }
            if (!$exist_table->getColumn('payload'))
                $this->addColumn('failed_jobs', 'payload', $this->text()->notNull());
             else{
                $this->alterColumn('failed_jobs', 'payload', $this->text()->notNull());
                }
            if (!$exist_table->getColumn('exception'))
                $this->addColumn('failed_jobs', 'exception', $this->text()->notNull());
             else{
                $this->alterColumn('failed_jobs', 'exception', $this->text()->notNull());
                }
            if (!$exist_table->getColumn('failed_at'))
                $this->addColumn('failed_jobs', 'failed_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('failed_jobs', 'failed_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            }
        /*Generating index*/

        /*Generating foreignkey*/


    }

   public function down()
    {
        echo 'M210417_111812_Failed_jobs cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Failed_jobs cannot be reverted.


        return false;
    }
    */
}
