<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111828_Order_status
 */
class M210417_111828_Order_status extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'order_status';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('order_status',
            [
                'id_order_status' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'order_status' =>$this->string(20)->notNull(),
                'order_status_description' =>$this->text(),
                 'PRIMARY KEY (`id_order_status`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_order_status'))
                $this->addColumn('order_status', 'id_order_status', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('order_status'))
                $this->addColumn('order_status', 'order_status', $this->string(20)->notNull());
             else{

                $this->alterColumn('order_status', 'order_status', 'VARCHAR(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL ');

                }
            if (!$exist_table->getColumn('order_status_description'))
                $this->addColumn('order_status', 'order_status_description', $this->text());
             else{
                $this->alterColumn('order_status', 'order_status_description', $this->text());
                }
            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('order_status', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'order_status',
                'order_status',
                ['order_status'],
                true
            );
        /*Generating foreignkey*/


    }

   public function down()
    {
        echo 'M210417_111812_Order_status cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Order_status cannot be reverted.


        return false;
    }
    */
}
