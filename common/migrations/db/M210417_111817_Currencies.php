<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111817_Currencies
 */
class M210417_111817_Currencies extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'currencies';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('currencies',
            [
                'id_currency' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'currency_siglas' =>$this->string(255),
                'currency_desc' =>$this->string(255),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                 'PRIMARY KEY (`id_currency`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_currency'))
                $this->addColumn('currencies', 'id_currency', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('currency_siglas'))
                $this->addColumn('currencies', 'currency_siglas', $this->string(255));
             else{

                $this->alterColumn('currencies', 'currency_siglas', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('currency_desc'))
                $this->addColumn('currencies', 'currency_desc', $this->string(255));
             else{

                $this->alterColumn('currencies', 'currency_desc', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('currencies', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('currencies', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('currencies', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('currencies', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('currency_desc', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'currency_desc',
                'currencies',
                ['currency_desc'],
                true
            );
        if ($exist_table === null || !array_key_exists('currency_siglas', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'currency_siglas',
                'currencies',
                ['currency_siglas'],
                true
            );
        /*Generating foreignkey*/


    }

   public function down()
    {
        echo 'M210417_111812_Currencies cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Currencies cannot be reverted.


        return false;
    }
    */
}
