<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111807_Cliente_ser_prods
 */
class M210417_111807_Cliente_ser_prods extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'cliente_ser_prods';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('cliente_ser_prods',
            [
                'id' =>$this->integer(20)->append('AUTO_INCREMENT')->notNull()->unique(),
                'id_cliente' =>$this->integer(10)->notNull(),
                'id_serv_prod' =>$this->integer(10),
                'comment' =>$this->text(),
                'type' =>$this->smallInteger(5)->notNull(),
                'coins' =>$this->double(22),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                 'PRIMARY KEY (`id`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id'))
                $this->addColumn('cliente_ser_prods', 'id', $this->integer(20)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('id_cliente'))
                $this->addColumn('cliente_ser_prods', 'id_cliente', $this->integer(10)->notNull());
             else{
                $this->alterColumn('cliente_ser_prods', 'id_cliente', $this->integer(10)->notNull());
                }
            if (!$exist_table->getColumn('id_serv_prod'))
                $this->addColumn('cliente_ser_prods', 'id_serv_prod', $this->integer(10));
             else{
                $this->alterColumn('cliente_ser_prods', 'id_serv_prod', $this->integer(10));
                }
            if (!$exist_table->getColumn('comment'))
                $this->addColumn('cliente_ser_prods', 'comment', $this->text());
             else{
                $this->alterColumn('cliente_ser_prods', 'comment', $this->text());
                }
            if (!$exist_table->getColumn('type'))
                $this->addColumn('cliente_ser_prods', 'type', $this->smallInteger(5)->notNull());
             else{
                $this->alterColumn('cliente_ser_prods', 'type', $this->smallInteger(5)->notNull());
                }
            if (!$exist_table->getColumn('coins'))
                $this->addColumn('cliente_ser_prods', 'coins', $this->double(22));
             else{
                $this->alterColumn('cliente_ser_prods', 'coins', $this->double(22));
                }
            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('cliente_ser_prods', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('cliente_ser_prods', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('cliente_ser_prods', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('cliente_ser_prods', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            }
        /*Generating index*/

        /*Generating foreignkey*/


    }

   public function down()
    {
        echo 'M210417_111812_Cliente_ser_prods cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Cliente_ser_prods cannot be reverted.


        return false;
    }
    */
}
