<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111845_Employees
 */
class M210417_111845_Employees extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'employees';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('employees',
            [
                'id_employee' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'employee_name' =>$this->string(255),
                'employee_last_name' =>$this->string(255),
                'employee_nif' =>$this->string(255),
                'employee_email_owned' =>$this->string(255),
                'employee_email_work' =>$this->string(255),
                'employee_address' =>$this->string(255),
                'employee_postal_code' =>$this->string(255),
                'country_id' =>$this->integer(10)->notNull(),
                'state_id' =>$this->integer(10)->notNull(),
                'language_id' =>$this->integer(10)->notNull(),
                'payment_type_id' =>$this->integer(10)->notNull(),
                'currency_id' =>$this->integer(10)->notNull(),
                'work_country_id' =>$this->integer(10)->notNull(),
                'employee_work_zone' =>$this->string(255),
                'employee_picture' =>$this->string(255),
                'employee_attachment' =>$this->string(255),
                'employee_status_id' =>$this->integer(10)->notNull(),
                'created_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                'updated_at' =>$this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'),
                 'PRIMARY KEY (`id_employee`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_employee'))
                $this->addColumn('employees', 'id_employee', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('employee_name'))
                $this->addColumn('employees', 'employee_name', $this->string(255));
             else{

                $this->alterColumn('employees', 'employee_name', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('employee_last_name'))
                $this->addColumn('employees', 'employee_last_name', $this->string(255));
             else{

                $this->alterColumn('employees', 'employee_last_name', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('employee_nif'))
                $this->addColumn('employees', 'employee_nif', $this->string(255));
             else{

                $this->alterColumn('employees', 'employee_nif', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('employee_email_owned'))
                $this->addColumn('employees', 'employee_email_owned', $this->string(255));
             else{

                $this->alterColumn('employees', 'employee_email_owned', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('employee_email_work'))
                $this->addColumn('employees', 'employee_email_work', $this->string(255));
             else{

                $this->alterColumn('employees', 'employee_email_work', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('employee_address'))
                $this->addColumn('employees', 'employee_address', $this->string(255));
             else{

                $this->alterColumn('employees', 'employee_address', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('employee_postal_code'))
                $this->addColumn('employees', 'employee_postal_code', $this->string(255));
             else{

                $this->alterColumn('employees', 'employee_postal_code', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('country_id'))
                $this->addColumn('employees', 'country_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('state_id'))
                $this->addColumn('employees', 'state_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('language_id'))
                $this->addColumn('employees', 'language_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('payment_type_id'))
                $this->addColumn('employees', 'payment_type_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('currency_id'))
                $this->addColumn('employees', 'currency_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('work_country_id'))
                $this->addColumn('employees', 'work_country_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('employee_work_zone'))
                $this->addColumn('employees', 'employee_work_zone', $this->string(255));
             else{

                $this->alterColumn('employees', 'employee_work_zone', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('employee_picture'))
                $this->addColumn('employees', 'employee_picture', $this->string(255));
             else{

                $this->alterColumn('employees', 'employee_picture', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('employee_attachment'))
                $this->addColumn('employees', 'employee_attachment', $this->string(255));
             else{

                $this->alterColumn('employees', 'employee_attachment', 'VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  ');

                }
            if (!$exist_table->getColumn('employee_status_id'))
                $this->addColumn('employees', 'employee_status_id', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('created_at'))
                $this->addColumn('employees', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('employees', 'created_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            if (!$exist_table->getColumn('updated_at'))
                $this->addColumn('employees', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
             else{
                $this->alterColumn('employees', 'updated_at', $this->timestamp()->notNull()->append(' DEFAULT CURRENT_TIMESTAMP'));
                }
            }
        /*Generating index*/

        if ($exist_table === null || !array_key_exists('email_work', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'email_work',
                'employees',
                ['employee_email_work'],
                true
            );
        if ($exist_table === null || !array_key_exists('nif', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'nif',
                'employees',
                ['employee_nif'],
                true
            );
        if ($exist_table === null || !array_key_exists('email_owned', $this->db->getSchema()->findUniqueIndexes($exist_table)))
        $this->createIndex(
                'email_owned',
                'employees',
                ['employee_email_owned'],
                true
            );
        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('employee_fk',$exist_table->foreignKeys) || !array_key_exists('country_id',$exist_table->foreignKeys['employee_fk'])) 
            $this->addForeignKey(
                'employee_fk',
                'employees',
                'country_id',
                'countries',
                'id_country',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('employee_fk','employees' );
            $this->addForeignKey(
                'employee_fk',
                'employees',
                'country_id',
                'countries',
                'id_country',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('employee_fk1',$exist_table->foreignKeys) || !array_key_exists('state_id',$exist_table->foreignKeys['employee_fk1'])) 
            $this->addForeignKey(
                'employee_fk1',
                'employees',
                'state_id',
                'states',
                'id_state',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('employee_fk1','employees' );
            $this->addForeignKey(
                'employee_fk1',
                'employees',
                'state_id',
                'states',
                'id_state',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('employee_fk2',$exist_table->foreignKeys) || !array_key_exists('language_id',$exist_table->foreignKeys['employee_fk2'])) 
            $this->addForeignKey(
                'employee_fk2',
                'employees',
                'language_id',
                'languages',
                'id_language',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('employee_fk2','employees' );
            $this->addForeignKey(
                'employee_fk2',
                'employees',
                'language_id',
                'languages',
                'id_language',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('employee_fk3',$exist_table->foreignKeys) || !array_key_exists('payment_type_id',$exist_table->foreignKeys['employee_fk3'])) 
            $this->addForeignKey(
                'employee_fk3',
                'employees',
                'payment_type_id',
                'payment_types',
                'id_payment_type',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('employee_fk3','employees' );
            $this->addForeignKey(
                'employee_fk3',
                'employees',
                'payment_type_id',
                'payment_types',
                'id_payment_type',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('employee_fk4',$exist_table->foreignKeys) || !array_key_exists('currency_id',$exist_table->foreignKeys['employee_fk4'])) 
            $this->addForeignKey(
                'employee_fk4',
                'employees',
                'currency_id',
                'currencies',
                'id_currency',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('employee_fk4','employees' );
            $this->addForeignKey(
                'employee_fk4',
                'employees',
                'currency_id',
                'currencies',
                'id_currency',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('employee_fk5',$exist_table->foreignKeys) || !array_key_exists('work_country_id',$exist_table->foreignKeys['employee_fk5'])) 
            $this->addForeignKey(
                'employee_fk5',
                'employees',
                'work_country_id',
                'countries',
                'id_country',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('employee_fk5','employees' );
            $this->addForeignKey(
                'employee_fk5',
                'employees',
                'work_country_id',
                'countries',
                'id_country',
                'CASCADE',
                'CASCADE'
            );
           }
        if ($exist_table === null || !array_key_exists('employee_fk6',$exist_table->foreignKeys) || !array_key_exists('employee_status_id',$exist_table->foreignKeys['employee_fk6'])) 
            $this->addForeignKey(
                'employee_fk6',
                'employees',
                'employee_status_id',
                'employee_status',
                'id_employee_status',
                'CASCADE',
                'CASCADE'
            );
           else {
            $this->dropForeignKey('employee_fk6','employees' );
            $this->addForeignKey(
                'employee_fk6',
                'employees',
                'employee_status_id',
                'employee_status',
                'id_employee_status',
                'CASCADE',
                'CASCADE'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Employees cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Employees cannot be reverted.


        return false;
    }
    */
}
