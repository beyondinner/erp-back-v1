<?php

namespace common\migrations\db;

use yii\db\Migration;

/**
 * Class M210417_111851_Payment_recurrency
 */
class M210417_111851_Payment_recurrency extends Migration
{

/**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $table_name = $this->db->tablePrefix . 'payment_recurrency';
        $exist_table = $this->getDb()->getTableSchema($table_name, true);
        /*Generating tables and columns*/
        if ($exist_table === null) {
            $this->createTable('payment_recurrency',
            [
                'id_payment_recurrency' =>$this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique(),
                'id_service' =>$this->integer(10),
                'id_club_members' =>$this->integer(10)->notNull(),
                'initial_date' =>$this->date()->notNull(),
                'recurrency_date' =>$this->integer(10)->notNull(),
                'active' =>$this->boolean(),
                 'PRIMARY KEY (`id_payment_recurrency`)'
            ],'ENGINE=InnoDB'
            );

        } else {

            if (!$exist_table->getColumn('id_payment_recurrency'))
                $this->addColumn('payment_recurrency', 'id_payment_recurrency', $this->integer(10)->append('AUTO_INCREMENT')->notNull()->unique());

            if (!$exist_table->getColumn('id_service'))
                $this->addColumn('payment_recurrency', 'id_service', $this->integer(10));

            if (!$exist_table->getColumn('id_club_members'))
                $this->addColumn('payment_recurrency', 'id_club_members', $this->integer(10)->notNull());

            if (!$exist_table->getColumn('initial_date'))
                $this->addColumn('payment_recurrency', 'initial_date', $this->date()->notNull());
             else{
                $this->alterColumn('payment_recurrency', 'initial_date', $this->date()->notNull());
                }
            if (!$exist_table->getColumn('recurrency_date'))
                $this->addColumn('payment_recurrency', 'recurrency_date', $this->integer(10)->notNull());
             else{
                $this->alterColumn('payment_recurrency', 'recurrency_date', $this->integer(10)->notNull());
                }
            if (!$exist_table->getColumn('active'))
                $this->addColumn('payment_recurrency', 'active', $this->boolean());
             else{
                $this->alterColumn('payment_recurrency', 'active', $this->boolean());
                }
            }
        /*Generating index*/

        /*Generating foreignkey*/

        if ($exist_table === null || !array_key_exists('payment_recurrency_fk',$exist_table->foreignKeys) || !array_key_exists('id_service',$exist_table->foreignKeys['payment_recurrency_fk'])) 
            $this->addForeignKey(
                'payment_recurrency_fk',
                'payment_recurrency',
                'id_service',
                'services',
                'id_service',
                'NO ACTION',
                'NO ACTION'
            );
           else {
            $this->dropForeignKey('payment_recurrency_fk','payment_recurrency' );
            $this->addForeignKey(
                'payment_recurrency_fk',
                'payment_recurrency',
                'id_service',
                'services',
                'id_service',
                'NO ACTION',
                'NO ACTION'
            );
           }
        if ($exist_table === null || !array_key_exists('payment_recurrency_fk1',$exist_table->foreignKeys) || !array_key_exists('id_club_members',$exist_table->foreignKeys['payment_recurrency_fk1'])) 
            $this->addForeignKey(
                'payment_recurrency_fk1',
                'payment_recurrency',
                'id_club_members',
                'club_members',
                'id_club_members',
                'NO ACTION',
                'NO ACTION'
            );
           else {
            $this->dropForeignKey('payment_recurrency_fk1','payment_recurrency' );
            $this->addForeignKey(
                'payment_recurrency_fk1',
                'payment_recurrency',
                'id_club_members',
                'club_members',
                'id_club_members',
                'NO ACTION',
                'NO ACTION'
            );
           }

    }

   public function down()
    {
        echo 'M210417_111812_Payment_recurrency cannot be reverted.';


        return false;
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo M210417_111812_Payment_recurrency cannot be reverted.


        return false;
    }
    */
}
