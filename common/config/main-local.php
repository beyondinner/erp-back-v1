<?php
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Sep 02 19:35:13 GMT-04:00 2020  
*@time Wed Sep 02 19:35:13 GMT-04:00 2020  
*/
return [
    'components' => [
           'mailer' => [
               'class' => 'yii\swiftmailer\Mailer',
               'transport' => [
                   'class' => 'Swift_SmtpTransport',
                   'host' => getenv('email_host'),  // ej. smtp.mandrillapp.com o smtp.gmail.com
                   'username' => getenv('email_username'),
                   'password' => getenv('email_password'),
                   'port' => getenv('email_port'),
                   'encryption'=>getenv('email_encryption')// Seguridad
               ],
           ],
        'db' => require(__DIR__ . '/db.php'),
        
    ],
];

