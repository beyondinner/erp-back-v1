<?php
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Sep 02 19:35:13 GMT-04:00 2020  
*@time Wed Sep 02 19:35:13 GMT-04:00 2020  
*/
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@erp', dirname(dirname(__DIR__)) . '/erp');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
