<?php

/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Sep 02 19:35:13 GMT-04:00 2020  
*@time Wed Sep 02 19:35:13 GMT-04:00 2020  
*/
(new Dotenv\Dotenv(__DIR__ . '/../'))->load();

   return [
        'class' => 'yii\db\Connection',
        'dsn' => 'mysql:host='.getenv('server_erp').';port='.getenv('port_erp').';dbname='.getenv('db_erp').'',
        'username' => getenv('user_erp'),
        'password' => getenv('password_db_erp'),
        'charset' => 'utf8',
        'tablePrefix' => '',
    ];


