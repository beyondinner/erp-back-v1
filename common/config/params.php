<?php

/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Sep 02 19:35:13 GMT-04:00 2020  
*@time Wed Sep 02 19:35:13 GMT-04:00 2020  
*/


   return [
    'adminEmail' => getenv('adminEmail'),
    'supportEmail' => getenv('adminEmail'),
    'accessrule' => getenv('accessrule'),
    'url' => getenv('url'),
    'user.passwordResetTokenExpire' => 3600,
];
