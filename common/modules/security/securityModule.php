<?php
/*Clase del Modulo security*/
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Sep 02 19:35:13 GMT-04:00 2020  
*@time Wed Sep 02 19:35:13 GMT-04:00 2020  
*/
namespace common\modules\security;
class securityModule extends \yii\base\Module 
{
    public $controllerNamespace = 'common\modules\security\controllers';
    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}

