<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Sep 02 19:35:13 GMT-04:00 2020  
*@time Wed Sep 02 19:35:13 GMT-04:00 2020  
*/
namespace common\modules\security\models;


/** 
*  Esta es  ActiveQuery clase de [[Users]].
 *
 * @see Users
 */
/**
 * UsersQuery representa la clase de Consulta del modelo Users
 */
class UsersQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Users[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Users|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

