<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Sep 02 19:35:13 GMT-04:00 2020  
*@time Wed Sep 02 19:35:13 GMT-04:00 2020  
*/
namespace common\modules\security\models;
use Yii;
use common\models\RestModel;

/**
 * Este es la clase modelo para la tabla role_action_access.
 *
 * Los siguientes son los campos de la tabla 'role_action_access':
 * @property integer $id_role_action_access
 * @property integer $id_role
 * @property integer $id_actions
 * @property boolean $range_role_action_access
 * @property boolean $enabled
 * @property date $start_range
 * @property date $end_range

 * Los siguientes son las relaciones de este modelo :

 * @property Actions $actions
 * @property Role $role
 */

class Role_action_access extends RestModel 
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_role_action_access';

    const MODEL = 'Role_action_access';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'role_action_access';
    }

     
        /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
       const RELATIONS = ['actions','role'];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

       const PKEY = 'id_role_action_access';

     /*
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['id_role','id_actions','range_role_action_access','enabled'],'required','on'=>'create'],
			[['id_role_action_access'],'required', 'on' => 'update'],
			[['id_role_action_access','id_role','id_actions'],'integer'],
			[['range_role_action_access','enabled'],'boolean'],
			[['start_range','end_range'],'safe'],
			['start_range','format_start_range'],
			['end_range','format_end_range'],
			[['range_role_action_access','enabled'],'safe'],
			[['id_role_action_access'], 'unique' , 'on' => 'create'],
			[['id_role','id_actions'], 'unique', 'targetAttribute' => ['id_role','id_actions'],'on' => 'create'],
			[['id_role','id_actions'], 'unique', 'targetAttribute' => ['id_role','id_actions'],'on' => 'update','when'=> function ($model) {
                $elem = self::find()->where($model->getAttributes(['id_role','id_actions']))->one();
                return !$elem ? false : $elem[$elem->primaryKey] != $model[$model->primaryKey];
            }],
        ];
    }
	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_actions
     */
	  public function getActions()
		{
			return $this->hasOne(Actions::class, ['id_actions' => 'id_actions']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_role
     */
	  public function getRole()
		{
			return $this->hasOne(Role::class, ['id_role' => 'id_role']);
		}

 /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'role_action_access.id_role_action_access as id', 'role_action_access.id_role_action_access as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('role_action_access.id_role_action_access LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
   public function format_start_range(){
        $timestamp = (strpos('/', $this->start_range) > -1) ?
            strtotime(str_replace('/', '-', $this->start_range)) : $this->start_range;
        $this->start_range = date('Y-m-d h:i:s', strtotime($timestamp));
    }
   public function format_end_range(){
        $timestamp = (strpos('/', $this->end_range) > -1) ?
            strtotime(str_replace('/', '-', $this->end_range)) : $this->end_range;
        $this->end_range = date('Y-m-d h:i:s', strtotime($timestamp));
    }
}
?>
