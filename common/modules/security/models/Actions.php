<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Sep 02 19:35:13 GMT-04:00 2020  
*@time Wed Sep 02 19:35:13 GMT-04:00 2020  
*/
namespace common\modules\security\models;
use Yii;
use common\models\RestModel;

/**
 * Este es la clase modelo para la tabla actions.
 *
 * Los siguientes son los campos de la tabla 'actions':
 * @property integer $id_actions
 * @property string $app
 * @property string $module
 * @property string $controller
 * @property string $action
 * @property string $default_access
 * @property boolean $enabled
 * @property boolean $global

 * Los siguientes son las relaciones de este modelo :

 * @property Role_action_access[] $arrayrole_action_access
 * @property User_action_access[] $arrayuser_action_access
 */

class Actions extends RestModel 
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_actions';

    const MODEL = 'Actions';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'actions';
    }

     
        /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
       const RELATIONS = ['arrayrole_action_access','arrayuser_action_access'];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

       const PKEY = 'id_actions';

     /*
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['app','module','controller','action','default_access','enabled'],'required','on'=>'create'],
			[['id_actions'],'required', 'on' => 'update'],
			[['id_actions'],'integer'],
			[['enabled','global'],'boolean'],
			[['enabled','global'],'safe'],
			[['app','module','controller','action'], 'string', 'max'=>40],
			[['default_access'], 'string', 'max'=>20],
			[['id_actions'], 'unique' , 'on' => 'create'],
        ];
    }

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_actions
     */
	  public function getArrayrole_action_access()
		{
			return $this->hasMany(Role_action_access::class, ['id_actions' => 'id_actions']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_actions
     */
	  public function getArrayuser_action_access()
		{
			return $this->hasMany(User_action_access::class, ['id_actions' => 'id_actions']);
		}
 /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'actions.id_actions as id', 'actions.app as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('actions.app LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
}
?>
