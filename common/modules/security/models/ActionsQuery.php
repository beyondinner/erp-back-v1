<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Sep 02 19:35:13 GMT-04:00 2020  
*@time Wed Sep 02 19:35:13 GMT-04:00 2020  
*/
namespace common\modules\security\models;


/** 
*  Esta es  ActiveQuery clase de [[Actions]].
 *
 * @see Actions
 */
/**
 * ActionsQuery representa la clase de Consulta del modelo Actions
 */
class ActionsQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Actions[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Actions|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

