<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Sep 02 19:35:13 GMT-04:00 2020  
*@time Wed Sep 02 19:35:13 GMT-04:00 2020  
*/
namespace common\modules\security\models;


/** 
*  Esta es  ActiveQuery clase de [[Role_action_access]].
 *
 * @see Role_action_access
 */
/**
 * Role_action_accessQuery representa la clase de Consulta del modelo Role_action_access
 */
class Role_action_accessQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Role_action_access[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Role_action_access|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

