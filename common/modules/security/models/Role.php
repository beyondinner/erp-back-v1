<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Sep 02 19:35:13 GMT-04:00 2020  
*@time Wed Sep 02 19:35:13 GMT-04:00 2020  
*/
namespace common\modules\security\models;
use Yii;
use common\models\RestModel;

/**
 * Este es la clase modelo para la tabla role.
 *
 * Los siguientes son los campos de la tabla 'role':
 * @property integer $id_role
 * @property integer $id_father_role
 * @property string $role_name

 * Los siguientes son las relaciones de este modelo :

 * @property Role $father_role
 * @property Role[] $arrayrole
 * @property Role_action_access[] $arrayrole_action_access
 * @property Users[] $arrayusers
 */

class Role extends RestModel 
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_role';

    const MODEL = 'Role';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'role';
    }

     
        /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
       const RELATIONS = ['father_role','arrayrole','arrayrole_action_access','arrayusers'];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

       const PKEY = 'id_role';

     /*
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['role_name'],'required','on'=>'create'],
			[['id_role'],'required', 'on' => 'update'],
			[['id_role','id_father_role'],'integer'],
			[['role_name'], 'string', 'max'=>255],
			[['id_role'], 'unique' , 'on' => 'create'],
        ];
    }
	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_father_role
     */
	  public function getFather_role()
		{
			return $this->hasOne(Role::class, ['id_role' => 'id_father_role']);
		}


	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_father_role
     */
	  public function getArrayrole()
		{
			return $this->hasMany(Role::class, ['id_father_role' => 'id_role']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_role
     */
	  public function getArrayrole_action_access()
		{
			return $this->hasMany(Role_action_access::class, ['id_role' => 'id_role']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_role
     */
	  public function getArrayusers()
		{
			return $this->hasMany(Users::class, ['id_role' => 'id_role']);
		}
 /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'role.id_role as id', 'role.role_name as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('role.role_name LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
}
?>
