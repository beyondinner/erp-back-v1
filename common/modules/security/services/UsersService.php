<?php
/**
 * /**Generate by ASGENS
 * @author Charlietyn
 * @date Wed Sep 02 19:35:13 GMT-04:00 2020
 * @time Wed Sep 02 19:35:13 GMT-04:00 2020
 */

namespace common\modules\security\services;


use common\modules\security\models\Users;
use common\services\Services;

class UsersService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'common\modules\security\models\Users';

    public function validate_pass($user,$pass)
    {
        $result=$user->validatePassword($pass);
        return ['success'=>$result];
    }
}
