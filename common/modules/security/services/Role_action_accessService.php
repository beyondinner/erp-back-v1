<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Sep 02 19:35:13 GMT-04:00 2020  
*@time Wed Sep 02 19:35:13 GMT-04:00 2020  
*/
namespace common\modules\security\services;


use common\services\Services;

class Role_action_accessService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'common\modules\security\models\Role_action_access';

}
