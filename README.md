<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">CRM System</h1>
    <br>
</p>

DIRECTORY STRUCTURE
-------------------

```
common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both erp and frontend
    tests/               contains tests for common classes    
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
erp
    assets/              contains application assets such as JavaScript and CSS
    config/              contains erp configurations
    controllers/         contains Web controller classes
    models/              contains erp-specific model classes
    modules/             contains erp-specific modules folder
    runtime/             contains files generated during runtime
    tests/               contains tests for erp application    
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources

vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
```
