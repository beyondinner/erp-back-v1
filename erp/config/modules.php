<?php

/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Sep 02 19:35:13 GMT-04:00 2020  
*@time Wed Sep 02 19:35:13 GMT-04:00 2020  
*/
	return [
        'managment' => ['class'=>'erp\modules\managment\managmentModule'],
        'billings' => ['class'=>'erp\modules\billings\billingsModule'],
        'nomenclatures' => ['class'=>'erp\modules\nomenclatures\nomenclaturesModule'],
        'reports' => ['class'=>'erp\modules\reports\reportsModule'],
        'security' => ['class'=>'erp\modules\security\securityModule'],
	   ]
?>

