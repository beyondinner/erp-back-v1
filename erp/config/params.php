<?php

/**Generate by ASGENS
 * @author Charlietyn
 * @date Wed Sep 02 19:35:13 GMT-04:00 2020
 * @time Wed Sep 02 19:35:13 GMT-04:00 2020
 */


return [
    'adminEmail' => getenv('adminEmail'),
    'accessrule' => getenv('accessrule'),
    'content_es' => '<h2>Hola {{name}} {{last_name}}</h2>
<p>Bienvenido al BeInClub, el primer club internacional de Evolución Interior. </p>
<p>Todo está listo para entrar, pertenecer y disfrutar. </p>
<p>Este Club es la forma que hemos elegido darle a la comunidad y movimiento que ya se había generado entre más de 100.000 personas que han formado parte de nuestros retiros de Evolución Interior®, las formaciones de nuestras distintas escuelas y academias, y por el equipo de facilitadores y colaboradores que llevamos transitando juntos este camino desde el año 2012.</p>
<p>Desde el conglomerado de empresas y plataformas que se han creado naturalmente en torno a esta propuesta, se te apoya en tu proceso de evolución interior siendo cada inversión que hagas revalorizada a través de nuestra divisa interna, el innercoin. Esta comunidad, cuya economía interna es una característica fundamental, te ofrece un sinfín de oportunidades. </p>
<p>Te describiremos una serie de pasos que debes realizar para poner tu cuenta al día: </p>
<p>Para poder entrar a tu cuenta personal, debes acceder al siguiente link para luego introducir tus credenciales, disponibles a continuación: </p>
<p>- Enlace para revisar estado de la cuenta: <a href="http://beinapp-wallet.thecloudgroup.tech">http://beinapp-wallet.thecloudgroup.tech</a> </p>
<p>- Usuario: <strong>{{email}}</strong> </p>
<p>- Contraseña: <strong>{{pass}}</strong></p> <br>


<p>En segundo lugar, queremos invitarte a realizar tu primera consulta médica gratuita que ofrecemos a todos nuestros socios para poder conocerte mejor y saber más de ti. Puedes organizarla mediante tu asesor personal, sabiendo que nuestro equipo médico está disponible de lunes a viernes de 10 a 13 y de 16 a 19 (GMT+2 - Madrid). </p>
<p>El siguiente paso para realizar es que hagas un recorrido por todas nuestras plataformas a través de tu asesor personal para conocer todas nuestras propuestas. Tu asesor te enviará, a través del medio que tú mismo designes con él, todas las propuestas, beneficios y ofertas sólo para socios que saldrán periódicamente. </p>

<p>Aquí dejamos por escrito todas las oportunidades que tienes dentro de este movimiento: </p>
<p>Retiros de evolución interior deInner Mastery: <a href="http://www.innermastery.eu">http://www.innermastery.eu</a></p>
<p>Centro de estudios Evolución Interior: <a href="http://www.evolucioninterior.es">http://www.evolucioninterior.es</a></p>
<p>Academia Alverto: <a href="http://www.academiaalverto.com">http://www.academiaalverto.com</a></p>
<p>Ayahuasca Travels (viajes a la selva): <a href="https://www.ayahuascatravels.com/">https://www.ayahuascatravels.com/</a></p>
<p>Entheos Planet (microdosis y asesoramiento): <a href="https://www.entheosplanet.com/">https://www.entheosplanet.com</a></p>
<p>Red Epicentros (comunidades disponibles para vivir y formarse): <a href="https://www.redepicentros.com/">https://www.redepicentros.com/</a></p>
<p>Pagina de Alberto Varela (con eventos donde está presente y plataforma online para ver contenido audiovisual y blog): <a href="https://albertojosevarela.com/">https://albertojosevarela.com/</a></p>
<p>Documental El Sexto Poder: <a href="https://www.sexto-poder.com">https://www.sexto-poder.com</a> </p><br/>
<p>Recuerda que cuentas con el acompañamiento personal de tu asesor para todo lo que vivas y experimentes dentro de esta nueva comunidad de gente afín. </p>
<p>Si tienes alguna inquietud o feedback que le quieras dar al club, este es nuestro correo oficial: <a href="mailto:admin@beyondinner.com">admin@beyondinner.com</a> </p>
<p>Dirección general del <strong>BEYOND INNER CLUB</strong> </p>
',
    'content_it' => '<h2>Ciao {{name}} {{last_name}}</h2>
<p>Benvenuti al BeInClub, il Primo Club Internazionale di Evoluzione Interiore. </p>

<p>Tutto è pronto per entrare, appartenere e gioire.</p>

<p>Questo Club incarna la forma che abbiamo scelto di dare alla comunità e al Movimento che ha preso vita dalle oltre 100.000 persone che hanno partecipato ai nostri ritiri di Evoluzione Interiore e alla Formazione delle nostre Scuole e Accademie, e che si sono unite a noi e alla nostra equipe di facilitatori e collaboratori con i quali camminiamo insieme lungo questo percorso fin dall’anno 2012.</p>

<p>Grazie al gruppo di imprese e piattaforme che sono via via naturalmente nate intorno a questa proposta, puoi ricevere un supporto al tuo processo di evoluzione interiore, anche attraverso la rivalutazione del tuo investimento in Innercoin, la nostra valuta interna. Questa comunità, la cui economia interna è una caratteristica fondamentale, ti offre molteplici opportunità. </p>

<p>Ecco qui una serie di passi da compiere per aggiornare il tuo conto:</p>

<p>Per poter entrare nel tuo account personale devi accedere al seguente link e successivamente inserire le tue credenziali, che troverai qui di seguito:</p>

<p>- Link per verificare lo stato del tuo conto: <a href="http://beinapp-wallet.thecloudgroup.tech">http://beinapp-wallet.thecloudgroup.tech</a> </p>

<p>- Utente: <strong>{{email}}</strong> </p>

<p>- Password: <strong>{{pass}}</strong></p> <br>



<p>Inoltre, vogliamo invitarti a usufruire della tua prima consuleza medica gratuita che offriamo a tutti i nostri soci per potersi conoscere meglio e scoprire di più su se stessi. Puoi fissarla attraverso il tuo consulente personale, tenendo conto che la nostra equipe medica è disponibile dal Lunedì al venerdì, dalle 10 alle 13, e dalle 16 alle 19 (GMT+2 - Madrid).</p>

<p>Il passo successivo è la visita alle nostre pagine, affiancato dal tuo consulente personale, così che tu possa conoscere tutte le nostre proposte. Il tuo consulente ti invierà approfondimenti e materiali, nonché offerte e vantaggi riservati solo ai soci, secondo la forma e il mezzo che tu stesso gli indicherai, e in ragione delle campagne e iniziative periodiche che via via lanceremo.  </p>


<p>Ecco qui una panoramica di tutte le proposte:</p>

<p>Ritiri di Evoluzione Interiore – Inner Mastery:<a href="http://www.innermastery.eu">http://www.innermastery.eu </a></p>

<p>Centro di Studi Evoluzione Interiore:  <a href="http://www.evolucioninterior.es">http://www.evolucioninterior.es </a>(per ora, solo in spagnolo)</p>

<p>Accademia Alverto: <a href="http://www.academiaalverto.com">http://www.academiaalverto.com</a> (per ora, solo in spagnolo)</p>

<p>Ayahuasca Travels (Viaggi nella Foresta Amazzonica):  <a href="https://www.ayahuascatravels.com/it/">https://www.ayahuascatravels.com/it/</a></p>

<p>Entheos Planet (microdosi y consulenza medica):  <a href="https://www.entheosplanet.com/it/">https://www.entheosplanet.com/it/</a></p>

<p>Red des Epicentros (Comunità dove vivere e formarsi): <a href="https://www.redepicentros.com/">https://www.redepicentros.com/</a> (il sito è in spagnolo, guarda la scheda dedicata a MILANO)</p>

<p>Sito di Alberto Varela (con indicazione degli eventi in cui Alberto è presente e da cui si accede alla piattaforma online per vedere materiale audiovisivo e leggere il suo Blog):  <a href="https://albertojosevarela.com/">https://albertojosevarela.com/</a> (per ora solo in spagnolo. Per il Blog in italiano: (per ora solo in spagnolo. Per il Blog in italiano: <a href="https://albertojosevarela.com/it">https://albertojosevarela.com/it</a></p>

<p>Doumentario IL SESTO POTERE: <a href="https://www.sexto-poder.com">https://www.sexto-poder.com</a> (per ora solo in Spagnolo, per visionare i Documentari in Italiano, chiedi al tuo consulente)</p><br/>

<p>Ricorda che puoi contare sull’appoggio e l’accompagnamento personale del tuo consulente per tutto ciò che stai vivendo e sperimentando nell’ambito di questa nuova Comunità di persone affini. </p>

<p>Se hai dubbi, domande o feedback che vuoi comunicarci, questo è la nostra email ufficiale:  <a href="mailto:admin@beyondinner.com">admin@beyondinner.com</a> </p>

<p>Direzione generale del <strong>BEYOND INNER CLUB</strong> </p>
',    'content_fr' => '<h2>Bonjour  {{name}} {{last_name}}</h2>
<p>Bienvenu au BeinClub, le premier Club international d\'Évolution Intérieure.</p>

<p>Tout est prêt pour entrer, faire partie et profiter</p>

<p>Ce Club est la forme que nous avons choisi de donner à notre communauté et mouvement qui s\'était déjà généré dans plus de 100.000 personnes qui ont fait partie de nos retraites d\'Évolution Intérieure®, les formations de nos différentes écoles et académies, et par l\'équipe de facilitateurs et collaborateurs qui transitons ensemble ce chemin depuis 2012. </p>

<p>Depuis le conglomérat d\'entreprises et plateformes qui se sont créées naturellement autour de cette proposition, vous êtes soutenu dans votre processus d\'évolution intérieure puisque chaque investissement que vous faites est revaloriser par notre monnaie interne, l\'innercoin. Cette communauté, dont l\'économie est une caractéristique fondamentale, vous offre des possibilités infinies. </p>

<p>Nous vous écrirons une série de pas à réaliser pour actualiser votre compte:</p>

<p>Pour accéder à votre compte personnel, vous devez accéder au lien suivant pour ensuite introduire vos identifiants, disponibles ci-dessous:</p>

<p>- Lien pour réviser l\'état de votre compte :  <a href="http://beinapp-wallet.thecloudgroup.tech">http://beinapp-wallet.thecloudgroup.tech</a> </p>

<p>- Nom d\'utilisateur:  <strong>{{email}}</strong> </p>

<p>- Mot de passe: <strong>{{pass}}</strong></p> <br>



<p>En deuxième lieu, nous voulons vous inviter à réaliser votre première consultation médicale gratuite que nous offrons à tous nos membres pour pouvoir mieux vous connaitre et savoir plus de vous. Vous pouvez l\'organiser avec votre conseiller personnel, en sachant que notre équipe médicale est disponible du lundi au vendredi de 10h à 13h et de 16h à 19h (GMT +2 - Madrid).</p>

<p>Le pas suivant à réaliser est de faire le tour de toutes nos plateformes à travers votre conseiller personnel pour connaitre toutes nos propositions. Votre conseiller vous enverra, par le moyen que vous concevez ensemble, toutes les propositions, bénéfices et offres seulement pour les membres qui payent périodiquement. </p>


<p>Ici nous laissons par écrit toutes les opportunités que vous avez au sein de ce mouvement:</p>

<p>Retraites d\'évolution intérieure d\'Inner Mastery: <a href="http://www.innermastery.eu/france">http://www.innermastery.eu </a></p>

<p>Centre d\'études d\'&Eacute;volution Intérieure: <a href="http://www.evolucioninterior.es">http://www.evolucioninterior.es </a></p>

<p>Académie Alverto:  <a href="http://www.academiaalverto.com">http://www.academiaalverto.com</a></p>

<p>Ayahuasca Travels (voyages dans la jungle): <a href="https://www.ayahuascatravels.com/">https://www.ayahuascatravels.com/</a></p>

<p>Entheos Planet (microdoses et conseils): <a href="https://www.entheosplanet.com/">https://www.entheosplanet.com/</a></p>

<p>Red Epicentros (communautés pour y vivre et se former): <a href="https://www.redepicentros.com/">https://www.redepicentros.com/</a></p>

<p>Page d\'Alberto Varela (avec les événements où il est présent et plateforme en ligne pour voir le contenu audiovisuel et blog): <a href="https://albertojosevarela.com/">https://albertojosevarela.com/</a></p>

<p>Documentaire le Sixième Pouvoir :  <a href="https://www.sexto-poder.com">https://www.sexto-poder.com</a> </p><br/>

<p>Rappelez-vous que vous pouvez compter sur l\'accompagnement de votre conseiller pour tout ce que vous vivez et expérimentez au sein de cette nouvelle communauté de personnes ayant les mêmes affinités. </p>

<p>Si vous avez des doutes ou un feedback à donner sur le Club, voici notre mail officiel:  <a href="mailto:admin@beyondinner.com">admin@beyondinner.com</a> </p>

<p>Direction générale du <strong>BEYOND INNER CLUB</strong> </p>
',
'content_en' => '<h2>Hi {{name}} {{last_name}}</h2>
<p>Thank you very much for joining the first international club of internal evolution with internal economy and our own currency Innercoin. 
You will find attached a document with the regulations of the club and as we have already confirmed you as a member, also your membership card !!</p>
<br>
<p>You can enter your club profile at  <a href="https://erpinnermastery.thecloudgroup.tech/">erpinnermastery</a> with the following credentials or also from our own APP looking for BEINAPP.</p>
 <br>
 <p>username: <strong>{{email}}</strong> </p>
 <p>password: <strong>{{pass}}</strong></p> <br>
  
We are very happy that you have joined this movement, now you can contact your personal advisor or  <a href="mailto:admin@beyondinner.com">admin@beyondinner.com</a> for any concerns.
 Kind regards, 
 Club General Management<p/> <br>
<p>Email send by BEYOND INNER -  THE INTERNAL HOLDING (<a  target="_blank" href="https://thecloud.group/desarrollo-de-software-y-programacion-profesional/">https://beyondinner.com</a>)</p>
', 'content_al' => '<h2>Hola {{name}} {{last_name}}</h2>

<p>Willkommen im BeInClub, dem ersten internationalen Club zur Inneren Evolution.</p>

<p>Alles ist bereit zum Eintreten, Dazugehören und Genießen.</p>

<p>Dieser Club ist die Form, die wir gewählt haben, um sie der Gemeinschaft und Bewegung zu geben, die bereits unter mehr als 100.000 Menschen entstanden ist, die Teil unserer Retreats zur Inneren Evolution® waren, und die Teil der Trainings unserer verschiedenen Schulen und Akademien, und des Teams von Facilitatoren und Mitarbeitern sind, die diesen Weg seit 2012 gemeinsam gehen.  </p>

<p>Du wirst du in deinem Prozess der inneren Evolution von dem Konglomerat von Unternehmen und Plattformen, die auf natürliche Weise um diesen Vorschlag herum entstanden sind, unterstützt indem jede Investition, die du tätigst, durch unsere interne Währung, die Innercoin, aufgewertet wird. Diese Gemeinschaft, deren Binnenwirtschaft ein wesentliches Merkmal ist, bietet dir unendlich viele Möglichkeiten. </p>

<p>Wir werden eine Reihe von Schritten beschreiben, die du durchführen musst, um dein Konto auf den neuesten Stand zu bringen:</p>

<p>Um dich in dein persönliches Konto einzuloggen, kannst du auf den folgenden Link zugreifen und dann deine Anmeldedaten eingeben, die unten verfügbar sind:</p>

<p>- Link zur Überprüfung des Kontostands: <a href="http://beinapp-wallet.thecloudgroup.tech">http://beinapp-wallet.thecloudgroup.tech</a> </p>

<p>- Benutzername: <strong>{{email}}</strong> </p>

<p>- Passwort: <strong>{{pass}}</strong></p> <br>



<p>Zweitens möchten wir dich zu deiner ersten kostenlosen medizinischen Beratung einladen, die wir allen unseren Mitgliedern anbieten, um dich besser kennen zu lernen und mehr über dich zu erfahren. Du kannst sie über deinen persönlichen Berater arrangieren; unser medizinisches Team ist von Montag bis Freitag von 10 bis 13 Uhr und von 16 bis 19 Uhr (GMT+2 - Madrid) erreichbar.</p>

<p>Der nächste Schritt ist, dass du mit deinem persönlichen Berater alle unsere Plattformen durchgehst, um alle Angebote kennenzulernen. Dein Berater wird dir auf dem von dir bestimmten Weg alle Vorschläge, Vorteile und Angebote exklusiv für Mitglieder zusenden, die in regelmäßigen Abständen veröffentlicht werden.</p>


<p>Hier beschreiben wir nochmals alle Möglichkeiten, die du innerhalb dieser Bewegung hast:</p>

<p>Inner Mastery&apos;s Inner Evolution Retreats:  <a href="http://www.innermastery.eu">http://www.innermastery.eu </a></p>

<p>Studienzentrum für innere Evolution: <a href="http://www.evolucioninterior.es">http://www.evolucioninterior.es </a></p>

<p>Alverto Akademie: <a href="http://www.academiaalverto.com">http://www.academiaalverto.com</a></p>

<p>Ayahuasca-Reisen (Dschungel-Reisen): <a href="https://www.ayahuascatravels.com/">https://www.ayahuascatravels.com/</a></p>

<p>Entheos Planet (Mikrodosierung und Beratung): <a href="https://www.entheosplanet.com/">https://www.entheosplanet.com/</a></p>

<p>Red Epicentros (Gemeinden, die für Wohnen und Ausbildung zur Verfügung stehen): <a href="https://www.redepicentros.com/">https://www.redepicentros.com/</a></p>

<p>Die Website von Alberto Varela (mit Veranstaltungen, bei denen er anwesend ist, und einer Online-Plattform für audiovisuelle Inhalte und einem Blog): <a href="https://albertojosevarela.com/">https://albertojosevarela.com/</a></p>

<p>Dokumentarfilm Die sechste Macht: <a href="https://www.sexto-poder.com">https://www.sexto-poder.com</a> </p><br/>

<p>Denke daran, dass du bei allem, was du in dieser neuen Gemeinschaft von Gleichgesinnten lebst und erlebst, die persönliche Begleitung deines Beraters hast. </p>

<p>Wenn du Fragen hast oder dem Club Feedback geben möchtest, ist dies unsere offizielle E-Mail: <a href="mailto:admin@beyondinner.com">admin@beyondinner.com</a> </p>

<p><strong>BEYOND INNER CLUB</strong> Allgemeine Adresse</p>
',
    'content_ro' => '<h2>Buna ziua {{name}} {{last_name}}</h2>
<p>Bine ai venit în BeInClub, primul club internațional de Evoluție Interioară.</p>

<p>Totul este gata ca să intri, să aparții și să te bucuri.</p>

<p>Acest Club este modul în care am ales să oferim comunității și mișcării care deja se generaseră cu peste 100.000 de oameni care au făcut parte din retreaturile noastre de Evoluție Interioară®, trainingurile diferitelor noastre școli și academii și de către echipa de facilitatori și colaboratori care suntem în proces de a parcurge acest drum împreună încă din 2012.</p>

<p>Din conglomeratul de companii și platforme care au fost create în mod natural în jurul acestei propuneri, primești susținere în procesul tău de evoluție interioară cu fiecare investiție pe care o faci revalorizată prin moneda noastră internă, InnerCoin. Această comunitate, a cărei economie internă este o caracteristică fundamentală, îți oferă nenumărate oportunități.</p>

<p>Îți vom descrie o serie de pași pe care trebuie să îi urmezi pentru a-ți actualiza contul:</p>

<p>Pentru a-ți introduce contul personal, trebuie să accesezi următorul link și apoi să introduci acreditările, disponibile mai jos:</p>

<p>- Link pentru a verifica starea contului <a href="http://beinapp-wallet.thecloudgroup.tech">http://beinapp-wallet.thecloudgroup.tech</a> </p>

<p>- Utilizator: <strong>{{email}}</strong> </p>

<p>- Parolă: <strong>{{pass}}</strong></p> <br>



<p>În al doilea rând, dorim să te invităm să programezi prima ta consultație medicală, pe care o oferim gratuit tuturor membrilor noștri, pentru a te cunoaște mai bine și a afla mai multe despre tine. Poți realiza programarea prin intermediul consilierului tău personal (persoana ta de contact din cadrul companiei), știind că echipa noastră medicală este disponibilă de luni până vineri între orele 11:00-14:00 și 17:00-20:00.</p>

<p>Următorul pas pe care trebuie să îl urmezi este să faci un tur al tuturor platformelor noastre prin intermediul consilierului tău personal pentru a afla despre toate propunerile noastre. Consilierul tău îți va trimite, prin mijloacele pe care le desemnați împreună cu acesta, toate propunerile, beneficiile și ofertele numai pentru membri care vor apărea periodic.</p>


<p>Aici scriem toate oportunitățile pe care le ai în cadrul acestei mișcări:</p>

<p>Inner Mastery - Retreaturi de Evoluție Interioară cu uz conștient de remedii ancestrale din Amazon: <a href="http://www.innermastery.eu/romania">http://www.innermastery.eu </a></p>

<p>Centrul de studiu Evolución Interior: <a href="http://www.evolucioninterior.es">http://www.evolucioninterior.es </a></p>

<p>Academia Alverto: <a href="http://www.academiaalverto.com">http://www.academiaalverto.com</a></p>

<p>Ayahuasca Travels (călătorii în junglă):  <a href="https://www.ayahuascatravels.com/">https://www.ayahuascatravels.com/</a></p>

<p>Entheos Planet (microdozare și consiliere): <a href="https://www.entheosplanet.com/">https://www.entheosplanet.com/</a></p>

<p>Red Epicentros (comunități disponibile pentru a trăi și a te forma): <a href="https://www.redepicentros.com/">https://www.redepicentros.com/</a></p>

<p>Pagina lui Alberto Varela (cu evenimente în care este prezent și platformă online pentru a vizualiza conținut audiovizual și blog): <a href="https://albertojosevarela.com/">https://albertojosevarela.com/</a></p>

<p>Documentar A Șasea Putere: <a href="https://www.sexto-poder.com">https://www.sexto-poder.com</a> </p><br/>

<p>Ține minte că poți conta pe acompaniamentul personal al consilierului tău pentru tot ceea ce trăiești și experimentezi în această nouă comunitate de oameni.</p>

<p>Dacă ai vreo nelămurire sau feedback pe care ai vrea să oferești Clubului, acesta este e-mailul nostru oficial: <a href="mailto:admin@beyondinner.com">admin@beyondinner.com</a> </p>

<p>Managementul general al <strong>BEYOND INNER CLUB</strong> </p>
',
];
?>
