<?php

/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Sep 02 19:35:13 GMT-04:00 2020  
*@time Wed Sep 02 19:35:13 GMT-04:00 2020  
*/

return [
    // customs
    'logout' => 'site/logout',
    'signup' => 'site/signup',
    'login' => 'site/login',

    //site
    'site/<action:\w+>/<id:\d+>' => 'site/<action>',
    'site/<action:\w+>/<id:\w+>' => 'site/<action>',
    'site/<action:\w+>' => 'site/<action>',

   //modules
    'GET,HEAD,OPTIONS <modules:\w+>/<controller:\w+>' => '<modules>/<controller>',
    'GET,HEAD,OPTIONS <modules:\w+>/<controller:\w+>/<id:\d+>' => '<modules>/<controller>/view',
    'PUT,PATCH,OPTIONS <modules:\w+>/<controller:\w+>/<id:\d+>' => '<modules>/<controller>/update',
    'POST,OPTIONS <modules:\w+>/<controller:\w+>' => '<modules>/<controller>/create',
    'DELETE,OPTIONS <modules:\w+>/<controller:\w+>/<id:\d+>' => '<modules>/<controller>/delete',
    '<modules:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<modules>/<controller>/<action>',];

