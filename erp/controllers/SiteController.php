<?php

namespace erp\controllers;

use common\controllers\SecureController;
use common\components\JwtValidationData;
use common\models\LoginForm;
use common\models\SignupForm;
use common\modules\security\models\Users;
use erp\modules\security\models\Logs;
use Yii;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

class SiteController extends SecureController
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = "";

    public function behaviors()
    {
        $array['authenticator']['except'] = ['login', 'signup', 'error','reset_pass'];
        return ArrayHelper::merge(
            $array,
            parent::behaviors(),
            [
                'cors' => [
                    'class' => Cors::class,
                    #special rules for particular action
                    'actions' => [
                        'your-action-name' => [
                            #web-servers which you alllow cross-domain access
                            'Origin' => ['*'],
                            'Access-Control-Request-Method' => ['POST', 'OPTIONS'],
                            'Access-Control-Request-Headers' => ['*'],
                            'Access-Control-Allow-Credentials' => null,
                            'Access-Control-Max-Age' => 86400,
                            'Access-Control-Expose-Headers' => [],
                        ]
                    ],
                ],
            ]
        );
    }

    /**
     * {@inheritdoc}
     * Asignar a cada accion por el metodo por el que va a salir
     */
    protected function verbs()
    {
        return [
            'logout' => ['POST', 'OPTIONS'],
            'login' => ['POST', 'OPTIONS'],
            'signup' => ['POST', 'OPTIONS'],
            'reset_pass' => ['POST', 'OPTIONS'],
        ];
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new LoginForm();
        if ($model->load(\Yii::$app->getRequest()->getBodyParams(), '') && $model->login()) {
            $token = JwtValidationData::get_token(Yii::$app->getUser()->getId(), Yii::$app->getUser()->identity);
            $this->register_login_logout();
            return ['token' => $token];
        } else {
            Yii::$app->getResponse()->setStatusCode(422);
            return array("error" => "Authenticate Error, username or password");
        }
    }

    public function register_login_logout($is_login = true)
    {
        $user = \Yii::$app->getUser()->identity;
        if ($user) {
            $log = new Logs();
            $log->log_action = $is_login ? "Login" : "Logout";
            $log->log_description = $is_login ? "Entrando al sitio" : "Saliendo del sitio";
            $log->user_id = $user->id_user;
            $log->id_table = $user->id_user;
            $log->name_user = $user->nombre_usuario . ' ' . $user->apellido_usuario;
            $log->table = substr(Users::class, strrpos(Users::class, '\\') + 1, strlen(Users::class));
            $log->created_at = date('Y-m-d h:i:s');
            $log->updated_at = date('Y-m-d h:i:s');
            $log->save();
        }
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new SignupForm();
        if ($model->load(\Yii::$app->getRequest()->getBodyParams(), '')) {
            $user = $model->signup();
            return $user;
        }
        return array('error' => "Data Error");
    }


    public function actionLogout()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $username = Yii::$app->user->identity->username;
        $this->register_login_logout(false);
        Yii::$app->user->logout();
        Yii::$app->getResponse()->setStatusCode(200);
        return ['username' => $username, 'action' => 'logout'];
    }


    public function actionReset_pass()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = \Yii::$app->request->getBodyParams();
        if (count($params) == 0)
            throw new HttpException(500, "Theres no parameters in request");
        if ( !isset($params['email']))
            throw new HttpException(500, "Se necesita el correo del usuario");
        $user=Users::find()->where(['email'=>$params['email']])->one();
        if(!$user)
            throw new HttpException(404, "Usuario no encontrado");
        $model = new SignupForm();
        $result=$model->sendEmail($user);
        return $result;
    }

    public function actionError()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            'statusText' => \Yii::$app->response->statusText,
            'statusCode' => \Yii::$app->response->statusCode,
        ];
    }
}
