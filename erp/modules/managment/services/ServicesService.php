<?php
/**
 * /**Generate by ASGENS
 * @author Charlietyn
 * @date Sat Sep 05 20:15:23 GMT-04:00 2020
 * @time Sat Sep 05 20:15:23 GMT-04:00 2020
 */

namespace erp\modules\managment\services;


use common\services\Services;
use erp\modules\managment\models\Club_member_history;
use erp\modules\managment\models\Services as MServices;
use yii\web\NotFoundHttpException;

class ServicesService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\managment\models\Services';

    public function buy($params, $club_memmber)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $result = [];
        $money_spent = 0;
        $success = true;
        $all_services_name = '';
        $test = isset($params['test']) ? $params['test'] : false;
        try {
            $p = (object)$params['service'];
            $p = (object)$p;
            $service = MServices::find()->where(['service_name' => $p->name])->one();
            if (!$service) {
                \Yii::$app->getResponse()->setStatusCode(404);
                throw new NotFoundHttpException('Service not found.');
            }
            $has_money = $club_memmber->clum_members_currency >= $service->service_total;
            if (!$has_money) {
                $r = new \stdClass();
                $r->success = false;
                $r->errors = 'El miembro con correo ' . $club_memmber->club_members_email . ' no tiene saldo suficiente para efectuar la compra';
                $result[] = $r;
                $success = false;
            }
            if ($success) {
                $money_spent += $service->service_total;
                $club_memmber->clum_members_currency = $club_memmber->clum_members_currency - $service->service_total;
                $club_memmber->save();
                $club_member_history = new Club_member_history();
                $club_member_history->club_member_id = $club_memmber->id_club_members;
                $club_member_history->services_id = $service->id_service;
                $club_member_history->ammount = $service->service_total;
                $club_member_history->created_at = date('Y-m-d h:i:s');
                $club_member_history->updated_at = date('Y-m-d h:i:s');
                $club_member_history->history_description="El usuario " . $club_memmber->club_members_email . ' a comprado el servcio' . $all_services_name . ' a un costo de ' . $money_spent;;
                $result[] = $club_member_history->model_create();
                $all_services_name = $service->service_name;
            }


            if ($success) {
                $r = new \stdClass();
                $r->success = true;
                $r->message = "El usuario " . $club_memmber->club_members_email . ' a comprado el servcio' . $all_services_name . ' a un costo de ' . $money_spent;
                $result[] = $r;
                if (!$test)
                    $transaction->commit();
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
        $result= $this->process_response($result);
        $result['club_member']=$club_memmber;
        return $result;
    }

}
