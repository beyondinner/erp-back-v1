<?php
/**
 * /**Generate by ASGENS
 * @author Charlietyn
 * @date Tue Oct 06 14:40:18 GMT-04:00 2020
 * @time Tue Oct 06 14:40:18 GMT-04:00 2020
 */

namespace erp\modules\managment\services;


use common\services\Services;
use erp\modules\managment\models\Amount_club_member;
use erp\modules\managment\models\Club_members;
use yii\web\NotFoundHttpException;

class Amount_club_memberService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\managment\models\Amount_club_member';

    public function buy($params, $club_memmber)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $result = [];
        $test = isset($params['test']) ? $params['test'] : false;
        try {
            $amount_club_member = new Amount_club_member();
            $amount_club_member->id_club_member = $club_memmber->id_club_members;
            $amount_club_member->id_user = $club_memmber->user->id_user;
            $amount_club_member->amount = $params['amount'];
            $amount_club_member->date_at = date('Y-m-d H:i');
            $amount_club_member->amiunt_description = "Usuario " . $club_memmber->club_members_email . " compró " . $amount_club_member->amount;
            $amount_club_member->type = "S+";
            $amount_club_member->setScenario("buy");
            $r = $amount_club_member->save();
            if ($r)
                $result[] = $amount_club_member->result();
            if (!$test)
                $transaction->commit();

        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
        return $this->process_response($result);
    }

}
