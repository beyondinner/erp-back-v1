<?php
/**
 * /**Generate by ASGENS
 * @author Charlietyn
 * @date Wed Oct 07 18:06:36 GMT-04:00 2020
 * @time Wed Oct 07 18:06:36 GMT-04:00 2020
 */

namespace erp\modules\managment\services;


use common\services\Services;
use erp\modules\managment\models\Event;
use erp\modules\managment\models\Event_participant;
use erp\modules\managment\models\Event_product;

class EventService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\managment\models\Event';

    public function registerTemplate($params)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $result = [];
        try {
            $event = new Event();
            $id_event = $params['id_event'];
            $original_event = Event::findOne($id_event);
            unset($params['id_event']);
            $event->load($params, '');
            $event->event_price = 0;
            $event->created_at = date('Y-m-d h:i:s');
            $result[] = $event->model_create();
            foreach ($original_event->arrayevent_participant as $event_participant) {
                $event_p = new Event_participant();
                $attr = $event_participant->attributes;
                unset($attr['id_event_participant']);
                $event_p->load($attr, '');
                $event_p->id_event = $event->id_event;
                $result[] = $event_p->model_create();
            }
            foreach ($original_event->arrayevent_product as $event_product) {
                $event_p = new Event_product();
                $attr = $event_product->attributes;
                unset($attr['id_event_product']);
                $event_p->load($attr, '');
                $event_p->id_event = $event->id_event;
                $result[] = $event_p->model_create();
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
        $result=$this->process_response($result);
        $result['id_event']=$event->id_event;
        return $result;
    }

}
