<?php
/**
 * /**Generate by ASGENS
 * @author Charlietyn
 * @date Wed Oct 07 18:06:36 GMT-04:00 2020
 * @time Wed Oct 07 18:06:36 GMT-04:00 2020
 */

namespace erp\modules\managment\services;


use common\services\Services;
use erp\modules\managment\models\Event;
use erp\modules\managment\models\Event_actions;

class Event_actionsService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\managment\models\Event_actions';

    public function registerPosponer($params)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $result = [];
        try {
            $event_action = new Event_actions();
            $event_action->load($params, '');
            $result[] = $event_action->model_create();
            $event = Event::findOne($event_action->id_event);
            $event->date_init = date('Y-m-d h:i:s', strtotime($params['date_init']));
            $event->date_end = date('Y-m-d h:i:s', strtotime($params['date_end']));
            $result[] = $event->update_model();
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
        return $this->process_response($result);
    }

    public function registerCancelar($params)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $result = [];
        try {
            foreach ($params['array_model'] as $eventiter) {
                $event_action = new Event_actions();
                $event_action->load($params, '');
                $event_action->id_event=$eventiter['id_event'];
                $result[] = $event_action->model_create();
                $event = Event::findOne($eventiter['id_event']);
                $event->state = "Cancelado";
                $result[] = $event->update_model();
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
        return $this->process_response($result);
    }
}
