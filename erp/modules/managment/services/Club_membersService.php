<?php
/**
 * /**Generate by ASGENS
 * @author Charlietyn
 * @date Sat Sep 05 20:15:23 GMT-04:00 2020
 * @time Sat Sep 05 20:15:23 GMT-04:00 2020
 */

namespace erp\modules\managment\services;


use common\models\User;
use common\modules\security\models\Users;
use common\services\Services;
use erp\modules\managment\models\Amount_club_member;
use erp\modules\managment\models\Club_member_history;
use erp\modules\managment\models\Club_members;
use erp\modules\nomenclatures\models\Adviser;
use Mpdf\Mpdf;
use yii\base\BaseObject;
use yii\web\NotFoundHttpException;

class Club_membersService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\managment\models\Club_members';

    public function registerMember($params)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $result = [];
        try {
            $pass = \Yii::$app->security->generateRandomString(8);
            $user_member = !isset($params['users']['id_user']) ? new Users() : Users::findOne($params['users']['id_user']);
            $user_member->load($params['users'], '');
            $user_member->id_role = 3;
            if (!isset($params['users']['id_user']))
                $user_member->pass = $pass;
            $user_member->status = 'P';
            $result[] = $user_member->model_create();
            $club_member = !isset($params['club_members']['id_club_members']) ? new Club_members() : Club_members::findOne($params['club_members']['id_club_members']);
            $club_member->id_languages = $club_member->id_languages != '' ? $club_member->id_languages : 2;
            $club_member->load($params['club_members'], '');
            $club_member->id_user = $user_member->id_user;
            $club_member->id_user_ref = $params['club_members']['id_adviser'];
            $club_member->created_at = date('Y-m-d h:i:s');
            $club_member->updated_at = date('Y-m-d h:i:s');
            $result[] = $club_member->model_create();
            if (!isset($params['club_members']['id_club_members'])) {
                $amount_history = new Amount_club_member();
                if (\Yii::$app->getUser()->identity)
                    $amount_history->id_user = \Yii::$app->getUser()->identity->getId();
                else {
                    $amount_history->id_user = $params['id_user'];
                }
                $amount_history->id_club_member = $club_member->id_club_members;
                $amount_history->amount = $club_member->clum_members_currency;
                $amount_history->amiunt_description = " Asignando $" . $club_member->clum_members_currency . " en la creación del miembro";
                $amount_history->date_at = date('Y-m-d');
                $amount_history->setScenario('create_member');
                $amount_history->save();
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
        return $this->process_response($result);
    }

    /**
     * @throws \Throwable
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function registerMemberApi($params)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $result = [];
        try {
            $pass = \Yii::$app->security->generateRandomString(8);
            /*Insertando el miembro*/
            $club_members = Club_members::find()->where(['club_members_dni'=>$params['club_members_dni']])->one();
            if (!$club_members)
                $club_members = new Club_members();
            $last_name = '';
            $name = '';

            $array_name = explode(' ', trim($params['club_members_name']));
            if (count($array_name) == 1) {
                $name = $array_name[0];
                $last_name = '--';
            }
            if (count($array_name) == 2) {
                $name = $array_name[0];
                $last_name = $array_name[1];
            }
            if (count($array_name) == 3) {
                $name = array_shift($array_name);
                $last_name = implode(' ', $array_name);
            }
            if (count($array_name) > 3) {
                $name = array_shift($array_name) . ' ' . array_shift($array_name);
                $last_name = implode(' ', $array_name);
            }
            $club_members->club_members_name = $name;
            $club_members->club_member_last_name = $last_name;
            $club_members->club_members_dni = $params['club_members_dni'];
            $club_members->club_members_birthday = $params['club_members_birthday'];
            $club_members->club_members_email = $params['club_members_email'];
            $club_members->club_members_phone = $params['club_members_phone'];
            $club_members->club_members_address = $params['club_members_address'];
            $club_members->created_at = date('Y-m-d h:i:s');
            $club_members->updated_at = date('Y-m-d h:i:s');
            $club_members->clum_members_currency = 0;
            $club_members->club_members_address = '--';
            $club_members->id_country = $params['id_country'];
            $club_members->id_languages = 1;
            $adviser = Users::find()->where(['email' => $params['email-109']])->one();
//            if (!$adviser) {
//                $adviser = new Adviser();
//                $adviser->adviser = $params['email-109'];
//                $adviser->save();
//            }
            if ($adviser) {
                $club_members->id_adviser = $adviser->id_user;
            }
            /*Crear usuario*/
            $user = Users::find()->where(['email' => $club_members->club_members_email])->one();
            if (!$user)
                $user = new Users();
            $user->id_role = 3;
            $user->nombre_usuario = $club_members->club_members_name;
            $user->apellido_usuario = $club_members->club_member_last_name;
            $user->username = $club_members->club_members_email;
            $user->email = $club_members->club_members_email;
            $user->pass = $pass;
            $user->active = 0;
            $user->image = 'user.jpg';
            $user->status = 'P';
            $result[] = $user->model_create();
            $club_members->id_user = $user->id_user;
            $result[] = $club_members->model_create();
            if (!isset($params['test']) || $params['test'] != true) {
                $transaction->commit();
            }
        } catch (\Exception | \Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
        return $this->process_response($result);
    }

    public function updateMemberAmount($member)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $result = [];
        try {
            $sum = 0;
            $history_list = Club_member_history::find()->where(['club_member_id' => $member->id_club_members])->all();
            foreach ($history_list as $hl) {
                if ($hl->services_id || $hl->product_id) {
                    $sum -= $hl->ammount;
                } else
                    $sum += $hl->ammount;
            }
            $member->clum_members_currency = $sum;
            $member->update();
            $result[] = $member->result();
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        return $this->process_response($result);
    }

    public function restartMember($user)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $result = [];
        try {
            $member = new Club_members();
            $member->club_members_name = $user->nombre_usuario;
            $member->club_member_last_name = $user->apellido_usuario;
            $member->club_members_dni = bin2hex(random_bytes(10));
            $member->club_members_birthday = date('Y-m-d');
            $member->club_members_email = $user->email;
            $member->club_members_phone = "---";
            $member->club_members_address = "--";
            $member->club_members_picture = "club_members_picture.jpg";
            $member->created_at = date('Y-m-d H:mm');
            $member->updated_at = date('Y-m-d H:mm');
            $member->club_member_code = bin2hex(random_bytes(10));
            $member->clum_members_currency = 0;
            $member->id_user = $user->id_user;
            $member->id_languages = 2;
            $member->id_adviser = 89;
            $member->id_state = 653;
            $member->id_country = 207;
            $member->postal_code = "--";
            $member->save();
            $member->update();
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
        return $this->process_response($result);
    }

    public function transferMoney($params)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $result = [];
        $test = isset($params['test']) ? $params['test'] : false;
        try {
            $club_memmber_send = Club_members::findOne($params['id']);
            $club_memmber_receive = Club_members::find()->where(['club_members_email' => $params['email_receptor']])->one();
            if (!$club_memmber_send) {
                \Yii::$app->getResponse()->setStatusCode(404);
                throw new NotFoundHttpException('Sender Member not found.');
            }
            if (!$club_memmber_receive) {
                \Yii::$app->getResponse()->setStatusCode(404);
                throw new NotFoundHttpException('Receptor Member not found.');
            }
            $amount = $params['amount'];
            $capacity = $club_memmber_send->clum_members_currency >= $amount;
            if ($capacity) {
                $club_memmber_send->clum_members_currency -= $amount;
                $club_memmber_receive->clum_members_currency += $amount;
                $result[] = $club_memmber_send->model_create();
                $result[] = $club_memmber_receive->model_create();

                /*Registrar recibo*/
                $amount_club_member = new Amount_club_member();
                $amount_club_member->setScenario("transfer_receive");
                $amount_club_member->amount = $amount;
                $amount_club_member->id_club_member = $club_memmber_receive->id_club_members;
                $amount_club_member->id_user = $club_memmber_send->user->id_user;
                $amount_club_member->date_at = date('Y-m-d h:i:s');
                $amount_club_member->type = "TR+";
                $amount_club_member->email_receptor = $club_memmber_receive->club_members_email;
                $amount_club_member->amiunt_description = 'Transferencia de dinero entre cuentas:  usuario ' . $club_memmber_receive->club_members_name . ' ' . $club_memmber_receive->club_member_last_name . '(' . $club_memmber_receive->club_member_code . ') recibió ' . $amount . 'innercoins del usuario ' . $club_memmber_send->club_members_name . ' ' . $club_memmber_send->club_member_last_name . '(' . $club_memmber_send->club_member_code . ')';
                $amount_club_member->save();

                /*Registrar envio*/
                $amount_club_member = new Amount_club_member();
                $amount_club_member->amount = $amount;
                $amount_club_member->setScenario("transfer_send");
                $amount_club_member->id_club_member = $club_memmber_send->id_club_members;
                $amount_club_member->id_user = $club_memmber_send->user->id_user;
                $amount_club_member->date_at = date('Y-m-d h:i:s');
                $amount_club_member->email_receptor = $club_memmber_receive->club_members_email;
                $amount_club_member->type = "TR-";
                $amount_club_member->amiunt_description = 'Transferencia de dinero entre cuentas:  usuario ' . $club_memmber_send->club_members_name . ' ' . $club_memmber_send->club_member_last_name . '(' . $club_memmber_send->club_member_code . ') envió ' . $amount . 'innercoins del usuario ' . $club_memmber_receive->club_members_name . ' ' . $club_memmber_receive->club_member_last_name . '(' . $club_memmber_receive->club_member_code . ')';
                $amount_club_member->save();
                $r = new \stdClass();
                $r->success = true;
                $r->message = "La transferencia fue realizada con exito";
                $result[] = $r;
            }
            if (!$capacity) {
                $r = new \stdClass();
                $r->success = false;
                $r->errors = [];
                $r->message = "No posee la cantidad suficiente para realizar la transferencia";
                $result[] = $r;
            }
            if (!$test)
                $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
        return $this->process_response($result);
    }

    public function sendEmail($club_member)
    {
        $mpdf = new Mpdf(['mode' => '+aCJK', 'autoScriptToLang' => true, 'autoLangToFont' => true]);
        $translate = json_decode(file_get_contents(__DIR__ . '/../../../templates/tarjeta/translate.json'));
        $stylesheet = file_get_contents(__DIR__ . '/../../../templates/tarjeta/tarjeta.css');
        $club_member->club_member_code = $club_member->country->iso2 . 'BEIN' . $club_member->id_club_members;
        $club_member->save();
        $pass = \Yii::$app->security->generateRandomString(8);
        $club_member->user->pass = $pass;
        if (!$club_member->user->image)
            $club_member->user->image = "user.jpg";
        $club_member->user->status = "A";
        $club_member->user->active = true;
        $text = \Yii::$app->params['content_es'];
        $key = array_search(strtolower($club_member->languages->language_siglas), array_column($translate, 'symbol'));
        $welcome = 'Bienvenido a';
        $lang_lower='es';
        if ($club_member->country->iso2 === 'DE') {
            $text = \Yii::$app->params['content_al'];
            $key = array_search(strtolower('DE'), array_column($translate, 'symbol'));
            $welcome = 'Willkommen zu';
            $lang_lower='de';


        }
        if ($club_member->country->iso2 === 'RO') {
            $text = \Yii::$app->params['content_ro'];
            $key = array_search(strtolower('RO'), array_column($translate, 'symbol'));
            $welcome = 'Bun venit la';
            $lang_lower='ro';

        }
        if ($club_member->country->iso2 === 'FR') {
            $text = \Yii::$app->params['content_fr'];
            $key = array_search(strtolower('FR'), array_column($translate, 'symbol'));
            $welcome = 'Bienvenue à';
            $lang_lower='fr';


        }
        if ($club_member->country->iso2 === 'IT') {
            $text = \Yii::$app->params['content_it'];
            $key = array_search(strtolower('IT'), array_column($translate, 'symbol'));
            $welcome = 'Benvenuto a';
            $lang_lower='it';


        }
        if ($club_member->country->iso2 === 'US') {
            $text = \Yii::$app->params['content_en'];
            $key = array_search(strtolower('EN'), array_column($translate, 'symbol'));
            $welcome = 'Welcome to';
            $lang_lower='us';


        }

        $text = str_replace("{{name}}", $club_member->club_members_name, str_replace("{{last_name}}", $club_member->club_member_last_name, str_replace("{{email}}", $club_member->club_members_email, str_replace("{{pass}}", $pass, $text))));
        $r = $club_member->user->save();
        $title = $translate[$key]->text;
        $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
        $mpdf->showImageErrors = true;
        $html = file_get_contents(__DIR__ . '/../../../templates/tarjeta/tarjeta.html');
        $html = str_replace("{{name}}", ucfirst($club_member->club_members_name), $html);
        $html = str_replace("{{number}}", $club_member->club_member_code, $html);
        $html = str_replace("{{titulo}}", $title, $html);
        $mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::HTML_BODY);
        $file = "../pdf/" . $club_member->club_member_code . '.pdf';
        $file_reglamento = "../pdf/reglamento_".$lang_lower.".pdf";
        $mpdf->Output($file, 'F');

//        /*Mandando el correo*/
        $mailer = \Yii::$app->mailer;
        $mailer->htmlLayout = '@common/mail/layouts/html';
        $mailer->textLayout = '@common/mail/layouts/text';
        $club_member->user->save();
        $message = $mailer
            ->compose(
                '@common/mail/layouts/layout_tarjeta', ['content' => $text]
            )
            ->attach($file)
            ->attach($file_reglamento)
            ->setFrom(\Yii::$app->params['adminEmail'])
            ->setTo($club_member->club_members_email)
            ->setSubject($welcome . ' BEINCLUB ' . $club_member->club_members_name . ' ' . $club_member->club_member_last_name . ' | Inner Mastery International');
        $result = $message->send();
        unlink($file);
        return ['success' => $result, 'message' => "Elemento actualizado"];
    }
}
