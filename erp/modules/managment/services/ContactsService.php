<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Sun Sep 06 18:31:24 GMT-04:00 2020  
*@time Sun Sep 06 18:31:24 GMT-04:00 2020  
*/
namespace erp\modules\managment\services;


use common\services\Services;

class ContactsService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\managment\models\Contacts';

}
