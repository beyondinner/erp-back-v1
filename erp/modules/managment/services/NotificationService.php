<?php


namespace erp\modules\managment\services;



use common\services\Services;

class NotificationService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\managment\models\Notification';
}