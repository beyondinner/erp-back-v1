<?php
/**
 * /**Generate by ASGENS
 * @author Charlietyn
 * @date Sat Sep 05 20:15:23 GMT-04:00 2020
 * @time Sat Sep 05 20:15:23 GMT-04:00 2020
 */

namespace erp\modules\managment\services;


use common\services\Services;
use erp\modules\managment\models\Club_member_history;
use erp\modules\managment\models\Club_members;
use erp\modules\managment\models\Products;

class ProductsService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\managment\models\Products';

    public function buy($params, $club_memmber)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $result = [];
        $money_spent = 0;
        $success = true;
        $all_products_name = '';
        $test = isset($params['test']) ? $params['test'] : false;
        try {
            $products = $params['productos'];
            foreach ($products as $p) {
                $p = (object)$p;
                $product = Products::find()->where(['product_name' => $p->name])->one();
                if (!$product) {
                    $product = new Products();
                    $product->product_name = $p->name;
                    $product->product_tax_buy = 0;
                    $product->product_cost = 0;
                    $product->product_subtotal = 0;
                    $product->product_tax_sale = 0;
                    $product->product_total = $p->price;
                    $product->product_stock = -1;
                    $product->product_weigth = 1;
                    $product->product_status_id = 1;
                    $product->product_picture = 'products.jpg';
                    $product->created_at = date('Y-m-d h:i:s');
                    $product->updated_at = date('Y-m-d h:i:s');
                    $result[] = $product->model_create();
                    if ($product->hasErrors()) {
                        $success = false;
                        break;
                    }
                }
                $cant = isset($p->cant) ? $p->cant : 1;
                $capacity = $product->product_stock >-1 ? $product->product_stock >= $cant : true;
                if (!$capacity) {
                    $r = new \stdClass();
                    $r->success = false;
                    $r->errors = 'El producto con id ' . $product->product_name . ' no tiene capacidad en el almacen para comprar';
                    $result[] = $r;
                    $success = false;
                    break;
                }
                $has_money = $club_memmber->clum_members_currency >= $cant * $product->product_total;
                if (!$has_money) {
                    $r = new \stdClass();
                    $r->success = false;
                    $r->errors = 'El miembro con correo ' . $club_memmber->club_members_email . ' no tiene saldo suficiente para efectuar la compra';
                    $result[] = $r;
                    $success = false;
                    break;
                }
                if(!$success)
                    break;
                $money_spent += $cant * $product->product_total;
                $product->product_stock = $product->product_stock - $cant;
                $result[] = $product->update_model();
                $club_memmber->clum_members_currency = $club_memmber->clum_members_currency - $cant * $product->product_total;
                $result[] = $club_memmber->update_model();
                $club_member_history = new Club_member_history();
                $club_member_history->club_member_id = $club_memmber->id_club_members;
                $club_member_history->product_id = $product->id_product;
                $club_member_history->cant_element = $cant;
                $club_member_history->ammount = $cant * $product->product_total;
                $club_member_history->created_at = date('Y-m-d h:i:s');
                $club_member_history->updated_at = date('Y-m-d h:i:s');
                $club_member_history->history_description = "El usuario " . $club_memmber->club_members_email . ' a comprado ' . $product->product_name . ' a un costo de ' . $product->product_total;;
                $result[] = $club_member_history->model_create();
                $all_products_name=$all_products_name.','.$product->product_name;
//                $r = new \stdClass();
//                $r->success = true;
//                $r->message = "El usuario " . $club_memmber->club_members_email . ' a comprado ' . $cant . ' unidades  de ' . $product->product_name;
//                $result[] = $r;
            }


            if ($success) {
                $r = new \stdClass();
                $r->success = true;
                $r->message = "El usuario " . $club_memmber->club_members_email . ' a comprado ' . $all_products_name . ' a un costo de ' . $money_spent;
                $result[] = $r;
                if (!$test)
                    $transaction->commit();
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
        return $this->process_response($result);
    }


}
