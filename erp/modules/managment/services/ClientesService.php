<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Thu Oct 08 21:27:58 GMT-04:00 2020  
*@time Thu Oct 08 21:27:58 GMT-04:00 2020  
*/
namespace erp\modules\managment\services;


use common\services\Services;

class ClientesService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\managment\models\Clientes';

}
