<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Mon Oct 26 00:59:21 GMT-04:00 2020  
*@time Mon Oct 26 00:59:21 GMT-04:00 2020  
*/
namespace erp\modules\managment\services;


use common\services\Services;

class Contact_service_historyService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\managment\models\Contact_service_history';

}
