<?php


namespace erp\modules\managment\services;


use common\services\Services;

class FichaService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\managment\models\Ficha_member';
}