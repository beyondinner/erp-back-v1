<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Fri Oct 09 01:26:07 GMT-04:00 2020  
*@time Fri Oct 09 01:26:07 GMT-04:00 2020  
*/
namespace erp\modules\managment\models;
use Yii;
use common\models\RestModel;

/**
 * Este es la clase modelo para la tabla event_product.
 *
 * Los siguientes son los campos de la tabla 'event_product':
 * @property integer $id_event_product
 * @property date $created_at
 * @property integer $id_event
 * @property integer $id_product
 * @property integer $amount

 * Los siguientes son las relaciones de este modelo :

 * @property Event $event
 * @property Products $product
 */

class Event_product extends RestModel 
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_event_product';

    const MODEL = 'Event_product';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_product';
    }

     
        /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
       const RELATIONS = ['event','product'];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

       const PKEY = 'id_event_product';

     /*
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['id_event','id_product','amount'],'required','on'=>['create','default']],
			[['id_event_product'],'required', 'on' => 'update'],
			[['id_event_product','id_event','id_product','amount'],'integer'],
			[['created_at'],'safe'],
			['created_at','format_created_at'],
			[['id_event_product'], 'unique' , 'on' => 'create'],
        ];
    }
	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_event
     */
	  public function getEvent()
		{
			return $this->hasOne(Event::class, ['id_event' => 'id_event']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_product
     */
	  public function getProduct()
		{
			return $this->hasOne(Products::class, ['id_product' => 'id_product']);
		}

 /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'event_product.id_event_product as id', 'event_product.id_event_product as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('event_product.id_event_product LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
   public function format_created_at(){
        $timestamp = (strpos('/', $this->created_at) > -1) ?
            strtotime(str_replace('/', '-', $this->created_at)) : $this->created_at;
        $this->created_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
}
?>
