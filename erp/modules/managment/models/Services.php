<?php
/**Generate by ASGENS
 * @author Charlietyn
 * @date Sat Sep 05 20:15:23 GMT-04:00 2020
 * @time Sat Sep 05 20:15:23 GMT-04:00 2020
 */
namespace erp\modules\managment\models;

use erp\modules\billings\models\Billing_service_list;
use erp\modules\billings\models\Budget_items_list;
use erp\modules\billings\models\Orders_product;
use mohorev\file\UploadBehavior;
use Yii;
use common\models\RestModel;

use erp\modules\nomenclatures\models\Service_status;

/**
 * Este es la clase modelo para la tabla services.
 *
 * Los siguientes son los campos de la tabla 'services':
 * @property integer $id_service
 * @property string $service_name
 * @property string $service_code
 * @property string $service_desc
 * @property float $service_cost
 * @property float $service_subtotal
 * @property float $service_tax_sale
 * @property float $service_total
 * @property string $service_picture
 * @property integer $service_status_id
 * @property datetime $created_at
 * @property datetime $updated_at
 * Los siguientes son las relaciones de este modelo :
 * @property Service_status $service_status
 * @property Billing_service_list[] $arraybilling_service_list
 * @property Budget_items_list[] $arraybudget_items_list
 * @property Club_member_history[] $arrayclub_member_history
 * @property Contact_service_history[] $arraycontact_service_history
 * @property Orders_product[] $arrayorders_product
 */
class Services extends RestModel
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_service';

    const MODEL = 'Services';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'services';
    }


    /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**
     * The names of the relation tables.
     *
     */
    const RELATIONS = ['service_status','arraybilling_service_list','arraybudget_items_list','arrayclub_member_history','arraycontact_service_history','arrayorders_product'];

    /**
     * @inheritdoc
     */
    function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::class,
                'attribute' => 'service_picture',
                'instanceByName' => true,
                'generateNewName' => true,
                'unlinkOnSave' => true,
                'scenarios' => ['create', 'update', 'default'],
                'path' => '@webroot/assets/services/',
                'url' => '@web/assets/services/',
            ],
        ];
    }

    /**
     * The primary key of the table
     *
     * @var mixed
     */

    const PKEY = 'id_service';

    /*
    * @return \yii\db\Connection the database connection used by this AR class.
    */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['service_name', 'service_code', 'service_cost', 'service_subtotal', 'service_tax_sale', 'service_total', 'service_status_id', 'created_at', 'updated_at'], 'required', 'on' => ['create', 'default']],
            [['id_service'], 'required', 'on' => 'update'],
            [['id_service', 'service_status_id'], 'integer'],
            [['service_cost', 'service_subtotal', 'service_tax_sale', 'service_total'], 'number'],
            [['service_cost', 'service_subtotal', 'service_tax_sale', 'service_total'], 'safe'],
            [['created_at', 'updated_at'], 'safe'],
            ['created_at', 'format_created_at'],
            ['updated_at', 'format_updated_at'],
            ['service_picture', 'file', 'extensions' => '', 'on' => ['create', 'update', 'default']],
            [['service_name', 'service_code'], 'string', 'max' => 255],
            [['service_desc'], 'string', 'max' => 65535],
            [['id_service'], 'unique', 'on' => 'create'],
            [['service_name'], 'unique', 'on' => 'create'],
            [['service_name'], 'unique', 'on' => 'update', 'when' => function ($model, $value) {
                $elem = self::find()->where([$value => $model[$value]])->one();
                return !$elem ? false : $elem[$elem->primaryKey] != $model[$model->primaryKey];
            }],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo service_status_id
     */
    public function getService_status()
    {
        return $this->hasOne(Service_status::class, ['id_service_status' => 'service_status_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_service
     */
    public function getArraybilling_service_list()
    {
        return $this->hasMany(Billing_service_list::class, ['id_service' => 'id_service']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_service
     */
    public function getArraybudget_items_list()
    {
        return $this->hasMany(Budget_items_list::class, ['id_service' => 'id_service']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo services_id
     */
    public function getArrayclub_member_history()
    {
        return $this->hasMany(Club_member_history::class, ['services_id' => 'id_service']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_services
     */
    public function getArraycontact_service_history()
    {
        return $this->hasMany(Contact_service_history::class, ['id_services' => 'id_service']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_services
     */
    public function getArrayorders_product()
    {
        return $this->hasMany(Orders_product::class, ['id_services' => 'id_service']);
    }

    /**
     * Get the list model with select2 schema.
     * @return array|mixed
     * @var $parameters array
     * @var $relation array
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'services.id_service as id', 'services.service_name as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data = $result->data->andWhere('services.service_name LIKE ' . "'%" . $like . "%'")->asArray()->all();
        return $result;
    }

    public function format_created_at()
    {
        $timestamp = (strpos('/', $this->created_at) > -1) ?
            strtotime(str_replace('/', '-', $this->created_at)) : $this->created_at;
        $this->created_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }

    public function format_updated_at()
    {
        $timestamp = (strpos('/', $this->updated_at) > -1) ?
            strtotime(str_replace('/', '-', $this->updated_at)) : $this->updated_at;
        $this->updated_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
}

?>
