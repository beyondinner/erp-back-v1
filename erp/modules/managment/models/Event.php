<?php
/**Generate by ASGENS
 * @author Charlietyn
 * @date Wed Oct 07 18:06:36 GMT-04:00 2020
 * @time Wed Oct 07 18:06:36 GMT-04:00 2020
 */
namespace erp\modules\managment\models;

use mohorev\file\UploadBehavior;
use Yii;
use common\models\RestModel;

use erp\modules\nomenclatures\models\Register_type;
use erp\modules\nomenclatures\models\Event_type;
use erp\modules\nomenclatures\models\Locations;

/**
 * Este es la clase modelo para la tabla event.
 *
 * Los siguientes son los campos de la tabla 'event':
 * @property integer $id_event
 * @property string $event_name
 * @property float $event_price
 * @property boolean $is_all_day
 * @property datetime $event_duration
 * @property string $emails
 * @property string $phones
 * @property integer $id_location
 * @property string $state
 * @property datetime $created_at
 * @property datetime $date_init
 * @property datetime $date_end
 * @property integer $id_register_type
 * @property integer $amount_guess
 * @property integer $id_event_type
 * @property string $documents
 * Los siguientes son las relaciones de este modelo :
 * @property Register_type $register_type
 * @property Event_type $event_type
 * @property Locations $location
 * @property Event_actions[] $arrayevent_actions
 * @property Event_participant[] $arrayevent_participant
 * @property Event_product[] $arrayevent_product
 */
class Event extends RestModel
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_event';

    const MODEL = 'Event';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event';
    }

    function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::class,
                'attribute' => 'documents',
                'instanceByName' => true,
                'generateNewName' => true,
                'unlinkOnSave' => true,
                'scenarios' => ['create', 'update', 'default'],
                'path' => '@webroot/assets/events/',
                'url' => '@web/assets/events/',
            ],
        ];
    }

    /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**
     * The names of the relation tables.
     *
     */
    const RELATIONS = ['register_type', 'event_type', 'location', 'arrayevent_actions'];


    /**
     * The primary key of the table
     *
     * @var mixed
     */

    const PKEY = 'id_event';

    /*
    * @return \yii\db\Connection the database connection used by this AR class.
    */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_name', 'event_price', 'is_all_day', 'amount_guess', 'event_duration', 'emails', 'phones', 'id_location', 'state', 'created_at', 'date_init', 'date_end', 'id_register_type', 'id_event_type'], 'required', 'on' => ['create', 'default']],
            [['id_event'], 'required', 'on' => 'update'],
            [['id_event', 'id_location', 'id_register_type', 'amount_guess', 'id_event_type'], 'integer'],
            [['is_all_day'], 'boolean'],
            [['event_price'], 'number'],
            [['event_price'], 'safe'],
            ['documents', 'file', 'extensions' => '', 'on' => ['create', 'update']],
            [['event_duration', 'created_at', 'date_init', 'date_end'], 'safe'],
            ['event_duration', 'format_event_duration'],
            ['created_at', 'format_created_at'],
            ['date_init', 'format_date_init'],
            ['date_end', 'format_date_end'],
            [['is_all_day'], 'safe'],
            [['event_name'], 'string', 'max' => 255],
            [['emails', 'phones'], 'string', 'max' => 65535],
            [['state'], 'string', 'max' => 20],
            [['id_event'], 'unique', 'on' => 'create'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_register_type
     */
    public function getRegister_type()
    {
        return $this->hasOne(Register_type::class, ['id_register_type' => 'id_register_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_event_type
     */
    public function getEvent_type()
    {
        return $this->hasOne(Event_type::class, ['id_event_type' => 'id_event_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_location
     */
    public function getLocation()
    {
        return $this->hasOne(Locations::class, ['id_location' => 'id_location']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_event
     */
    public function getArrayevent_participant()
    {
        return $this->hasMany(Event_participant::class, ['id_event' => 'id_event']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_event
     */
    public function getArrayevent_product()
    {
        return $this->hasMany(Event_product::class, ['id_event' => 'id_event']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_event
     */
    public function getArrayevent_actions()
    {
        return $this->hasMany(Event_actions::class, ['id_event' => 'id_event']);
    }

    /**
     * Get the list model with select2 schema.
     * @return array|mixed
     * @var $parameters array
     * @var $relation array
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'event.id_event as id', 'event.event_name as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data = $result->data->andWhere('event.event_name LIKE ' . "'%" . $like . "%'")->asArray()->all();
        return $result;

    }

    public function format_event_duration()
    {
        $timestamp = (strpos('/', $this->event_duration) > -1) ?
            strtotime(str_replace('/', '-', $this->event_duration)) : $this->event_duration;
        $this->event_duration = date('Y-m-d h:i:s', strtotime($timestamp));
    }

    public function format_created_at()
    {
        $timestamp = (strpos('/', $this->created_at) > -1) ?
            strtotime(str_replace('/', '-', $this->created_at)) : $this->created_at;
        $this->created_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }

    public function format_date_init()
    {
        $timestamp = (strpos('/', $this->date_init) > -1) ?
            strtotime(str_replace('/', '-', $this->date_init)) : $this->date_init;
        $this->date_init = date('Y-m-d h:i:s', strtotime($timestamp));
    }

    public function format_date_end()
    {
        $timestamp = (strpos('/', $this->date_end) > -1) ?
            strtotime(str_replace('/', '-', $this->date_end)) : $this->date_end;
        $this->date_end = date('Y-m-d h:i:s', strtotime($timestamp));
    }
}

?>
