<?php
/**Generate by ASGENS
 * @author Charlietyn
 * @date Wed Oct 07 18:06:36 GMT-04:00 2020
 * @time Wed Oct 07 18:06:36 GMT-04:00 2020
 */
namespace erp\modules\managment\models;

use erp\modules\billings\models\Billing_product_list;
use erp\modules\billings\models\Budget_items_list;
use erp\modules\billings\models\Orders_product;
use mohorev\file\UploadBehavior;
use Yii;
use common\models\RestModel;

use erp\modules\nomenclatures\models\Product_status;

/**
 * Este es la clase modelo para la tabla products.
 *
 * Los siguientes son los campos de la tabla 'products':
 * @property integer $id_product
 * @property string $product_name
 * @property string $product_desc
 * @property integer $contact_id
 * @property float $product_tax_buy
 * @property float $product_cost
 * @property float $product_subtotal
 * @property float $product_tax_sale
 * @property float $product_total
 * @property float $product_stock
 * @property string $product_sku
 * @property string $product_bar_code
 * @property string $product_factory_code
 * @property float $product_weigth
 * @property integer $product_status_id
 * @property string $product_picture
 * @property datetime $created_at
 * @property datetime $updated_at
 * Los siguientes son las relaciones de este modelo :
 * @property Contacts $contact
 * @property Product_status $product_status
 * @property Club_member_history[] $arrayclub_member_history
 * @property Budget_items_list[] $arraybudget_items_list
 * @property Billing_product_list[] $arraybilling_product_list
 * @property Orders_product[] $arrayorders_product
 * @property Event_product[] $arrayevent_product
 */
class Products extends RestModel
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_product';

    const MODEL = 'Products';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::class,
                'attribute' => 'product_picture',
                'instanceByName' => true,
                'generateNewName' => true,
                'unlinkOnSave' => true,
                'scenarios' => ['create', 'update', 'default'],
                'path' => '@webroot/assets/products/',
                'url' => '@web/assets/products/',
            ]
        ];
    }

    /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**
     * The names of the relation tables.
     *
     */
    const RELATIONS = ['product_status','arraybilling_product_list','arraybudget_items_list','arrayclub_member_history','arrayevent_product','arrayorders_product'];


    /**
     * The primary key of the table
     *
     * @var mixed
     */

    const PKEY = 'id_product';

    /*
    * @return \yii\db\Connection the database connection used by this AR class.
    */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_name', 'product_tax_buy', 'product_cost', 'product_subtotal', 'product_tax_sale', 'product_total', 'product_stock', 'product_weigth', 'product_status_id', 'created_at', 'updated_at'], 'required', 'on' => ['create', 'default']],
            [['id_product'], 'required', 'on' => 'update'],
            [['id_product', 'contact_id', 'product_status_id'], 'integer'],
            [['product_tax_buy', 'product_cost', 'product_subtotal', 'product_tax_sale', 'product_total', 'product_stock', 'product_weigth'], 'number'],
            [['product_tax_buy', 'product_cost', 'product_subtotal', 'product_tax_sale', 'product_total', 'product_stock', 'product_weigth'], 'safe'],
            [['created_at', 'updated_at'], 'safe'],
            ['created_at', 'format_created_at'],
            ['updated_at', 'format_updated_at'],
            [['product_name', 'product_sku', 'product_bar_code', 'product_factory_code'], 'string', 'max' => 255],
            ['product_picture', 'file', 'extensions' => '', 'on' => ['create', 'update']],
            [['product_desc'], 'string', 'max' => 65535],
            [['id_product'], 'unique', 'on' => 'create'],
            [['product_name', 'contact_id'], 'unique', 'targetAttribute' => ['product_name', 'contact_id'], 'on' => ['create', 'default']],
            [['product_name', 'contact_id'], 'unique', 'targetAttribute' => ['product_name', 'contact_id'], 'on' => 'update', 'when' => function ($model) {
                $elem = self::find()->where($model->getAttributes(['product_name', 'contact_id']))->one();
                return !$elem ? false : $elem[$elem->primaryKey] != $model[$model->primaryKey];
            }],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo contact_id
     */
    public function getContact()
    {
        return $this->hasOne(Contacts::class, ['id_contact' => 'contact_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo product_status_id
     */
    public function getProduct_status()
    {
        return $this->hasOne(Product_status::class, ['id_product_status' => 'product_status_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo product_id
     */
    public function getArrayclub_member_history()
    {
        return $this->hasMany(Club_member_history::class, ['product_id' => 'id_product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_product
     */
    public function getArrayorders_product()
    {
        return $this->hasMany(Orders_product::class, ['id_product' => 'id_product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_product
     */
    public function getArraybudget_items_list()
    {
        return $this->hasMany(Budget_items_list::class, ['id_product' => 'id_product']);
    }


    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_product
     */
    public function getArraybilling_product_list()
    {
        return $this->hasMany(Billing_product_list::class, ['id_product' => 'id_product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_product
     */
    public function getArrayevent_product()
    {
        return $this->hasMany(Event_product::class, ['id_product' => 'id_product']);
    }


    /**
     * Get the list model with select2 schema.
     * @return array|mixed
     * @var $parameters array
     * @var $relation array
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'products.id_product as id', 'products.product_name as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data = $result->data->andWhere('products.product_name LIKE ' . "'%" . $like . "%'")->asArray()->all();
        return $result;

    }

    public function format_created_at()
    {
        $timestamp = (strpos('/', $this->created_at) > -1) ?
            strtotime(str_replace('/', '-', $this->created_at)) : $this->created_at;
        $this->created_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }

    public function format_updated_at()
    {
        $timestamp = (strpos('/', $this->updated_at) > -1) ?
            strtotime(str_replace('/', '-', $this->updated_at)) : $this->updated_at;
        $this->updated_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
}

?>
