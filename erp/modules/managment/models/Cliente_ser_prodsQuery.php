<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Thu Oct 08 21:27:58 GMT-04:00 2020  
*@time Thu Oct 08 21:27:58 GMT-04:00 2020  
*/
namespace erp\modules\managment\models;


/** 
*  Esta es  ActiveQuery clase de [[Cliente_ser_prods]].
 *
 * @see Cliente_ser_prods
 */
/**
 * Cliente_ser_prodsQuery representa la clase de Consulta del modelo Cliente_ser_prods
 */
class Cliente_ser_prodsQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Cliente_ser_prods[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Cliente_ser_prods|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

