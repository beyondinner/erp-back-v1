<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Sat Sep 05 20:15:23 GMT-04:00 2020  
*@time Sat Sep 05 20:15:23 GMT-04:00 2020  
*/
namespace erp\modules\managment\models;
use mohorev\file\UploadBehavior;
use Yii;
use common\models\RestModel;

use erp\modules\nomenclatures\models\Payroll_status;
/**
 * Este es la clase modelo para la tabla payrolls.
 *
 * Los siguientes son los campos de la tabla 'payrolls':
 * @property integer $id_payroll
 * @property integer $employee_id
 * @property date $payroll_date
 * @property string $payroll_desc
 * @property string $payroll_concept
 * @property string $payroll_account
 * @property float $payroll_tax
 * @property integer $payroll_total
 * @property string $payroll_tags
 * @property string $payroll_default_account
 * @property string $payroll_attachment
 * @property integer $payroll_status_id
 * @property datetime $created_at
 * @property datetime $updated_at

 * Los siguientes son las relaciones de este modelo :

 * @property Payroll_status $payroll_status
 * @property Employees $employees
 */

class Payrolls extends RestModel 
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_payroll';

    const MODEL = 'Payrolls';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payrolls';
    }

     
        /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
       const RELATIONS = ['payroll_status','employees'];
	   
	 /**
     * @inheritdoc
     */
    function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::class,
                'attribute' => 'payroll_attachment',
                'instanceByName' => true,
                'generateNewName' => true,
                'unlinkOnSave' => true,
                'scenarios' => ['create', 'update', 'default'],
                'path' => '@webroot/assets/payrolls/',
                'url' => '@web/assets/payrolls/',
            ],
        ];
    }



    /**
     * The primary key of the table
     *
     * @var mixed
     */

       const PKEY = 'id_payroll';

     /*
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['id_user','payroll_date','payroll_concept','payroll_account','payroll_default_account'],'required','on'=>['create','default']],
			[['id_payroll'],'required', 'on' => 'update'],
			[['id_payroll','employee_id','payroll_total','payroll_status_id'],'integer'],
			[['payroll_tax'],'number'],
			[['payroll_tax'],'safe'],
			['payroll_attachment', 'file', 'extensions' => '', 'on' => ['create', 'update','default']],
			[['payroll_date','created_at','updated_at'],'safe'],
			['payroll_date','format_payroll_date'],
			['created_at','format_created_at'],
			['updated_at','format_updated_at'],
			[['payroll_desc'], 'string', 'max'=>65535],
			[['payroll_concept','payroll_account','payroll_tags','payroll_default_account'], 'string', 'max'=>255],
			[['id_payroll'], 'unique' , 'on' => 'create'],
            ['employee_id', 'default', 'value' => function () {
                $employee_query = Employees::find()->one();
                return $employee_query->id_employee;
            }],
            ['created_at', 'default', 'value' => date("Y-m-d H:i:s")],
            ['updated_at', 'default', 'value' => date("Y-m-d H:i:s")],
            ['payroll_status_id', 'default', 'value' => 2],
        ];
    }
	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo payroll_status_id
     */
	  public function getPayroll_status()
		{
			return $this->hasOne(Payroll_status::class, ['id_payroll_status' => 'payroll_status_id']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo employee_id
     */
	  public function getEmployees()
		{
			return $this->hasOne(Employees::class, ['id_employee' => 'employee_id']);
		}

 /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'payrolls.id_payroll as id', 'payrolls.payroll_concept as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('payrolls.payroll_concept LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
   public function format_payroll_date(){
        $timestamp = (strpos('/', $this->payroll_date) > -1) ?
            strtotime(str_replace('/', '-', $this->payroll_date)) : $this->payroll_date;
        $this->payroll_date = date('Y-m-d h:i:s', strtotime($timestamp));
    }
   public function format_created_at(){
        $timestamp = (strpos('/', $this->created_at) > -1) ?
            strtotime(str_replace('/', '-', $this->created_at)) : $this->created_at;
        $this->created_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
   public function format_updated_at(){
        $timestamp = (strpos('/', $this->updated_at) > -1) ?
            strtotime(str_replace('/', '-', $this->updated_at)) : $this->updated_at;
        $this->updated_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
}
?>
