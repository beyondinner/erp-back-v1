<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Tue Oct 06 14:40:18 GMT-04:00 2020  
*@time Tue Oct 06 14:40:18 GMT-04:00 2020  
*/
namespace erp\modules\managment\models;


/** 
*  Esta es  ActiveQuery clase de [[Amount_club_member]].
 *
 * @see Amount_club_member
 */
/**
 * Amount_club_memberQuery representa la clase de Consulta del modelo Amount_club_member
 */
class Amount_club_memberQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Amount_club_member[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Amount_club_member|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

