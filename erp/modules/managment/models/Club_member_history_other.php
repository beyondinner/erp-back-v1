<?php

/**
 *@author Jose M. Llorca
 *@date Tuesday july 06 14:32:10 GMT-05:00 2020
 *@time Tuesday july 06 14:32:10 GMT-05:00 2020
 */
namespace erp\modules\managment\models;
use Yii;
use common\models\RestModel;

/**
 * Este es la clase modelo para la tabla club_member_other_history.
 *
 * Los siguientes son los campos de la tabla 'club_member_other_history':
 * @property integer $history_id
 * @property integer $history_type
 * @property integer $history_member_id
 * @property datetime $history_create_at
 * @property datetime $history_update_at
 * @property string $history_description

 *
 * Los siguientes son las relaciones de este modelo :

 * @property Club_members $club_member
 * @property Products $product
 * @property Services $services
 * @property Amount_club_member $amount_club_member
 */



class Club_member_history_other extends RestModel
{
    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;


    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'history_id';

    const MODEL = 'Club_member_history_other';


    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'club_member_other_history';
    }

    /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];

    /**

     * The names of the relation tables.
     *
     */
    const RELATIONS = ['club_member'];

    /**
     * The primary key of the table
     *
     * @var mixed
     */

    const PKEY = 'history_id';

    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'club_member_other_history.history_member_id as id', 'club_member_other_history.history_member_id as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data = $result->data->andWhere('club_member_other_history.history_member_id LIKE ' . "'%" . $like . "%'")->asArray()->all();
        return $result;
    }

    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    public function rules()
    {
        return [
            [['history_member_id','history_create_at','history_update_at', 'history_description'],'required','on'=>['create','default']],
            ['history_type', 'default', 'value' => 0],
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo club_member_id
     */
    public function getClub_member()
    {
        return $this->hasOne(Club_members::class,  ['id_club_members' => 'history_member_id']);
    }


    public function format_created_at(){
        $timestamp = str_replace('/', '-', $this->history_create_at);
        $this->history_create_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
    public function format_updated_at(){
        $timestamp = str_replace('/', '-', $this->history_update_at);
        $this->history_update_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }

}