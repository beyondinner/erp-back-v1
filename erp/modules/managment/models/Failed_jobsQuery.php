<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Thu Oct 08 21:27:58 GMT-04:00 2020  
*@time Thu Oct 08 21:27:58 GMT-04:00 2020  
*/
namespace erp\modules\managment\models;


/** 
*  Esta es  ActiveQuery clase de [[Failed_jobs]].
 *
 * @see Failed_jobs
 */
/**
 * Failed_jobsQuery representa la clase de Consulta del modelo Failed_jobs
 */
class Failed_jobsQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Failed_jobs[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Failed_jobs|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

