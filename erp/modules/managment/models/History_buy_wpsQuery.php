<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Sat Oct 31 01:01:55 GMT-04:00 2020  
*@time Sat Oct 31 01:01:55 GMT-04:00 2020  
*/
namespace erp\modules\managment\models;


/** 
*  Esta es  ActiveQuery clase de [[History_buy_wps]].
 *
 * @see History_buy_wps
 */
/**
 * History_buy_wpsQuery representa la clase de Consulta del modelo History_buy_wps
 */
class History_buy_wpsQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return History_buy_wps[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return History_buy_wps|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

