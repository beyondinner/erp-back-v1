<?php
/**Generate by ASGENS
 *@author Charlietyn
 *@date Mon Oct 26 01:28:52 GMT-04:00 2020
 *@time Mon Oct 26 01:28:52 GMT-04:00 2020
 */
namespace erp\modules\managment\models;
use Yii;
use common\models\RestModel;

/**
 * Este es la clase modelo para la tabla contact_service_history.
 *
 * Los siguientes son los campos de la tabla 'contact_service_history':
 * @property integer $id_contact_service_history
 * @property integer $id_contact
 * @property integer $id_services
 * @property string $contact_service_history_desc
 * @property integer $type
 * @property datetime $created_at
 * @property datetime $updated_at
 * @property float $amount

 * Los siguientes son las relaciones de este modelo :

 * @property Contacts $contact
 * @property Services $services
 */

class Contact_service_history extends RestModel
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_contact_service_history';

    const MODEL = 'Contact_service_history';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact_service_history';
    }


    /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
    const RELATIONS = ['contact','services'];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

    const PKEY = 'id_contact_service_history';

    /*
    * @return \yii\db\Connection the database connection used by this AR class.
    */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_contact'],'required','on'=>['create','default']],
            [['id_contact_service_history'],'required', 'on' => 'update'],
            [['id_contact_service_history','id_contact','id_services','type'],'integer'],
            [['amount'],'number'],
            [['amount'],'safe'],
            [['created_at','updated_at'],'safe'],
            ['created_at','format_created_at'],
            ['updated_at','format_updated_at'],
            [['contact_service_history_desc'], 'string', 'max'=>65535],
            [['id_contact_service_history'], 'unique' , 'on' => 'create'],
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_contact
     */
    public function getContact()
    {
        return $this->hasOne(Contacts::class, ['id_contact' => 'id_contact']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_services
     */
    public function getServices()
    {
        return $this->hasOne(Services::class, ['id_service' => 'id_services']);
    }

    /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'contact_service_history.id_contact_service_history as id', 'contact_service_history.id_contact_service_history as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('contact_service_history.id_contact_service_history LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
    public function format_created_at(){
        $timestamp = str_replace('/', '-', $this->created_at);
        $this->created_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
    public function format_updated_at(){
        $timestamp = str_replace('/', '-', $this->updated_at);
        $this->updated_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
}
?>
