<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Mon Oct 26 00:59:21 GMT-04:00 2020  
*@time Mon Oct 26 00:59:21 GMT-04:00 2020  
*/
namespace erp\modules\managment\models;


/** 
*  Esta es  ActiveQuery clase de [[Contact_service_history]].
 *
 * @see Contact_service_history
 */
/**
 * Contact_service_historyQuery representa la clase de Consulta del modelo Contact_service_history
 */
class Contact_service_historyQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Contact_service_history[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Contact_service_history|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

