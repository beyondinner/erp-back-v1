<?php


namespace erp\modules\managment\models;


class Ficha_member extends \common\models\RestModel
{
    /**
     * @var mixed|null
     */
    private $formName;

    public static function tableName()
    {
        return 'form_ficha_name';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['form_name'], 'required', 'on' => ['create', 'default']],
            [['form_id','label_name'], 'required', 'on' => 'update'],
        ];
    }
}