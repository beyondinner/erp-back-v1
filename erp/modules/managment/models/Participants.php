<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Thu Oct 29 16:44:52 GMT-04:00 2020  
*@time Thu Oct 29 16:44:52 GMT-04:00 2020  
*/
namespace erp\modules\managment\models;
use Yii;
use common\models\RestModel;

use erp\modules\nomenclatures\models\Payment_types;
/**
 * Este es la clase modelo para la tabla participants.
 *
 * Los siguientes son los campos de la tabla 'participants':
 * @property integer $id_participant
 * @property string $participant_name
 * @property string $apellidos
 * @property string $participant_email
 * @property string $participant_phone
 * @property integer $id_payment_types
 * @property integer $id_contact
 * @property integer $id_club_member

 * Los siguientes son las relaciones de este modelo :

 * @property Payment_types $payment_types
 * @property Contacts $contact
 * @property Club_members $club_member
 * @property Event_participant[] $arrayevent_participant
 */

class Participants extends RestModel 
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_participant';

    const MODEL = 'Participants';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'participants';
    }

     
        /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
       const RELATIONS = ['payment_types','contact','club_member','arrayevent_participant'];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

       const PKEY = 'id_participant';

     /*
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['id_participant'],'required', 'on' => 'update'],
			[['id_participant','id_payment_types','id_contact','id_club_member'],'integer'],
			[['participant_name'], 'string', 'max'=>50],
			[['apellidos'], 'string', 'max'=>100],
			[['participant_email'], 'string', 'max'=>80],
			[['participant_phone'], 'string', 'max'=>20],
			[['id_participant'], 'unique' , 'on' => 'create'],
        ];
    }
	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_payment_types
     */
	  public function getPayment_types()
		{
			return $this->hasOne(Payment_types::class, ['id_payment_type' => 'id_payment_types']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_contact
     */
	  public function getContact()
		{
			return $this->hasOne(Contacts::class, ['id_contact' => 'id_contact']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_club_member
     */
	  public function getClub_member()
		{
			return $this->hasOne(Club_members::class, ['id_club_members' => 'id_club_member']);
		}


	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_participant
     */
	  public function getArrayevent_participant()
		{
			return $this->hasMany(Event_participant::class, ['id_participant' => 'id_participant']);
		}
 /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'participants.id_participant as id', 'concat_ws(\' \', participant_name, apellidos) as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('participants.participant_name LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
}
?>
