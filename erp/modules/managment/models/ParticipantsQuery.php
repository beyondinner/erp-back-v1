<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Thu Oct 08 21:27:58 GMT-04:00 2020  
*@time Thu Oct 08 21:27:58 GMT-04:00 2020  
*/
namespace erp\modules\managment\models;


/** 
*  Esta es  ActiveQuery clase de [[Participants]].
 *
 * @see Participants
 */
/**
 * ParticipantsQuery representa la clase de Consulta del modelo Participants
 */
class ParticipantsQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Participants[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Participants|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

