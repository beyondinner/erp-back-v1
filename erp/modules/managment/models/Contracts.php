<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Sat Sep 05 20:15:23 GMT-04:00 2020  
*@time Sat Sep 05 20:15:23 GMT-04:00 2020  
*/
namespace erp\modules\managment\models;
use mohorev\file\UploadBehavior;
use Yii;
use common\models\RestModel;

use erp\modules\nomenclatures\models\Contract_types;
use erp\modules\nomenclatures\models\Payment_types;
use erp\modules\nomenclatures\models\Contract_payment_unities;
use erp\modules\nomenclatures\models\Contract_status;
/**
 * Este es la clase modelo para la tabla contracts.
 *
 * Los siguientes son los campos de la tabla 'contracts':
 * @property integer $id_contract
 * @property integer $employee_id
 * @property date $contract_start_date
 * @property date $contract_end_date
 * @property string $contract_title
 * @property integer $contract_type_id
 * @property integer $payment_type_id
 * @property integer $contract_payment_unity_id
 * @property float $contract_net_salary
 * @property integer $contract_payment_quantity
 * @property integer $contract_work_hours
 * @property integer $contract_days_per_week
 * @property string $contract_attachment
 * @property integer $contract_status_id
 * @property datetime $created_at
 * @property datetime $updated_at

 * Los siguientes son las relaciones de este modelo :

 * @property Employees $employees
 * @property Contract_types $contract_types
 * @property Payment_types $payment_types
 * @property Contract_payment_unities $contract_payment_unities
 * @property Contract_status $contract_status
 */

class Contracts extends RestModel 
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_contract';

    const MODEL = 'Contracts';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contracts';
    }

     
        /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
       const RELATIONS = ['employees','contract_types','payment_types','contract_payment_unities','contract_status'];
	
	/**
     * @inheritdoc
     */
    function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::class,
                'attribute' => 'contract_attachment',
                'instanceByName' => true,
                'generateNewName' => true,
                'unlinkOnSave' => true,
                'scenarios' => ['create', 'update', 'default'],
                'path' => '@webroot/assets/contracts/',
                'url' => '@web/assets/contracts/',
            ],
        ];
    }



    /**
     * The primary key of the table
     *
     * @var mixed
     */

       const PKEY = 'id_contract';

     /*
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['employee_id','contract_start_date','contract_end_date','contract_title','contract_type_id','payment_type_id','contract_payment_unity_id','contract_net_salary','contract_payment_quantity','contract_work_hours','contract_days_per_week','contract_status_id','created_at','updated_at'],'required','on'=>['create','default']],
			[['id_contract'],'required', 'on' => 'update'],
			[['id_contract','employee_id','contract_type_id','payment_type_id','contract_payment_unity_id','contract_payment_quantity','contract_work_hours','contract_days_per_week','contract_status_id'],'integer'],
			[['contract_net_salary'],'number'],
			[['contract_net_salary'],'safe'],
			['contract_attachment', 'file', 'extensions' => '', 'on' => ['create', 'update','default']],
			[['contract_start_date','contract_end_date','created_at','updated_at'],'safe'],
			['contract_start_date','format_contract_start_date'],
			['contract_end_date','format_contract_end_date'],
			['created_at','format_created_at'],
			['updated_at','format_updated_at'],
			[['contract_title'], 'string', 'max'=>255],
			[['id_contract'], 'unique' , 'on' => 'create'],
        ];
    }
	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo employee_id
     */
	  public function getEmployees()
		{
			return $this->hasOne(Employees::class, ['id_employee' => 'employee_id']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo contract_type_id
     */
	  public function getContract_types()
		{
			return $this->hasOne(Contract_types::class, ['id_contract_type' => 'contract_type_id']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo payment_type_id
     */
	  public function getPayment_types()
		{
			return $this->hasOne(Payment_types::class, ['id_payment_type' => 'payment_type_id']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo contract_payment_unity_id
     */
	  public function getContract_payment_unities()
		{
			return $this->hasOne(Contract_payment_unities::class, ['id_contract_payment_unity' => 'contract_payment_unity_id']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo contract_status_id
     */
	  public function getContract_status()
		{
			return $this->hasOne(Contract_status::class, ['id_contract_status' => 'contract_status_id']);
		}

 /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'contracts.id_contract as id', 'contracts.contract_title as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('contracts.contract_title LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
   public function format_contract_start_date(){
        $timestamp = (strpos('/', $this->contract_start_date) > -1) ?
            strtotime(str_replace('/', '-', $this->contract_start_date)) : $this->contract_start_date;
        $this->contract_start_date = date('Y-m-d h:i:s', strtotime($timestamp));
    }
   public function format_contract_end_date(){
        $timestamp = (strpos('/', $this->contract_end_date) > -1) ?
            strtotime(str_replace('/', '-', $this->contract_end_date)) : $this->contract_end_date;
        $this->contract_end_date = date('Y-m-d h:i:s', strtotime($timestamp));
    }
   public function format_created_at(){
        $timestamp = (strpos('/', $this->created_at) > -1) ?
            strtotime(str_replace('/', '-', $this->created_at)) : $this->created_at;
        $this->created_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
   public function format_updated_at(){
        $timestamp = (strpos('/', $this->updated_at) > -1) ?
            strtotime(str_replace('/', '-', $this->updated_at)) : $this->updated_at;
        $this->updated_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
}
?>
