<?php


namespace erp\modules\managment\models;

use common\models\User;
use common\modules\security\models\Users;
use Yii;
use common\models\RestModel;
/**
 * Este es la clase modelo para la tabla club_member_history.
 *
 * Los siguientes son los campos de la tabla 'notifications':
 * @property integer $not_id
 * @property string $not_desc
 * @property integer $not_id_user
 * @property integer $not_read
 * @property datetime $not_date

 * Los siguientes son las relaciones de este modelo :

 * @property Users $users
 *
 **/

class Notification extends RestModel
{
    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'not_id';

    const MODEL = 'Notification';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notifications';
    }


    /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];


    /**
     *
     * The names of the relation tables.
     *
     */
    const RELATIONS = ['users'];


    /**
     * The primary key of the table
     *
     * @var mixed
     */

    const PKEY = 'not_id';


    /*
     *
     *
     * @return \yii\db\Connection the database connection used by this AR class.
     *
     *
    */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return[
            [['not_desc','not_id_user','not_read','not_date'],'required','on'=>['create','default']],
            [['not_date'],'safe'],
            [['not_id'], 'unique' , 'on' => 'create'],
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo not_id_user
     */
    public function getUsers()
    {
        return $this->hasOne(User::class, ['id_user' => 'not_id_user']);
    }

    /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'notifications.not_id_user as id', 'notifications.not_id_user as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('notifications.not_id_user LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }

    public function format_created_at(){
        $timestamp = str_replace('/', '-', $this->created_at);
        $this->created_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
    public function format_updated_at(){
        $timestamp = str_replace('/', '-', $this->updated_at);
        $this->updated_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }

}