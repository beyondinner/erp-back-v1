<?php
/**Generate by ASGENS
 *@author Charlietyn
 *@date Sat Nov 07 16:32:10 GMT-05:00 2020
 *@time Sat Nov 07 16:32:10 GMT-05:00 2020
 */
namespace erp\modules\managment\models;
use Yii;
use common\models\RestModel;

/**
 * Este es la clase modelo para la tabla club_member_history.
 *
 * Los siguientes son los campos de la tabla 'club_member_history':
 * @property integer $id_club_member_history
 * @property integer $club_member_id
 * @property float $ammount
 * @property integer $product_id
 * @property datetime $created_at
 * @property datetime $updated_at
 * @property string $history_description
 * @property integer $services_id
 * @property integer $id_amount_club_member
 * @property integer $cant_element

 * Los siguientes son las relaciones de este modelo :

 * @property Club_members $club_member
 * @property Products $product
 * @property Services $services
 * @property Amount_club_member $amount_club_member
 */

class Club_member_history extends RestModel
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_club_member_history';

    const MODEL = 'Club_member_history';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'club_member_history';
    }


    /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
    const RELATIONS = ['club_member','product','services','amount_club_member'];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

    const PKEY = 'id_club_member_history';

    /*
    * @return \yii\db\Connection the database connection used by this AR class.
    */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['club_member_id','ammount','created_at','updated_at'],'required','on'=>['create','default']],
            [['id_club_member_history'],'required', 'on' => 'update'],
            [['id_club_member_history','club_member_id','product_id','services_id','id_amount_club_member','cant_element'],'integer'],
            [['created_at','updated_at'],'safe'],
            [['ammount'], 'number'],
            [['ammount'],'safe'],
            ['created_at','format_created_at'],
            ['updated_at','format_updated_at'],
            ['cant_element', 'default', 'value' => 1],
            [['history_description'], 'string', 'max'=>65535],
            [['id_club_member_history'], 'unique' , 'on' => 'create'],
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo club_member_id
     */
    public function getClub_member()
    {
        return $this->hasOne(Club_members::class, ['id_club_members' => 'club_member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo product_id
     */
    public function getProduct()
    {
        return $this->hasOne(Products::class, ['id_product' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo services_id
     */
    public function getServices()
    {
        return $this->hasOne(Services::class, ['id_service' => 'services_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_amount_club_member
     */
    public function getAmount_club_member()
    {
        return $this->hasOne(Amount_club_member::class, ['id_amount_club_member' => 'id_amount_club_member']);
    }

    /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'club_member_history.id_club_member_history as id', 'club_member_history.id_club_member_history as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('club_member_history.id_club_member_history LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
    public function format_created_at(){
        $timestamp = str_replace('/', '-', $this->created_at);
        $this->created_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
    public function format_updated_at(){
        $timestamp = str_replace('/', '-', $this->updated_at);
        $this->updated_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
}
?>
