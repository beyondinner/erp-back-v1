<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Sat Sep 05 20:15:23 GMT-04:00 2020  
*@time Sat Sep 05 20:15:23 GMT-04:00 2020  
*/
namespace erp\modules\managment\models;


/** 
*  Esta es  ActiveQuery clase de [[Club_member_history]].
 *
 * @see Club_member_history
 */
/**
 * Club_member_historyQuery representa la clase de Consulta del modelo Club_member_history
 */
class Club_member_historyQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Club_member_history[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Club_member_history|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

