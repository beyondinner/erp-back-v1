<?php


namespace erp\modules\managment\models;
use Yii;
use common\models\RestModel;

/**
 * Class Ficha_member_label
 * @package erp\modules\managment\models
 *
 * @property integer $label_id
 * @property integer $label_form_id
 * @property string $label_name
 */

class Ficha_member_label extends RestModel
{
    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'label_id';

    const MODEL = 'Ficha_member_label';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form_ficha_label';
    }

    /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];

    /**
     * The names of the relation tables.
     *
     */
    const RELATIONS = ['Form'];

    /**
     * The primary key of the table
     *
     * @var mixed
     */

    const PKEY = 'label_id';

    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['label_form_id','label_name'], 'required', 'on' => ['create', 'default']],
            [['label_form_id','label_name'], 'required', 'on' => 'update'],
        ];
    }

    /**
     * Get the list model with select2 schema.
     * @return array|mixed
     * @var $parameters array
     * @var $relation array
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'form_ficha_label.label_id as id', 'form_ficha_label.label_name as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data = $result->data->andWhere('form_ficha_label.label_form_id LIKE ' . "'%" . $like . "%'")->asArray()->all();
        return $result;

    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_user
     */
    public function getForm()
    {
        return $this->hasOne(Ficha_member::class, ['form_id' => 'label_form_id']);
    }
}