<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Thu Oct 08 21:27:58 GMT-04:00 2020  
*@time Thu Oct 08 21:27:58 GMT-04:00 2020  
*/
namespace erp\modules\managment\models;
use Yii;
use common\models\RestModel;

use erp\modules\nomenclatures\models\Payment_types;
/**
 * Este es la clase modelo para la tabla event_participant.
 *
 * Los siguientes son los campos de la tabla 'event_participant':
 * @property integer $id_event_participant
 * @property integer $id_event
 * @property integer $id_participant
 * @property integer $days
 * @property boolean $consume_products
 * @property integer $id_payment_types

 * Los siguientes son las relaciones de este modelo :

 * @property Event $event
 * @property Participants $participant
 * @property Payment_types $payment_types
 */

class Event_participant extends RestModel 
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_event_participant';

    const MODEL = 'Event_participant';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_participant';
    }

     
        /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
       const RELATIONS = ['event','participant','payment_types'];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

       const PKEY = 'id_event_participant';

     /*
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['id_event','id_participant','days','consume_products','id_payment_types'],'required','on'=>['create','default']],
			[['id_event_participant'],'required', 'on' => 'update'],
			[['id_event_participant','id_event','id_participant','days','id_payment_types'],'integer'],
			[['consume_products'],'boolean'],
			[['consume_products'],'safe'],
			[['id_event_participant'], 'unique' , 'on' => 'create'],
        ];
    }
	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_event
     */
	  public function getEvent()
		{
			return $this->hasOne(Event::class, ['id_event' => 'id_event']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_participant
     */
	  public function getParticipant()
		{
			return $this->hasOne(Participants::class, ['id_participant' => 'id_participant']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_payment_types
     */
	  public function getPayment_types()
		{
			return $this->hasOne(Payment_types::class, ['id_payment_type' => 'id_payment_types']);
		}

 /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'event_participant.id_event_participant as id', 'event_participant.id_event_participant as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('event_participant.id_event_participant LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
}
?>
