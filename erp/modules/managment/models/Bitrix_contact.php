<?php


namespace erp\modules\managment\models;
/**
 * Class Bitrix_contact
 * @package erp\modules\managment\models
 * @property int $bitrix_contact_id [int]  id de esta tabla
 * @property int $bitrix_id [int]  id del contacto delCRM
 * @property int $bitrix_user_id [int]  id del usuario del ERP
 * @property string $bitrix_user_name [varchar(200)]
 * @property string $bitrix_user_second_name [varchar(200)]
 * @property string $bitrix_user_last_name [varchar(200)]
 * @property int $bitrix_user_lead_id [int]
 * @property int $bitrix_user_has_phone [int]
 * @property int $bitrix_user_has_email [int]
 * @property string $bitrix_user_date_create [date]
 * @property int $bitrix_user_phone [int]
 * @property string $bitrix_user_email [varchar(200)]
 */

class Bitrix_contact extends \common\models\RestModel
{

    public static function tableName(): string
    {
        return 'bitrix_contact';
    }


}