<?php


namespace erp\modules\managment\models;

use mohorev\file\UploadBehavior;
use Yii;

/**
 * Class Ficha_inputs
 * @package erp\modules\managment\models
 *
 * @property integer $val_id
 * @property integer $val_form_ficha_label_id
 * @property integer $val_label_form_id
 * @property string $val_text
 * @property int $val_id_user
 * @property datetime $val_created [datetime]
 *
 */

class Ficha_inputs extends \common\models\RestModel
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'val_id';

    const MODEL = 'Ficha_inputs';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form_ficha_label_val';
    }


    /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];

    /**
     * The names of the relation tables.
     *
     */
    const RELATIONS = ['club_member'];

    /**
     * The primary key of the table
     *
     * @var mixed
     */

    const PKEY = 'val_id';

    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['val_label_form_id','val_text','val_id_user'], 'required', 'on' => ['create', 'default']],
            [['val_id'], 'required', 'on' => 'update'],
            ['val_created', 'default', 'value' => date("Y-m-d H:i:s")],
        ];
    }

    /**
     * Get the list model with select2 schema.
     * @return array|mixed
     * @var $parameters array
     * @var $relation array
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'form_ficha_label_val.val_id as id', 'form_ficha_label_val.val_text as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data = $result->data->andWhere('form_ficha_label_val.val_text LIKE ' . "'%" . $like . "%'")->asArray()->all();
        return $result;

    }
}