<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Sat Oct 31 01:01:55 GMT-04:00 2020  
*@time Sat Oct 31 01:01:55 GMT-04:00 2020  
*/
namespace erp\modules\managment\models;
use Yii;
use common\models\RestModel;

/**
 * Este es la clase modelo para la tabla history_buy_wps.
 *
 * Los siguientes son los campos de la tabla 'history_buy_wps':
 * @property integer $id
 * @property string $email
 * @property string $products
 * @property datetime $created_at
 * @property datetime $updated_at

 * Los siguientes son las relaciones de este modelo :

 */

class History_buy_wps extends RestModel 
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id';

    const MODEL = 'History_buy_wps';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'history_buy_wps';
    }

     
        /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
       const RELATIONS = [];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

       const PKEY = 'id';

     /*
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['email','products'],'required','on'=>['create','default']],
			[['id'],'required', 'on' => 'update'],
			[['id'],'integer'],
			[['created_at','updated_at'],'safe'],
			['created_at','format_created_at'],
			['updated_at','format_updated_at'],
			[['email','products'], 'string', 'max'=>255],
			[['id'], 'unique' , 'on' => 'create'],
        ];
    }
 /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'history_buy_wps.id as id', 'history_buy_wps.email as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('history_buy_wps.email LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
   public function format_created_at(){
        $timestamp = str_replace('/', '-', $this->created_at);
        $this->created_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
   public function format_updated_at(){
        $timestamp = str_replace('/', '-', $this->updated_at);
        $this->updated_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
}
?>
