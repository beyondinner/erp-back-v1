<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Sat Sep 05 20:15:23 GMT-04:00 2020  
*@time Sat Sep 05 20:15:23 GMT-04:00 2020  
*/
namespace erp\modules\managment\models;


/** 
*  Esta es  ActiveQuery clase de [[Contracts]].
 *
 * @see Contracts
 */
/**
 * ContractsQuery representa la clase de Consulta del modelo Contracts
 */
class ContractsQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Contracts[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Contracts|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

