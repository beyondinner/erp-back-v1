<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Oct 07 18:06:36 GMT-04:00 2020  
*@time Wed Oct 07 18:06:36 GMT-04:00 2020  
*/
namespace erp\modules\managment\controllers;

use common\controllers\SecureController;
use erp\modules\managment\models\Event_product;

class Event_product_soapController extends SecureController
{

    /** @var bool */
    public $enableCsrfValidation = false;

    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\managment\models\Event_product';


    public function behaviors()
    {
        $array = parent::behaviors();
        $array['authenticator']['except'] = ['event_product_data'];
        return $array;
    }
    /**
     * {@inheritdoc}
     * redefine las acciones restful de la controladora
     */
    public function actions()
    {
        return [
            'event_product_data' => [
                'class' => 'mongosoft\soapserver\Action',
                'classMap' => ['Event_product' => 'erp\modules\managment\models\Event_product'],
                'serviceOptions' => [
                    'disableWsdlMode' => true,
                ]
            ],
        ];
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        parent::checkAccess($action, $model, $params); // TODO: Change the autogenerated stub
    }


    public function encode_result($result)
    {
        $encode = false;
        if (strpos(\Yii::$app->getRequest()->contentType, "application/json") !== false)
            $encode = true;
        if ($encode)
            return json_encode($result, JSON_UNESCAPED_UNICODE);
        return $result;
    }

    /**
     * Simple test which returns a List of event_product in order to see how the wsdl pans out
     * @param [] $params
     * @return object[]
     * @soap
     */

    public function event_product_list()
    {
        $params=[];
        if (func_get_args())
            $params = func_get_args()[0]['params'];
        return $this->encode_result(Event_product::list_model($params));
    }

    /**
     * Simple test which returns a data of event_product  with id in order to see how the wsdl pans out
     * @param int $id
     * @return erp\modules\managment\models\Event_product
     * @soap
     */
    public function event_product_view($id)
    {
        $params=[];
        if (func_get_args())
            $params = func_get_args()[0]['params'];
        return $this->encode_result(Event_product::view($id, $params));
    }
}
