<?php
/**
 * /**Generate by ASGENS
 * @author Charlietyn
 * @date Tue Oct 06 14:40:18 GMT-04:00 2020
 * @time Tue Oct 06 14:40:18 GMT-04:00 2020
 */

namespace erp\modules\managment\controllers;


use common\controllers\RestController;
use erp\modules\managment\models\Club_members;
use erp\modules\managment\services\Amount_club_memberService;
use yii\helpers\Url;
use yii\filters\Cors;
use erp\modules\managment\models\Amount_club_member;
use yii\web\HttpException;
use yii\web\ServerErrorHttpException;

class Amount_club_memberController extends RestController
{
    /**
     * {@inheritdoc}
     */
    public $service;
    public $modelClass = 'erp\modules\managment\models\Amount_club_member';


    public function behaviors()
    {
        $array = parent::behaviors();
        $array['authenticator']['except'] = ['index', 'buy', 'create', 'update', 'delete', 'view', 'select_2_list', 'validate', 'delete_parameters', 'delete_by_id', 'update_multiple'];
        $array['cors'] = [
            'class' => Cors::class,
            'actions' => [
                'your-action-name' => [
                    #web-servers which you alllow cross-domain access
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => [],
                ]
            ],
        ];
        return $array;
    }

    protected function verbs()
    {
        $array = [
            'buy' => ['OPTIONS', 'POST'],
        ];
        return array_merge(parent::verbs(), $array);
    }

    public function getService()
    {
        if ($this->service == null)
            $this->service = new Amount_club_memberService();
        return $this->service;
    }

    public function actionBuy()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = \Yii::$app->request->getBodyParams();
        if (count($params) == 0)
            throw new HttpException(500, "Theres no parameters in request");
        if (!isset($params['amount']) || !isset($params['id']))
            throw new HttpException(500, "Se necesita la cantidad y el id del miembro para hacer la compra");
        $club_memmber = Club_members::find()->where(['id_club_members' => $params['id']])->one();
        if (!$club_memmber)
            throw new HttpException(404, "No se encontro un miembro con ese id");

        return $this->getService()->buy($params, $club_memmber);
    }

}
