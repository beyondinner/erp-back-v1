<?php


namespace erp\modules\managment\controllers;

use common\services\Services;
use erp\modules\managment\services\FichaMemberLabelService;
use yii\helpers\Url;
use yii\filters\Cors;

class Get_ficha_contentController extends \common\controllers\RestController
{
//    public function actionIndex()
//    {
//        echo Url::to(['form/id']);
//    }
    public function behaviors()
    {
        $array= parent::behaviors();
        $array['authenticator']['except']= ['index','register_template','create', 'update', 'delete', 'view', 'select_2_list', 'validate', 'delete_parameters', 'delete_by_id','update_multiple'];
        $array['cors']=[
            'class' => Cors::class,
            'actions' => [
                'your-action-name' => [
                    #web-servers which you alllow cross-domain access
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST','OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => [],
                ]
            ],
        ];
        return $array;
    }

    protected function verbs()
    {
        $array= [];
        return array_merge(parent::verbs(),$array);
    }
    public function getService()
    {
        if ($this->service == null)
            $this->service = new FichaMemberLabelService();
        return $this->service;
    }
}