<?php
/**
 * /**Generate by ASGENS
 * @author Charlietyn
 * @date Wed Oct 07 18:06:36 GMT-04:00 2020
 * @time Wed Oct 07 18:06:36 GMT-04:00 2020
 */

namespace erp\modules\managment\controllers;


use common\controllers\RestController;
use erp\modules\managment\services\Event_actionsService;
use yii\helpers\Url;
use yii\filters\Cors;
use erp\modules\managment\models\Event_actions;
use yii\web\HttpException;
use yii\web\ServerErrorHttpException;

class Event_actionsController extends RestController
{
    /**
     * {@inheritdoc}
     */
    public $service;
    public $modelClass = 'erp\modules\managment\models\Event_actions';


    public function behaviors()
    {
        $array = parent::behaviors();
        $array['authenticator']['except'] = ['index', 'register_action', 'create', 'update', 'delete', 'view', 'select_2_list', 'validate', 'delete_parameters', 'delete_by_id', 'update_multiple'];
        $array['cors'] = [
            'class' => Cors::class,
            'actions' => [
                'your-action-name' => [
                    #web-servers which you alllow cross-domain access
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => [],
                ]
            ],
        ];
        return $array;
    }

    protected function verbs()
    {
        $array = [];
        return array_merge(parent::verbs(), $array);
    }

    public function getService()
    {
        if ($this->service == null)
            $this->service = new Event_actionsService();
        return $this->service;
    }

    public function actionRegister_action()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = \Yii::$app->request->getBodyParams();
        if (count($params) == 0)
            throw new HttpException(500, "Theres no parameters in request");
        if ($params['action'] == "Cambiar fecha")
            return $this->getService()->registerPosponer($params);
        if ($params['action'] == "Cancelar Evento")
            return $this->getService()->registerCancelar($params);
        else
            throw new HttpException(404, "Action not found");
    }
}
