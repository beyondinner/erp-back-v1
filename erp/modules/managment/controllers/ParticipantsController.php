<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Thu Oct 08 21:27:58 GMT-04:00 2020  
*@time Thu Oct 08 21:27:58 GMT-04:00 2020  
*/
namespace erp\modules\managment\controllers;


use common\controllers\RestController;
use erp\modules\managment\services\ParticipantsService;
use yii\helpers\Url;
use yii\filters\Cors;
use erp\modules\managment\models\Participants;
use yii\web\ServerErrorHttpException;

class ParticipantsController extends RestController
{
    /**
     * {@inheritdoc}
     */
    public $service ;
    public $modelClass = 'erp\modules\managment\models\Participants';


    public function behaviors()
    {
        $array= parent::behaviors();
        $array['authenticator']['except']= ['index','create', 'update', 'delete', 'view', 'select_2_list', 'validate', 'delete_parameters', 'delete_by_id','update_multiple'];
        $array['cors']=[
            'class' => Cors::class,
            'actions' => [
                'your-action-name' => [
                    #web-servers which you alllow cross-domain access
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST','OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => [],
                ]
            ],
        ];
        return $array;
    }

    protected function verbs()
    {
        $array= [];
        return array_merge(parent::verbs(),$array);
    }
   public function getService()
    {
        if ($this->service == null)
            $this->service = new ParticipantsService();
        return $this->service;
    }

}
