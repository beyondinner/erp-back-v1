<?php
/**
 * /**Generate by ASGENS
 * @author Charlietyn
 * @date Sat Sep 05 20:15:23 GMT-04:00 2020
 * @time Sat Sep 05 20:15:23 GMT-04:00 2020
 */

namespace erp\modules\managment\controllers;


use common\controllers\RestController;
use common\modules\security\models\Users;
use erp\modules\managment\models\Club_member_history_other;
use erp\modules\managment\services\Club_membersService;
use yii\helpers\Url;
use yii\filters\Cors;
use erp\modules\managment\models\Club_members;
use yii\web\HttpException;
use yii\web\ServerErrorHttpException;

class Club_membersController extends RestController
{
    /**
     * {@inheritdoc}
     */
    public $service;
    public $modelClass = 'erp\modules\managment\models\Club_members';


    public function behaviors()
    {
        $array = parent::behaviors();
        $array['authenticator']['except'] = ['register_member', 'update_ammount', 'restart_member', 'register_member_api', 'send_email_array', 'send_email', 'transfer_money', 'index', 'create', 'update', 'delete', 'view', 'select_2_list', 'validate', 'delete_parameters', 'delete_by_id', 'update_multiple'];
        $array['cors'] = [
            'class' => Cors::class,
            'actions' => [
                'your-action-name' => [
                    #web-servers which you alllow cross-domain access
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => [],
                ]
            ],
        ];
        return $array;
    }

    protected function verbs()
    {
        $array = [
            'register_member' => ['OPTIONS', 'POST'],
            'register_member_api' => ['OPTIONS', 'POST'],
            'transfer_money' => ['OPTIONS', 'POST'],
            'send_email' => ['OPTIONS', 'POST', 'GET'],
            'Send_email_array' => ['OPTIONS', 'POST', 'GET'],
        ];
        return array_merge(parent::verbs(), $array);
    }


    public function getService()
    {
        if ($this->service == null)
            $this->service = new Club_membersService();
        return $this->service;
    }

    public function actionRegister_member()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = \Yii::$app->request->getBodyParams();
        if (count($params) == 0)
            throw new HttpException(500, "Theres no parameters in request");

        $history_log = new Club_member_history_other();
        $history_log->history_member_id = $params['club_members']['id_adviser'];
        $history_log->history_update_at = date('Y-m-d h:i:s');
        $history_log->history_create_at= date('Y-m-d h:i:s');
        $history_log->history_description = "Creacion del miembro: ".$params['users']['nombre_usuario'].' '.$params['users']['apellido_usuario'];
        if (!$history_log->save()){
            print_r($history_log->getErrors());
        }

        return $this->getService()->registerMember($params);
    }

    public function actionRegister_member_api()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = \Yii::$app->request->getBodyParams();
        if (count($params) == 0)
            throw new HttpException(500, "Theres no parameters in request");
        if (!isset($params['id_country']))
            $params['id_country'] = 207;
        $club_members = Club_members::find()->where(['club_members_dni' => $params['club_members_dni']])->one();
        if ($club_members)
            throw new HttpException(406, "Ya existe un miembro con este dni");
        $club_members = Club_members::find()->where(['club_members_email' => $params['club_members_email']])->one();
        if ($club_members)
            throw new HttpException(406, "Ya existe un miembro con este email");

        return $this->getService()->registerMemberApi($params);
    }

    public function actionTransfer_money()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = \Yii::$app->request->getBodyParams();
        if (count($params) == 0)
            throw new HttpException(500, "Theres no parameters in request");
        if (!isset($params['email_receptor']) || !isset($params['id']) || !isset($params['amount']))
            throw new HttpException(500, "Se necesita el miembro receptor , el miembro emisor y la cantidad a transferir para hacer la tranferencia");
        return $this->getService()->transferMoney($params);
    }

    public function actionSend_email()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = \Yii::$app->request->getBodyParams();
        if (count($params) == 0)
            throw new HttpException(500, "Theres no parameters in request");
        if (!isset($params['id']))
            throw new HttpException(500, "Se necesita el miembro para activar");
        $clubmembers = Club_members::find()->where(['id_club_members' => $params['id']])->one();
        if (!$clubmembers)
            throw new HttpException(404, "Miembro no encontrado");
        return $this->getService()->sendEmail($clubmembers);
    }

    public function actionSend_email_array()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = \Yii::$app->request->getBodyParams();
        if (count($params) == 0)
            throw new HttpException(500, "Theres no parameters in request");
        foreach ($params as $s) {
            $clubmembers = Club_members::find()->where(['id_club_members' => $params['id']])->one();
            if (!$clubmembers)
                continue;
            $this->getService()->sendEmail($clubmembers);
            sleep(5);
        }
        return ['success' => true];
    }

    public function actionUpdate_ammount($id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $clubmember = Club_members::findOne($id);
        if (!$clubmember)
            throw new HttpException(404, "Member not found");
        return $this->getService()->updateMemberAmount($clubmember);
    }

    public function actionRestart_member()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = \Yii::$app->request->getBodyParams();
        if (count($params) == 0)
            throw new HttpException(500, "Theres no parameters in request");
        $user = Users::findOne($params['id']);
        if (!$user)
            throw new HttpException(404, "User not found");
        if ($user->id_role != 3) {
            \Yii::$app->response->statusCode = 403;
            return ['success' => false, 'message' => 'Este usuario debe ser un miembro del club'];
        }
        if (count($user->arrayclub_members) > 0) {
            return ['success' => false, 'message' => 'Este usuario ya tiene un miembro asociado'];
        }
        $this->getService()->restartMember($user);
        return ['success' => true];
    }

}
