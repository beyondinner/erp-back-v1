<?php


namespace erp\modules\managment\controllers;


use common\controllers\RestController;
use common\services\Services;
use erp\modules\managment\services\FichaService;
use yii\filters\Cors;

class Get_fichaController extends RestController
{
    /**
     * {@inheritdoc}
     */
    public $service ;
    public $modelClass = 'erp\modules\managment\models\Ficha_member';


    public function behaviors()
    {
        $array= parent::behaviors();
        $array['authenticator']['except']= ['index','register_template','create', 'update', 'delete', 'view', 'select_2_list', 'validate', 'delete_parameters', 'delete_by_id','update_multiple'];
        $array['cors']=[
            'class' => Cors::class,
            'actions' => [
                'your-action-name' => [
                    #web-servers which you alllow cross-domain access
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST','OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => [],
                ]
            ],
        ];
        return $array;
    }

    protected function verbs()
    {
        $array= [];
        return array_merge(parent::verbs(),$array);
    }
    public function getService()
    {
        if ($this->service == null)
            $this->service = new FichaService();
        return $this->service;
    }


}