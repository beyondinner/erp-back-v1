<?php
/**
/**Generate by ASGENS
 *@author Charlietyn
 *@date Sat Sep 05 20:15:23 GMT-04:00 2020
 *@time Sat Sep 05 20:15:23 GMT-04:00 2020
 */
namespace erp\modules\managment\controllers;


use common\controllers\RestController;
use common\services\Services;
use erp\modules\managment\services\Club_member_history_otherService;
use erp\modules\managment\services\Club_member_historyService;
use yii\helpers\Url;
use yii\filters\Cors;
use erp\modules\managment\models\Club_member_history;
use yii\web\ServerErrorHttpException;

class Club_member_history_otherController extends RestController
{

//    public function actionIndex(){
//        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
//
//        $data = [
//            'a'=>'a'
//        ];
//
//        return $data;
//    }

    /**
     * {@inheritdoc}
     */
    public $service ;
    public $modelClass = 'erp\modules\managment\models\Club_member_history_other';


    public function behaviors()
    {
        $array= parent::behaviors();
        $array['authenticator']['except']= ['index','create', 'update', 'delete', 'view', 'select_2_list', 'validate', 'delete_parameters', 'delete_by_id','update_multiple'];
        $array['cors']=[
            'class' => Cors::class,
            'actions' => [
                'your-action-name' => [
                    #web-servers which you alllow cross-domain access
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST','OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => [],
                ]
            ],
        ];
        return $array;
    }

    protected function verbs()
    {
        $array= [];
        return array_merge(parent::verbs(),$array);
    }
    public function getService()
    {
        if ($this->service == null)
            $this->service = new Club_member_history_otherService();
        return $this->service;
    }

}
