<?php


namespace erp\modules\managment\controllers;


use yii\filters\Cors;

class TestController extends \common\controllers\RestController
{
    public function behaviors()
    {
        $array = parent::behaviors();
        $array['authenticator']['except'] = ['register_member', 'update_ammount', 'restart_member', 'register_member_api', 'send_email_array', 'send_email', 'transfer_money', 'index', 'create', 'update', 'delete', 'view', 'select_2_list', 'validate', 'delete_parameters', 'delete_by_id', 'update_multiple'];
        $array['cors'] = [
            'class' => Cors::class,
            'actions' => [
                'your-action-name' => [
                    #web-servers which you alllow cross-domain access
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => [],
                ]
            ],
        ];
        return $array;
    }
    public function actionRegister_member(){

    }
}