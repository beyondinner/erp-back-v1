<?php


namespace erp\modules\managment\controllers;


use common\modules\security\models\Users;
use common\services\Services;
use CRest;
use erp\modules\managment\models\Bitrix_contact;
use erp\modules\managment\services\BitrixService;
use erp\modules\managment\services\ProductsService;
use yii\filters\Cors;
use common\controllers\RestController;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

class BitrixController extends RestController
{
    public $service ;
    public function behaviors()
    {
        $array= parent::behaviors();
        $array['authenticator']['except']= ['index','bitrix_getlist','bitrix_getdeals','register_template','create', 'update', 'delete', 'view', 'select_2_list', 'validate', 'delete_parameters', 'delete_by_id','update_multiple'];
        $array['cors']=[
            'class' => Cors::class,
            'actions' => [
                'your-action-name' => [
                    #web-servers which you alllow cross-domain access
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST','OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => [],
                ]
            ],
        ];
        return $array;
    }
    public function actionBirtix_getlead(){

    }
    public function actionBitrix_getdeals(){
        require_once('src/crest.php');
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = \Yii::$app->request->getBodyParams();
        if ($params['search_val']===""){
            $result = (CRest::call(
                'crm.deal.list',
                [
                    "start"=> $params['offset']+50,
                    "order"=> ["ID" => "DESC"],
                    "select" => [ "ID", "TITLE", "TYPE_ID", "STAGE_ID", "CURRENCY_ID","OPPORTUNITY", "LEAD_ID", "DATE_CREATE", "COMMENTS" ]
                ])
            );
        }else{
            $result = (CRest::call(
                'crm.deal.list',
                [
                    "filter"=> ["STAGE_ID"=> $params['search_val']],
                    "start"=> $params['offset']+50,
                    "order"=> ["ID" => "DESC"],
                    "select" => [ "ID", "TITLE", "TYPE_ID", "STAGE_ID", "CURRENCY_ID","OPPORTUNITY", "LEAD_ID", "DATE_CREATE", "COMMENTS" ]
                ])
            );
        }
        return $result;
    }
    public function actionBitrix_getlist(){
        require_once('src/crest.php');
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = \Yii::$app->request->getBodyParams();
        if ($params['search_val']===""){
            $result = (CRest::call(
                'crm.contact.list',
                [
                    "start"=> $params['offset']+50,
                    "order"=> ["ID" => "DESC"],
                    "select" => [ "ID", "NAME", "SECOND_NAME", "LAST_NAME", "LEAD_ID","HAS_PHONE", "HAS_EMAIL", "DATE_CREATE", "PHONE", "EMAIL" ]
                ])
            );
        }else{
            $result = (CRest::call(
                'crm.contact.list',
                [
                    "filter"=> ["EMAIL"=> $params['search_val']],
                    "start"=> $params['offset']+50,
                    "order"=> ["ID" => "DESC"],
                    "select" => [ "ID", "NAME", "SECOND_NAME", "LAST_NAME", "LEAD_ID","HAS_PHONE", "HAS_EMAIL", "DATE_CREATE", "PHONE", "EMAIL" ]
                ])
            );
        }
        return $result;
    }
    public function actionBritix_import(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = \Yii::$app->request->getBodyParams();
        if (count($params) == 0)
            throw new HttpException(500, "Theres no parameters in request");
        if (!isset($params['email']) )
            throw new HttpException(500, "Se necesita un email");

        require_once('src/crest.php');
        $result = (CRest::call(
            'crm.contact.list',
            [
                "filter"=> ["EMAIL"=> $params['email']],
                "select" => [ "ID", "NAME", "SECOND_NAME", "LAST_NAME", "LEAD_ID","HAS_PHONE", "HAS_EMAIL", "DATE_CREATE", "PHONE", "EMAIL" ]
            ])
        );
        if ($result == null){
            $result = array(
                "result"=>false,
                "message"=>"Error de coneccion al CRM"
            );
        }else{
            if (ArrayHelper::getValue($result, 'total')==0){
                $result = array(
                    "result"=>false,
                    "message"=>"No hay usuario con ese email en el CRM"
                );
            }else{
                $id_user_erp = Users::find()->where(['email'=> $params['email']])->one();
                if (!$id_user_erp){
                    $id_user = 1096;
                }else{
                    $id_user = $id_user_erp->id_user;
                }
                $bitrixContact = Bitrix_contact::find()->where(['bitrix_user_email' => $params['email']])->one();
                if (!$bitrixContact){
                    $bContact = new Bitrix_contact();
                    $bContact->bitrix_id = ArrayHelper::getValue($result, 'result.0.ID');
                    $bContact->bitrix_user_id = $id_user;
                    $bContact->bitrix_user_name = ArrayHelper::getValue($result, 'result.0.NAME');
                    $bContact->bitrix_user_second_name = ArrayHelper::getValue($result, 'result.0.SECOND_NAME');
                    $bContact->bitrix_user_last_name = ArrayHelper::getValue($result, 'result.0.LAST_NAME');
                    $bContact->bitrix_user_lead_id = ArrayHelper::getValue($result, 'result.0.LEAD_ID');
                    $bContact->bitrix_user_has_phone = ArrayHelper::getValue($result, 'result.0.HAS_PHONE');
                    $bContact->bitrix_user_has_email = ArrayHelper::getValue($result, 'result.0.HAS_EMAIL');
                    $bContact->bitrix_user_date_create = ArrayHelper::getValue($result, 'result.0.DATE_CREATE');
                    $bContact->bitrix_user_phone = ArrayHelper::getValue($result, 'result.0.PHONE.0.VALUE');
                    $bContact->bitrix_user_email = ArrayHelper::getValue($result, 'result.0.EMAIL.0.VALUE');
                    $bContact->save();
                }
            }
        }

//        return ArrayHelper::getValue($result, 'result.0.ID');
        return $result;
    }
    public function actionBritix_getlist(){
        require_once('src/crest.php');
        $result = (CRest::call(
            'crm.contact.list',
            [
                "filter"=> ["EMAIL"=> "chinanaikz@yahoo.com"],
                "select" => [ "ID", "NAME", "LAST_NAME", "PHONE", "EMAIL" ]
            ])
        );
        if ($result == null){
            $result = array(
              "result"=>false
            );
        }

        return $result;
    }

    protected function verbs()
    {
        $array = [
            'britix_import' => ['OPTIONS', 'POST'],
            'bitrix_getlist' => ['OPTIONS', 'POST'],
            'bitrix_getdeals' => ['OPTIONS', 'POST']
        ];
        return array_merge(parent::verbs(),$array);
    }

    public function getService()
    {
        if ($this->service == null)
            $this->service = new BitrixService();
        return $this->service;
    }
}