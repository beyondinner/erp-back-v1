<?php


namespace erp\modules\managment\controllers;


use common\services\Services;
use erp\modules\managment\models\Club_member_history_other;
use erp\modules\managment\models\Ficha_inputs;
use erp\modules\managment\models\Ficha_member;
use erp\modules\managment\services\FichaMemberLabelValService;
use yii\filters\Cors;

class Ficha_memberController extends \common\controllers\RestController
{
    public function behaviors()
    {
        $array= parent::behaviors();
        $array['authenticator']['except']= ['index','register_template','create', 'update', 'delete', 'view', 'select_2_list', 'validate', 'delete_parameters', 'delete_by_id','update_multiple'];
        $array['cors']=[
            'class' => Cors::class,
            'actions' => [
                'your-action-name' => [
                    #web-servers which you alllow cross-domain access
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST','OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => [],
                ]
            ],
        ];
        return $array;
    }

    protected function verbs()
    {
        $array= [];
        return array_merge(parent::verbs(),$array);
    }
    public function actionCreate(){
        $historyResult = '';
        //$a_create = parent::actionCreate();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = \Yii::$app->request->getBodyParams();
        $id_user = $params['val_id_user'];
        $id_form = $params['val_label_form_id'];
        $val_text = json_encode($params['val_text']);

        $model = new Ficha_inputs();
        $model->val_id_user = $id_user;
        $model->val_label_form_id = $id_form;
        $model->val_text = $val_text;
        $model->val_form_ficha_label_id=0;

        $historyMember = $params['val_text']['input_values'][0]['member'];
        $typeId = $params['val_text']['input_values'][0]['val_label_form_id'];
        $fichaMember = Ficha_member::findOne(['form_id'=>$typeId]);

        $createHistory = new Club_member_history_other();
        $createHistory->history_type = 1;
        $createHistory->history_member_id = $historyMember;
        $createHistory->history_description = 'Ha creado: '.$fichaMember->form_name;
        $createHistory->history_create_at = \Yii::$app->formatter->asDatetime('now', 'php:Y-m-d H:i:s');
        $createHistory->history_update_at = \Yii::$app->formatter->asDatetime('now', 'php:Y-m-d H:i:s');
        if (!$createHistory->save()){
            $historyResult = $createHistory->getErrors();
        }else{
            $historyResult = $createHistory;
        }
        if (!$model->save()){
            print_r($model->getErrors());
        }
        $result = [
            'historyResult'=>$historyResult,
            'fichaResult'=>$model
        ];
        return $result;
//        return $model;
    }
    public function getService()
    {
        if ($this->service == null)
            $this->service = new FichaMemberLabelValService();
        return $this->service;
    }
}