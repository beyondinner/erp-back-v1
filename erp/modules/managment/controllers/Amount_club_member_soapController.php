<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Tue Oct 06 14:40:18 GMT-04:00 2020  
*@time Tue Oct 06 14:40:18 GMT-04:00 2020  
*/
namespace erp\modules\managment\controllers;

use common\controllers\SecureController;
use erp\modules\managment\models\Amount_club_member;

class Amount_club_member_soapController extends SecureController
{

    /** @var bool */
    public $enableCsrfValidation = false;

    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\managment\models\Amount_club_member';


    public function behaviors()
    {
        $array = parent::behaviors();
        $array['authenticator']['except'] = ['amount_club_member_data'];
        return $array;
    }
    /**
     * {@inheritdoc}
     * redefine las acciones restful de la controladora
     */
    public function actions()
    {
        return [
            'amount_club_member_data' => [
                'class' => 'mongosoft\soapserver\Action',
                'classMap' => ['Amount_club_member' => 'erp\modules\managment\models\Amount_club_member'],
                'serviceOptions' => [
                    'disableWsdlMode' => true,
                ]
            ],
        ];
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        parent::checkAccess($action, $model, $params); // TODO: Change the autogenerated stub
    }


    public function encode_result($result)
    {
        $encode = false;
        if (strpos(\Yii::$app->getRequest()->contentType, "application/json") !== false)
            $encode = true;
        if ($encode)
            return json_encode($result, JSON_UNESCAPED_UNICODE);
        return $result;
    }

    /**
     * Simple test which returns a List of amount_club_member in order to see how the wsdl pans out
     * @param [] $params
     * @return object[]
     * @soap
     */

    public function amount_club_member_list()
    {
        $params=[];
        if (func_get_args())
            $params = func_get_args()[0]['params'];
        return $this->encode_result(Amount_club_member::list_model($params));
    }

    /**
     * Simple test which returns a data of amount_club_member  with id in order to see how the wsdl pans out
     * @param int $id
     * @return erp\modules\managment\models\Amount_club_member
     * @soap
     */
    public function amount_club_member_view($id)
    {
        $params=[];
        if (func_get_args())
            $params = func_get_args()[0]['params'];
        return $this->encode_result(Amount_club_member::view($id, $params));
    }
}
