<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Sat Sep 05 20:15:23 GMT-04:00 2020  
*@time Sat Sep 05 20:15:23 GMT-04:00 2020  
*/
namespace erp\modules\managment\controllers;


use common\controllers\RestController;
use erp\modules\managment\models\Club_members;
use erp\modules\managment\services\ServicesService;
use yii\helpers\Url;
use yii\filters\Cors;
use erp\modules\managment\models\Services;
use yii\web\HttpException;
use yii\web\ServerErrorHttpException;

class ServicesController extends RestController
{
    /**
     * {@inheritdoc}
     */
    public $service ;
    public $modelClass = 'erp\modules\managment\models\Services';


    public function behaviors()
    {
        $array= parent::behaviors();
        $array['authenticator']['except']= ['index','buy','create', 'update', 'delete', 'view', 'select_2_list', 'validate', 'delete_parameters', 'delete_by_id','update_multiple'];
        $array['cors']=[
            'class' => Cors::class,
            'actions' => [
                'your-action-name' => [
                    #web-servers which you alllow cross-domain access
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST','OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => [],
                ]
            ],
        ];
        return $array;
    }
    protected function verbs()
    {
        $array= [
            'buy' => ['OPTIONS', 'POST'],

        ];
        return array_merge(parent::verbs(),$array);
    }

   public function getService()
    {
        if ($this->service == null)
            $this->service = new ServicesService();
        return $this->service;
    }

    public function actionBuy(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = \Yii::$app->request->getBodyParams();
        if (count($params) == 0)
            throw new HttpException(500, "Theres no parameters in request");
        if (!isset($params['service']) || !isset($params['email']) )
            throw new HttpException(500, "Se necesita el servicio y el miembro para hacer la compra");
        $club_memmber = Club_members::find()->where(['club_members_email'=>$params['email']])->one();
        if(!$club_memmber)
            throw new HttpException(404, "No se encontro un miembro con ese correo");

        return $this->getService()->buy($params,$club_memmber);
    }
}
