<?php
/**
 * /**Generate by ASGENS
 * @author Charlietyn
 * @date Sun Sep 06 18:31:24 GMT-04:00 2020
 * @time Sun Sep 06 18:31:24 GMT-04:00 2020
 */

namespace erp\modules\managment\controllers;


use Codeception\Lib\Connector\Guzzle;
use common\controllers\RestController;
use CRest;
use erp\modules\managment\services\ContactsService;
use erp\modules\nomenclatures\models\Languages;
use erp\modules\nomenclatures\models\States;
use GuzzleHttp\Client;
use http\Params;
use yii\helpers\Url;
use yii\filters\Cors;
use erp\modules\managment\models\Contacts;
use yii\web\HttpException;
use yii\web\ServerErrorHttpException;

class ContactsController extends RestController
{
    /**
     * @var mixed|string
     */
    private static $bitrix_api_options;
    /**
     * {@inheritdoc}
     */
    public $service;
    public $modelClass = 'erp\modules\managment\models\Contacts';
    static $bitrix_api_rest = "k4e1bz5swpothdls";
    static $bitrix_api_url = "https://innermastery.bitrix24.eu/rest/1/";
    static $bitrix_api_option = '/'; // /crm.contact.get.json
    static $bitrix_api_params = "?ID=";


    public function behaviors()
    {
        $array = parent::behaviors();
        $array['authenticator']['except'] = ['create', 'britix_create', 'britix_getlist', 'index', 'update', 'delete', 'view', 'select_2_list', 'validate', 'delete_parameters', 'delete_by_id', 'update_multiple'];
        $array['cors'] = [
            'class' => Cors::class,
            'actions' => [
                'your-action-name' => [
                    #web-servers which you alllow cross-domain access
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST', 'OPTIONS', 'GET'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => [],
                ]
            ],
        ];
        return $array;
    }

    protected function verbs()
    {
        $array = [
            'britix_create' => ['OPTIONS', 'POST'],
            'britix_getlist' => ['OPTIONS', 'GET']
        ];
        return array_merge(parent::verbs(), $array);
    }

    public function getService()
    {
        if ($this->service == null)
            $this->service = new ContactsService();
        return $this->service;
    }
    public function actionBritix_getlist(){
        require_once('src/crest.php');
        //echo '<PRE>';
        return (CRest::call(
            'crm.contact.list',
            [
                    "filter"=> ["EMAIL"=> "chinanaikz@yahoo.com"],
                    "select" => [ "ID", "NAME", "LAST_NAME", "PHONE", "EMAIL" ]
            ])
        );

        //echo '</PRE>';
    }
    public function actionBritix_create()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = \Yii::$app->request->getBodyParams();
        $logparams = new \stdClass();
        $logparams->params = $params;
        $logparams->post = $_POST;
        //$id=$params['data']['FIELDS']['ID'];
        $countries = json_decode(file_get_contents('assets/import/country.json'));
        $client=new  Client();
        //$response=$client->request('GET','http://innermastery.bitrix24.eu/rest/1/k4e1bz5swpothdls/crm.contact.get.json?ID='.$id);
        $response=$client->request('GET','http://innermastery.bitrix24.eu/rest/1/k4e1bz5swpothdls/crm.contact.list.json');
        $response=json_decode($response->getBody()->getContents());
        $contact_send=$response->result;
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $contact = new Contacts();
            $selected_country = null;
            $pos = array_search($contact_send->ADDRESS_COUNTRY, array_column($countries, 'VALUE'));
            $id_country = null;
            $id_state = null;
            $id_language = null;
            if ($pos !== false) {
                $selected_country = $countries[$pos];
                $id_country = $selected_country->ID;
            }
            $state = States::find()->where(['state_name' => $contact_send->ADDRESS_CITY])->one();
            if ($state)
                $id_state = $state->id_state;
            $contact->contact_full_name = trim($contact_send->NAME) . ' ' . trim($contact_send->SECOND_NAME).' ' . trim($contact_send->LAST_NAME);
            $contact->contact_legal_type_id = 3;
            $contact->contact_internal_reference = $contact_send->COMMENTS;
            $contact->contact_type_id = 3;
            if ($contact_send->HAS_EMAIL == "Y")
                $contact->contact_email = $contact_send->EMAIL[0]->VALUE;
            if ($contact_send->HAS_PHONE == "Y")
                $contact->contact_phone = $contact_send->PHONE[0]->VALUE;
            $contact->contact_address = $contact_send->ADDRESS?$contact_send->ADDRESS:"--";
            $contact->country_id = $id_country;
            $contact->state_id = $id_state;
            $contact->language_id = 2;
            $contact->currency_id = 2;
            $contact->payment_type_id = 3;
            $contact->contact_status_id = 1;
            $contact->created_at = date('Y-m-d h:i:s');
            $contact->updated_at = date('Y-m-d h:i:s');
            $contact->source="Britix";
            $contact->save();
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
        return $contact->result();

    }

}
