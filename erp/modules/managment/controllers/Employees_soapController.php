<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Sat Sep 05 20:15:23 GMT-04:00 2020  
*@time Sat Sep 05 20:15:23 GMT-04:00 2020  
*/
namespace erp\modules\managment\controllers;

use common\controllers\SecureController;
use erp\modules\managment\models\Employees;

class Employees_soapController extends SecureController
{

    /** @var bool */
    public $enableCsrfValidation = false;

    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\managment\models\Employees';


    public function behaviors()
    {
        $array = parent::behaviors();
        $array['authenticator']['except'] = ['employees_data'];
        return $array;
    }
    /**
     * {@inheritdoc}
     * redefine las acciones restful de la controladora
     */
    public function actions()
    {
        return [
            'employees_data' => [
                'class' => 'mongosoft\soapserver\Action',
                'classMap' => ['Employees' => 'erp\modules\managment\models\Employees'],
                'serviceOptions' => [
                    'disableWsdlMode' => true,
                ]
            ],
        ];
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        parent::checkAccess($action, $model, $params); // TODO: Change the autogenerated stub
    }


    public function encode_result($result)
    {
        $encode = false;
        if (strpos(\Yii::$app->getRequest()->contentType, "application/json") !== false)
            $encode = true;
        if ($encode)
            return json_encode($result, JSON_UNESCAPED_UNICODE);
        return $result;
    }

    /**
     * Simple test which returns a List of employees in order to see how the wsdl pans out
     * @param [] $params
     * @return object[]
     * @soap
     */

    public function employees_list()
    {
        $params=[];
        if (func_get_args())
            $params = func_get_args()[0]['params'];
        return $this->encode_result(Employees::list_model($params));
    }

    /**
     * Simple test which returns a data of employees  with id in order to see how the wsdl pans out
     * @param int $id
     * @return erp\modules\managment\models\Employees
     * @soap
     */
    public function employees_view($id)
    {
        $params=[];
        if (func_get_args())
            $params = func_get_args()[0]['params'];
        return $this->encode_result(Employees::view($id, $params));
    }
}
