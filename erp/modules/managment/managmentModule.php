<?php
/*Clase del Modulo managment*/
/**Generate by ASGENS
*@author Charlietyn  
*@date Sun Sep 06 18:31:23 GMT-04:00 2020  
*@time Sun Sep 06 18:31:23 GMT-04:00 2020  
*/
namespace erp\modules\managment;
class managmentModule extends \yii\base\Module 
{
    public $controllerNamespace = 'erp\modules\managment\controllers';
    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}

