<?php
/*Clase del Modulo nomenclatures*/
/**Generate by ASGENS
*@author Charlietyn  
*@date Sun Sep 06 18:41:57 GMT-04:00 2020  
*@time Sun Sep 06 18:41:57 GMT-04:00 2020  
*/
namespace erp\modules\nomenclatures;
class nomenclaturesModule extends \yii\base\Module 
{
    public $controllerNamespace = 'erp\modules\nomenclatures\controllers';
    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}

