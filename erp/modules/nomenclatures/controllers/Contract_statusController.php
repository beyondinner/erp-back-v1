<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Sun Sep 06 18:31:24 GMT-04:00 2020  
*@time Sun Sep 06 18:31:24 GMT-04:00 2020  
*/
namespace erp\modules\nomenclatures\controllers;


use common\controllers\RestController;
use erp\modules\nomenclatures\services\Contract_statusService;
use yii\helpers\Url;
use yii\filters\Cors;
use erp\modules\nomenclatures\models\Contract_status;
use yii\web\ServerErrorHttpException;

class Contract_statusController extends RestController
{
    /**
     * {@inheritdoc}
     */
    public $service ;
    public $modelClass = 'erp\modules\nomenclatures\models\Contract_status';


    public function behaviors()
    {
        $array= parent::behaviors();
        $array['authenticator']['except']= ['index','create', 'update', 'delete', 'view', 'select_2_list', 'validate', 'delete_parameters', 'delete_by_id','update_multiple'];
        $array['cors']=[
            'class' => Cors::class,
            'actions' => [
                'your-action-name' => [
                    #web-servers which you alllow cross-domain access
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST','OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => [],
                ]
            ],
        ];
        return $array;
    }

    protected function verbs()
    {
        $array= [];
        return array_merge(parent::verbs(),$array);
    }
   public function getService()
    {
        if ($this->service == null)
            $this->service = new Contract_statusService();
        return $this->service;
    }

}
