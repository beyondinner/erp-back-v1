<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Tue Oct 13 18:00:12 GMT-04:00 2020  
*@time Tue Oct 13 18:00:12 GMT-04:00 2020  
*/
namespace erp\modules\nomenclatures\models;


/** 
*  Esta es  ActiveQuery clase de [[Adviser]].
 *
 * @see Adviser
 */
/**
 * AdviserQuery representa la clase de Consulta del modelo Adviser
 */
class AdviserQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Adviser[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Adviser|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

