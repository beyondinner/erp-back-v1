<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Sat Sep 05 20:15:24 GMT-04:00 2020  
*@time Sat Sep 05 20:15:24 GMT-04:00 2020  
*/
namespace erp\modules\nomenclatures\models;


/** 
*  Esta es  ActiveQuery clase de [[Contract_types]].
 *
 * @see Contract_types
 */
/**
 * Contract_typesQuery representa la clase de Consulta del modelo Contract_types
 */
class Contract_typesQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Contract_types[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Contract_types|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

