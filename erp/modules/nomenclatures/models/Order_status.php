<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Nov 25 10:19:28 GMT-05:00 2020  
*@time Wed Nov 25 10:19:28 GMT-05:00 2020  
*/
namespace erp\modules\nomenclatures\models;
use Yii;
use common\models\RestModel;

use erp\modules\billings\models\Orders;
/**
 * Este es la clase modelo para la tabla order_status.
 *
 * Los siguientes son los campos de la tabla 'order_status':
 * @property integer $id_order_status
 * @property string $order_status
 * @property string $order_status_description

 * Los siguientes son las relaciones de este modelo :

 * @property Orders[] $arrayorders
 */

class Order_status extends RestModel 
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_order_status';

    const MODEL = 'Order_status';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_status';
    }

     
        /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
       const RELATIONS = ['arrayorders'];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

       const PKEY = 'id_order_status';

     /*
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['order_status'],'required','on'=>['create','default']],
			[['id_order_status'],'required', 'on' => 'update'],
			[['id_order_status'],'integer'],
			[['order_status'], 'string', 'max'=>20],
			[['order_status_description'], 'string', 'max'=>65535],
			[['id_order_status'], 'unique' , 'on' => 'create'],
			[['order_status'], 'unique' , 'on' => 'create'],
			[['order_status'], 'unique' , 'on' => 'update','when' =>function ($model, $value) {
                $elem = self::find()->where([$value => $model[$value]])->one();
                return !$elem ? false : $elem[$elem->primaryKey] != $model[$model->primaryKey];
            }],
        ];
    }

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_order_status
     */
	  public function getArrayorders()
		{
			return $this->hasMany(Orders::class, ['id_order_status' => 'id_order_status']);
		}
 /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'order_status.id_order_status as id', 'order_status.order_status as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('order_status.order_status LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
}
?>
