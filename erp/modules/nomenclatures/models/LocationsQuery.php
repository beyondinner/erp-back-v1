<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Oct 07 18:06:36 GMT-04:00 2020  
*@time Wed Oct 07 18:06:36 GMT-04:00 2020  
*/
namespace erp\modules\nomenclatures\models;


/** 
*  Esta es  ActiveQuery clase de [[Locations]].
 *
 * @see Locations
 */
/**
 * LocationsQuery representa la clase de Consulta del modelo Locations
 */
class LocationsQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Locations[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Locations|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

