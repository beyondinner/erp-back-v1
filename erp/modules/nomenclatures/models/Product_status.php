<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Sat Sep 05 20:15:24 GMT-04:00 2020  
*@time Sat Sep 05 20:15:24 GMT-04:00 2020  
*/
namespace erp\modules\nomenclatures\models;
use Yii;
use common\models\RestModel;

use erp\modules\managment\models\Products;
/**
 * Este es la clase modelo para la tabla product_status.
 *
 * Los siguientes son los campos de la tabla 'product_status':
 * @property integer $id_product_status
 * @property string $product_status_siglas
 * @property string $product_status_desc
 * @property datetime $created_at
 * @property datetime $updated_at

 * Los siguientes son las relaciones de este modelo :

 * @property Products[] $arrayproducts
 */

class Product_status extends RestModel 
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_product_status';

    const MODEL = 'Product_status';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_status';
    }

     
        /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
       const RELATIONS = ['arrayproducts'];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

       const PKEY = 'id_product_status';

     /*
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['product_status_siglas','product_status_desc','created_at','updated_at'],'required','on'=>['create','default']],
			[['id_product_status'],'required', 'on' => 'update'],
			[['id_product_status'],'integer'],
			[['created_at','updated_at'],'safe'],
			['created_at','format_created_at'],
			['updated_at','format_updated_at'],
			[['product_status_siglas','product_status_desc'], 'string', 'max'=>255],
			[['id_product_status'], 'unique' , 'on' => 'create'],
			[['product_status_siglas'], 'unique' , 'on' => 'create'],
			[['product_status_desc'], 'unique' , 'on' => 'create'],
			[['product_status_siglas'], 'unique' , 'on' => 'update','when' =>function ($model, $value) {
                $elem = self::find()->where([$value => $model[$value]])->one();
                return !$elem ? false : $elem[$elem->primaryKey] != $model[$model->primaryKey];
            }],
			[['product_status_desc'], 'unique' , 'on' => 'update','when' =>function ($model, $value) {
                $elem = self::find()->where([$value => $model[$value]])->one();
                return !$elem ? false : $elem[$elem->primaryKey] != $model[$model->primaryKey];
            }],
        ];
    }

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo product_status_id
     */
	  public function getArrayproducts()
		{
			return $this->hasMany(Products::class, ['product_status_id' => 'id_product_status']);
		}
 /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'product_status.id_product_status as id', 'product_status.product_status_siglas as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('product_status.product_status_siglas LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
   public function format_created_at(){
        $timestamp = (strpos('/', $this->created_at) > -1) ?
            strtotime(str_replace('/', '-', $this->created_at)) : $this->created_at;
        $this->created_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
   public function format_updated_at(){
        $timestamp = (strpos('/', $this->updated_at) > -1) ?
            strtotime(str_replace('/', '-', $this->updated_at)) : $this->updated_at;
        $this->updated_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
}
?>
