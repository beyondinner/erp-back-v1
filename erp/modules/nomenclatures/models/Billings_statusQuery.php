<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Sat Sep 05 20:15:23 GMT-04:00 2020  
*@time Sat Sep 05 20:15:23 GMT-04:00 2020  
*/
namespace erp\modules\nomenclatures\models;


/** 
*  Esta es  ActiveQuery clase de [[Billings_status]].
 *
 * @see Billings_status
 */
/**
 * Billings_statusQuery representa la clase de Consulta del modelo Billings_status
 */
class Billings_statusQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Billings_status[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Billings_status|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

