<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Oct 07 18:06:36 GMT-04:00 2020  
*@time Wed Oct 07 18:06:36 GMT-04:00 2020  
*/
namespace erp\modules\nomenclatures\models;


/** 
*  Esta es  ActiveQuery clase de [[Register_type]].
 *
 * @see Register_type
 */
/**
 * Register_typeQuery representa la clase de Consulta del modelo Register_type
 */
class Register_typeQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Register_type[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Register_type|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

