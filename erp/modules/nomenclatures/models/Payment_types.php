<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Sat Sep 05 20:15:23 GMT-04:00 2020  
*@time Sat Sep 05 20:15:23 GMT-04:00 2020  
*/
namespace erp\modules\nomenclatures\models;
use Yii;
use common\models\RestModel;

use erp\modules\billings\models\Budgets;
use erp\modules\managment\models\Contacts;
use erp\modules\managment\models\Contracts;
use erp\modules\managment\models\Employees;
/**
 * Este es la clase modelo para la tabla payment_types.
 *
 * Los siguientes son los campos de la tabla 'payment_types':
 * @property integer $id_payment_type
 * @property string $payment_type_siglas
 * @property string $payment_type_desc
 * @property datetime $created_at
 * @property datetime $updated_at

 * Los siguientes son las relaciones de este modelo :

 * @property Budgets[] $arraybudgets
 * @property Contacts[] $arraycontacts
 * @property Contracts[] $arraycontracts
 * @property Employees[] $arrayemployees
 */

class Payment_types extends RestModel 
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_payment_type';

    const MODEL = 'Payment_types';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_types';
    }

     
        /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
       const RELATIONS = ['arraybudgets','arraycontacts','arraycontracts','arrayemployees'];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

       const PKEY = 'id_payment_type';

     /*
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['payment_type_siglas','payment_type_desc','created_at','updated_at'],'required','on'=>['create','default']],
			[['id_payment_type'],'required', 'on' => 'update'],
			[['id_payment_type'],'integer'],
			[['created_at','updated_at'],'safe'],
			['created_at','format_created_at'],
			['updated_at','format_updated_at'],
			[['payment_type_siglas','payment_type_desc'], 'string', 'max'=>255],
			[['id_payment_type'], 'unique' , 'on' => 'create'],
			[['payment_type_siglas'], 'unique' , 'on' => 'create'],
			[['payment_type_desc'], 'unique' , 'on' => 'create'],
			[['payment_type_siglas'], 'unique' , 'on' => 'update','when' =>function ($model, $value) {
                $elem = self::find()->where([$value => $model[$value]])->one();
                return !$elem ? false : $elem[$elem->primaryKey] != $model[$model->primaryKey];
            }],
			[['payment_type_desc'], 'unique' , 'on' => 'update','when' =>function ($model, $value) {
                $elem = self::find()->where([$value => $model[$value]])->one();
                return !$elem ? false : $elem[$elem->primaryKey] != $model[$model->primaryKey];
            }],
        ];
    }

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo payment_type_id
     */
	  public function getArraybudgets()
		{
			return $this->hasMany(Budgets::class, ['payment_type_id' => 'id_payment_type']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo payment_type_id
     */
	  public function getArraycontacts()
		{
			return $this->hasMany(Contacts::class, ['payment_type_id' => 'id_payment_type']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo payment_type_id
     */
	  public function getArraycontracts()
		{
			return $this->hasMany(Contracts::class, ['payment_type_id' => 'id_payment_type']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo payment_type_id
     */
	  public function getArrayemployees()
		{
			return $this->hasMany(Employees::class, ['payment_type_id' => 'id_payment_type']);
		}
 /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'payment_types.id_payment_type as id', 'payment_types.payment_type_siglas as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('payment_types.payment_type_siglas LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
   public function format_created_at(){
        $timestamp = (strpos('/', $this->created_at) > -1) ?
            strtotime(str_replace('/', '-', $this->created_at)) : $this->created_at;
        $this->created_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
   public function format_updated_at(){
        $timestamp = (strpos('/', $this->updated_at) > -1) ?
            strtotime(str_replace('/', '-', $this->updated_at)) : $this->updated_at;
        $this->updated_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
}
?>
