<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Tue Oct 13 18:00:12 GMT-04:00 2020  
*@time Tue Oct 13 18:00:12 GMT-04:00 2020  
*/
namespace erp\modules\nomenclatures\models;
use Yii;
use common\models\RestModel;

use erp\modules\managment\models\Club_members;
/**
 * Este es la clase modelo para la tabla adviser.
 *
 * Los siguientes son los campos de la tabla 'adviser':
 * @property integer $id_adviser
 * @property string $adviser

 * Los siguientes son las relaciones de este modelo :

 * @property Club_members[] $arrayclub_members
 */

class Adviser extends RestModel 
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_adviser';

    const MODEL = 'Adviser';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

     
        /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
       const RELATIONS = ['arrayclub_members'];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

       const PKEY = 'id_user';

     /*
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['adviser'],'required','on'=>['create','default']],
			[['id_adviser'],'required', 'on' => 'update'],
			[['id_adviser'],'integer'],
			[['adviser'], 'string', 'max'=>100],
			[['id_adviser'], 'unique' , 'on' => 'create'],
			[['adviser'], 'unique' , 'on' => 'create'],
			[['adviser'], 'unique' , 'on' => 'update','when' =>function ($model, $value) {
                $elem = self::find()->where([$value => $model[$value]])->one();
                return !$elem ? false : $elem[$elem->primaryKey] != $model[$model->primaryKey];
            }],
        ];
    }

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_adviser
     */
	  public function getArrayclub_members()
		{
			return $this->hasMany(Club_members::class, ['id_adviser' => 'id_adviser']);
		}
 /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'users.id_user as id', 'users.nombre_usuario as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select)->where(['id_role' => 4,]);
        $result->data=$result->data->andWhere('users.id_role LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
}
?>
