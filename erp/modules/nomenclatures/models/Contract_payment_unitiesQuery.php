<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Sat Sep 05 20:15:24 GMT-04:00 2020  
*@time Sat Sep 05 20:15:24 GMT-04:00 2020  
*/
namespace erp\modules\nomenclatures\models;


/** 
*  Esta es  ActiveQuery clase de [[Contract_payment_unities]].
 *
 * @see Contract_payment_unities
 */
/**
 * Contract_payment_unitiesQuery representa la clase de Consulta del modelo Contract_payment_unities
 */
class Contract_payment_unitiesQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Contract_payment_unities[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Contract_payment_unities|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

