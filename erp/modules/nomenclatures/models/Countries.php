<?php
/**Generate by ASGENS
 *@author Charlietyn
 *@date Wed Nov 11 14:29:24 GMT-05:00 2020
 *@time Wed Nov 11 14:29:24 GMT-05:00 2020
 */
namespace erp\modules\nomenclatures\models;
use Yii;
use common\models\RestModel;

use erp\modules\managment\models\Club_members;
use erp\modules\managment\models\Contacts;
use erp\modules\managment\models\Employees;
/**
 * Este es la clase modelo para la tabla countries.
 *
 * Los siguientes son los campos de la tabla 'countries':
 * @property integer $id_country
 * @property string $country_siglas
 * @property string $country_name
 * @property integer $country_code
 * @property datetime $created_at
 * @property datetime $updated_at
 * @property string $iso2

 * Los siguientes son las relaciones de este modelo :

 * @property Club_members[] $arrayclub_members
 * @property Contacts[] $arraycontacts
 * @property Employees[] $arraycountry
 * @property Employees[] $arraywork_country
 * @property States[] $arraystates
 */

class Countries extends RestModel
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_country';

    const MODEL = 'Countries';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'countries';
    }


    /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
    const RELATIONS = ['arrayclub_members','arraycontacts','arraywork_country','arraycountry','arraystates'];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

    const PKEY = 'id_country';

    /*
    * @return \yii\db\Connection the database connection used by this AR class.
    */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at','updated_at'],'required','on'=>['create','default']],
            [['id_country'],'required', 'on' => 'update'],
            [['id_country','country_code'],'integer'],
            [['created_at','updated_at'],'safe'],
            ['created_at','format_created_at'],
            ['updated_at','format_updated_at'],
            [['country_siglas','country_name'], 'string', 'max'=>255],
            [['iso2'], 'string', 'max'=>20],
            [['id_country'], 'unique' , 'on' => 'create'],
            [['country_name'], 'unique' , 'on' => 'create'],
            [['country_siglas'], 'unique' , 'on' => 'create'],
            [['country_name'], 'unique' , 'on' => 'update','when' =>function ($model, $value) {
                $elem = self::find()->where([$value => $model[$value]])->one();
                return !$elem ? false : $elem[$elem->primaryKey] != $model[$model->primaryKey];
            }],
            [['country_siglas'], 'unique' , 'on' => 'update','when' =>function ($model, $value) {
                $elem = self::find()->where([$value => $model[$value]])->one();
                return !$elem ? false : $elem[$elem->primaryKey] != $model[$model->primaryKey];
            }],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_country
     */
    public function getArrayclub_members()
    {
        return $this->hasMany(Club_members::class, ['id_country' => 'id_country']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo country_id
     */
    public function getArraycontacts()
    {
        return $this->hasMany(Contacts::class, ['country_id' => 'id_country']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo country_id
     */
    public function getArraycountry()
    {
        return $this->hasMany(Employees::class, ['country_id' => 'id_country']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo work_country_id
     */
    public function getArraywork_country()
    {
        return $this->hasMany(Employees::class, ['work_country_id' => 'id_country']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo country_id
     */
    public function getArraystates()
    {
        return $this->hasMany(States::class, ['country_id' => 'id_country']);
    }
    /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'countries.id_country as id', 'countries.country_name as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('countries.country_name LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
    public function format_created_at(){
        $timestamp = str_replace('/', '-', $this->created_at);
        $this->created_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
    public function format_updated_at(){
        $timestamp = str_replace('/', '-', $this->updated_at);
        $this->updated_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
}
?>
