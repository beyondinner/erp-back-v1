<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Sat Sep 05 20:15:24 GMT-04:00 2020  
*@time Sat Sep 05 20:15:24 GMT-04:00 2020  
*/
namespace erp\modules\nomenclatures\models;
use Yii;
use common\models\RestModel;

use erp\modules\managment\models\Contracts;
/**
 * Este es la clase modelo para la tabla contract_types.
 *
 * Los siguientes son los campos de la tabla 'contract_types':
 * @property integer $id_contract_type
 * @property string $contract_type_siglas
 * @property string $contract_type_desc
 * @property datetime $created_at
 * @property datetime $updated_at

 * Los siguientes son las relaciones de este modelo :

 * @property Contracts[] $arraycontracts
 */

class Contract_types extends RestModel 
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_contract_type';

    const MODEL = 'Contract_types';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contract_types';
    }

     
        /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
       const RELATIONS = ['arraycontracts'];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

       const PKEY = 'id_contract_type';

     /*
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['contract_type_siglas','contract_type_desc','created_at','updated_at'],'required','on'=>['create','default']],
			[['id_contract_type'],'required', 'on' => 'update'],
			[['id_contract_type'],'integer'],
			[['created_at','updated_at'],'safe'],
			['created_at','format_created_at'],
			['updated_at','format_updated_at'],
			[['contract_type_siglas','contract_type_desc'], 'string', 'max'=>255],
			[['id_contract_type'], 'unique' , 'on' => 'create'],
			[['contract_type_siglas'], 'unique' , 'on' => 'create'],
			[['contract_type_desc'], 'unique' , 'on' => 'create'],
			[['contract_type_siglas'], 'unique' , 'on' => 'update','when' =>function ($model, $value) {
                $elem = self::find()->where([$value => $model[$value]])->one();
                return !$elem ? false : $elem[$elem->primaryKey] != $model[$model->primaryKey];
            }],
			[['contract_type_desc'], 'unique' , 'on' => 'update','when' =>function ($model, $value) {
                $elem = self::find()->where([$value => $model[$value]])->one();
                return !$elem ? false : $elem[$elem->primaryKey] != $model[$model->primaryKey];
            }],
        ];
    }

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo contract_type_id
     */
	  public function getArraycontracts()
		{
			return $this->hasMany(Contracts::class, ['contract_type_id' => 'id_contract_type']);
		}
 /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'contract_types.id_contract_type as id', 'contract_types.contract_type_siglas as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('contract_types.contract_type_siglas LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
   public function format_created_at(){
        $timestamp = (strpos('/', $this->created_at) > -1) ?
            strtotime(str_replace('/', '-', $this->created_at)) : $this->created_at;
        $this->created_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
   public function format_updated_at(){
        $timestamp = (strpos('/', $this->updated_at) > -1) ?
            strtotime(str_replace('/', '-', $this->updated_at)) : $this->updated_at;
        $this->updated_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
}
?>
