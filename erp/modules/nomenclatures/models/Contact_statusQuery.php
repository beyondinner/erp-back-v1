<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Sun Sep 06 18:41:57 GMT-04:00 2020  
*@time Sun Sep 06 18:41:57 GMT-04:00 2020  
*/
namespace erp\modules\nomenclatures\models;


/** 
*  Esta es  ActiveQuery clase de [[Contact_status]].
 *
 * @see Contact_status
 */
/**
 * Contact_statusQuery representa la clase de Consulta del modelo Contact_status
 */
class Contact_statusQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Contact_status[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Contact_status|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

