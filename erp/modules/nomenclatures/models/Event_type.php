<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Oct 07 18:06:36 GMT-04:00 2020  
*@time Wed Oct 07 18:06:36 GMT-04:00 2020  
*/
namespace erp\modules\nomenclatures\models;
use Yii;
use common\models\RestModel;

use erp\modules\managment\models\Event;
/**
 * Este es la clase modelo para la tabla event_type.
 *
 * Los siguientes son los campos de la tabla 'event_type':
 * @property integer $id_event_type
 * @property string $event_type
 * @property string $event_type_description

 * Los siguientes son las relaciones de este modelo :

 * @property Event[] $arrayevent
 */

class Event_type extends RestModel 
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_event_type';

    const MODEL = 'Event_type';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_type';
    }

     
        /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
       const RELATIONS = ['arrayevent'];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

       const PKEY = 'id_event_type';

     /*
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['event_type'],'required','on'=>['create','default']],
			[['id_event_type'],'required', 'on' => 'update'],
			[['id_event_type'],'integer'],
			[['event_type'], 'string', 'max'=>20],
			[['event_type_description'], 'string', 'max'=>65535],
			[['id_event_type'], 'unique' , 'on' => 'create'],
			[['event_type'], 'unique' , 'on' => 'create'],
			[['event_type'], 'unique' , 'on' => 'update','when' =>function ($model, $value) {
                $elem = self::find()->where([$value => $model[$value]])->one();
                return !$elem ? false : $elem[$elem->primaryKey] != $model[$model->primaryKey];
            }],
        ];
    }

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_event_type
     */
	  public function getArrayevent()
		{
			return $this->hasMany(Event::class, ['id_event_type' => 'id_event_type']);
		}
 /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'event_type.id_event_type as id', 'event_type.event_type as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('event_type.event_type LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
}
?>
