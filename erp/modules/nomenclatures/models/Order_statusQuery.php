<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Nov 25 10:19:28 GMT-05:00 2020  
*@time Wed Nov 25 10:19:28 GMT-05:00 2020  
*/
namespace erp\modules\nomenclatures\models;


/** 
*  Esta es  ActiveQuery clase de [[Order_status]].
 *
 * @see Order_status
 */
/**
 * Order_statusQuery representa la clase de Consulta del modelo Order_status
 */
class Order_statusQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Order_status[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Order_status|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

