<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Sat Sep 05 20:15:23 GMT-04:00 2020  
*@time Sat Sep 05 20:15:23 GMT-04:00 2020  
*/
namespace erp\modules\nomenclatures\models;


/** 
*  Esta es  ActiveQuery clase de [[Payment_types]].
 *
 * @see Payment_types
 */
/**
 * Payment_typesQuery representa la clase de Consulta del modelo Payment_types
 */
class Payment_typesQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Payment_types[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Payment_types|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

