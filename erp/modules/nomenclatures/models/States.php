<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Oct 07 18:06:36 GMT-04:00 2020  
*@time Wed Oct 07 18:06:36 GMT-04:00 2020  
*/
namespace erp\modules\nomenclatures\models;
use Yii;
use common\models\RestModel;

use erp\modules\managment\models\Contacts;
use erp\modules\managment\models\Employees;
/**
 * Este es la clase modelo para la tabla states.
 *
 * Los siguientes son los campos de la tabla 'states':
 * @property integer $id_state
 * @property string $state_name
 * @property string $country_code
 * @property string $district
 * @property integer $country_id
 * @property datetime $created_at
 * @property datetime $updated_at
 * @property integer $population

 * Los siguientes son las relaciones de este modelo :

 * @property Countries $country
 * @property Contacts[] $arraycontacts
 * @property Employees[] $arrayemployees
 */

class States extends RestModel 
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_state';

    const MODEL = 'States';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'states';
    }

     
        /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
       const RELATIONS = ['country','arraycontacts','arrayemployees'];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

       const PKEY = 'id_state';

     /*
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['state_name','created_at','updated_at'],'required','on'=>['create','default']],
			[['id_state'],'required', 'on' => 'update'],
			[['id_state','country_id','population'],'integer'],
			[['created_at','updated_at'],'safe'],
			['created_at','format_created_at'],
			['updated_at','format_updated_at'],
			[['state_name','country_code'], 'string', 'max'=>255],
			[['district'], 'string', 'max'=>20],
			[['id_state'], 'unique' , 'on' => 'create'],
        ];
    }
	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo country_id
     */
	  public function getCountry()
		{
			return $this->hasOne(Countries::class, ['id_country' => 'country_id']);
		}


	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo state_id
     */
	  public function getArraycontacts()
		{
			return $this->hasMany(Contacts::class, ['state_id' => 'id_state']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo state_id
     */
	  public function getArrayemployees()
		{
			return $this->hasMany(Employees::class, ['state_id' => 'id_state']);
		}
 /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'states.id_state as id', 'states.state_name as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('states.state_name LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
   public function format_created_at(){
        $timestamp = (strpos('/', $this->created_at) > -1) ?
            strtotime(str_replace('/', '-', $this->created_at)) : $this->created_at;
        $this->created_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
   public function format_updated_at(){
        $timestamp = (strpos('/', $this->updated_at) > -1) ?
            strtotime(str_replace('/', '-', $this->updated_at)) : $this->updated_at;
        $this->updated_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
}
?>
