<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Sun Sep 06 18:31:24 GMT-04:00 2020  
*@time Sun Sep 06 18:31:24 GMT-04:00 2020  
*/
namespace erp\modules\nomenclatures\models;


/** 
*  Esta es  ActiveQuery clase de [[Contract_status]].
 *
 * @see Contract_status
 */
/**
 * Contract_statusQuery representa la clase de Consulta del modelo Contract_status
 */
class Contract_statusQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Contract_status[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Contract_status|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

