<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Tue Oct 13 18:00:12 GMT-04:00 2020  
*@time Tue Oct 13 18:00:12 GMT-04:00 2020  
*/
namespace erp\modules\nomenclatures\services;


use common\services\Services;

class AdviserService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\nomenclatures\models\Adviser';

}
