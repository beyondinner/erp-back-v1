<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Oct 07 18:06:36 GMT-04:00 2020  
*@time Wed Oct 07 18:06:36 GMT-04:00 2020  
*/
namespace erp\modules\nomenclatures\services;


use common\services\Services;

class Event_typeService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\nomenclatures\models\Event_type';

}
