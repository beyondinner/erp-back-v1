<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Sat Sep 05 20:15:23 GMT-04:00 2020  
*@time Sat Sep 05 20:15:23 GMT-04:00 2020  
*/
namespace erp\modules\nomenclatures\services;


use common\services\Services;

class Contact_typesService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\nomenclatures\models\Contact_types';

}
