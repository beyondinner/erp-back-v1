<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Sun Sep 06 18:31:24 GMT-04:00 2020  
*@time Sun Sep 06 18:31:24 GMT-04:00 2020  
*/
namespace erp\modules\nomenclatures\services;


use common\services\Services;

class Contract_statusService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\nomenclatures\models\Contract_status';

}
