<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Nov 25 10:19:28 GMT-05:00 2020  
*@time Wed Nov 25 10:19:28 GMT-05:00 2020  
*/
namespace erp\modules\nomenclatures\services;


use common\services\Services;

class Order_statusService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\nomenclatures\models\Order_status';

}
