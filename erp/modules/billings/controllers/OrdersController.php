<?php
/**
 * /**Generate by ASGENS
 * @author Charlietyn
 * @date Tue Nov 24 13:23:36 GMT-05:00 2020
 * @time Tue Nov 24 13:23:36 GMT-05:00 2020
 */

namespace erp\modules\billings\controllers;


use common\controllers\RestController;
use erp\modules\billings\services\OrdersService;
use yii\helpers\Url;
use yii\filters\Cors;
use erp\modules\billings\models\Orders;
use yii\web\HttpException;
use yii\web\ServerErrorHttpException;

class OrdersController extends RestController
{
    /**
     * {@inheritdoc}
     */
    public $service;
    public $modelClass = 'erp\modules\billings\models\Orders';


    public function behaviors()
    {
        $array = parent::behaviors();
        $array['authenticator']['except'] = ['index', 'create', 'register_order', 'update', 'delete', 'view', 'select_2_list', 'validate', 'delete_parameters', 'delete_by_id', 'update_multiple'];
        $array['cors'] = [
            'class' => Cors::class,
            'actions' => [
                'your-action-name' => [
                    #web-servers which you alllow cross-domain access
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => [],
                ]
            ],
        ];
        return $array;
    }

    protected function verbs()
    {
        $array = ['register_order' => ['OPTIONS', 'POST']];
        return array_merge(parent::verbs(), $array);
    }

    public function getService()
    {
        if ($this->service == null)
            $this->service = new OrdersService();
        return $this->service;
    }

    public function actionRegister_order()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = \Yii::$app->request->getBodyParams();
        if (count($params) == 0)
            throw new HttpException(500, "Theres no parameters in request");
        return $this->getService()->register_order($params);
    }

}
