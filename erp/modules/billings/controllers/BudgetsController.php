<?php
/**
 * /**Generate by ASGENS
 * @author Charlietyn
 * @date Sat Sep 05 20:15:23 GMT-04:00 2020
 * @time Sat Sep 05 20:15:23 GMT-04:00 2020
 */

namespace erp\modules\billings\controllers;


use common\controllers\RestController;
use erp\modules\billings\services\BudgetsService;
use yii\helpers\Url;
use yii\filters\Cors;
use erp\modules\billings\models\Budgets;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

class BudgetsController extends RestController
{
    /**
     * {@inheritdoc}
     */
    public $service;
    public $modelClass = 'erp\modules\billings\models\Budgets';


    public function behaviors()
    {
        $array = parent::behaviors();
        $array['authenticator']['except'] = ['index','pdf','register_budget','register_budget_bill', 'create', 'update', 'delete', 'view', 'select_2_list', 'validate', 'delete_parameters', 'delete_by_id', 'update_multiple'];
        $array['cors'] = [
            'class' => Cors::class,
            'actions' => [
                'your-action-name' => [
                    #web-servers which you alllow cross-domain access
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => [],
                ]
            ],
        ];
        return $array;
    }

    protected function verbs()
    {
        $array = [
            'register_budget' => ['GET', 'HEAD', 'PUT', 'OPTIONS', 'POST'],
            'register_budget_bill' => ['GET', 'HEAD', 'PUT', 'OPTIONS', 'POST'],
            'pdf' => ['GET', 'HEAD', 'PUT', 'OPTIONS', 'POST'],
        ];
        return array_merge(parent::verbs(), $array);
    }

    public function getService()
    {
        if ($this->service == null)
            $this->service = new BudgetsService();
        return $this->service;
    }

    public function actionRegister_budget()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = \Yii::$app->request->getBodyParams();
        return $this->getService()->register($params);
    }

    public function actionPdf($id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model= Budgets::findOne($id);
        if (is_null($model)) {
            \Yii::$app->getResponse()->setStatusCode(404);
            throw new NotFoundHttpException('Element not found.');
        }
        return $this->getService()->pdf($model);
    }

    public function actionRegister_budget_bill()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = \Yii::$app->request->getBodyParams();
        $model= Budgets::findOne($params['id_budget']);
        if (is_null($model)) {
            \Yii::$app->getResponse()->setStatusCode(404);
            throw new NotFoundHttpException('Element not found.');
        }
        try {
            return $this->getService()->register_budget_bill($params);
        } catch (\Throwable $e) {
            throw new NotFoundHttpException($e->getMessage());
        }

    }

}
