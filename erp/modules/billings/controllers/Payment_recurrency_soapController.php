<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Mar 17 23:53:31 GMT-04:00 2021  
*@time Wed Mar 17 23:53:31 GMT-04:00 2021  
*/
namespace erp\modules\billings\controllers;

use common\controllers\SecureController;
use erp\modules\billings\models\Payment_recurrency;

class Payment_recurrency_soapController extends SecureController
{

    /** @var bool */
    public $enableCsrfValidation = false;

    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\billings\models\Payment_recurrency';


    public function behaviors()
    {
        $array = parent::behaviors();
        $array['authenticator']['except'] = ['payment_recurrency_data'];
        return $array;
    }
    /**
     * {@inheritdoc}
     * redefine las acciones restful de la controladora
     */
    public function actions()
    {
        return [
            'payment_recurrency_data' => [
                'class' => 'mongosoft\soapserver\Action',
                'classMap' => ['Payment_recurrency' => 'erp\modules\billings\models\Payment_recurrency'],
                'serviceOptions' => [
                    'disableWsdlMode' => true,
                ]
            ],
        ];
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        parent::checkAccess($action, $model, $params); // TODO: Change the autogenerated stub
    }


    public function encode_result($result)
    {
        $encode = false;
        if (strpos(\Yii::$app->getRequest()->contentType, "application/json") !== false)
            $encode = true;
        if ($encode)
            return json_encode($result, JSON_UNESCAPED_UNICODE);
        return $result;
    }

    /**
     * Simple test which returns a List of payment_recurrency in order to see how the wsdl pans out
     * @param [] $params
     * @return object[]
     * @soap
     */

    public function payment_recurrency_list()
    {
        $params=[];
        if (func_get_args())
            $params = func_get_args()[0]['params'];
        return $this->encode_result(Payment_recurrency::list_model($params));
    }

    /**
     * Simple test which returns a data of payment_recurrency  with id in order to see how the wsdl pans out
     * @param int $id
     * @return erp\modules\billings\models\Payment_recurrency
     * @soap
     */
    public function payment_recurrency_view($id)
    {
        $params=[];
        if (func_get_args())
            $params = func_get_args()[0]['params'];
        return $this->encode_result(Payment_recurrency::view($id, $params));
    }
}
