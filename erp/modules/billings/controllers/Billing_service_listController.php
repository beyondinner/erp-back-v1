<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Mon Oct 19 15:57:25 GMT-04:00 2020  
*@time Mon Oct 19 15:57:25 GMT-04:00 2020  
*/
namespace erp\modules\billings\controllers;


use common\controllers\RestController;
use erp\modules\billings\services\Billing_service_listService;
use yii\helpers\Url;
use yii\filters\Cors;
use erp\modules\billings\models\Billing_service_list;
use yii\web\ServerErrorHttpException;

class Billing_service_listController extends RestController
{
    /**
     * {@inheritdoc}
     */
    public $service ;
    public $modelClass = 'erp\modules\billings\models\Billing_service_list';


    public function behaviors()
    {
        $array= parent::behaviors();
        $array['authenticator']['except']= ['index','create', 'update', 'delete', 'view', 'select_2_list', 'validate', 'delete_parameters', 'delete_by_id','update_multiple'];
        $array['cors']=[
            'class' => Cors::class,
            'actions' => [
                'your-action-name' => [
                    #web-servers which you alllow cross-domain access
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST','OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => [],
                ]
            ],
        ];
        return $array;
    }

    protected function verbs()
    {
        $array= [];
        return array_merge(parent::verbs(),$array);
    }
   public function getService()
    {
        if ($this->service == null)
            $this->service = new Billing_service_listService();
        return $this->service;
    }

}
