<?php
/**
 * /**Generate by ASGENS
 * @author Charlietyn
 * @date Wed Mar 17 23:53:31 GMT-04:00 2021
 * @time Wed Mar 17 23:53:31 GMT-04:00 2021
 */

namespace erp\modules\billings\controllers;


use common\controllers\RestController;
use erp\modules\billings\services\Payment_recurrencyService;
use yii\helpers\Url;
use yii\filters\Cors;
use erp\modules\billings\models\Payment_recurrency;
use yii\web\ServerErrorHttpException;

class Payment_recurrencyController extends RestController
{
    /**
     * {@inheritdoc}
     */
    public $service;
    public $modelClass = 'erp\modules\billings\models\Payment_recurrency';


    public function behaviors()
    {
        $array = parent::behaviors();
        $array['authenticator']['except'] = ['index', 'recurrency', 'create', 'update', 'delete', 'view', 'select_2_list', 'validate', 'delete_parameters', 'delete_by_id', 'update_multiple'];
        $array['cors'] = [
            'class' => Cors::class,
            'actions' => [
                'your-action-name' => [
                    #web-servers which you alllow cross-domain access
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => [],
                ]
            ],
        ];
        return $array;
    }

    protected function verbs()
    {
        $array = [];
        return array_merge(parent::verbs(), $array);
    }

    public function getService()
    {
        if ($this->service == null)
            $this->service = new Payment_recurrencyService();
        return $this->service;
    }

    public function actionRecurrency()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = \Yii::$app->request->getBodyParams();
        return $this->getService()->recurrency($params);
    }

}
