<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Tue Nov 24 13:23:36 GMT-05:00 2020  
*@time Tue Nov 24 13:23:36 GMT-05:00 2020  
*/
namespace erp\modules\billings\controllers;

use common\controllers\SecureController;
use erp\modules\billings\models\Orders;

class Orders_soapController extends SecureController
{

    /** @var bool */
    public $enableCsrfValidation = false;

    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\billings\models\Orders';


    public function behaviors()
    {
        $array = parent::behaviors();
        $array['authenticator']['except'] = ['orders_data'];
        return $array;
    }
    /**
     * {@inheritdoc}
     * redefine las acciones restful de la controladora
     */
    public function actions()
    {
        return [
            'orders_data' => [
                'class' => 'mongosoft\soapserver\Action',
                'classMap' => ['Orders' => 'erp\modules\billings\models\Orders'],
                'serviceOptions' => [
                    'disableWsdlMode' => true,
                ]
            ],
        ];
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        parent::checkAccess($action, $model, $params); // TODO: Change the autogenerated stub
    }


    public function encode_result($result)
    {
        $encode = false;
        if (strpos(\Yii::$app->getRequest()->contentType, "application/json") !== false)
            $encode = true;
        if ($encode)
            return json_encode($result, JSON_UNESCAPED_UNICODE);
        return $result;
    }

    /**
     * Simple test which returns a List of orders in order to see how the wsdl pans out
     * @param [] $params
     * @return object[]
     * @soap
     */

    public function orders_list()
    {
        $params=[];
        if (func_get_args())
            $params = func_get_args()[0]['params'];
        return $this->encode_result(Orders::list_model($params));
    }

    /**
     * Simple test which returns a data of orders  with id in order to see how the wsdl pans out
     * @param int $id
     * @return erp\modules\billings\models\Orders
     * @soap
     */
    public function orders_view($id)
    {
        $params=[];
        if (func_get_args())
            $params = func_get_args()[0]['params'];
        return $this->encode_result(Orders::view($id, $params));
    }
}
