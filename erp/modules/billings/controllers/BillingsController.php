<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Sat Sep 05 20:15:23 GMT-04:00 2020  
*@time Sat Sep 05 20:15:23 GMT-04:00 2020  
*/
namespace erp\modules\billings\controllers;


use common\controllers\RestController;
use erp\modules\billings\services\BillingsService;
use yii\helpers\Url;
use yii\filters\Cors;
use erp\modules\billings\models\Billings;
use yii\web\HttpException;
use yii\web\ServerErrorHttpException;

class BillingsController extends RestController
{
    /**
     * {@inheritdoc}
     */
    public $service ;
    public $modelClass = 'erp\modules\billings\models\Billings';


    public function behaviors()
    {
        $array= parent::behaviors();
        $array['authenticator']['except']= ['index','create', 'register_billing', 'update', 'delete', 'view', 'select_2_list', 'validate', 'delete_parameters', 'delete_by_id','update_multiple'];
        $array['cors']=[
            'class' => Cors::class,
            'actions' => [
                'your-action-name' => [
                    #web-servers which you alllow cross-domain access
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST','OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => [],
                ]
            ],
        ];
        return $array;
    }

    protected function verbs()
    {
        $array= [
            'register_billing' => ['OPTIONS', 'POST'],
        ];
        return array_merge(parent::verbs(),$array);
    }

   public function getService()
    {
        if ($this->service == null)
            $this->service = new BillingsService();
        return $this->service;
    }

    public function actionRegister_billing()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = \Yii::$app->request->getBodyParams();
        if (count($params) == 0)
            throw new HttpException(500, "Theres no parameters in request");
        return $this->getService()->register($params);
    }

}
