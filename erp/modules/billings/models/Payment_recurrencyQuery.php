<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Mar 17 23:53:31 GMT-04:00 2021  
*@time Wed Mar 17 23:53:31 GMT-04:00 2021  
*/
namespace erp\modules\billings\models;


/** 
*  Esta es  ActiveQuery clase de [[Payment_recurrency]].
 *
 * @see Payment_recurrency
 */
/**
 * Payment_recurrencyQuery representa la clase de Consulta del modelo Payment_recurrency
 */
class Payment_recurrencyQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Payment_recurrency[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Payment_recurrency|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

