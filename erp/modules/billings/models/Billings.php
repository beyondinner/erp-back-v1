<?php
/**Generate by ASGENS
 *@author Charlietyn
 *@date Mon Oct 19 22:24:35 GMT-04:00 2020
 *@time Mon Oct 19 22:24:35 GMT-04:00 2020
 */
namespace erp\modules\billings\models;
use Yii;
use common\models\RestModel;

use erp\modules\nomenclatures\models\Billings_status;
use erp\modules\managment\models\Contacts;
use erp\modules\nomenclatures\models\Payment_types;
/**
 * Este es la clase modelo para la tabla billings.
 *
 * Los siguientes son los campos de la tabla 'billings':
 * @property integer $id_billings
 * @property integer $budget_id
 * @property integer $billings_status_id
 * @property string $billings_doc_number
 * @property datetime $created_at
 * @property datetime $updated_at
 * @property integer $id_contact
 * @property datetime $date_at
 * @property datetime $expiration_date
 * @property integer $id_payment_type
 * @property string $billing_description
 * @property string $billing_types

 * Los siguientes son las relaciones de este modelo :

 * @property Budgets $budget
 * @property Billings_status $billings_status
 * @property Contacts $contact
 * @property Payment_types $payment_type
 * @property Billing_product_list[] $arraybilling_product_list
 * @property Billing_service_list[] $arraybilling_service_list
 */

class Billings extends RestModel
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_billings';

    const MODEL = 'Billings';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'billings';
    }


    /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
    const RELATIONS = ['budget','billings_status','contact','payment_type','arraybilling_product_list','arraybilling_service_list'];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

    const PKEY = 'id_billings';

    /*
    * @return \yii\db\Connection the database connection used by this AR class.
    */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['billings_status_id','billings_doc_number','created_at','updated_at','date_at','expiration_date','billing_description'],'required','on'=>['create','default']],
            [['id_billings'],'required', 'on' => 'update'],
            [['id_billings','budget_id','billings_status_id','id_contact','id_payment_type'],'integer'],
            [['created_at','updated_at','date_at','expiration_date'],'safe'],
            ['created_at','format_created_at'],
            ['updated_at','format_updated_at'],
            ['date_at','format_date_at'],
            ['expiration_date','format_expiration_date'],
            [['billings_doc_number'], 'string', 'max'=>255],
            [['billing_types'], 'string', 'max'=>20],
            [['billing_description'], 'string', 'max'=>65535],
            [['id_billings'], 'unique' , 'on' => 'create'],
            [['billings_doc_number'], 'unique' , 'on' => 'create'],
            [['billings_doc_number'], 'unique' , 'on' => 'update','when' =>function ($model, $value) {
                $elem = self::find()->where([$value => $model[$value]])->one();
                return !$elem ? false : $elem[$elem->primaryKey] != $model[$model->primaryKey];
            }],
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo budget_id
     */
    public function getBudget()
    {
        return $this->hasOne(Budgets::class, ['id_budget' => 'budget_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo billings_status_id
     */
    public function getBillings_status()
    {
        return $this->hasOne(Billings_status::class, ['id_billings_status' => 'billings_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_contact
     */
    public function getContact()
    {
        return $this->hasOne(Contacts::class, ['id_contact' => 'id_contact']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_payment_type
     */
    public function getPayment_type()
    {
        return $this->hasOne(Payment_types::class, ['id_payment_type' => 'id_payment_type']);
    }


    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_billing
     */
    public function getArraybilling_product_list()
    {
        return $this->hasMany(Billing_product_list::class, ['id_billing' => 'id_billings']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_billing
     */
    public function getArraybilling_service_list()
    {
        return $this->hasMany(Billing_service_list::class, ['id_billing' => 'id_billings']);
    }
    /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'billings.id_billings as id', 'billings.billings_doc_number as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('billings.billings_doc_number LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
    public function format_created_at(){
        $timestamp = str_replace('/', '-', $this->created_at);
        $this->created_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
    public function format_updated_at(){
        $timestamp = str_replace('/', '-', $this->updated_at);
        $this->updated_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
    public function format_date_at(){
        $timestamp = str_replace('/', '-', $this->date_at);
        $this->date_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
    public function format_expiration_date(){
        $timestamp = str_replace('/', '-', $this->expiration_date);
        $this->expiration_date = date('Y-m-d h:i:s', strtotime($timestamp));
    }
}
?>
