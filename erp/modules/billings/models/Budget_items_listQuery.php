<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Sat Sep 05 20:15:23 GMT-04:00 2020  
*@time Sat Sep 05 20:15:23 GMT-04:00 2020  
*/
namespace erp\modules\billings\models;


/** 
*  Esta es  ActiveQuery clase de [[Budget_items_list]].
 *
 * @see Budget_items_list
 */
/**
 * Budget_items_listQuery representa la clase de Consulta del modelo Budget_items_list
 */
class Budget_items_listQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Budget_items_list[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Budget_items_list|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

