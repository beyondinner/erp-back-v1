<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Mon Oct 12 01:30:42 GMT-04:00 2020  
*@time Mon Oct 12 01:30:42 GMT-04:00 2020  
*/
namespace erp\modules\billings\models;
use Yii;
use common\models\RestModel;

/**
 * Este es la clase modelo para la tabla budget_items_list_taxes.
 *
 * Los siguientes son los campos de la tabla 'budget_items_list_taxes':
 * @property integer $id_budget_items_list_taxes
 * @property string $tax_name
 * @property integer $budget_items_list_taxes_tax
 * @property datetime $created_at
 * @property datetime $updated_at

 * Los siguientes son las relaciones de este modelo :

 * @property Budget_items_list[] $arraybudget_items_list
 */

class Budget_items_list_taxes extends RestModel 
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_budget_items_list_taxes';

    const MODEL = 'Budget_items_list_taxes';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'budget_items_list_taxes';
    }

     
        /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
       const RELATIONS = ['arraybudget_items_list'];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

       const PKEY = 'id_budget_items_list_taxes';

     /*
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['tax_name','budget_items_list_taxes_tax','created_at','updated_at'],'required','on'=>['create','default']],
			[['id_budget_items_list_taxes'],'required', 'on' => 'update'],
			[['id_budget_items_list_taxes','budget_items_list_taxes_tax'],'integer'],
			[['created_at','updated_at'],'safe'],
			['created_at','format_created_at'],
			['updated_at','format_updated_at'],
			[['tax_name'], 'string', 'max'=>20],
			[['id_budget_items_list_taxes'], 'unique' , 'on' => 'create'],
			[['tax_name'], 'unique' , 'on' => 'create'],
			[['tax_name'], 'unique' , 'on' => 'update','when' =>function ($model, $value) {
                $elem = self::find()->where([$value => $model[$value]])->one();
                return !$elem ? false : $elem[$elem->primaryKey] != $model[$model->primaryKey];
            }],
        ];
    }

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo tax_id
     */
	  public function getArraybudget_items_list()
		{
			return $this->hasMany(Budget_items_list::class, ['tax_id' => 'id_budget_items_list_taxes']);
		}
 /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'budget_items_list_taxes.id_budget_items_list_taxes as id', 'budget_items_list_taxes.tax_name as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('budget_items_list_taxes.tax_name LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
   public function format_created_at(){
        $timestamp = (strpos('/', $this->created_at) > -1) ?
            strtotime(str_replace('/', '-', $this->created_at)) : $this->created_at;
        $this->created_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
   public function format_updated_at(){
        $timestamp = (strpos('/', $this->updated_at) > -1) ?
            strtotime(str_replace('/', '-', $this->updated_at)) : $this->updated_at;
        $this->updated_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
}
?>
