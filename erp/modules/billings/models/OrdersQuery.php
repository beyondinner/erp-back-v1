<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Tue Nov 24 13:23:36 GMT-05:00 2020  
*@time Tue Nov 24 13:23:36 GMT-05:00 2020  
*/
namespace erp\modules\billings\models;


/** 
*  Esta es  ActiveQuery clase de [[Orders]].
 *
 * @see Orders
 */
/**
 * OrdersQuery representa la clase de Consulta del modelo Orders
 */
class OrdersQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Orders[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Orders|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

