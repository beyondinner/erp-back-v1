<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Mon Oct 19 15:57:25 GMT-04:00 2020  
*@time Mon Oct 19 15:57:25 GMT-04:00 2020  
*/
namespace erp\modules\billings\models;
use Yii;
use common\models\RestModel;

use erp\modules\managment\models\Products;
/**
 * Este es la clase modelo para la tabla billing_product_list.
 *
 * Los siguientes son los campos de la tabla 'billing_product_list':
 * @property integer $id_billing_product_list
 * @property float $billing_product_list_price
 * @property string $billing_product_list_concept
 * @property string $billing_product_list_desc
 * @property integer $billing_product_list_quantity
 * @property integer $tax_id
 * @property datetime $created_at
 * @property datetime $updated_at
 * @property integer $id_product
 * @property integer $id_billing

 * Los siguientes son las relaciones de este modelo :

 * @property Products $product
 * @property Budget_items_list_taxes $tax
 * @property Billings $billing
 */

class Billing_product_list extends RestModel 
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_billing_product_list';

    const MODEL = 'Billing_product_list';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'billing_product_list';
    }

     
        /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
       const RELATIONS = ['product','tax','billing'];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

       const PKEY = 'id_billing_product_list';

     /*
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['billing_product_list_price','billing_product_list_concept','billing_product_list_desc','billing_product_list_quantity','tax_id','created_at','updated_at','id_billing'],'required','on'=>['create','default']],
			[['id_billing_product_list'],'required', 'on' => 'update'],
			[['id_billing_product_list','billing_product_list_quantity','tax_id','id_product','id_billing'],'integer'],
			[['billing_product_list_price'],'number'],
			[['billing_product_list_price'],'safe'],
			[['created_at','updated_at'],'safe'],
			['created_at','format_created_at'],
			['updated_at','format_updated_at'],
			[['billing_product_list_concept','billing_product_list_desc'], 'string', 'max'=>255],
			[['id_billing_product_list'], 'unique' , 'on' => 'create'],
        ];
    }
	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_product
     */
	  public function getProduct()
		{
			return $this->hasOne(Products::class, ['id_product' => 'id_product']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo tax_id
     */
	  public function getTax()
		{
			return $this->hasOne(Budget_items_list_taxes::class, ['id_budget_items_list_taxes' => 'tax_id']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_billing
     */
	  public function getBilling()
		{
			return $this->hasOne(Billings::class, ['id_billings' => 'id_billing']);
		}

 /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'billing_product_list.id_billing_product_list as id', 'billing_product_list.billing_product_list_concept as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('billing_product_list.billing_product_list_concept LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
   public function format_created_at(){
        $timestamp = str_replace('/', '-', $this->created_at);
        $this->created_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
   public function format_updated_at(){
        $timestamp = str_replace('/', '-', $this->updated_at);
        $this->updated_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
}
?>
