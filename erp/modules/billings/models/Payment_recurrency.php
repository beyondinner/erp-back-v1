<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Thu Mar 18 00:31:16 GMT-04:00 2021  
*@time Thu Mar 18 00:31:16 GMT-04:00 2021  
*/
namespace erp\modules\billings\models;
use Yii;
use common\models\RestModel;

use erp\modules\managment\models\Services;
use erp\modules\managment\models\Club_members;
/**
 * Este es la clase modelo para la tabla payment_recurrency.
 *
 * Los siguientes son los campos de la tabla 'payment_recurrency':
 * @property integer $id_payment_recurrency
 * @property integer $id_service
 * @property integer $id_club_members
 * @property date $initial_date
 * @property integer $recurrency_date
 * @property boolean $active

 * Los siguientes son las relaciones de este modelo :

 * @property Services $service
 * @property Club_members $club_members
 */

class Payment_recurrency extends RestModel 
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_payment_recurrency';

    const MODEL = 'Payment_recurrency';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_recurrency';
    }

     
        /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
       const RELATIONS = ['service','club_members'];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

       const PKEY = 'id_payment_recurrency';

     /*
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['id_club_members','initial_date','recurrency_date'],'required','on'=>['create','default']],
			[['id_payment_recurrency'],'required', 'on' => 'update'],
			[['id_payment_recurrency','id_service','id_club_members','recurrency_date'],'integer'],
			[['active'],'boolean'],
			[['initial_date'],'safe'],
			['initial_date','format_initial_date'],
			[['active'],'safe'],
			[['id_payment_recurrency'], 'unique' , 'on' => 'create'],
        ];
    }
	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_service
     */
	  public function getService()
		{
			return $this->hasOne(Services::class, ['id_service' => 'id_service']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_club_members
     */
	  public function getClub_members()
		{
			return $this->hasOne(Club_members::class, ['id_club_members' => 'id_club_members']);
		}

 /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'payment_recurrency.id_payment_recurrency as id', 'payment_recurrency.id_payment_recurrency as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('payment_recurrency.id_payment_recurrency LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
   public function format_initial_date(){
        $timestamp = str_replace('/', '-', $this->initial_date);
        $this->initial_date = date('Y-m-d h:i:s', strtotime($timestamp));
    }
}
?>
