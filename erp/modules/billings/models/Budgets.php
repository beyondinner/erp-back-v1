<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Sat Sep 05 20:15:23 GMT-04:00 2020  
*@time Sat Sep 05 20:15:23 GMT-04:00 2020  
*/
namespace erp\modules\billings\models;
use Yii;
use common\models\RestModel;

use erp\modules\nomenclatures\models\Budget_status;
use erp\modules\nomenclatures\models\Payment_types;
use erp\modules\managment\models\Contacts;
/**
 * Este es la clase modelo para la tabla budgets.
 *
 * Los siguientes son los campos de la tabla 'budgets':
 * @property integer $id_budget
 * @property integer $contact_id
 * @property string $budget_doc_number
 * @property date $budget_start_date
 * @property date $budget_expiration_date
 * @property integer $payment_type_id
 * @property string $budget_internal_desc
 * @property integer $budget_status_id
 * @property datetime $created_at
 * @property datetime $updated_at

 * Los siguientes son las relaciones de este modelo :

 * @property Budget_status $budget_status
 * @property Payment_types $payment_types
 * @property Contacts $contacts
 * @property Billings[] $arraybillings
 * @property Budget_items_list[] $arraybudget_items_list
 */

class Budgets extends RestModel 
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_budget';

    const MODEL = 'Budgets';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'budgets';
    }

     
        /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
       const RELATIONS = ['budget_status','payment_types','contacts','arraybillings','arraybudget_items_list'];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

       const PKEY = 'id_budget';

     /*
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['contact_id','budget_doc_number','budget_start_date','budget_expiration_date','payment_type_id','budget_status_id','created_at','updated_at'],'required','on'=>['create','default']],
			[['id_budget'],'required', 'on' => 'update'],
			[['id_budget','contact_id','payment_type_id','budget_status_id'],'integer'],
			[['budget_start_date','budget_expiration_date','created_at','updated_at'],'safe'],
			['budget_start_date','format_budget_start_date'],
			['budget_expiration_date','format_budget_expiration_date'],
			['created_at','format_created_at'],
			['updated_at','format_updated_at'],
			[['budget_doc_number'], 'string', 'max'=>255],
			[['budget_internal_desc'], 'string', 'max'=>65535],
			[['id_budget'], 'unique' , 'on' => 'create'],
        ];
    }
	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo budget_status_id
     */
	  public function getBudget_status()
		{
			return $this->hasOne(Budget_status::class, ['id_budget_status' => 'budget_status_id']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo payment_type_id
     */
	  public function getPayment_types()
		{
			return $this->hasOne(Payment_types::class, ['id_payment_type' => 'payment_type_id']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo contact_id
     */
	  public function getContacts()
		{
			return $this->hasOne(Contacts::class, ['id_contact' => 'contact_id']);
		}


	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo budget_id
     */
	  public function getArraybillings()
		{
			return $this->hasMany(Billings::class, ['budget_id' => 'id_budget']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo budget_id
     */
	  public function getArraybudget_items_list()
		{
			return $this->hasMany(Budget_items_list::class, ['budget_id' => 'id_budget']);
		}
 /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'budgets.id_budget as id', 'budgets.budget_doc_number as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('budgets.budget_doc_number LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
   public function format_budget_start_date(){
        $timestamp = (strpos('/', $this->budget_start_date) > -1) ?
            strtotime(str_replace('/', '-', $this->budget_start_date)) : $this->budget_start_date;
        $this->budget_start_date = date('Y-m-d h:i:s', strtotime($timestamp));
    }
   public function format_budget_expiration_date(){
        $timestamp = (strpos('/', $this->budget_expiration_date) > -1) ?
            strtotime(str_replace('/', '-', $this->budget_expiration_date)) : $this->budget_expiration_date;
        $this->budget_expiration_date = date('Y-m-d h:i:s', strtotime($timestamp));
    }
   public function format_created_at(){
        $timestamp = (strpos('/', $this->created_at) > -1) ?
            strtotime(str_replace('/', '-', $this->created_at)) : $this->created_at;
        $this->created_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
   public function format_updated_at(){
        $timestamp = (strpos('/', $this->updated_at) > -1) ?
            strtotime(str_replace('/', '-', $this->updated_at)) : $this->updated_at;
        $this->updated_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
}
?>
