<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Tue Nov 24 13:23:36 GMT-05:00 2020  
*@time Tue Nov 24 13:23:36 GMT-05:00 2020  
*/
namespace erp\modules\billings\models;


/** 
*  Esta es  ActiveQuery clase de [[Orders_product]].
 *
 * @see Orders_product
 */
/**
 * Orders_productQuery representa la clase de Consulta del modelo Orders_product
 */
class Orders_productQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Orders_product[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Orders_product|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

