<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Tue Nov 24 13:23:36 GMT-05:00 2020  
*@time Tue Nov 24 13:23:36 GMT-05:00 2020  
*/
namespace erp\modules\billings\models;
use Yii;
use common\models\RestModel;

use erp\modules\managment\models\Products;
use erp\modules\managment\models\Services;
/**
 * Este es la clase modelo para la tabla orders_product.
 *
 * Los siguientes son los campos de la tabla 'orders_product':
 * @property integer $id_order_products
 * @property integer $id_order
 * @property integer $id_product
 * @property integer $id_services
 * @property integer $amount
 * @property integer $tax_id

 * Los siguientes son las relaciones de este modelo :

 * @property Orders $order
 * @property Products $product
 * @property Services $services
 */

class Orders_product extends RestModel 
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_order_products';

    const MODEL = 'Orders_product';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders_product';
    }

     
        /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
       const RELATIONS = ['order','product','services'];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

       const PKEY = 'id_order_products';

     /*
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['id_order'],'required','on'=>['create','default']],
			[['id_order_products'],'required', 'on' => 'update'],
			[['id_order_products','id_order','id_product','id_services','amount','tax_id'],'integer'],
			[['id_order_products'], 'unique' , 'on' => 'create'],
        ];
    }
	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_order
     */
	  public function getOrder()
		{
			return $this->hasOne(Orders::class, ['id_orders' => 'id_order']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_product
     */
	  public function getProduct()
		{
			return $this->hasOne(Products::class, ['id_product' => 'id_product']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_services
     */
	  public function getServices()
		{
			return $this->hasOne(Services::class, ['id_service' => 'id_services']);
		}

 /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'orders_product.id_order_products as id', 'orders_product.id_order_products as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('orders_product.id_order_products LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
}
?>
