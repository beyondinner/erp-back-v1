<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Mon Oct 19 15:57:25 GMT-04:00 2020  
*@time Mon Oct 19 15:57:25 GMT-04:00 2020  
*/
namespace erp\modules\billings\models;


/** 
*  Esta es  ActiveQuery clase de [[Billing_service_list]].
 *
 * @see Billing_service_list
 */
/**
 * Billing_service_listQuery representa la clase de Consulta del modelo Billing_service_list
 */
class Billing_service_listQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Billing_service_list[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Billing_service_list|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

