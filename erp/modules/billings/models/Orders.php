<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Nov 25 15:20:38 GMT-05:00 2020  
*@time Wed Nov 25 15:20:38 GMT-05:00 2020  
*/
namespace erp\modules\billings\models;
use Yii;
use common\models\RestModel;

use erp\modules\managment\models\Contacts;
use common\modules\security\models\Users;
use erp\modules\nomenclatures\models\Order_status;
/**
 * Este es la clase modelo para la tabla orders.
 *
 * Los siguientes son los campos de la tabla 'orders':
 * @property integer $id_orders
 * @property string $orders_code
 * @property datetime $date_at
 * @property integer $id_contact
 * @property integer $id_user
 * @property integer $id_order_status

 * Los siguientes son las relaciones de este modelo :

 * @property Contacts $contact
 * @property Users $user
 * @property Order_status $order_status
 * @property Orders_product[] $arrayorders_product
 */

class Orders extends RestModel 
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_orders';

    const MODEL = 'Orders';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

     
        /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
       const RELATIONS = ['contact','user','order_status','arrayorders_product'];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

       const PKEY = 'id_orders';

     /*
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['orders_code','id_order_status'],'required','on'=>['create','default']],
			[['id_orders'],'required', 'on' => 'update'],
			[['id_orders','id_contact','id_user','id_order_status'],'integer'],
			[['date_at'],'safe'],
			['date_at','format_date_at'],
			[['orders_code'], 'string', 'max'=>20],
			[['id_orders'], 'unique' , 'on' => 'create'],
			[['orders_code'], 'unique' , 'on' => 'create'],
			[['orders_code'], 'unique' , 'on' => 'update','when' =>function ($model, $value) {
                $elem = self::find()->where([$value => $model[$value]])->one();
                return !$elem ? false : $elem[$elem->primaryKey] != $model[$model->primaryKey];
            }],
        ];
    }
	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_contact
     */
	  public function getContact()
		{
			return $this->hasOne(Contacts::class, ['id_contact' => 'id_contact']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_user
     */
	  public function getUser()
		{
			return $this->hasOne(Users::class, ['id_user' => 'id_user']);
		}

	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_order_status
     */
	  public function getOrder_status()
		{
			return $this->hasOne(Order_status::class, ['id_order_status' => 'id_order_status']);
		}


	 /**
     * @return \yii\db\ActiveQuery
     * @description hace referencia al campo foráneo id_order
     */
	  public function getArrayorders_product()
		{
			return $this->hasMany(Orders_product::class, ['id_order' => 'id_orders']);
		}
 /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'orders.id_orders as id', 'orders.orders_code as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('orders.orders_code LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
   public function format_date_at(){
        $timestamp = str_replace('/', '-', $this->date_at);
        $this->date_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($insert && $this->orders_code=='--'){
            $this->orders_code="ORD-".str_pad($this->id_orders, 6, '0', STR_PAD_LEFT);
            $this->save();
        }
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }
}
?>
