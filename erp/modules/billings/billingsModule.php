<?php
/*Clase del Modulo billings*/
/**Generate by ASGENS
*@author Charlietyn  
*@date Sat Sep 05 20:15:23 GMT-04:00 2020  
*@time Sat Sep 05 20:15:23 GMT-04:00 2020  
*/
namespace erp\modules\billings;
class billingsModule extends \yii\base\Module 
{
    public $controllerNamespace = 'erp\modules\billings\controllers';
    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}

