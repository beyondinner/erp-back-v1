<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Sat Sep 05 20:15:23 GMT-04:00 2020  
*@time Sat Sep 05 20:15:23 GMT-04:00 2020  
*/
namespace erp\modules\billings\services;


use common\services\Services;

class Budget_items_listService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\billings\models\Budget_items_list';

}
