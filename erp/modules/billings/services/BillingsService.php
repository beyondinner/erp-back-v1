<?php
/**
 * /**Generate by ASGENS
 * @author Charlietyn
 * @date Sat Sep 05 20:15:23 GMT-04:00 2020
 * @time Sat Sep 05 20:15:23 GMT-04:00 2020
 */

namespace erp\modules\billings\services;


use common\services\Services;
use erp\modules\billings\models\Billing_product_list;
use erp\modules\billings\models\Billings;

class BillingsService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\billings\models\Billings';

    public function register($params)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $result = [];
        try {
            if (!isset($params['id_billings'])) {
                $billing = new Billings();
                $billing->load($params, '');
                $billing->billings_doc_number = '--';
                $billing->created_at = date('DD/MM/YYYY');
                $billing->updated_at = date('DD/MM/YYYY');
                $result[] = $billing->model_create();
                $doc_number =
                    'P' .
                    str_pad(
                        $billing->id_billings,
                        6,
                        '0',
                        STR_PAD_LEFT
                    );
                $billing->billings_doc_number = $doc_number;
                $billing->save();
                foreach ($params['billing_product_list'] as $bil) {
                    $billing_item_list = new Billing_product_list();
                    $billing_item_list->load($bil, '');
                    $billing_item_list->created_at = $billing->created_at;
                    $billing_item_list->updated_at = $billing->updated_at;
                    $billing_item_list->id_billing = $billing->id_billings;
                    $result[] = $billing_item_list->model_create();
                }
            } else {
                //Actualizar
                $billing = Billings::findOne($params['id_billings']);
                $billing->load($params, '');
                $billing->save();
                if ($billing->budget_id == null)
                    foreach ($params['billing_product_list'] as $bil) {
                        if (isset($bil['id_billing_product_list'])) {
                            $billing_item_list = Billing_product_list::findOne($bil['id_billing_product_list']);
                        } else {
                            $billing_item_list = new Billing_product_list();
                        }
                        $billing_item_list->load($bil, '');
                        $billing_item_list->created_at = $billing->created_at;
                        $billing_item_list->updated_at = $billing->updated_at;
                        $billing_item_list->id_billing = $billing->id_billings;
                        $result[] = $billing_item_list->model_create();
                    }
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
        return $this->process_response($result);
    }


}
