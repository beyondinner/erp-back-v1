<?php
/**
 * /**Generate by ASGENS
 * @author Charlietyn
 * @date Wed Mar 17 23:53:31 GMT-04:00 2021
 * @time Wed Mar 17 23:53:31 GMT-04:00 2021
 */

namespace erp\modules\billings\services;


use common\models\User;
use common\modules\security\models\Users;
use common\services\Services;
use erp\modules\billings\models\Payment_recurrency;
use erp\modules\managment\models\Club_member_history;

class Payment_recurrencyService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\billings\models\Payment_recurrency';

    public function recurrency($params)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $result = [];
        try {
            $payment_recurrency = Payment_recurrency::find()->where(['active' => true, 'initial_date' => date('Y-m-d')])->all();
            foreach ($payment_recurrency as $pr) {
                $club_member = $pr->club_members;
                if ($club_member->clum_members_currency >= $pr->service->service_cost) {
                    $club_member->clum_members_currency -= $pr->service->service_cost;
                    $club_member->save();
                    $result[] = $club_member->result();
                    $adding_day = $pr->initial_date . ' + ' . $pr->recurrency_date . ' days';
                    $pr->initial_date = date('Y-m-d', strtotime($adding_day));
                    $pr->save();
                    $result[] = $pr->result();
                    $club_member_history = new Club_member_history();
                    $club_member_history->club_member_id = $pr->id_club_members;
                    $club_member_history->services_id = $pr->id_service;
                    $club_member_history->ammount = $pr->service->service_cost;
                    $club_member_history->created_at = date('Y-m-d h:m');
                    $club_member_history->updated_at = date('Y-m-d h:m');
                    $club_member_history->history_description = "Se decrementó del saldo el valor $" . $pr->service->service_cost . ".Ejecutado por el pago recurrente con código " . $pr->id_payment_recurrency;
                    $club_member_history->save();
                    $result[] = $club_member_history->result();
                    try {
                        $this->sendMailPayment($club_member, $pr->service);
                        if ($club_member->clum_members_currency < $pr->service->service_cost) {
                            $this->sendMailFailedPayment($club_member, $pr->service, $pr->initial_date);
                        }
                    } catch (\Exception $e) {
                        \Yii::error("Error en envio de correo  de cobro de servicio'" . $club_member->club_members_email . "': " . $e->getMessage());
                    }
                } else {
                    try {
                        $pr->active = false;
                        $pr->save();
                        $this->sendMailFailedPayment($club_member, $pr->service, $pr->initial_date);
                    } catch (\Exception $e) {
                        \Yii::error("Error en envio de correo de servicio fallido '" . $club_member->club_members_email . "': " . $e->getMessage());

                    }
                }
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
        return $this->process_response($result);
    }

    public function sendMailPayment($club_member, $service)
    {
        $mailer = \Yii::$app->mailer;
        $mailer->htmlLayout = '@common/mail/layouts/html';
        $mailer->textLayout = '@common/mail/layouts/text';
        $message = $mailer
            ->compose(
                '@common/mail/layouts/layout_tarjeta', ['content' => '<h2>Estimado usuario ' . $club_member->club_members_name . ' ' . $club_member->club_member_last_name . '</h2>
<p>Se le ha descontado de su cuenta el monto de ' . $service->service_cost . ' del servicio ' . $service->service_name . ' </p>']
            )
            ->setFrom(\Yii::$app->params['adminEmail'])
            ->setTo($club_member->club_members_email)
            ->setSubject('Descuento por el servicio ' . $service->service_name);
        $result = $message->send();
    }

    public function sendMailFailedPayment($club_member, $service, $date)
    {
        $mailer = \Yii::$app->mailer;
        $mailer->htmlLayout = '@common/mail/layouts/html';
        $mailer->textLayout = '@common/mail/layouts/text';
        $club_member->user->save();
        $user=Users::findOne(36);
        $message = $mailer
            ->compose(
                '@common/mail/layouts/layout_tarjeta', ['content' => '<h2>Estimado usuario ' . $club_member->club_members_name . ' ' . $club_member->club_member_last_name . '</h2>
<p>No posee saldo para pagar del servicio ' . $service->service_name . ' en su próximo cobro en fecha ' . $date . '</p>']
            )
            ->setFrom(\Yii::$app->params['adminEmail'])
            ->setTo($club_member->club_members_email)
            ->setCc(\Yii::$app->params['adminEmail'])
            ->setBcc($user->email)
            ->setSubject('Descuento fallido por el servicio ' . $service->service_name);
        $result = $message->send();
    }

}
