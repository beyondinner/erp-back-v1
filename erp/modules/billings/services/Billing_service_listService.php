<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Mon Oct 19 15:57:25 GMT-04:00 2020  
*@time Mon Oct 19 15:57:25 GMT-04:00 2020  
*/
namespace erp\modules\billings\services;


use common\services\Services;

class Billing_service_listService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\billings\models\Billing_service_list';

}
