<?php
/**
 * /**Generate by ASGENS
 * @author Charlietyn
 * @date Tue Nov 24 13:23:36 GMT-05:00 2020
 * @time Tue Nov 24 13:23:36 GMT-05:00 2020
 */

namespace erp\modules\billings\services;


use common\services\Services;
use erp\modules\billings\models\Orders;
use erp\modules\billings\models\Orders_product;
use yii\web\NotFoundHttpException;

class OrdersService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\billings\models\Orders';

    public function register_order($params)
    {


        $transaction = \Yii::$app->db->beginTransaction();
        $result = [];
        $orders=null;
        try {
            if(!isset($params['id_orders'])) {
                $order = new Orders();
                $order->orders_code = '--';
                $order->date_at = date('Y-m-d h:i:s');
                $order->created_at = date('Y-m-d h:i:s');
                $order->id_order_status = 1;
            }
            else{
                $order=Orders::findOne($params['id_orders']);
            }
            $order->id_contact = $params['id_contact'];
            $order->id_user = $params['id_user'];
            $order->order_description = $params['order_description'];
            $order->updated_at = date('Y-m-d h:i:s');
            $result[] = $order->model_create();
            Orders_product::deleteAll(["id_order"=>$order->id_orders]);
            foreach ($params['order_product_list'] as $o) {
                $o = (object)$o;
                $order_product = new Orders_product();
                $order_product->id_order = $order->id_orders;
                $order_product->id_product = isset($o->id_product) ? $o->id_product : null;
                $order_product->id_services = isset($o->id_service) ? $o->id_service : null;
                $order_product->amount = $o->order_product_list_quantity;
                $order_product->tax_id = $o->tax_id;
                $result[] = $order_product->model_create();
            }

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
        return $this->process_response($result);
    }
}
