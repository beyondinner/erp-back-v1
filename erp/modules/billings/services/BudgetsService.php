<?php
/**
 * /**Generate by ASGENS
 * @author Charlietyn
 * @date Sat Sep 05 20:15:23 GMT-04:00 2020
 * @time Sat Sep 05 20:15:23 GMT-04:00 2020
 */

namespace erp\modules\billings\services;


use common\services\Services;
use erp\modules\billings\models\Billings;
use erp\modules\billings\models\Budget_items_list;
use erp\modules\billings\models\Budgets;
use erp\modules\nomenclatures\models\Billings_status;
use Mpdf\Mpdf;

class BudgetsService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\billings\models\Budgets';

    public function register($params)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $result = [];
        try {
            if (!isset($params['id_budget'])) {
                $budget = new Budgets();
                $budget->load($params, '');
                $budget->budget_doc_number = '--';
                $budget->created_at = date('Y-m-d');
                $budget->updated_at = date('Y-m-d');
                $result[] = $budget->model_create();
                $doc_number =
                    'P' .
                    str_pad(
                        $budget->id_budget,
                        6,
                        '0',
                        STR_PAD_LEFT
                    );
                $budget->budget_doc_number = $doc_number;
                $budget->save();
                foreach ($params['budget_items_list'] as $bil) {
                    $budget_item_list = new Budget_items_list();
                    $budget_item_list->load($bil, '');
                    $budget_item_list->budget_id = $budget->id_budget;
                    $budget_item_list->created_at = $budget->created_at;
                    $budget_item_list->updated_at = $budget->updated_at;
                    $result[] = $budget_item_list->model_create();
                }
            } else {
                //Actualizar
                $budget = Budgets::findOne($params['id_budget']);
                $budget->load($params, '');
                $budget->save();
                foreach ($params['budget_items_list'] as $bil) {
                    if (isset($bil['id_budget_items_list'])) {
                        $budget_item_list = Budget_items_list::findOne($bil['id_budget_items_list']);
                    } else {
                        $budget_item_list = new Budget_items_list();
                    }
                    $budget_item_list->load($bil, '');
                    $budget_item_list->budget_id = $budget->id_budget;
                    $budget_item_list->created_at = $budget->created_at;
                    $budget_item_list->updated_at = $budget->updated_at;
                    $result[] = $budget_item_list->model_create();
                }
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
        return $this->process_response($result);
    }

    public function pdf($budget)
    {
//        $budget = new Budgets();
        $tax[1] = 0;
        $tax[2] = 4;
        $tax[3] = 10;
        $tax[4] = 21;
        $impuesto = 0;
        $subtotal = 0;
        $precio_total = 0;
        $mpdf = new Mpdf(['mode' => '+aCJK', 'autoScriptToLang' => true, 'autoLangToFont' => true]);
        $stylesheet = file_get_contents(__DIR__ . '/../../../templates/pdf/pdf.css');
        $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
        $mpdf->showImageErrors = true;
        $html = '<div style="font-size: 12px"><div style="margin-bottom: 20px;line-height: 2px">
      <div style="float: left;width: 50%"><img src="../../static/assets/img/logo1.png" height="86" alt=""></div>
      <div style="float: left;width: 50%">
          <h1 style="float: right">Beyond Inner</h1>
          <h4>Información del presupuesto:' . $budget->budget_doc_number . '</h4>
          <p><strong>Fecha de creación</strong>:' . date('d/m/Y', strtotime($budget->created_at)) . '</p>
      </div>
  </div>
  <div style="line-height: 2px;float: left;width: 65%">
      <h4>Información del Contacto<hr></h4>
      <p><strong>Nombre y Apellidos: </strong>' . $budget->contacts->contact_full_name . '</p>
      <p><strong>Correo: </strong>' . $budget->contacts->contact_email . '</p>
      <p><strong>Teléfono: </strong>' . $budget->contacts->contact_cellphone . '</p>
  </div>
  <div style="line-height: 2px;float: left;width: 35%">
      <h4>Información del Presupuesto<hr></h4>
      <p><strong>Inicio: </strong>' . date('d/m/Y', strtotime($budget->budget_start_date)) . '</p>
      <p><strong>Fin: </strong>' . date('d/m/Y', strtotime($budget->budget_expiration_date)) . '</p>
      <p><strong>Tipo de pago: </strong>' . $budget->payment_types->payment_type_siglas . '</p>
  </div>
  <table class="table mb-0" style="font-size: 12px;margin-top: 10px">
      <thead class="thead-light">
      <tr>
          <th scope="col">Concepto</th>
          <th scope="col">Tipo</th>
          <th scope="col">Cantidad</th>
          <th scope="col">Precio</th>
          <th scope="col">Impuesto</th>
          <th scope="col">Total</th>
      </tr>
      </thead>
      <tbody>';
        foreach ($budget->arraybudget_items_list as $bi) {
            $impuesto += floatval($bi->budget_items_list_price) * floatval($tax[$bi->tax_id]) / 100;
            $subtotal += floatval($bi->budget_items_list_price);
            $total = floatval($bi->budget_items_list_price) + floatval($bi->budget_items_list_price) * floatval($tax[$bi->tax_id]) / 100;
            $precio_total += $total;

            $type = $bi->id_product ? "Producto" : "Servicio";
            $html .= '<tr >
          <th scope = "row" > ' . $bi->budget_items_list_concept . '</th >
          <td >' . $type . '</td >
          <td >' . $bi->budget_items_list_quantity . '</td >
          <td > ' . $bi->budget_items_list_price . '€</td >
          <td > ' . $tax[$bi->tax_id] . '%</td >
          <td > ' . $total . ' €</td >
      </tr >';
        }
        $html .= '</tbody>
  </table>
  <hr>
  <div style="line-height: 2px;float: left;width: 75%">
     <p></p>
  </div>
  <div style="line-height: 2px;float: right;width: 35%">
      <p><strong>Subtotal: </strong>' . $subtotal . '€</p>
      <p><strong>Impuesto: </strong>' . $impuesto . '€</p>
      <p><strong>Precio Total: </strong>' . $precio_total . '€</p>
  </div>
  <div style="margin-top: 100px">
  <div style="line-height: 2px;float: left;width: 20%">
     <p></p>
  </div>
  <div style="line-height: 2px;float: right;width: 60%">
      <hr>
      <p style="text-align: center">Firma del Contacto</p>
      <p style="text-align: center;white-space: nowrap">' . $budget->contacts->contact_full_name . '</p>
  </div>
  <div style="line-height: 2px;float: left;width: 20%">
     <p></p>
  </div>
  </div>
  </div>';
        $mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::HTML_BODY);
        $mpdf->Output();
    }

    public function register_budget_bill($params)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $result = [];
        try {
            if (isset($params['id_budget'])) {
                $status = Billings_status::find()->where(['billings_status_siglas' => 'Pendiente'])->one();
                //Actualizar
                $budget = Budgets::findOne($params['id_budget']);
                $billings = new Billings();
                $billings->budget_id = $budget->id_budget;
                $billings->billings_status_id = $status->id_billings_status;
                $billings->billings_doc_number = '--';
                $billings->created_at = date('Y-m-d');
                $billings->updated_at = date('Y-m-d');
                $billings->id_contact = $budget->contact_id;
                $billings->date_at = date('Y-m-d');
                $billings->expiration_date = date('Y-m-d');
                $billings->id_payment_type = $budget->payment_type_id;
                $billings->billing_description = $budget->budget_internal_desc;
                $result[] = $billings->model_create();
                $doc_number =
                    'P' .
                    str_pad(
                        $billings->id_billings,
                        6,
                        '0',
                        STR_PAD_LEFT
                    );
                $billings->billings_doc_number = $doc_number;
                $billings->save();
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
        return $this->process_response($result);
    }

}
