<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Tue Nov 24 13:23:36 GMT-05:00 2020  
*@time Tue Nov 24 13:23:36 GMT-05:00 2020  
*/
namespace erp\modules\billings\services;


use common\services\Services;

class Orders_productService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\billings\models\Orders_product';

}
