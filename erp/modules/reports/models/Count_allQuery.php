<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Nov 18 01:20:32 GMT-05:00 2020  
*@time Wed Nov 18 01:20:32 GMT-05:00 2020  
*/
namespace erp\modules\reports\models;


/** 
*  Esta es  ActiveQuery clase de [[Count_all]].
 *
 * @see Count_all
 */
/**
 * Count_allQuery representa la clase de Consulta del modelo Count_all
 */
class Count_allQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Count_all[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Count_all|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

