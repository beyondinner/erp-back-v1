<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Nov 18 01:20:32 GMT-05:00 2020  
*@time Wed Nov 18 01:20:32 GMT-05:00 2020  
*/
namespace erp\modules\reports\models;
use Yii;
use common\models\RestModel;

/**
 * Este es la clase modelo para la tabla count_all.
 *
 * Los siguientes son los campos de la tabla 'count_all':
 * @property integer $count_all

 * Los siguientes son las relaciones de este modelo :

 */

class Count_all extends RestModel 
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'count_all';

    const MODEL = 'Count_all';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'count_all';
    }

     
        /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
       const RELATIONS = [];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

       const PKEY = 'count_all';

     /*
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
        ];
    }
 /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'count_all.count_all as id', 'count_all.count_all as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('count_all.count_all LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
}
?>
