<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Dec 09 23:48:56 GMT-05:00 2020  
*@time Wed Dec 09 23:48:56 GMT-05:00 2020  
*/
namespace erp\modules\reports\models;


/** 
*  Esta es  ActiveQuery clase de [[Cant_transaction]].
 *
 * @see Cant_transaction
 */
/**
 * Cant_transactionQuery representa la clase de Consulta del modelo Cant_transaction
 */
class Cant_transactionQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Cant_transaction[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Cant_transaction|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

