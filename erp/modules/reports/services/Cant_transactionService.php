<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Dec 09 23:48:56 GMT-05:00 2020  
*@time Wed Dec 09 23:48:56 GMT-05:00 2020  
*/
namespace erp\modules\reports\services;


use common\services\Services;

class Cant_transactionService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\reports\models\Cant_transaction';

}
