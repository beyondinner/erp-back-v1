<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Nov 18 01:20:32 GMT-05:00 2020  
*@time Wed Nov 18 01:20:32 GMT-05:00 2020  
*/
namespace erp\modules\reports\services;


use common\services\Services;

class Count_allService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\reports\models\Count_all';

}
