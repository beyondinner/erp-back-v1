<?php
/*Clase del Modulo reports*/
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Nov 18 01:20:32 GMT-05:00 2020  
*@time Wed Nov 18 01:20:32 GMT-05:00 2020  
*/
namespace erp\modules\reports;
class reportsModule extends \yii\base\Module 
{
    public $controllerNamespace = 'erp\modules\reports\controllers';
    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}

