<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Sun Aug 30 22:57:29 GMT-04:00 2020  
*@time Sun Aug 30 22:57:29 GMT-04:00 2020  
*/
namespace erp\modules\security\controllers;


use common\controllers\RestController;
use common\modules\security\services\UsersService;
use yii\helpers\Url;
use yii\filters\Cors;
use common\modules\security\models\Users;
use yii\web\HttpException;
use yii\web\ServerErrorHttpException;

class UsersController extends RestController
{
    /**
     * {@inheritdoc}
     */
    public $service ;
    public $modelClass = 'common\modules\security\models\Users';


    public function behaviors()
    {
        $array= parent::behaviors();
        $array['authenticator']['except']= ['index','validate_pass','create', 'update', 'delete', 'view', 'select_2_list', 'validate', 'delete_parameters', 'delete_by_id','update_multiple'];
        $array['cors']=[
            'class' => Cors::class,
            'actions' => [
                'your-action-name' => [
                    #web-servers which you alllow cross-domain access
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST','OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => null,
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => [],
                ]
            ],
        ];
        return $array;
    }

    protected function verbs()
    {
        $array= ['validate_pass' => ['OPTIONS', 'POST']];
        return array_merge(parent::verbs(),$array);
    }

   public function getService()
    {
        if ($this->service == null)
            $this->service = new UsersService();
        return $this->service;
    }

    public function actionValidate_pass()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $params = \Yii::$app->request->getBodyParams();
        if (count($params) == 0)
            throw new HttpException(500, "Theres no parameters in request");
        if ( !isset($params['id_user']) || !isset($params['pass']))
            throw new HttpException(500, "Se necesita el id y la contraseña del usuario para validar la contraseña");
        $users=Users::find()->where(['id_user'=>$params['id_user']])->one();
        if ( !$users)
            throw new HttpException(404, "Usuario no encontrado");
        return $this->getService()->validate_pass($users,$params['pass']);
    }
}
