<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Sat Sep 05 20:15:24 GMT-04:00 2020  
*@time Sat Sep 05 20:15:24 GMT-04:00 2020  
*/
namespace erp\modules\security\models;


/** 
*  Esta es  ActiveQuery clase de [[Logs]].
 *
 * @see Logs
 */
/**
 * LogsQuery representa la clase de Consulta del modelo Logs
 */
class LogsQuery extends \yii\db\ActiveQuery{
/*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Logs[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Logs|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

