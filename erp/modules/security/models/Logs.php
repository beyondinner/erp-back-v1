<?php 
/**Generate by ASGENS
*@author Charlietyn  
*@date Fri Oct 16 13:30:08 GMT-04:00 2020  
*@time Fri Oct 16 13:30:08 GMT-04:00 2020  
*/
namespace erp\modules\security\models;
use Yii;
use common\models\RestModel;

/**
 * Este es la clase modelo para la tabla logs.
 *
 * Los siguientes son los campos de la tabla 'logs':
 * @property integer $id_log
 * @property string $log_action
 * @property string $log_description
 * @property integer $user_id
 * @property datetime $created_at
 * @property datetime $updated_at
 * @property string $table
 * @property integer $id_table
 * @property string $name_user

 * Los siguientes son las relaciones de este modelo :

 */

class Logs extends RestModel 
{

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 15;

    /**
     * The primarykey associated with the table-model.
     *
     * @var integer
     */
    protected $primaryKey = 'id_log';

    const MODEL = 'Logs';

    /**
     * @return string the associated database table name
     */
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logs';
    }

     
        /**
     *
     * The names of the hidden fields.
     *
     * @var array
     */
    const HIDE = [];
    /**

     * The names of the relation tables.
     *
     */
       const RELATIONS = [];



    /**
     * The primary key of the table
     *
     * @var mixed
     */

       const PKEY = 'id_log';

     /*
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['log_action','log_description','user_id','created_at','updated_at','name_user'],'required','on'=>['create','default']],
			[['id_log'],'required', 'on' => 'update'],
			[['id_log','user_id','id_table'],'integer'],
			[['created_at','updated_at'],'safe'],
			['created_at','format_created_at'],
			['updated_at','format_updated_at'],
			[['log_action','log_description'], 'string', 'max'=>255],
			[['table'], 'string', 'max'=>100],
			[['name_user'], 'string', 'max'=>80],
			[['id_log'], 'unique' , 'on' => 'create'],
        ];
    }
 /**
     * Get the list model with select2 schema.
     * @var $relation array
     * @var $parameters array
     * @return array|mixed
     */
    static function select_2_list($parameters = [])
    {
        $parameters = get_called_class()::parameters_request($parameters);
        $like = '';
        if (isset($_GET['q']))
            $like = $_GET['q'];
        else
            if (isset($parameters->q))
                $like = $parameters->q;
        $query = get_called_class()::query_list($parameters);
        get_called_class()::process_find_parameters($query, $parameters);
        $select = ['*', 'logs.id_log as id', 'logs.log_action as text'];
        $result = new \stdClass();
        $result->data = [];
        if ($parameters->relations == 'all')
            $result->data = $query->select($select)->with(get_called_class()::RELATIONS);
        if (!is_null($parameters->relations) && $parameters->relations != 'all')
            $result->data = $query->select($select)->with($parameters->relations);
        if (is_null($parameters->relations))
            $result->data = $query->select($select);
        $result->data=$result->data->andWhere('logs.log_action LIKE '."'%".$like."%'")->asArray()->all();
        return $result;

    }
   public function format_created_at(){
        $timestamp = str_replace('/', '-', $this->created_at);
        $this->created_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
   public function format_updated_at(){
        $timestamp = str_replace('/', '-', $this->updated_at);
        $this->updated_at = date('Y-m-d h:i:s', strtotime($timestamp));
    }
}
?>
