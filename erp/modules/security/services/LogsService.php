<?php
/**
/**Generate by ASGENS
*@author Charlietyn  
*@date Wed Sep 02 19:35:13 GMT-04:00 2020  
*@time Wed Sep 02 19:35:13 GMT-04:00 2020  
*/
namespace erp\modules\security\services;


use common\services\Services;

class LogsService extends Services
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'erp\modules\security\models\Logs';

}
